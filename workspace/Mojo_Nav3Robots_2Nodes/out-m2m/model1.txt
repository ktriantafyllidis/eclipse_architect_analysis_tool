--Generated using 'Marte2Mast Converter' @ 10/04/2013 - 16:57:44 - user: ktrianta


-- Processing Resources

Processing_Resource (
    Type                   => Regular_Processor
,   Name                   => Station_T420
,   Speed_Factor           => 0.8    
,   Worst_ISR_Switch      => 2.5E-6			
,   Best_ISR_Switch        => 2.5E-6			
,   Avg_ISR_Switch         => 2.5E-6			
,   Max_Interrupt_Priority => 19
,   Min_Interrupt_Priority => 19	
);
		

Processing_Resource (
    Type                   => Regular_Processor
,   Name                   => Station_HP
,   Speed_Factor           => 0.9    
,   Worst_ISR_Switch      => 2.5E-6			
,   Best_ISR_Switch        => 2.5E-6			
,   Avg_ISR_Switch         => 2.5E-6			
,   Max_Interrupt_Priority => 19
,   Min_Interrupt_Priority => 19	
);
		

Processing_Resource (
    Type                         => Packet_Based_Network
,   Name                         => Robot_PC1
,   Speed_Factor                 => 1.0    
,   Throughput                   => 11534336    
,   Transmission                 => Half_Duplex		
,   Max_Packet_Size              => 32
);	    


Processing_Resource (
    Type                         => Packet_Based_Network
,   Name                         => Robot_PC2
,   Speed_Factor                 => 1.0    
,   Throughput                   => 11534336    
,   Transmission                 => Half_Duplex		
,   Max_Packet_Size              => 32
);	    


Processing_Resource (
    Type                         => Packet_Based_Network
,   Name                         => PC1_PC2
,   Speed_Factor                 => 1.0    
,   Throughput                   => 23068672    
,   Transmission                 => Half_Duplex		
,   Max_Packet_Size              => 32
);	    



-- Schedulers

Scheduler (
    Type   => Primary_Scheduler
,   Name   => Station_T420_Scheduler
,   Policy => (            Type                 => Fixed_Priority
,           Worst_Context_Switch => 5.0E-6				
,           Best_Context_Switch  => 4.0E-6	
,           Avg_Context_Switch   => 4.2E-6						
,           Max_Priority         => 19
,           Min_Priority         => 1		
    )	
,   Host   => Station_T420
);	
		

Scheduler (
    Type   => Primary_Scheduler
,   Name   => Station_HP_Scheduler
,   Policy => (            Type                 => Fixed_Priority
,           Worst_Context_Switch => 5.0E-6				
,           Best_Context_Switch  => 4.0E-6	
,           Avg_Context_Switch   => 4.2E-6						
,           Max_Priority         => 19
,           Min_Priority         => 1		
    )	
,   Host   => Station_HP
);	
		

Scheduler (
    Type   => Primary_Scheduler
,   Name   => Robot_PC1_Scheduler
,   Policy => (        
        Type                     => FP_Packet_Based
     -- Max_Priority             => Not available in MARTE,
     -- Min_Priority             => Not available in MARTE
    )	
,   Host   => Robot_PC1
);	
		

Scheduler (
    Type   => Primary_Scheduler
,   Name   => Robot_PC2_Scheduler
,   Policy => (        
        Type                     => FP_Packet_Based
     -- Max_Priority             => Not available in MARTE,
     -- Min_Priority             => Not available in MARTE
    )	
,   Host   => Robot_PC2
);	
		

Scheduler (
    Type   => Primary_Scheduler
,   Name   => PC1_PC2_Scheduler
,   Policy => (        
        Type                     => FP_Packet_Based
     -- Max_Priority             => Not available in MARTE,
     -- Min_Priority             => Not available in MARTE
    )	
,   Host   => PC1_PC2
);	
		

-- Scheduling Servers

Scheduling_Server (
    Type                       => Regular
,   Name                       => Move_Base_Task
,   Server_Sched_Parameters    => (
		
     		   Type		       => Fixed_Priority_Policy 
      		  ,The_Priority    => 15
    )
,   Scheduler                  => Station_T420_Scheduler    	
);  
		

Scheduling_Server (
    Type                       => Regular
,   Name                       => Mojo_Frames_Task
,   Server_Sched_Parameters    => (
		
     		   Type		       => Fixed_Priority_Policy 
      		  ,The_Priority    => 15
    )
,   Scheduler                  => Station_T420_Scheduler    	
);  
		

Scheduling_Server (
    Type                       => Regular
,   Name                       => Robot_PC1
,   Server_Sched_Parameters    => (
     		   Type		       => Fixed_Priority_Policy 
      		  ,The_Priority    => 15
    )
,   Scheduler                  => Robot_PC1_Scheduler    	
);  
		

Scheduling_Server (
    Type                       => Regular
,   Name                       => Mojo_Driver_Task
,   Server_Sched_Parameters    => (
		
     		   Type		       => Fixed_Priority_Policy 
      		  ,The_Priority    => 15
    )
,   Scheduler                  => Station_T420_Scheduler    	
);  
		

Scheduling_Server (
    Type                       => Regular
,   Name                       => AMCL_Task
,   Server_Sched_Parameters    => (
		
     		   Type		       => Fixed_Priority_Policy 
      		  ,The_Priority    => 15
    )
,   Scheduler                  => Station_HP_Scheduler    	
);  
		

Scheduling_Server (
    Type                       => Regular
,   Name                       => Gmapping
,   Server_Sched_Parameters    => (
		
     		   Type		       => Fixed_Priority_Policy 
      		  ,The_Priority    => 15
    )
,   Scheduler                  => Station_HP_Scheduler    	
);  
		

Scheduling_Server (
    Type                       => Regular
,   Name                       => Robot_PC2
,   Server_Sched_Parameters    => (
     		   Type		       => Fixed_Priority_Policy 
      		  ,The_Priority    => 15
    )
,   Scheduler                  => Robot_PC2_Scheduler    	
);  
		

Scheduling_Server (
    Type                       => Regular
,   Name                       => PC1_PC2
,   Server_Sched_Parameters    => (
     		   Type		       => Fixed_Priority_Policy 
      		  ,The_Priority    => 15
    )
,   Scheduler                  => PC1_PC2_Scheduler    	
);  
		



-- Resources
Shared_Resource (
    Type         => Priority_Inheritance_Resource
,   Name         => Map_Data
);

		
Shared_Resource (
    Type         => Priority_Inheritance_Resource
,   Name         => Robot_Data
);

		
	



		
		
		
		
		
		
		


-- Operations


Operation (
    Type                        => Simple,
    Name                        => NewGoal,
    Worst_Case_Execution_Time   => 0.97E-3,
    Best_Case_Execution_Time    => 0.83E-3,
    Avg_Case_Execution_Time     => 0.93E-3
);

Operation (
    Type                        => Simple,
    Name                        => ExecuteCycles,
    Worst_Case_Execution_Time   => 10.651E-3,
    Best_Case_Execution_Time    => 0.83E-3,
    Avg_Case_Execution_Time     => 7.24E-3
);

Operation (
    Type                        => Simple,
    Name                        => VelCmd,
    Worst_Case_Execution_Time   => 0.1E-3,
    Best_Case_Execution_Time    => 0.1E-3,
    Avg_Case_Execution_Time     => 0.1E-3
);

Operation (
    Type                        => Simple,
    Name                        => data_subscribe,
    Worst_Case_Execution_Time   => 0.97E-3,
    Best_Case_Execution_Time    => 0.83E-3,
    Avg_Case_Execution_Time     => 0.93E-3
);

Operation (
    Type                        => Simple,
    Name                        => AddScan,
    Worst_Case_Execution_Time   => 393.595E-3,
    Best_Case_Execution_Time    => 0.47E-3,
    Avg_Case_Execution_Time     => 8.3E-3
);

Operation (
    Type                        => Simple,
    Name                        => UpdateMap,
    Worst_Case_Execution_Time   => 481.31E-3,
    Best_Case_Execution_Time    => 420.42E-3,
    Avg_Case_Execution_Time     => 458.06E-3
);

Operation (
    Type                        => Message_Transmission,
    Name                        => sendVel_cmd,
    Max_Message_Size		=> 1024.0,
    Avg_Message_Size    	=> 1024.0,
    Min_Message_Size     	=> 1024.0
);

Operation (
    Type                        => Message_Transmission,
    Name                        => sendLaser,
    Max_Message_Size		=> 4096.0,
    Avg_Message_Size    	=> 4096.0,
    Min_Message_Size     	=> 4096.0
);

Operation (
    Type                        => Message_Transmission,
    Name                        => sendOdom,
    Max_Message_Size		=> 1024.0,
    Avg_Message_Size    	=> 1024.0,
    Min_Message_Size     	=> 1024.0
);


-- Transactions


Transaction (
    Type            => Regular,
    Name            => Global_plan,
    External_Events => (
        (Type 		=> Periodic,
         Name 		=> InitialNode1,
         Period 	=> 500.0E-3)
    ),
    Internal_Events => (
        (Type 	=> Regular,
         Name 	=> Global_plan__Internal_Event_1
        )
    ),
    Event_Handlers  => (
        (Type                 => Activity,
         Input_Event          => InitialNode1,
         Output_Event         => Global_plan__Internal_Event_1,
         Activity_Operation   => NewGoal,
         Activity_Server      => Move_Base_Task
        )
    )
);

Transaction (
    Type            => Regular,
    Name            => MB_Nav,
    External_Events => (
        (Type 		=> Periodic,
         Name 		=> InitialNode1,
         Period 	=> 50.0E-3)
    ),
    Internal_Events => (
        (Type 	=> Regular,
         Name 	=> MB_Nav__Internal_Event_1
        ),
        (Type 	=> Regular,
         Name 	=> MB_Nav__Internal_Event_2
        ),
        (Type 	=> Regular,
         Name 	=> MB_Nav__Internal_Event_3,
         Timing_Requirements => (
                Type              => Soft_Global_Deadline,
                Deadline          => 50.0E-3,
                Referenced_Event  => InitialNode1
         )
        )
    ),
    Event_Handlers  => (
        (Type                 => Activity,
         Input_Event          => InitialNode1,
         Output_Event         => MB_Nav__Internal_Event_1,
         Activity_Operation   => ExecuteCycles,
         Activity_Server      => Move_Base_Task
        ),
        (Type                 => Activity,
         Input_Event          => MB_Nav__Internal_Event_1,
         Output_Event         => MB_Nav__Internal_Event_2,
         Activity_Operation   => sendVel_cmd,
         Activity_Server      => Robot_PC1
        ),
        (Type                 => Activity,
         Input_Event          => MB_Nav__Internal_Event_2,
         Output_Event         => MB_Nav__Internal_Event_3,
         Activity_Operation   => VelCmd,
         Activity_Server      => Mojo_Driver_Task
        )
    )
);

Transaction (
    Type            => Regular,
    Name            => LaserMB,
    External_Events => (
        (Type 		=> Periodic,
         Name 		=> InitialNode1,
         Period 	=> 79.0E-3)
    ),
    Internal_Events => (
        (Type 	=> Regular,
         Name 	=> LaserMB__Internal_Event_1
        ),
        (Type 	=> Regular,
         Name 	=> LaserMB__Internal_Event_2
        ),
        (Type 	=> Regular,
         Name 	=> LaserMB__Internal_Event_3,
         Timing_Requirements => (
                Type              => Soft_Global_Deadline,
                Deadline          => 79.0E-3,
                Referenced_Event  => InitialNode1
         )
        )
    ),
    Event_Handlers  => (
        (Type                 => Activity,
         Input_Event          => InitialNode1,
         Output_Event         => LaserMB__Internal_Event_1,
         Activity_Operation   => data_subscribe,
         Activity_Server      => Move_Base_Task
        ),
        (Type                 => Activity,
         Input_Event          => LaserMB__Internal_Event_1,
         Output_Event         => LaserMB__Internal_Event_2,
         Activity_Operation   => sendLaser,
         Activity_Server      => Robot_PC1
        ),
        (Type                 => Activity,
         Input_Event          => LaserMB__Internal_Event_2,
         Output_Event         => LaserMB__Internal_Event_3,
         Activity_Operation   => data_subscribe,
         Activity_Server      => Move_Base_Task
        )
    )
);

Transaction (
    Type            => Regular,
    Name            => OdomMB,
    External_Events => (
        (Type 		=> Periodic,
         Name 		=> InitialNode1,
         Period 	=> 37.0E-3)
    ),
    Internal_Events => (
        (Type 	=> Regular,
         Name 	=> OdomMB__Internal_Event_1
        ),
        (Type 	=> Regular,
         Name 	=> OdomMB__Internal_Event_2
        ),
        (Type 	=> Regular,
         Name 	=> OdomMB__Internal_Event_3,
         Timing_Requirements => (
                Type              => Soft_Global_Deadline,
                Deadline          => 79.0E-3,
                Referenced_Event  => InitialNode1
         )
        )
    ),
    Event_Handlers  => (
        (Type                 => Activity,
         Input_Event          => InitialNode1,
         Output_Event         => OdomMB__Internal_Event_1,
         Activity_Operation   => data_subscribe,
         Activity_Server      => Move_Base_Task
        ),
        (Type                 => Activity,
         Input_Event          => OdomMB__Internal_Event_1,
         Output_Event         => OdomMB__Internal_Event_2,
         Activity_Operation   => sendOdom,
         Activity_Server      => Robot_PC1
        ),
        (Type                 => Activity,
         Input_Event          => OdomMB__Internal_Event_2,
         Output_Event         => OdomMB__Internal_Event_3,
         Activity_Operation   => data_subscribe,
         Activity_Server      => Move_Base_Task
        )
    )
);

Transaction (
    Type            => Regular,
    Name            => LaserGM,
    External_Events => (
        (Type 		=> Periodic,
         Name 		=> InitialNode1,
         Period 	=> 79.0E-3)
    ),
    Internal_Events => (
        (Type 	=> Regular,
         Name 	=> LaserGM__Internal_Event_1
        ),
        (Type 	=> Regular,
         Name 	=> LaserGM__Internal_Event_2
        ),
        (Type 	=> Regular,
         Name 	=> LaserGM__Internal_Event_3,
         Timing_Requirements => (
                Type              => Soft_Global_Deadline,
                Deadline          => 79.0E-3,
                Referenced_Event  => InitialNode1
         )
        )
    ),
    Event_Handlers  => (
        (Type                 => Activity,
         Input_Event          => InitialNode1,
         Output_Event         => LaserGM__Internal_Event_1,
         Activity_Operation   => AddScan,
         Activity_Server      => Gmapping
        ),
        (Type                 => Activity,
         Input_Event          => LaserGM__Internal_Event_1,
         Output_Event         => LaserGM__Internal_Event_2,
         Activity_Operation   => sendLaser,
         Activity_Server      => Robot_PC1
        ),
        (Type                 => Activity,
         Input_Event          => LaserGM__Internal_Event_2,
         Output_Event         => LaserGM__Internal_Event_3,
         Activity_Operation   => AddScan,
         Activity_Server      => Gmapping
        )
    )
);

Transaction (
    Type            => Regular,
    Name            => OdomGM,
    External_Events => (
        (Type 		=> Periodic,
         Name 		=> InitialNode1,
         Period 	=> 37.0E-3)
    ),
    Internal_Events => (
        (Type 	=> Regular,
         Name 	=> OdomGM__Internal_Event_1
        ),
        (Type 	=> Regular,
         Name 	=> OdomGM__Internal_Event_2
        ),
        (Type 	=> Regular,
         Name 	=> OdomGM__Internal_Event_3,
         Timing_Requirements => (
                Type              => Soft_Global_Deadline,
                Deadline          => 37.0E-3,
                Referenced_Event  => InitialNode1
         )
        )
    ),
    Event_Handlers  => (
        (Type                 => Activity,
         Input_Event          => InitialNode1,
         Output_Event         => OdomGM__Internal_Event_1,
         Activity_Operation   => AddScan,
         Activity_Server      => Gmapping
        ),
        (Type                 => Activity,
         Input_Event          => OdomGM__Internal_Event_1,
         Output_Event         => OdomGM__Internal_Event_2,
         Activity_Operation   => sendOdom,
         Activity_Server      => Robot_PC1
        ),
        (Type                 => Activity,
         Input_Event          => OdomGM__Internal_Event_2,
         Output_Event         => OdomGM__Internal_Event_3,
         Activity_Operation   => AddScan,
         Activity_Server      => Gmapping
        )
    )
);


