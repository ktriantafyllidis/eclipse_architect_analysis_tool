package jmetal.problems.AO3Objectives;

import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.util.JMException;


public class ao3objectives extends Problem{

	//this is a variable that defines the max Number of the CPUS
	private int NumOfCPUs = 5;
	private int numberOfVariables;
	private int numberOfObjectives;
	
	 /** 
	  * Creates a default Archiecture optimization problem 
	  * The 32 objectives are:
	  * 1. Utilization
	  * 2. Deadlines
	  * 3. Cost
	  * 
	  * @param solutionType The solution type must "Real" or "BinaryReal". 
	  */
	public ao3objectives(String solutionType)
	{
		
	    numberOfVariables_  = this.numberOfVariables;
	    numberOfObjectives_ = this.numberOfObjectives;
	    numberOfConstraints_= 0;
		problemName_ = "ao3obs";
		
		
	}
	
	
	public void Utilization()
	{
		//read the Utilization vector and get an average
		
		
	}
	

	@Override
	public void evaluate(Solution solution) throws JMException {
		// TODO Auto-generated method stub
		
	}
	
	public void setMaxCPUs(int NumOfCPUs)
	{
		this.NumOfCPUs = NumOfCPUs;
	}
	
	
	//Enters the MAST file and gets the number of the tasks 
	/*
	 * Topology: 1
	 * CPU types: NumOfCPUs
	 * Network type: 1
	 * SW mapping: NumOfTasks
	 * numberOfVariables = 1 +  NumOfCPUs + 1 + NumOfTasks
	 * */
	public void get_numberOfVariables()
	{
		//NumOfTasks = get the num of tasks
		this.numberOfVariables = 2 + this.NumOfCPUs  /* + NumOfTasks*/;
		
	}

}
