(* ::Package:: *)

(* ::Subsection:: *)
(*Predictor for wireless/wired network delays*)
(*Author : Waqar Aslam (w.aslam@tue.nl)*)


ClearAll["Global`*"];
debug=0;
If[debug==1,dir=NotebookDirectory[],dir=DirectoryName[$InputFileName]];
SetDirectory[dir];
filenw="nw.txt";
filenwerror="nwerror.txt";
fileinput="input.txt";
fileoutput="delay.txt";
(*fileoutputdirect="delaydirect.txt";*)


(*NW=ToLowerCase[Import[filenw]];*)
NWdata=ReadList[filenw];
NW=ToLowerCase[ToString[NWdata[[1]]]];
(*Print[NW];*)
If[!MemberQ[{"b","a","g","e"},NW],Export[filenwerror,"Delay predictions for ",NW," network are not available!"];Exit[]];
us2ms=1/10^3;ms2us=10^3;
RTP=12;UDP=8;IP=20;hmac=28;hethernet=28;(*headers*)
ethheaders=RTP+UDP+IP+hethernet;
headers=RTP+UDP+IP+hmac;

(*802.11 PHY/MAC parameters*)
ACKsize=14;
Tprop=1;

If[NW=="b",SIFS=10;aSlotTime=20;MACtxrate=11;aPreableLength=192;hphy=aPreableLength;DIFS=SIFS+2*aSlotTime;srateb=1.375(*symbols per us*);sbitsb=8(*bits per symbol*)];
If[NW=="a",SIFS=16;aSlotTime=9;MACtxrate=54;aPreableLength=20;hphy=aPreableLength;DIFS=SIFS+2*aSlotTime;sratea=4(*us per symbol*);sbitsa=216(*bits per symbol*);tailbitsperframe=6(*bits*)];
If[NW=="g",SIFS=10;aSlotTime=9;MACtxrate=54;aPreableLength=20;hphy=aPreableLength;DIFS=SIFS+2*aSlotTime;srateg=4(*us per symbol*);sbitsg=216(*bits per symbol*);extragapperframe=6(*us*)];


(*Time of Tx for Ethernet types: 10, 100, 1000, 10000*)
fnEthernetLinkDelay[Apayload_,Erate_,linktype_,linklen_]:=Module[{framepayload,Tframe,propspeed},
If[linktype=="cable",propspeed=2*10^5];
If[linktype=="optic",propspeed=3*10^5];
framepayload=Apayload+ethheaders;
Tframe=framepayload*8/(Erate)+linklen/propspeed;(*time in us*)
Return[N[Tframe]]];

(*Time of Tx for 802.11 a,b,g*)
fnTimeTxTran[Apayload_,nw_]:=Module[{MPDUpayload,Tmpdu,Tack},
MPDUpayload=Apayload+headers;
If[nw=="b",Tmpdu=hphy+Ceiling[MPDUpayload*8/sbitsb]/srateb;Tack=hphy+Ceiling[ACKsize*8/sbitsb]/srateb];
If[nw=="a",Tmpdu=hphy+Ceiling[(MPDUpayload*8+tailbitsperframe)/sbitsa]*sratea;Tack=hphy+Ceiling[(ACKsize*8+tailbitsperframe)/sbitsa]*sratea];
If[nw=="g",Tmpdu=hphy+Ceiling[(MPDUpayload*8/sbitsg)]*srateg+extragapperframe;Tack=hphy+Ceiling[(ACKsize*8/sbitsg)]*srateg+extragapperframe];
(*Print["fnTimeTxTran: ",hphy,",",nw,",",Tmpdu,",",Tack]*)
Return[N[(DIFS+Tmpdu+Tprop+SIFS+Tack+Tprop)]]];

(*Compute collision subsets/permutations without the reference process involved: pass the refernce process and the max collision size to consider*)
fnCSkprime[kprime_,mx_]:=Module[{compkk,cs},
cs=Subsets[kprime,{#}]&/@Range[0,Min[Length[kprime],mx]];
Return[cs]];

(*Compute collision subsets with reference process involved: pass the reference process, a collison subset of some size (\[GreaterEqual]pos-1) without the reference process and the position where the reference process is inserted*)
fnCSk[cs_,kk_,pos_]:=Module[{csk},
csk=Insert[#,kk,pos]&/@cs;
Return[csk]];

(*Compute time to resolve a given collision for the reference process at a given position*)
fnTresolve[tuple_,kk_]:=Module[{L,part,sum},
(*L=Length[tuple];*)
part=Take[tuple,Position[tuple,kk][[1,1]]];
sum=Total[(fnTimeTxTran[input[[#,ipktsize]],NW]+backoffT)&/@part];
Return[sum]];

(*Compute average time per collision subset, which also includes the reference process*)
fnAvgTcol[csk_,kk_]:=Module[{cskprime,Pcol,pktsizemax,Tcol,cskperm,avgT},
(*Compute probability of collision for a given collision subset with the reference process*)
(*Print["fnavgTcol: ",csk];*)
cskprime=Complement[P,csk];(*process indices that are not involved in collision*)
Pcol=Product[revperiod[[i]],{i,csk}]*Product[1-revperiod[[j]],{j,cskprime}]/PcollisionTotal;
(*Pcol=Product[revperiod[[i]],{i,csk}]*Product[1-revperiod[[j]],{j,cskprime}]/PcollisionTotal;*)
(*Compute collision time by finding the largest MPDU size involved in the collision*)
pktsizemax=Max[input[[#,ipktsize]]&/@csk];
(*Print["fnavgTcol:Max packet size in collisiion: ",Tcol];*)
Tcol=fnTimeTxTran[pktsizemax,NW];
(*If only a single process is passed in csk, then no collision*)
If[Length[csk]==1,Tcol=0];
(*Print["fnavgTcol:Collisiion tuple",csk," time: ",Tcol];*)
(*Find possible ways to resolve from a csk using permutations*)
cskperm=Permutations[csk,{Length[csk]}];(*These permutations represent resolution tuples. Each one is equally likely *)
avgT=Total[1/Length[cskperm]*fnTresolve[#,kk]&/@cskperm];
Return[Pcol*(Tcol+avgT)]];

(*Compute collision time of a given collision subset*)
fnTcollision[csk_]:=Module[{cskprime,Pcol,pktsizemax,Tcol,cskperm,avgT},
(*Compute collision time by finding the largest MPDU size involved in the collision*)
pktsizemax=Max[input[[#,ipktsize]]&/@csk];
Tcol=fnTimeTxTran[pktsizemax,NW];
(*If only a single process is passed in csk, then no collision*)
If[Length[csk]==1,Tcol=0];
Return[Tcol]];

(*Compute probability of collision for a given collision subset with the reference process: used for normalizing collision probabilities*)
fnPcol[csk_]:=Module[{cskprime,Pcol},
cskprime=Complement[P,csk];
(*Print[fnPcol: "csk:",csk," cskprime:",cskprime, " cskmod:",#&/@csk," cskprimemod:",#&/@cskprime];*)
Pcol=Product[revperiod[[i]],{i,csk}]*Product[1-revperiod[[j]],{j,cskprime}];
Return[Pcol]];


(*Read data from a text file: "input.txt"*)
data=ReadList[fileinput];
ilinkname=1;iperiod=2;ipktsize=3;


(*For Ethernet only*)
irate=2;ilinktype=3;ilinklen=4;
If[NW=="e",erate=NWdata[[irate]];lnktype=ToLowerCase[ToString[NWdata[[ilinktype]]]];lnklen=NWdata[[ilinklen]];delay=fnEthernetLinkDelay[#,erate,lnktype,lnklen]&/@data[[All,ipktsize]];Export[fileoutput,Flatten[#,1]&/@Transpose[{data[[All,ilinkname]],delay*us2ms}]];Exit[]];


(*For WLANs only*)
input=Transpose[{data[[All,ilinkname]],data[[All,iperiod]]*ms2us/aSlotTime,data[[All,ipktsize]]}];
revperiod=1/#&/@input[[All,iperiod]];
P=Range[1,Length[input]];
CW=64;backoffT=aSlotTime*CW;
colsizemax=3;


delay={};
Do[
Kprime=Complement[P,{K}];
csprime=fnCSkprime[Kprime,colsizemax-1](*collision subsets excluding the reference process*);
(*Length[#]&/@csprime
Binomial[Length[Kprime],#]&/@Range[0,Min[Length[Kprime],colsizemax-1]]*)
pos=1;(*Insert the reference process at an arbitrary position within csprime*)
csk=fnCSk[#,K,pos]&/@csprime;
(*Pcollision=Map[fnPcol,csk,{2}];*)
(*Print[#,"--",fnPcol[#]]&/@#&/@csk;*)
Pcollision=fnPcol[#]&/@#&/@csk;
PcollisionTotal=Total[Flatten[Pcollision]];
(*Tcollision=Map[fnavgTcol,csk,{2}];*)
Tcollision=fnAvgTcol[#,K]&/@#&/@csk;
delayBest=fnTimeTxTran[input[[K,ipktsize]],NW];
delayAvg=Total[Flatten[Tcollision]];
delayWorst=fnTcollision[P]+Total[backoffT/2+fnTimeTxTran[input[[#,ipktsize]],NW]&/@P];
(*Sort[{delayBest,delayAvg,delayWorst}];*)
AppendTo[delay,{delayBest,delayAvg,delayWorst}],
{K,P}];
delay;


(*Save results to a file*)
Export[fileoutput,Flatten[#,1]&/@Transpose[{data[[All,ilinkname]],delay*us2ms}]];
