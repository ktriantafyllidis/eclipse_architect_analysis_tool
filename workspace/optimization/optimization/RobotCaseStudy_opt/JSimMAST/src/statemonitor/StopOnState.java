/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: StopOnState
 * Description: Stop the execution of tne simulation when the resource 
 * came in a specified state. May to be conditional or unconditional 
 * stop monitor. 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package statemonitor;

import core.*;
import resource.*;

public class StopOnState extends StateMonitor implements ConditionalStop {
	
	/* FIELDS */
	
	// True if is a conditional stop monitor. It is setting in the constructor 
	private boolean isConditional;
	
	// State in which the execution is stopped
	private int stopState;
	
	// True if the the stop condition is satisfied.
	private boolean onStopState = false;
	
	
	/* METHODS */
	
	// Constructor
	public StopOnState(Resource resource, int stopState, boolean isConditional){
		super(resource);
		this.stopState = stopState;
		this.isConditional = isConditional;
	}
	
	public void state(int newState){
		if (isConditional)
			onStopState = newState == stopState;
		else if (newState == stopState) 
			Controller.executiveStop();
	}
	
	public boolean stop(){
		return onStopState;
	}
}
