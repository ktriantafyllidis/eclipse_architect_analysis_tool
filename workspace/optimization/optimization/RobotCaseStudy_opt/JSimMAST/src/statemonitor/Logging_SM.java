/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: DisplayChangeStateMonitor
 * Description: Log the change of the resource state. 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package statemonitor;

import resource.Resource;

public abstract class Logging_SM extends StateMonitor {
	
	/* FIELDS */
	
	protected int mID;
	
	
	/* METHODS */
	
	// Constructor
	protected Logging_SM(Resource resource, int mID){
		super(resource);
		this.mID = mID;
	}

}
