/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: PercentOnState
 * Description: Stores the statistic of the time of each state. 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package statemonitor;

import core.*;
import resource.*;
import java.util.*;

public class PercentOnState extends StateMonitor { //implements ResultMonitor {
	
	/* ENUMS */
	
	//public enum UtilizationType{Total, Application, ContextSwitch, Timer, Interrupt}
	
	
	/* FIELDS */
	
	// Time of the last state change  
	private long lastChange = 0;
	
	// State in which the execution is stopped
	private int currentState = 0; // The state FREE is the initial state for any resource
	
	// Accumulators of state remaining time.
	private Vector<Long> accumulatedTimes;
	
	
	/* METHODS */
	
	// Constructor
	public PercentOnState(Resource resource){
		super(resource);
		accumulatedTimes = new Vector<Long>();
		/*for (int i = 0; i < hostRsrc.numMonitoredStates(); i++) {
			accumulatedTimes.add(i, 0L);
		}*/
	}

	public void state(int newState){
		long newAccumulatedTime;
		if (currentState < accumulatedTimes.size() && accumulatedTimes.get(currentState) != null){
			newAccumulatedTime = accumulatedTimes.get(currentState) + (Clock.currentTime() - lastChange);
			accumulatedTimes.set(currentState, newAccumulatedTime);
		} else {
			if (currentState > accumulatedTimes.size())
				accumulatedTimes.setSize(currentState);
			newAccumulatedTime = Clock.currentTime() - lastChange;
			accumulatedTimes.add(currentState, newAccumulatedTime);
		}
		lastChange = Clock.currentTime();
		currentState = newState;
			
	}
	
	public double utilizationResults(int state){
		if (currentState < accumulatedTimes.size() && accumulatedTimes.get(currentState) != null) {
			long newAccumulatedTime;
			newAccumulatedTime = accumulatedTimes.get(currentState) + (Clock.currentTime() - lastChange);
			accumulatedTimes.set(currentState, newAccumulatedTime);
			lastChange = Clock.currentTime();
		}
		if (state < accumulatedTimes.size() && accumulatedTimes.get(state) != null)
			return 100 * ((double)accumulatedTimes.elementAt(state)) / Clock.currentTime();
		else if(state == 0)
			return 100.0;
		else
			return 0.0;
	}
}
