package guis;

import eventmonitor.Statistical_EM;
import exceptions.JSimMastException;

import java.util.Iterator;
import java.util.Vector;

import module.Tools;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import preProcessor.MASTpreProcessor;
import utilityClasses.XMLparser;
import core.Clock;
import core.Controller;
import core.Repository;

public class JSimMastGUI {
		
	/* CONSTANTS */
	
	private static final boolean gui = true;
	
	private final int NUM_DECIMALS = 4;
	
	
	static String modelName;
	static String resultsName;
	static String tracesName;
	static String profile_combo = "";
	
	//gadem
	String FILES_PATH = "";
	//private final String FILES_PATH = "";
//	private final String FILE_NAME = "Scada_CEDI.mdl.xml";
	
	private final int NUM_TRACES_ON_TABLE = 15;
	private final String DEFAULT_PROFILE = "PERFORMANCE";
	private final String DEFAULT_TIME_UNITS = "s";
	private final String MAX_EXEC_TIME = "0.1";
	private final String TRANS_MIN_NUM = "0";
	
	// States
	private final int READY_TO_ACCEPT_MODEL_FILE = 0;
	private final int READY_TO_CHECK_MODEL_FILE = 1;
	private final int READY_TO_START_SIMULATION = 2;
	private final int SIMULATING = 3;
	private final int READY_TO_RESUME_SIMULATION = 4;
	
	
	/* FIELDS */
	
	private static JSimMastGUI thisClass;
	private int topTraceIndex;
	private int numTraces;
	
	private java.util.List<Element> e2eFlowResultsElemList;
	private java.util.List<Element> simTimingResultsElemList;
	private java.util.List<Element> processingRsrcResultsElemList;
	private java.util.List<Element> mutexResultsElemList;
	
	private static long finalTime; // ns
	private static long minNumTransactions;
	
	private boolean endButtonClicked = false;
	private int currentState = 0;

	
	// GUI elements
	private Shell sShell = null;
	private Group filesGroup = null;
	private Group simulationControlGroup = null;
	private Combo profileCombo = null;
	private Button startButton = null;
	private Button resumeButton = null;
	private Button pauseButton = null;
	private Button endButton = null;
	private TabFolder tabFolder = null;
	private Composite executionsComposite = null;
	private Composite tracesComposite = null;
	private Composite schedulingResultsComposite = null;
	private Composite resourcesUsagesComposite = null;
	private Label maxExecTimeLabel = null;
	private Text maxExecTimeText = null;
	private Label transMinNumLabel = null;
	private Text transMinNumText = null;
	private Label currentTimeLabel = null;
	private Text currentTimeText = null;
	private Table executionsTable = null;
	private Table tracesTable = null;
	private Label transesLabel = null;
	private Label localDataLabel = null;
	private Label globalDataLabel = null;
	private Label processingRsrcsLabel = null;
	private Label mutexesLabel = null;
	private Group messagesGroup = null;
	private List messagesList = null;
	private Label profileLabel = null;
	private Label modelLabel = null;
	private Text modelText = null;
	private Label resultsLabel = null;
	private Text resultsText = null;
	private Label tracesLabel = null;
	private Text tracesText = null;
	private Label mssgsLabel = null;
	private Text mssgsText = null;
	private Button analizeButton = null;
	private Button exitButton = null;
	private Combo transesCombo = null;
	private Table localDataTable = null;
	private Table globalDataTable = null;
	private Label label = null;
	private Label timeUnitsLabel = null;
	private Combo timeUnitsCombo = null;
	private Button reTracesButton = null;
	private Button avTracesButton = null;
	private Label eventsLabel = null;
	private Combo eventsCombo = null;
	private Label localMaxMissRatioLabel = null;
	private Label globalMaxMissRatioLabel = null;
	private Table localMaxMissRatioTable = null;
	private Table globalMaxMissRatioTable = null;
	private Combo processingRsrcsCombo = null;
	private Label processingRsrcsUtilizationsLabel = null;
	private Table processingRsrcsUtilizationsTable = null;
	private Table mutexesTable = null;
	
	/**
	 * @param args
	 */
	/*public static void main(String[] args) {
		 Before this is run, be sure to set up the launch configuration (Arguments->VM Arguments)
		 * for the correct SWT library path in order to run with the SWT dlls. 
		 * The dlls are located in the SWT plugin jar.  
		 * For example, on Windows the Eclipse SWT 3.1 plugin jar is:
		 *       installation_directory\plugins\org.eclipse.swt.win32_3.1.0.jar
		 
		if (gui == true)
		{
		
			Display display = Display.getDefault();
			thisClass = new JSimMastGUI();
			
			thisClass.createSShell();
			thisClass.sShell.open();
	
			while (!thisClass.sShell.isDisposed()) {
				if (!display.readAndDispatch())
					display.sleep();
			}
			display.dispose();
		}
		else
		{
			modelName = "model2.mdl.xml";
			resultsName = "models2.res.xml";
			tracesName = "model2.trc.xml";
			profile_combo = "TRACES";
			finalTime = 1000;
			minNumTransactions = 1000;
			//call here the function for no gui
			thisClass = new JSimMastGUI();
			thisClass.goOn_no_gui();
			
		}
		
	}*/
	
	public void sim_mast_no_gui(String _modelName, String _resultsName, String _tracesName, String _profile_combo, long _finalTime, long _minNumTransactions)
	{
		modelName = _modelName;
		resultsName = _resultsName;
		tracesName = _tracesName;
		profile_combo = _profile_combo;
		finalTime = _finalTime;
		minNumTransactions = _minNumTransactions;
		//call here the function for no gui
		//thisClass = new JSimMastGUI();
		goOn_no_gui();
				
	}
	
	
	private void createSShell() {
		GridLayout gridLayout = new GridLayout();
			gridLayout.numColumns = 2;
		sShell = new Shell(SWT.CLOSE);
			sShell.setText("JSimMast: Mast Simulator");
			//sShell.setLocation(new Point(44, 58));
			sShell.setLocation(new Point(50, 50));                   //JMD
			//sShell.setSize(new Point(925, 475));
			sShell.setSize(new Point(1100, 475));                    //JMD
			sShell.setLayout(gridLayout);
		createFilesGroup();
		createTabFolder();
		createSimulationControlGroup();
		createMessagesGroup();
		//setState(READY_TO_ACCEPT_MODEL_FILE);
		resetGUI();
	}
	
	/**
	 * This method initializes modelGroup	
	 *
	 */
	private void createFilesGroup() {
		GridData gridData47 = new GridData();
			gridData47.widthHint = 140;
			gridData47.heightHint = 15;
		GridData gridData46 = new GridData();
			gridData46.heightHint = 15;
			gridData46.widthHint = 35;
		GridData gridData45 = new GridData();
			gridData45.widthHint = 140;
			gridData45.heightHint = 15;
		GridData gridData11 = new GridData();
			gridData11.heightHint = 15;
			gridData11.widthHint = 35;
		GridData gridData6 = new GridData();
			gridData6.widthHint = 140;
			gridData6.heightHint = 15;
		GridData gridData5 = new GridData();
			gridData5.widthHint = 35;
			gridData5.heightHint = 15;
		GridData gridData4 = new GridData();
			gridData4.heightHint = 15;
			gridData4.widthHint = 35;
		GridData gridData3 = new GridData();
			gridData3.widthHint = 140;
			gridData3.heightHint = 15;
		GridData gridData110 = new GridData();
			gridData110.grabExcessVerticalSpace = false;
			gridData110.widthHint = 200;
			gridData110.verticalAlignment = org.eclipse.swt.layout.GridData.BEGINNING;
			gridData110.heightHint = 110;
		GridLayout gridLayout1 = new GridLayout();
			gridLayout1.makeColumnsEqualWidth = false;
			gridLayout1.numColumns = 2;
		filesGroup = new Group(sShell, SWT.NONE);
			filesGroup.setText("Files");
			filesGroup.setLayoutData(gridData110);
			filesGroup.setLayout(gridLayout1);
		modelLabel = new Label(filesGroup, SWT.NONE);
			modelLabel.setText("model");
			modelLabel.setLayoutData(gridData4);
		modelText = new Text(filesGroup, SWT.BORDER);
			modelText.setEnabled(true);
			modelText.setLayoutData(gridData3);
		
			modelText.addKeyListener(new KeyAdapter() {				
				public void keyReleased(KeyEvent e) {					
					if(e.keyCode == 13){
						messagesList.setEnabled(true);
						messagesList.removeAll();
						//gadem
						
						if (!modelText.getText().endsWith(".mdl.xml")) {
							messagesList.add(modelText.getText() + " is NOT a MAST model file");
							setState(READY_TO_ACCEPT_MODEL_FILE);
						} else {
							messagesList.add(modelText.getText() + " is a MAST model file");
							checkConsistent(FILES_PATH + modelText.getText());
						}
					}
				}
			});
		resultsLabel = new Label(filesGroup, SWT.NONE);
			resultsLabel.setText("results");
			resultsLabel.setLayoutData(gridData5);
		resultsText = new Text(filesGroup, SWT.BORDER);			
			resultsText.setLayoutData(gridData6);
		tracesLabel = new Label(filesGroup, SWT.NONE);
			tracesLabel.setText("traces");
			tracesLabel.setLayoutData(gridData11);
		tracesText = new Text(filesGroup, SWT.BORDER);
			tracesText.setLayoutData(gridData45);
		mssgsLabel = new Label(filesGroup, SWT.NONE);
			mssgsLabel.setText("mssgs");
			mssgsLabel.setLayoutData(gridData46);
		mssgsText = new Text(filesGroup, SWT.BORDER);
			mssgsText.setLayoutData(gridData47);
	}

	/**
	 * This method initializes simulationControlGroup	
	 *
	 */
	private void createSimulationControlGroup() {
		GridData gridData37 = new GridData();
			gridData37.grabExcessVerticalSpace = true;
			gridData37.horizontalAlignment = GridData.FILL;
		GridData gridData49 = new GridData();
			gridData49.widthHint = 80;
			gridData49.horizontalAlignment = GridData.CENTER;
			gridData49.heightHint = -1;
		GridData gridData48 = new GridData();
			gridData48.widthHint = 80;
			gridData48.horizontalAlignment = GridData.CENTER;
			gridData48.heightHint = -1;
		GridData gridData2 = new GridData();
			gridData2.grabExcessVerticalSpace = false;
			gridData2.heightHint = -1;
			gridData2.horizontalAlignment = GridData.FILL;
			gridData2.verticalAlignment = org.eclipse.swt.layout.GridData.BEGINNING;
			gridData2.widthHint = -1;
		GridData gridData17 = new GridData();
			gridData17.grabExcessHorizontalSpace = true;
			gridData17.horizontalAlignment = GridData.FILL;
		GridData gridData12 = new GridData();
			gridData12.widthHint = 80;
			gridData12.horizontalAlignment = GridData.CENTER;
			gridData12.heightHint = -1;
		GridData gridData10 = new GridData();
			gridData10.heightHint = -1;
			gridData10.grabExcessHorizontalSpace = false;
			gridData10.horizontalAlignment = GridData.CENTER;
			gridData10.widthHint = 80;
		GridData gridData9 = new GridData();
			gridData9.heightHint = -1;
			gridData9.grabExcessHorizontalSpace = true;
			gridData9.horizontalAlignment = GridData.CENTER;
			gridData9.grabExcessVerticalSpace = false;
			gridData9.widthHint = 80;
		GridData gridData8 = new GridData();
			gridData8.heightHint = -1;
			gridData8.grabExcessHorizontalSpace = true;
			gridData8.horizontalAlignment = GridData.CENTER;
			gridData8.widthHint = 80;
		GridLayout gridLayout2 = new GridLayout();
			gridLayout2.numColumns = 2;
			gridLayout2.makeColumnsEqualWidth = true;
		simulationControlGroup = new Group(sShell, SWT.NONE);
			simulationControlGroup.setText("Simulation control");
			simulationControlGroup.setLayoutData(gridData2);
			simulationControlGroup.setLayout(gridLayout2);
		profileLabel = new Label(simulationControlGroup, SWT.NONE);
			profileLabel.setText("Profile");
			profileLabel.setLayoutData(gridData17);
		timeUnitsLabel = new Label(simulationControlGroup, SWT.CENTER);
			timeUnitsLabel.setText("Time units");
			timeUnitsLabel.setLayoutData(gridData37);
		
		createProfileCombo();
		createTimeUnitsCombo();
		
		startButton = new Button(simulationControlGroup, SWT.NONE);
			startButton.setText("Start");
			startButton.setLayoutData(gridData9);
			startButton.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {	
					if (messagesList.isEnabled())
						messagesList.removeAll();
					else
						messagesList.setEnabled(true);
			
					Controller.initController(FILES_PATH + modelText.getText(), 
							                  Controller.ProfileType.valueOf(profileCombo.getText()), 
							                  FILES_PATH + resultsText.getText(), 
							                  FILES_PATH + tracesText.getText(), 
							                  thisClass);
/*
					Controller.initController(FILES_PATH + modelName, 
			                  Controller.ProfileType.valueOf(profileCombo.getText()), 
			                  FILES_PATH + resultsName, 
			                  FILES_PATH + tracesName, 
			                  thisClass);*/
					setState(SIMULATING);
					goOn();
				}
			});
		resumeButton = new Button(simulationControlGroup, SWT.NONE);
			resumeButton.setText("Resume");
			resumeButton.setLayoutData(gridData8);
			resumeButton.addSelectionListener(new SelectionAdapter() {   
				public void widgetSelected(SelectionEvent e) {
					setState(SIMULATING);
					goOn();
				}
			});
		pauseButton = new Button(simulationControlGroup, SWT.NONE);
			pauseButton.setText("Pause");
			pauseButton.setLayoutData(gridData10);
			pauseButton.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					Controller.executiveStop();
				}
			});
		endButton = new Button(simulationControlGroup, SWT.NONE);
			endButton.setText("End");
			endButton.setLayoutData(gridData12);
			endButton.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {					
					if (currentState == SIMULATING) {
						Controller.executiveStop();
						endButtonClicked = true;
					} else // currentState == READY_TO_RESUME_SIMULATION
						setState(READY_TO_START_SIMULATION);
					messagesList.removeAll();
				}
			});
		analizeButton = new Button(simulationControlGroup, SWT.NONE);
			analizeButton.setText("Analize");
			analizeButton.setLayoutData(gridData48);
			analizeButton.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					messagesList.removeAll();
					checkConsistent(FILES_PATH + modelText.getText());
				}
			});
		exitButton = new Button(simulationControlGroup, SWT.NONE);
			exitButton.setText("Exit");
			exitButton.setLayoutData(gridData49);
			exitButton.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					sShell.close();
				}
			});
	}

	/**
	 * This method initializes profileCombo	
	 *
	 */
	private void createProfileCombo() {
		GridData gridData7 = new GridData();
			gridData7.heightHint = -1;
			gridData7.horizontalAlignment = GridData.FILL;
			gridData7.widthHint = 50;
		profileCombo = new Combo(simulationControlGroup, SWT.NONE);
			profileCombo.setEnabled(false);
			profileCombo.setLayoutData(gridData7);
			profileCombo.add("VERBOSE");
			profileCombo.add("TRACES");
			profileCombo.add("PERFORMANCE");
			profileCombo.add("SCHEDULABILITY");
	}

	/**
	 * This method initializes timeUnitsCombo	
	 *
	 */
	private void createTimeUnitsCombo() {
		GridData gridData16 = new GridData();
			gridData16.horizontalAlignment = GridData.CENTER;
			gridData16.widthHint = 30;
		timeUnitsCombo = new Combo(simulationControlGroup, SWT.NONE);
			timeUnitsCombo.setLayoutData(gridData16);
			timeUnitsCombo.setEnabled(false);
			timeUnitsCombo.add("s");
			timeUnitsCombo.add("ms");
			timeUnitsCombo.add("us");
			timeUnitsCombo.add("ns");
	}
	
	/**
	 * This method initializes tabFolder	
	 *
	 */
	private void createTabFolder() {
			
		GridData gridData = new GridData();
			gridData.verticalSpan = 2;
			gridData.heightHint = 250;
			gridData.grabExcessHorizontalSpace = true;
			gridData.horizontalAlignment = GridData.FILL;
			gridData.grabExcessVerticalSpace = false;
			gridData.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;
			gridData.widthHint = -1;
		tabFolder = new TabFolder(sShell, SWT.NONE);
			tabFolder.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			tabFolder.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			tabFolder.setLayoutData(gridData);
		
		// Controls (composites) for each tab
		createExecutionsComposite();
		createTracesComposite();
		createSchedulingResultsComposite();
		createResourcesUsagesComposite();
		
		// Tabs
		TabItem executionsTabItem = new TabItem(tabFolder, SWT.NONE);
			executionsTabItem.setControl(executionsComposite);
			executionsTabItem.setText("Executions");
		TabItem tracesTabItem = new TabItem(tabFolder, SWT.NONE);
			tracesTabItem.setControl(tracesComposite);
			tracesTabItem.setText("Traces");
		TabItem schedulingResultsTabItem = new TabItem(tabFolder, SWT.NONE);
			schedulingResultsTabItem.setControl(schedulingResultsComposite);
			schedulingResultsTabItem.setText("Scheduling results");
		TabItem resourcesUsagesTabItem = new TabItem(tabFolder, SWT.NONE);
			resourcesUsagesTabItem.setControl(resourcesUsagesComposite);
			resourcesUsagesTabItem.setText("Resources usages");
	}

	/**
	 * This method initializes executionsComposite	
	 *
	 */
	private void createExecutionsComposite() {
		GridData gridData31 = new GridData();
			gridData31.horizontalAlignment = GridData.FILL;
		GridData gridData30 = new GridData();
			gridData30.horizontalAlignment = GridData.FILL;
		GridData gridData29 = new GridData();
			gridData29.grabExcessHorizontalSpace = true;
			gridData29.horizontalAlignment = GridData.FILL;
		GridData gridData24 = new GridData();
			gridData24.widthHint = 80;
			gridData24.heightHint = 15;
		GridData gridData23 = new GridData();
			gridData23.widthHint = 80;
			gridData23.heightHint = 15;
		GridData gridData22 = new GridData();
			gridData22.widthHint = 80;
			gridData22.heightHint = 15;
		GridData gridData20 = new GridData();
			gridData20.widthHint = 80;
			gridData20.heightHint = 15;
		GridData gridData19 = new GridData();
			gridData19.widthHint = 80;
			gridData19.grabExcessVerticalSpace = false;
			gridData19.heightHint = 15;
		GridData gridData18 = new GridData();
			gridData18.widthHint = 80;
			gridData18.heightHint = 15;
			gridData18.grabExcessVerticalSpace = false;
		GridData gridData13 = new GridData();
			gridData13.verticalSpan = 9;
			gridData13.heightHint = -1;
			gridData13.grabExcessHorizontalSpace = true;
			gridData13.grabExcessVerticalSpace = true;
			gridData13.verticalAlignment = GridData.FILL;
			gridData13.horizontalAlignment = GridData.FILL;
			gridData13.widthHint = 465;
		GridLayout gridLayout3 = new GridLayout();
			gridLayout3.numColumns = 2;
			gridLayout3.horizontalSpacing = 10;
			
		executionsComposite = new Composite(tabFolder, SWT.NONE);
			executionsComposite.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			executionsComposite.setForeground(new Color(Display.getCurrent(), 0, 0, 0));
			executionsComposite.setSize(new Point(860, 250));
			executionsComposite.setLayout(gridLayout3);
		
		maxExecTimeLabel = new Label(executionsComposite, SWT.NONE);
			maxExecTimeLabel.setText("Max. exec. time");
			maxExecTimeLabel.setLayoutData(gridData18);
		
		executionsTable = new Table(executionsComposite, SWT.BORDER);
			executionsTable.setHeaderVisible(true);
			executionsTable.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.BOLD));
			executionsTable.setLayoutData(gridData13);
			executionsTable.setLinesVisible(true);
			
		TableColumn transColumn = new TableColumn(executionsTable, SWT.CENTER);
			transColumn.setText("Deadline");
			transColumn.setResizable(true);
			transColumn.setWidth(200);
		TableColumn numColumn = new TableColumn(executionsTable, SWT.CENTER);
			numColumn.setText("Num.");
			numColumn.setResizable(true);
			numColumn.setWidth(50);
		TableColumn worstResponseTimeColumn = new TableColumn(executionsTable, SWT.CENTER);
			worstResponseTimeColumn.setText("Worst resp. time");
			worstResponseTimeColumn.setWidth(120);
			worstResponseTimeColumn.setResizable(true);
		TableColumn nominalDeadlineColumn = new TableColumn(executionsTable, SWT.CENTER);
			nominalDeadlineColumn.setText("Nominal deadline");
			nominalDeadlineColumn.setWidth(120);
			nominalDeadlineColumn.setResizable(true);
		TableColumn confColumn = new TableColumn(executionsTable, SWT.CENTER);
			confColumn.setText("Confidence (%)");
			confColumn.setResizable(true);
			confColumn.setWidth(120);
						
		maxExecTimeText = new Text(executionsComposite, SWT.BORDER);
			maxExecTimeText.setLayoutData(gridData22);
			maxExecTimeText.setEnabled(false);
			maxExecTimeText.setEditable(true);
		Label filler1Label = new Label(executionsComposite, SWT.NONE);
			filler1Label.setLayoutData(gridData29);
		transMinNumLabel = new Label(executionsComposite, SWT.NONE);
			transMinNumLabel.setText("Trans. min. num.");
			transMinNumLabel.setLayoutData(gridData19);
		transMinNumText = new Text(executionsComposite, SWT.BORDER);
			transMinNumText.setLayoutData(gridData23);
			transMinNumText.setEditable(true);
		Label filler2Label = new Label(executionsComposite, SWT.NONE);
			filler2Label.setLayoutData(gridData30);
		currentTimeLabel = new Label(executionsComposite, SWT.NONE);
			currentTimeLabel.setText("Current time");
			currentTimeLabel.setLayoutData(gridData20);
		currentTimeText = new Text(executionsComposite, SWT.BORDER);
			currentTimeText.setLayoutData(gridData24);
			currentTimeText.setEditable(false);
		Label filler3Label = new Label(executionsComposite, SWT.NONE);
			filler3Label.setLayoutData(gridData31);
	}

	/**
	 * This method initializes tracesComposite	
	 *
	 */
	private void createTracesComposite() {
		GridData gridData21 = new GridData();
			gridData21.grabExcessVerticalSpace = true;
		GridData gridData28 = new GridData();
			gridData28.horizontalAlignment = GridData.FILL;
			gridData28.verticalAlignment = GridData.CENTER;
			gridData28.grabExcessVerticalSpace = false;
		GridData gridData27 = new GridData();
			gridData27.horizontalAlignment = GridData.FILL;
			gridData27.verticalAlignment = GridData.FILL;
			gridData27.grabExcessVerticalSpace = false;
		GridData gridData14 = new GridData();
			gridData14.verticalSpan = 3;
			gridData14.heightHint = 285;
			gridData14.grabExcessHorizontalSpace = true;
			gridData14.grabExcessVerticalSpace = true;
			gridData14.horizontalAlignment = GridData.FILL;
			gridData14.widthHint = 535;
		GridLayout gridLayout4 = new GridLayout();
			gridLayout4.numColumns = 2;
		tracesComposite = new Composite(tabFolder, SWT.NONE);
			tracesComposite.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			tracesComposite.setLayout(gridLayout4);
			tracesComposite.setEnabled(false);
			tracesComposite.setSize(new Point(860, 250));
		Label filler3 = new Label(tracesComposite, SWT.NONE);
			filler3.setLayoutData(gridData21);
		tracesTable = new Table(tracesComposite, SWT.BORDER);
			tracesTable.setHeaderVisible(true);
			tracesTable.setLayoutData(gridData14);
			tracesTable.setLinesVisible(true);
		TableColumn numColumn = new TableColumn(tracesTable, SWT.NONE);
			numColumn.setWidth(60);
			numColumn.setText("Num.");
		TableColumn timeColumn = new TableColumn(tracesTable, SWT.NONE);
			timeColumn.setText("Time");
			timeColumn.setResizable(false);
			timeColumn.setWidth(100);
		TableColumn sourceColumn = new TableColumn(tracesTable, SWT.NONE);
			sourceColumn.setText("Source");
			sourceColumn.setWidth(195);
			sourceColumn.setResizable(false);
		TableColumn messageTypeColumn = new TableColumn(tracesTable, SWT.NONE);
			messageTypeColumn.setWidth(430);
			messageTypeColumn.setText("Message type");
		reTracesButton = new Button(tracesComposite, SWT.BORDER);
			reTracesButton.setText("Traces -");
			reTracesButton.setLayoutData(gridData27);
			reTracesButton.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					tracesTable.removeAll();
					if (!avTracesButton.isEnabled())
						avTracesButton.setEnabled(true);
					topTraceIndex -= NUM_TRACES_ON_TABLE;
					try {
						parseTracesFile(FILES_PATH + tracesText.getText());
					} catch (JSimMastException e1) {
						e1.printStackTrace();
					}
				}
			});
		avTracesButton = new Button(tracesComposite, SWT.BORDER);
			avTracesButton.setText("Traces +");
			avTracesButton.setLayoutData(gridData28);
			avTracesButton.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					tracesTable.removeAll();
					topTraceIndex += NUM_TRACES_ON_TABLE;
					if (topTraceIndex == numTraces - NUM_TRACES_ON_TABLE + 1)
						avTracesButton.setEnabled(false);
					try {
						parseTracesFile(FILES_PATH + tracesText.getText());
					} catch (JSimMastException e1) {
						e1.printStackTrace();
					}
				}
			});
	}

	/**
	 * This method initializes schedulingResultsComposite	
	 *
	 */
	private void createSchedulingResultsComposite() {
		GridData gridData44 = new GridData();
			gridData44.verticalAlignment = GridData.FILL;
		GridData gridData43 = new GridData();
			gridData43.verticalAlignment = GridData.FILL;
		GridData gridData42 = new GridData();
			gridData42.verticalAlignment = GridData.FILL;
		GridData gridData26 = new GridData();
			gridData26.horizontalAlignment = GridData.BEGINNING;
			gridData26.verticalAlignment = GridData.FILL;
		GridData gridData25 = new GridData();
			gridData25.widthHint = -1;
			gridData25.grabExcessVerticalSpace = false;
			gridData25.heightHint = 25;
			gridData25.horizontalAlignment = GridData.BEGINNING;
			gridData25.verticalAlignment = GridData.FILL;
		GridData gridData34 = new GridData();
			gridData34.grabExcessHorizontalSpace = false;
			gridData34.verticalAlignment = GridData.FILL;
			gridData34.grabExcessVerticalSpace = false;
			gridData34.heightHint = 25;
			gridData34.horizontalAlignment = GridData.BEGINNING;
			gridData34.widthHint = -1;
		GridData gridData32 = new GridData();
			gridData32.grabExcessHorizontalSpace = false;
			gridData32.verticalAlignment = GridData.FILL;
		GridData gridData36 = new GridData();
			gridData36.widthHint = -1;
			gridData36.grabExcessVerticalSpace = false;
			gridData36.verticalAlignment = GridData.FILL;
			gridData36.horizontalAlignment = GridData.BEGINNING;
			gridData36.horizontalSpan = 2;
			gridData36.grabExcessHorizontalSpace = false;
			gridData36.heightHint = 25;
		GridData gridData35 = new GridData();
			gridData35.widthHint = -1;
			gridData35.grabExcessVerticalSpace = false;
			gridData35.verticalAlignment = GridData.FILL;
			gridData35.horizontalAlignment = GridData.BEGINNING;
			gridData35.horizontalSpan = 2;
			gridData35.grabExcessHorizontalSpace = false;
			gridData35.heightHint = 25;
		GridData gridData41 = new GridData();
			gridData41.heightHint = -1;
			gridData41.verticalAlignment = GridData.FILL;
			gridData41.grabExcessHorizontalSpace = false;
			gridData41.horizontalAlignment = GridData.BEGINNING;
			gridData41.grabExcessVerticalSpace = false;
			gridData41.widthHint = -1;
		GridLayout gridLayout5 = new GridLayout();
			gridLayout5.numColumns = 2;
			gridLayout5.horizontalSpacing = 20;   //JMD
			
		schedulingResultsComposite = new Composite(tabFolder, SWT.NONE);
			schedulingResultsComposite.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			schedulingResultsComposite.setLayout(gridLayout5);
			schedulingResultsComposite.setEnabled(false);
			schedulingResultsComposite.setSize(new Point(860, 250));
		transesLabel = new Label(schedulingResultsComposite, SWT.NONE);
			transesLabel.setText("Transactions");
			transesLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.BOLD));
			transesLabel.setLayoutData(gridData41);	
		eventsLabel = new Label(schedulingResultsComposite, SWT.NONE);
			eventsLabel.setText("Events");
			eventsLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.BOLD));
			eventsLabel.setLayoutData(gridData32);
		
		createTransesCombo();
		createEventsCombo();
		
		localDataLabel = new Label(schedulingResultsComposite, SWT.NONE);
			localDataLabel.setText("Local data");
			localDataLabel.setLayoutData(gridData26);
			localDataLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.BOLD));
		Label filler4 = new Label(schedulingResultsComposite, SWT.NONE);
		localDataTable = new Table(schedulingResultsComposite, SWT.BORDER);
			localDataTable.setHeaderVisible(true);
			localDataTable.setLayoutData(gridData35);
			localDataTable.setLinesVisible(true);
		TableColumn worstResponseTimeColumn = new TableColumn(localDataTable, SWT.NONE);
			worstResponseTimeColumn.setWidth(80);
			worstResponseTimeColumn.setText("Worst resp. t.");
		TableColumn avgResponseTimeColumn = new TableColumn(localDataTable, SWT.NONE);
			avgResponseTimeColumn.setWidth(80);
			avgResponseTimeColumn.setText("Avg. resp. t.");
		TableColumn bestResponseTimeColumn = new TableColumn(localDataTable, SWT.NONE);
			bestResponseTimeColumn.setWidth(80);
			bestResponseTimeColumn.setText("Best resp. t.");
		TableColumn numQueuedActivationColumn= new TableColumn(localDataTable, SWT.NONE);
			numQueuedActivationColumn.setWidth(85);
			numQueuedActivationColumn.setText("#Queued Act.");
		TableColumn maxWaitTimeColumn = new TableColumn(localDataTable, SWT.NONE);
			maxWaitTimeColumn.setWidth(80);
			maxWaitTimeColumn.setText("Max. wait. t.");
		TableColumn avgWaitTimeColumn = new TableColumn(localDataTable, SWT.NONE);
			avgWaitTimeColumn.setWidth(80);
			avgWaitTimeColumn.setText("Avg. wait. t.");
		TableColumn maxSuspensionTimeColumn = new TableColumn(localDataTable, SWT.NONE);
			maxSuspensionTimeColumn.setWidth(80);
			maxSuspensionTimeColumn.setText("Max. susp. t.");
		TableColumn avgSuspensionTimeColumn = new TableColumn(localDataTable, SWT.NONE);
			avgSuspensionTimeColumn.setWidth(80);
			avgSuspensionTimeColumn.setText("Avg. susp. t.");
		TableColumn maxBlockingTimeColumn = new TableColumn(localDataTable, SWT.NONE);
			maxBlockingTimeColumn.setWidth(80);
			maxBlockingTimeColumn.setText("Max. block. t.");
		TableColumn avgBlockingTimeColumn = new TableColumn(localDataTable, SWT.NONE);
			avgBlockingTimeColumn.setWidth(80);
			avgBlockingTimeColumn.setText("Avg. block. t.");
		globalDataLabel = new Label(schedulingResultsComposite, SWT.NONE);
			globalDataLabel.setText("Global data");
			globalDataLabel.setLayoutData(gridData42);
			globalDataLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.BOLD));
			
		Label filler21 = new Label(schedulingResultsComposite, SWT.NONE);
		globalDataTable = new Table(schedulingResultsComposite, SWT.BORDER);
			globalDataTable.setHeaderVisible(true);
			globalDataTable.setLayoutData(gridData36);
			globalDataTable.setLinesVisible(true);
		localMaxMissRatioLabel = new Label(schedulingResultsComposite, SWT.NONE);
			localMaxMissRatioLabel.setText("Local Max Miss Ratio");
			localMaxMissRatioLabel.setLayoutData(gridData43);
			localMaxMissRatioLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.BOLD));
		globalMaxMissRatioLabel = new Label(schedulingResultsComposite, SWT.NONE);
			globalMaxMissRatioLabel.setText("Global Max Miss Ratio");
			globalMaxMissRatioLabel.setLayoutData(gridData44);
			globalMaxMissRatioLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.BOLD));
		localMaxMissRatioTable = new Table(schedulingResultsComposite, SWT.BORDER);
			localMaxMissRatioTable.setHeaderVisible(true);
			localMaxMissRatioTable.setLayoutData(gridData34);
			localMaxMissRatioTable.setLinesVisible(true);
		TableColumn tableColumn4 = new TableColumn(localMaxMissRatioTable, SWT.CENTER);
			tableColumn4.setWidth(100);
			tableColumn4.setText("Deadline");
		TableColumn tableColumn5 = new TableColumn(localMaxMissRatioTable, SWT.CENTER);
			tableColumn5.setWidth(100);
			tableColumn5.setText("Ratio");
		globalMaxMissRatioTable = new Table(schedulingResultsComposite, SWT.BORDER);
			globalMaxMissRatioTable.setHeaderVisible(true);
			globalMaxMissRatioTable.setLayoutData(gridData25);
			globalMaxMissRatioTable.setLinesVisible(true);
		TableColumn tableColumn6 = new TableColumn(globalMaxMissRatioTable, SWT.CENTER);
			tableColumn6.setWidth(150);
			tableColumn6.setText("Referenced event");
		TableColumn tableColumn7 = new TableColumn(globalMaxMissRatioTable, SWT.CENTER);
			tableColumn7.setWidth(100);
			tableColumn7.setText("Deadline");
		TableColumn tableColumn8 = new TableColumn(globalMaxMissRatioTable, SWT.CENTER);
			tableColumn8.setWidth(100);
			tableColumn8.setText("Ratio");
		TableColumn referencedEventCol = new TableColumn(globalDataTable, SWT.LEFT);
			referencedEventCol.setWidth(170);
			referencedEventCol.setText("Referenced event");
		TableColumn worstGlobalResponseColumn = new TableColumn(globalDataTable, SWT.NONE);
			worstGlobalResponseColumn.setWidth(85);
			worstGlobalResponseColumn.setResizable(false);
			worstGlobalResponseColumn.setText("Worst resp.");
		TableColumn avgGlobalResponseColumn = new TableColumn(globalDataTable, SWT.NONE);
			avgGlobalResponseColumn.setWidth(85);
			avgGlobalResponseColumn.setResizable(false);
			avgGlobalResponseColumn.setText("Avg. resp.");
		TableColumn bestGlobalResponseColumn = new TableColumn(globalDataTable, SWT.NONE);
			bestGlobalResponseColumn.setWidth(85);
			bestGlobalResponseColumn.setResizable(false);
			bestGlobalResponseColumn.setText("Best resp.");
		TableColumn maxJitterColumn = new TableColumn(globalDataTable, SWT.NONE);
			maxJitterColumn.setWidth(85);
			maxJitterColumn.setResizable(false);
			maxJitterColumn.setText("Max. jitter");
		TableColumn avgJitterColumn = new TableColumn(globalDataTable, SWT.NONE);
			avgJitterColumn.setWidth(85);
			avgJitterColumn.setResizable(false);
			avgJitterColumn.setText("Avg. jitter");
	}

	/**
	 * This method initializes transesCombo	
	 *
	 */
	private void createTransesCombo() {
		GridData gridData15 = new GridData();
			gridData15.widthHint = -1;
			gridData15.heightHint = -1;
			gridData15.grabExcessVerticalSpace = true;
			gridData15.grabExcessHorizontalSpace = false;
			gridData15.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;
			gridData15.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;
		transesCombo = new Combo(schedulingResultsComposite, SWT.V_SCROLL | SWT.DROP_DOWN | SWT.BORDER | SWT.READ_ONLY);
			transesCombo.setVisibleItemCount(25);
			transesCombo.setLayoutData(gridData15);
			transesCombo.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					eventsCombo.setEnabled(true);
					eventsCombo.removeAll();
					// I look for the selected transaction and fill the events combo
					for (Element e2eFlowResultsElem : e2eFlowResultsElemList) {
						if (e2eFlowResultsElem.getAttribute("Name").equals(transesCombo.getText())) {
							// I fill the events combo with <mast_res:Simulation_Timing_Results> children
							simTimingResultsElemList = XMLparser.getChildElem(e2eFlowResultsElem, "Simulation_Timing_Results");
							for (Element simTimingResultsElem : simTimingResultsElemList) {
								eventsCombo.add(simTimingResultsElem.getAttribute("Event"));
							}
							eventsCombo.select(0);
							eventsCombo.notifyListeners(SWT.Selection, new Event());
							break;
						}
					}
				}
			});
	}
	
	/**
	 * This method initializes eventsCombo	
	 *
	 */
	private void createEventsCombo() {
		GridData gridData33 = new GridData();
			gridData33.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;
			gridData33.grabExcessHorizontalSpace = false;
			gridData33.horizontalIndent = 0;
			gridData33.widthHint = -1;
		eventsCombo = new Combo(schedulingResultsComposite, SWT.BORDER);
			eventsCombo.setEnabled(false);
			eventsCombo.setLayoutData(gridData33);
			eventsCombo.setVisibleItemCount(10);
			eventsCombo.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					
					// I clear the tables
					localDataTable.removeAll();
					globalDataTable.removeAll();
					localMaxMissRatioTable.removeAll();
					globalMaxMissRatioTable.removeAll();
					
					// I look for the selected event and fill the tables					
					for (Element simTimingResultsElem : simTimingResultsElemList) {
						
						if (simTimingResultsElem.getAttribute("Event").equals(eventsCombo.getText())) {
							
							java.util.List<Element> worstGlobalResponseTimeElemList = XMLparser.getChildElem(simTimingResultsElem, "Worst_Global_Response_Time");
							java.util.List<Element> bestGlobalResponseTimeElemList = XMLparser.getChildElem(simTimingResultsElem, "Best_Global_Response_Time");
							java.util.List<Element> avgGlobalResponseTimeElemList = XMLparser.getChildElem(simTimingResultsElem, "Avg_Global_Response_Time");
							java.util.List<Element> maxJitterElemList = XMLparser.getChildElem(simTimingResultsElem, "Max_Jitter");
							java.util.List<Element> avgJitterElemList = XMLparser.getChildElem(simTimingResultsElem, "Avg_Jitter");
							java.util.List<Element> localMissRatioElemList = XMLparser.getChildElem(simTimingResultsElem, "Local_Miss_Ratio");
							java.util.List<Element> globalMissRatioElemList = XMLparser.getChildElem(simTimingResultsElem, "Global_Miss_Ratio");
							
							TableItem item;
							
							// Local data table
							item = new TableItem(localDataTable, SWT.NONE);
							item.setText(new String[]{Tools.prettyPrint(simTimingResultsElem.getAttribute("Worst_Local_Response_Time"), NUM_DECIMALS), 
									                  Tools.prettyPrint(simTimingResultsElem.getAttribute("Avg_Local_Response_Time"),NUM_DECIMALS), 
									                  Tools.prettyPrint(simTimingResultsElem.getAttribute("Best_Local_Response_Time"), NUM_DECIMALS), 
									  				  simTimingResultsElem.getAttribute("Num_Of_Queued_Activations"),
									  				  Tools.prettyPrint(simTimingResultsElem.getAttribute("Max_Waiting_Time"), NUM_DECIMALS), 
									  				  Tools.prettyPrint(simTimingResultsElem.getAttribute("Avg_Waiting_Time"), NUM_DECIMALS), 
									  				  Tools.prettyPrint(simTimingResultsElem.getAttribute("Max_Suspension_Time"), NUM_DECIMALS), 
									  				  Tools.prettyPrint(simTimingResultsElem.getAttribute("Avg_Suspension_Time"), NUM_DECIMALS), 
									  				  Tools.prettyPrint(simTimingResultsElem.getAttribute("Max_Blocking_Time"), NUM_DECIMALS), 
									  				  Tools.prettyPrint(simTimingResultsElem.getAttribute("Avg_Blocking_Time"), NUM_DECIMALS)});
							// Global data table
							if (worstGlobalResponseTimeElemList.size() > 0) {
								String refEvent;
								String wgrt;
								String agrt = "";
								String bgrt = "";
								String mjt="";
								String ajt="";
								for (Element worstGlobalResponseTimeElem : worstGlobalResponseTimeElemList) {
									refEvent = worstGlobalResponseTimeElem.getAttribute("Referenced_Event");
									wgrt = worstGlobalResponseTimeElem.getAttribute("Value");
									// I look for the corresponding <mast_res:Best_Global_Response_Time>
									for (Element bestGlobalResponseTimeElem : bestGlobalResponseTimeElemList) {
										if (bestGlobalResponseTimeElem.getAttribute("Referenced_Event").equals(refEvent)) {
											bgrt = bestGlobalResponseTimeElem.getAttribute("Value");
											break;
										}
									}
									// I look for the corresponding <mast_res:Avg_Global_Response_Time>
									for (Element avgGlobalResponseTimeElem : avgGlobalResponseTimeElemList) {
										if (avgGlobalResponseTimeElem.getAttribute("Referenced_Event").equals(refEvent)) {
											agrt = avgGlobalResponseTimeElem.getAttribute("Value");
											break;
										}
									}
									// I look for the corresponding <mast_res:Max_Jitter>
									for (Element maxJitterElem : maxJitterElemList) {
										if (maxJitterElem.getAttribute("Referenced_Event").equals(refEvent)) {
											mjt = maxJitterElem.getAttribute("Value");
											break;
										}
									}
									// I look for the corresponding <mast_res:Avg_Jitter>
									for (Element avgJitterElem : avgJitterElemList) {
										if (avgJitterElem.getAttribute("Referenced_Event").equals(refEvent)) {
											ajt = avgJitterElem.getAttribute("Value");
											break;
										}
									}
									item = new TableItem(globalDataTable, SWT.NONE);
									item.setText(new String[]{transesCombo.getText() + "/" + refEvent, 
															  Tools.prettyPrint(wgrt, NUM_DECIMALS),
									                          Tools.prettyPrint(agrt, NUM_DECIMALS), 
											                  Tools.prettyPrint(bgrt, NUM_DECIMALS),
													          Tools.prettyPrint(mjt, NUM_DECIMALS),
															  Tools.prettyPrint(ajt, NUM_DECIMALS)});
								}
							}
							// Local max miss ratio table
							if (localMissRatioElemList.size() > 0) {
								for (Element localMissRatioElem : localMissRatioElemList) {
									item = new TableItem(localMaxMissRatioTable, SWT.NONE);
									item.setText(new String[]{Tools.prettyPrint(localMissRatioElem.getAttribute("Deadline"), NUM_DECIMALS),
											                  Tools.prettyPrint(localMissRatioElem.getAttribute("Ratio"), NUM_DECIMALS)}); 
								}
							}
							// Global max miss ratio table
							if (globalMissRatioElemList.size() > 0) {
								for (Element globalMissRatioElem : globalMissRatioElemList) {
									item = new TableItem(globalMaxMissRatioTable, SWT.NONE);
									item.setText(new String[]{transesCombo.getText() + "/" + globalMissRatioElem.getAttribute("Referenced_Event"),
											                  Tools.prettyPrint(globalMissRatioElem.getAttribute("Deadline"), NUM_DECIMALS),
													          Tools.prettyPrint(globalMissRatioElem.getAttribute("Ratio"), NUM_DECIMALS)}); 
								}
							}
							break;
						}
					}
				}
			});
	}
	
	/**
	 * This method initializes resourcesUsagesComposite	
	 *
	 */
	private void createResourcesUsagesComposite() {
			
		GridData gridData56 = new GridData();
		gridData56.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;
		GridData gridData55 = new GridData();
		gridData55.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;
		GridData gridData54 = new GridData();
		gridData54.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;
		GridData gridData53 = new GridData();
		gridData53.horizontalAlignment = org.eclipse.swt.layout.GridData.BEGINNING;
		GridData gridData52 = new GridData();
		gridData52.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;
		GridData gridData51 = new GridData();
		gridData51.grabExcessHorizontalSpace = false;
		gridData51.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;
		GridData gridData50 = new GridData();
		gridData50.grabExcessHorizontalSpace = false;
		gridData50.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;
		GridData gridData40 = new GridData();
		gridData40.grabExcessVerticalSpace = true;
		gridData40.widthHint = -1;
		gridData40.grabExcessHorizontalSpace = true;
		gridData40.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;
		GridData gridData39 = new GridData();
			gridData39.grabExcessHorizontalSpace = false;
			gridData39.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;
			gridData39.widthHint = -1;
		GridLayout gridLayout6 = new GridLayout();
			gridLayout6.numColumns = 2;
			gridLayout6.horizontalSpacing = 20;
			gridLayout6.makeColumnsEqualWidth = false;
		resourcesUsagesComposite = new Composite(tabFolder, SWT.NONE);
			resourcesUsagesComposite.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			resourcesUsagesComposite.setLayout(gridLayout6);
			resourcesUsagesComposite.setEnabled(false);
			resourcesUsagesComposite.setSize(new Point(860, 250));
		processingRsrcsLabel = new Label(resourcesUsagesComposite, SWT.NONE);
			processingRsrcsLabel.setText("Processing resources");
			processingRsrcsLabel.setLayoutData(gridData50);
			processingRsrcsLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.BOLD));
		processingRsrcsUtilizationsLabel = new Label(resourcesUsagesComposite, SWT.NONE);
			processingRsrcsUtilizationsLabel.setText("Utilizations (%)");
			processingRsrcsUtilizationsLabel.setLayoutData(gridData51);
			processingRsrcsUtilizationsLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.BOLD));
		
		createProcessingRsrcsCombo();
		
		processingRsrcsUtilizationsTable = new Table(resourcesUsagesComposite, SWT.BORDER);
			processingRsrcsUtilizationsTable.setHeaderVisible(true);
			processingRsrcsUtilizationsTable.setLayoutData(gridData39);
			processingRsrcsUtilizationsTable.setLinesVisible(true);
		TableColumn tableColumn = new TableColumn(processingRsrcsUtilizationsTable, SWT.NONE);
			tableColumn.setWidth(60);
			tableColumn.setText("Total");
		TableColumn tableColumn1 = new TableColumn(processingRsrcsUtilizationsTable, SWT.NONE);
			tableColumn1.setWidth(135);
			tableColumn1.setText("Processing / Transmitting");
		TableColumn tableColumn2 = new TableColumn(processingRsrcsUtilizationsTable, SWT.NONE);
			tableColumn2.setWidth(90);
			tableColumn2.setText("Context switch");
		TableColumn tableColumn3 = new TableColumn(processingRsrcsUtilizationsTable, SWT.NONE);
			tableColumn3.setWidth(70);
			tableColumn3.setText("Timer");
		TableColumn tableColumn4 = new TableColumn(processingRsrcsUtilizationsTable, SWT.NONE);
			tableColumn4.setWidth(70);
			tableColumn4.setText("Interrupt");
		Label filler2 = new Label(resourcesUsagesComposite, SWT.NONE);
			filler2.setLayoutData(gridData52);
		label = new Label(resourcesUsagesComposite, SWT.NONE);
			label.setText("");
			label.setLayoutData(gridData53);
		Label filler1 = new Label(resourcesUsagesComposite, SWT.NONE);
			filler1.setLayoutData(gridData54);
		mutexesLabel = new Label(resourcesUsagesComposite, SWT.NONE);
			mutexesLabel.setText("Mutexes");
			mutexesLabel.setLayoutData(gridData55);
		mutexesLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.BOLD));
		Label filler = new Label(resourcesUsagesComposite, SWT.NONE);
			filler.setLayoutData(gridData56);
		mutexesTable = new Table(resourcesUsagesComposite, SWT.BORDER);
			mutexesTable.setHeaderVisible(true);
			mutexesTable.setLayoutData(gridData40);
			mutexesTable.setLinesVisible(true);
		TableColumn tableColumn21 = new TableColumn(mutexesTable, SWT.NONE);
			tableColumn21.setWidth(150);
			tableColumn21.setResizable(false);
			tableColumn21.setText("Name");
		TableColumn tableColumn31 = new TableColumn(mutexesTable, SWT.NONE);
			tableColumn31.setWidth(90);
			tableColumn31.setText("Utilization (%)");
	}

	/**
	 * This method initializes processingRsrcsCombo	
	 *
	 */
	private void createProcessingRsrcsCombo() {
		GridData gridData38 = new GridData();
			gridData38.grabExcessHorizontalSpace = false;
			gridData38.verticalAlignment = org.eclipse.swt.layout.GridData.FILL;
			gridData38.horizontalAlignment = org.eclipse.swt.layout.GridData.BEGINNING;
		processingRsrcsCombo = new Combo(resourcesUsagesComposite, SWT.V_SCROLL | SWT.DROP_DOWN | SWT.BORDER | SWT.READ_ONLY);
			processingRsrcsCombo.setLayoutData(gridData38);
			processingRsrcsCombo.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					processingRsrcsUtilizationsTable.removeAll();
					String processingRsrcName = processingRsrcsCombo.getText();
					// I look for the selected processing resource and fill the utilizations table
					for (Element processingRsrcResultsElem : processingRsrcResultsElemList) {
						if (processingRsrcResultsElem.getAttribute("Name").equals(processingRsrcName)) {
							Element detailedUtilizationResultsElem = XMLparser.getChildElem(processingRsrcResultsElem, "Detailed_Utilization_Results").elementAt(0);				
							TableItem item = new TableItem(processingRsrcsUtilizationsTable, SWT.NONE);
							item.setText(new String[]{Tools.prettyPrint(detailedUtilizationResultsElem.getAttribute("Total"), NUM_DECIMALS), 
									                  Tools.prettyPrint(detailedUtilizationResultsElem.getAttribute("Application"), NUM_DECIMALS), 
											          Tools.prettyPrint(detailedUtilizationResultsElem.getAttribute("Context_Switch"), NUM_DECIMALS),
													  Tools.prettyPrint(detailedUtilizationResultsElem.getAttribute("Timer"), NUM_DECIMALS),
													  Tools.prettyPrint(detailedUtilizationResultsElem.getAttribute("Interrupt"), NUM_DECIMALS)});
							mutexesTable.removeAll();
							// I look for the <mast_res:Mutex_Results> associated to the selected processing resource and fill the mutexes table
							Element utilizationResultsElem;
							for (Element mutexResultsElem : mutexResultsElemList) {
								if (mutexResultsElem.getAttribute("Name").startsWith(processingRsrcName)) {
									utilizationResultsElem = XMLparser.getChildElem(mutexResultsElem, "Utilization_Results").firstElement();
									item = new TableItem(mutexesTable, SWT.NONE);
									item.setText(new String[]{mutexResultsElem.getAttribute("Name"), 
											                  Tools.prettyPrint(utilizationResultsElem.getAttribute("Total"), NUM_DECIMALS)});
								}
							}
							break;
						}
					}	
				}
			});
	}
	
	/**
	 * This method initializes messagesGroup	
	 *
	 */
	private void createMessagesGroup() {
		GridData gridData1 = new GridData();
			gridData1.horizontalSpan = 2;
			gridData1.heightHint = -1;
			gridData1.verticalAlignment = GridData.FILL;
			gridData1.grabExcessVerticalSpace = true;
			gridData1.grabExcessHorizontalSpace = true;
			gridData1.horizontalAlignment = GridData.FILL;
			gridData1.widthHint = -1;
		FillLayout fillLayout = new FillLayout();
			fillLayout.marginHeight = 3;
			fillLayout.spacing = 0;
			fillLayout.type = SWT.HORIZONTAL;
			fillLayout.marginWidth = 3;
		messagesGroup = new Group(sShell, SWT.NONE);
			messagesGroup.setText("Messages");
			messagesGroup.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
			messagesGroup.setLayoutData(gridData1);
			messagesGroup.setLayout(fillLayout);
		messagesList = new List(messagesGroup, SWT.V_SCROLL | SWT.BORDER);
			messagesList.setEnabled(false);
	}

	/*
	 * This method manipulates some GUI controls when the simulation is stopped.
	 */
	private void simulationStopped(){
		pauseButton.setEnabled(false);
		maxExecTimeText.setEditable(true);
		transMinNumText.setEditable(true);
		currentTimeText.setEnabled(false);
	}
	
	/*
	 * This method manipulates some GUI controls when the simulation is finished.
	 */
	private void resetGUI(){
		resultsText.setEnabled(false);
		resultsText.setText("");
		tracesText.setEnabled(false);
		tracesText.setText("");
		mssgsText.setEnabled(false);
		mssgsText.setText("");
		profileCombo.setEnabled(false);
		profileCombo.setText("");
		timeUnitsCombo.setEnabled(false);
		timeUnitsCombo.setText("");
		startButton.setEnabled(false);
		resumeButton.setEnabled(false);
		pauseButton.setEnabled(false);
		endButton.setEnabled(false);
		analizeButton.setEnabled(false);
		exitButton.setEnabled(true);
		maxExecTimeText.setEnabled(false);
		maxExecTimeText.setText("");
		transMinNumText.setEnabled(false);
		transMinNumText.setText("");
	}
	
	/*
	 * This method implements the GUI states machine.
	 */
	private void setState(int newState){
		if (newState != currentState) {
			switch (newState) {
			case READY_TO_ACCEPT_MODEL_FILE:
				if (currentState == READY_TO_CHECK_MODEL_FILE)
					analizeButton.setEnabled(false);					
				else // currentState == READY_TO_START_SIMULATION
					resetGUI();
				break;
			case READY_TO_CHECK_MODEL_FILE:
				if (currentState == READY_TO_START_SIMULATION)
					resetGUI();
				analizeButton.setEnabled(true);						
				break;
			case READY_TO_START_SIMULATION:
				switch (currentState) {
				case READY_TO_ACCEPT_MODEL_FILE:
					resultsText.setEnabled(true);
					resultsText.setText(modelText.getText().replace("mdl", "res"));
					tracesText.setEnabled(true);
					tracesText.setText(modelText.getText().replace("mdl", "trc"));
					mssgsText.setEnabled(true);
					mssgsText.setText(modelText.getText().replace("mdl.xml", "txt"));
					profileCombo.setEnabled(true);
					profileCombo.setText(DEFAULT_PROFILE);
					timeUnitsCombo.setEnabled(true);
					timeUnitsCombo.setText(DEFAULT_TIME_UNITS);
					startButton.setEnabled(true);
					maxExecTimeText.setEnabled(true);
					maxExecTimeText.setEditable(true);
					maxExecTimeText.setText(MAX_EXEC_TIME);
					transMinNumText.setEnabled(true);
					transMinNumText.setEditable(true);
					transMinNumText.setText(TRANS_MIN_NUM);
					currentTimeText.setText("");
					break;
				case READY_TO_CHECK_MODEL_FILE:
					modelText.setEditable(true);
					resultsText.setEnabled(true);
					resultsText.setText(modelText.getText().replace("mdl", "res"));
					tracesText.setEnabled(true);
					tracesText.setText(modelText.getText().replace("mdl", "trc"));
					mssgsText.setEnabled(true);
					mssgsText.setText(modelText.getText().replace("mdl.xml", "txt"));
					profileCombo.setEnabled(true);
					profileCombo.setText(DEFAULT_PROFILE);
					timeUnitsCombo.setEnabled(true);
					timeUnitsCombo.setText(DEFAULT_TIME_UNITS);
					startButton.setEnabled(true);
					analizeButton.setEnabled(false);
					maxExecTimeText.setEnabled(true);
					maxExecTimeText.setEditable(true);
					maxExecTimeText.setText(MAX_EXEC_TIME);
					transMinNumText.setEnabled(true);
					transMinNumText.setEditable(true);
					transMinNumText.setText(TRANS_MIN_NUM);
					currentTimeText.setText("");
					break;
				case SIMULATING:
					modelText.setEditable(true);
					resultsText.setEditable(true);
					tracesText.setEditable(true);
					mssgsText.setEditable(true);
					profileCombo.setEnabled(true);
					profileCombo.setText(DEFAULT_PROFILE);
					timeUnitsCombo.setEnabled(true);
					timeUnitsCombo.setText(DEFAULT_TIME_UNITS);
					startButton.setEnabled(true);
					//pauseButton.setEnabled(false);
					endButton.setEnabled(false);
					//maxExecTimeText.setEditable(true);
					maxExecTimeText.setText(MAX_EXEC_TIME);
					transMinNumText.setEnabled(true);
					//transMinNumText.setEditable(true);
					transMinNumText.setText(TRANS_MIN_NUM);
					//currentTimeText.setEnabled(false);
					currentTimeText.setText("");
					break;
				case READY_TO_RESUME_SIMULATION:
					modelText.setEditable(true);
					resultsText.setEditable(true);
					tracesText.setEditable(true);
					mssgsText.setEditable(true);
					profileCombo.setEnabled(true);
					profileCombo.setText(DEFAULT_PROFILE);
					timeUnitsCombo.setEnabled(true);
					timeUnitsCombo.setText(DEFAULT_TIME_UNITS);
					startButton.setEnabled(true);
					resumeButton.setEnabled(false);
					endButton.setEnabled(false);
					maxExecTimeText.setText(MAX_EXEC_TIME);
					transMinNumText.setText(TRANS_MIN_NUM);
					currentTimeText.setEnabled(false);
					currentTimeText.setText("");
				default:
					break;
				}
				startButton.setFocus();
				break;
			case SIMULATING:
				if (currentState == READY_TO_START_SIMULATION) {
					modelText.setEditable(false);
					tracesText.setEditable(false);
					resultsText.setEditable(false);
					mssgsText.setEditable(false);
					profileCombo.setEnabled(false);					
					timeUnitsCombo.setEnabled(false);				
					startButton.setEnabled(false);
					pauseButton.setEnabled(true);
					endButton.setEnabled(true);
					maxExecTimeText.setEditable(false);
					transMinNumText.setEditable(false);
					currentTimeText.setEnabled(true);
				} else { // currentState == READY_TO_RESUME_SIMULATION
					resumeButton.setEnabled(false);
					pauseButton.setEnabled(true);
				}
				break;
			case READY_TO_RESUME_SIMULATION: // currentState = SIMULATING
				Display.getDefault().asyncExec(new Runnable(){
					public void run() {
						resumeButton.setEnabled(true);
					}
				});
				break;
			default:
				break;
			}
			currentState = newState;
		}
	}
	
	/*
	 * This method checks the model file consistency using the MASTpreProcessor class
	 */
	private void checkConsistent(String modelFilePath){
		MASTpreProcessor.init(thisClass);
		try {
			MASTpreProcessor.performProcess(modelFilePath); // CesarException
			if (MASTpreProcessor.numFoundIncidences() > 0) {
				messagesList.add(modelText.getText() + " is NOT WELL-FORMED", 0);
				MASTpreProcessor.report();
				setState(READY_TO_CHECK_MODEL_FILE);
			} else {
				messagesList.add(modelText.getText() + " is WELL FORMED", 0);
				setState(READY_TO_START_SIMULATION);
			}
		} catch (JSimMastException e1) { 
			//e1.printStackTrace();
			messagesList.add(e1.getMessage()); //createExceptionMessageBox(e1.getMessage());
			setState(READY_TO_CHECK_MODEL_FILE);
		}
	}
	
	/*
	 * This method parses the *.res.xml file (if generated) and fills 
	 * the 'Scheduling results' and 'Resources usages' tabs.
	 */
	private void parseResultsFile(String resultsFilePath) throws JSimMastException {
		
		Document domTree = XMLparser.parseXMLfile(resultsFilePath);
		Element root = domTree.getDocumentElement();
	
		e2eFlowResultsElemList = XMLparser.getChildElem(root, "End_To_End_Flow_Results");
		processingRsrcResultsElemList = XMLparser.getChildElem(root, "Computing_Resource_Results");
		processingRsrcResultsElemList.addAll(XMLparser.getChildElem(root, "Network_Results"));
		mutexResultsElemList = XMLparser.getChildElem(root, "Immediate_Ceiling_Mutex_Results");
		mutexResultsElemList.addAll(XMLparser.getChildElem(root, "SRP_Mutex_Results"));
		mutexResultsElemList.addAll(XMLparser.getChildElem(root, "Priority_Inheritance_Mutex_Results"));
		
		// I fill the transactions combo with <mast_res:End_To_End_Flow_Results>
		transesCombo.removeAll();		
		for (Element e2eFlowResultsElem : e2eFlowResultsElemList) {
			transesCombo.add(e2eFlowResultsElem.getAttribute("Name"));
		}
		transesCombo.select(0);
		transesCombo.notifyListeners(SWT.Selection, new Event());
	
		/*
		 * I fill the processing resources combo with <mast_res:Computing_Resource_Results> 
		 *                                          & <mast_res:Network_Results>
		 */
		processingRsrcsCombo.removeAll();
		for (Element processingRsrcResultsElem : processingRsrcResultsElemList) {
			processingRsrcsCombo.add(processingRsrcResultsElem.getAttribute("Name"));
		}
		processingRsrcsCombo.select(0);
		processingRsrcsCombo.notifyListeners(SWT.Selection, new Event());
	}
	
	/*
	 * This method parses the *.trc.xml file (if generated) and fills
	 * the 'Traces' tab.
	 */
	private void parseTracesFile(String tracesFilePath) throws JSimMastException {
		
		Document domTree = XMLparser.parseXMLfile(tracesFilePath);
		Element root = domTree.getDocumentElement();
		
		// <mast_trace:Msg_Type_List>
		Element msgTypeListElem = XMLparser.getChildElem(root, "Msg_Type_List").get(0);
		java.util.List<Element> msgTypeElemList = XMLparser.getChildElem(msgTypeListElem);
		Vector<String> msgTypeNameVec = new Vector<String>();
		int mID;
		String msgTypeName;
		for (Element msgTypeElem : msgTypeElemList) {
			mID = Integer.parseInt(msgTypeElem.getAttribute("Mid"));
			msgTypeName = msgTypeElem.getAttribute("Type");
			msgTypeNameVec.add(mID, msgTypeName);
		}
		
		// <mast_trace:Src_List>
		Element srcListElem = XMLparser.getChildElem(root, "Src_List").get(0);
		java.util.List<Element> srcElemList = XMLparser.getChildElem(srcListElem);
		Vector<String> srcNameVec = new Vector<String>();
		int sID;
		String srcName;
		for (Element srcElem : srcElemList) {
			sID = Integer.parseInt(srcElem.getAttribute("Sid"));
			srcName = srcElem.getAttribute("Name");
			srcNameVec.add(sID, srcName);
		}
		
		// <mast_trace:Msg_List>
		Element msgListElem = XMLparser.getChildElem(root, "Msg_List").get(0);
		java.util.List<Element> msgElemList = XMLparser.getChildElem(msgListElem);
		numTraces = msgElemList.size();
		
		// If parsing the 1st time (due to goOn() after pushing [Start] under TRACES), I fill 
		// the traces table with the last bunch of traces
		if (topTraceIndex == 0)
			topTraceIndex = numTraces - NUM_TRACES_ON_TABLE + 1; // numTraces = 100 => top = 100 - 15 + 1 = 86
		
		TableItem item;
		long tns;
		double t;
		for (int i = topTraceIndex; i < topTraceIndex + NUM_TRACES_ON_TABLE; i++) { // i = 86 .. 100
			tns = Long.parseLong(msgElemList.get(i-1).getAttribute("T"));
			t = convertFromNanosec(tns);
			sID = Integer.parseInt(msgElemList.get(i-1).getAttribute("S"));
			mID = Integer.parseInt(msgElemList.get(i-1).getAttribute("M"));
			item = new TableItem(tracesTable, SWT.NONE);
			item.setText(new String[]{"" + i, "" + t, srcNameVec.get(sID), msgTypeNameVec.get(mID)});
		}
	}
	
	
	//gadem
	private void goOn_no_gui()
	{
		
		String currentDir = System.getProperty("user.dir");
        //System.out.println("Current dir using System:" +currentDir);
		Controller controller = new Controller();

		FILES_PATH = currentDir + "/models_simulation/";
		controller.initController(FILES_PATH + modelName, Controller.ProfileType.valueOf(profile_combo), FILES_PATH + resultsName, FILES_PATH + tracesName);
				
		Thread executingThread = new Thread(new Runnable(){
			public void run() 
			{				
				//readEndConditions();
				//set the time and the transactions required
				finalTime *= 1000000000;
				Controller.execute_no_gui(finalTime, minNumTransactions);

						
				//refreshExecutionsTable();
				//****************************
				
			
				if (profile_combo.equals("TRACES")) 
				{
					Controller.generateTracesFile_no_gui();
				} 
				else if (profile_combo.equals("PERFORMANCE") || profile_combo.equals("SCHEDULABILITY")) 
				{
					Controller.generateResultsFile_no_gui();
					
				}

			}
			
		});
		executingThread.start();
		
		
	}
	
	
	
	/*
	 * This method executes the simulation and checks the selected profile to
	 * generate the appropriate files (*.res.xml or *.trc.xml). 
	 * Besides that, the generated files are parsed, performing the 
	 * corresponding effects on the tabs.
	 */
	private void goOn(){
		Thread executingThread = new Thread(new Runnable(){
			public void run() {				
				readEndConditions();
				Controller.execute(finalTime, minNumTransactions);
				Display.getDefault().asyncExec(new Runnable(){
					public void run() {
						
						refreshExecutionsTable();
						
						String profile = profileCombo.getText();
						if (profile.equals("TRACES")) {
							Controller.generateTracesFile();
							// Effects on 'Traces' tab
							tracesComposite.setEnabled(true);
							avTracesButton.setEnabled(false);
							tracesTable.removeAll();
							try {						
								topTraceIndex = 0;
								parseTracesFile(FILES_PATH + tracesText.getText());
							} catch (JSimMastException e1) {
								//e1.printStackTrace();
								messagesList.add(e1.getMessage(), 0); //createExceptionMessageBox(e1.getMessage());
							}
						} else if (profile.equals("PERFORMANCE") || profile.equals("SCHEDULABILITY")) {
							Controller.generateResultsFile();
							// Effects on 'Scheduling results' tab
							schedulingResultsComposite.setEnabled(true);
							resourcesUsagesComposite.setEnabled(true);
							try {													
								parseResultsFile(FILES_PATH + resultsText.getText());
							} catch (JSimMastException e1) {
								messagesList.add(e1.getMessage(), 0); //createExceptionMessageBox(e1.getMessage());
								e1.printStackTrace();
							}
						}

						simulationStopped();
						if (endButtonClicked){
							setState(READY_TO_START_SIMULATION);
							endButtonClicked = false;
						} else
							setState(READY_TO_RESUME_SIMULATION);
					}
				});
			}			
		});
		executingThread.start();
		
		/*if (profileCombo.getText().equals("TRACES")) {
			Controller.generateTracesFile();
			// Effects on 'Traces' tab
			tracesComposite.setEnabled(true);
			tracesTable.removeAll();
			try {						
				topTraceIndex = 0;
				parseTracesFile(FILES_PATH + trcText.getText());
			} catch (CesarException e1) {
				e1.printStackTrace();
				messagesList.add(e1.getMessage(), 0); //createExceptionMessageBox(e1.getMessage());
			}
		} else if (profileCombo.getText().equals("PERFORMANCE") || profileCombo.getText().equals("SCHEDULABILITY")) {
			Controller.generateResultsFile();
			// Effects on 'Scheduling results' and 'Resources usages' tab
			schedulingResultsComposite.setEnabled(true);
			resourcesUsagesComposite.setEnabled(true);
			try {													
				parseResultsFile(FILES_PATH + resText.getText());
			} catch (CesarException e1) {
				e1.printStackTrace();
				messagesList.add(e1.getMessage(), 0); //createExceptionMessageBox(e1.getMessage());
			}
		}*/
	}
	
	/*
	 * This method reads, in a synchronous fashion, the contents of 
	 * the 'maxExecTimeText' and the 'transMinNumText', storing them 
	 * in the 'finalTime' and 'minNumTransactions' fields. The 1st one
	 * is stored converted to ns.
	 */
	private void readEndConditions(){
		Display.getDefault().syncExec(new Runnable(){
			public void run() {
				finalTime = convertToNanosec(Double.parseDouble(maxExecTimeText.getText()));
				if (finalTime == 0)
					finalTime = Long.MAX_VALUE;
				minNumTransactions = Long.parseLong(transMinNumText.getText());
				if (minNumTransactions == 0)
					minNumTransactions = Long.MAX_VALUE;
				
			}
		});
	}
	
	/*
	 * This method... 
	 */
	private void refreshExecutionsTable(){
		currentTimeText.setText("" + convertFromNanosec(Clock.currentTime()));
		//currentStepText.setText("" + Controller.transExecution());
		executionsTable.removeAll();
		TableItem tableItem;
		Iterator<Statistical_EM> iter = Repository.timeMonitorListIter();
		Statistical_EM timeMonitor;
		while (iter.hasNext()) {
			timeMonitor = iter.next();
			tableItem = new TableItem(executionsTable, SWT.NONE);
			double worstRespTime = convertFromNanosec(Long.parseLong(timeMonitor.worstResponseTime()));
			double nominalDeadline = convertFromSec(Double.parseDouble(timeMonitor.nominalDeadline()));
			if (worstRespTime > nominalDeadline)
				tableItem.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_RED));
			else
				tableItem.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_BLACK));
			tableItem.setText(new String[]{timeMonitor.fullFepName(), 
					                       timeMonitor.eventNum(), 
					                       "" + worstRespTime,
					                       "" + nominalDeadline,
					                       timeMonitor.confidenceLevel()});
		}
	}
	
	/*
	 * This method converts the argument (whose units are specified
	 * by the 'timeUnitsCombo') to its value in nanosecs.
	 */
	private long convertToNanosec(double time){
		if (timeUnitsCombo.getText().equals("s"))
			return (long)(time * 1E9);
		else if (timeUnitsCombo.getText().equals("ms"))
			return (long)(time * 1E6);
		else if (timeUnitsCombo.getText().equals("us"))
			return (long)(time * 1E3);
		else // ns
			return (long)time;
	}
	
	/*
	 * This method converts the argument (whose units are ns) to 
	 * its value in units specified by the 'timeUnitsCombo'.
	 */
	private double convertFromNanosec(long nsTime){
		if (timeUnitsCombo.getText().equals("s"))
			return nsTime / 1.0E9;
		else if (timeUnitsCombo.getText().equals("ms"))
			return nsTime / 1.0E6;
		else if (timeUnitsCombo.getText().equals("us"))
			return nsTime / 1.0E3;
		else // ns
			return (double)nsTime;
	}
	
	/*
	 * This method converts the argument (whose units are secs) to 
	 * its value in units specified by the 'timeUnitsCombo'.
	 */
	private double convertFromSec(double secTime){
		if (timeUnitsCombo.getText().equals("ns"))
			return secTime * 1.0E9;
		else if (timeUnitsCombo.getText().equals("ms"))
			return secTime * 1.0E3;
		else if (timeUnitsCombo.getText().equals("us"))
			return secTime * 1.0E6;
		else // s
			return secTime;
	}
	
	/*
	 * This method adds, in an asynchronous fashion, the 'message' argument to
	 * the 'messagesList', putting it in the 1st position.
	 * If 'message' starts with '%', the 'executionsTable' is refreshed.
	 */
	public void processMessage(final String message) {
		Display.getDefault().asyncExec(new Runnable(){
			public void run() {
				messagesList.add(message, 0);
				if (message.startsWith("%")) {
					refreshExecutionsTable();
				}
			}
		});
	}
	
	public void addMessage(String message){
		messagesList.add(message, 0);
	}

}

