package timers;

import module.ETG;
import module.Tools;
import module.WABETG;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import resource.RegularProcessor;

public abstract class Timer extends TimingObject {

	/* ATTRIBUTES */
	
	// Regular processor owner of the timer 
	protected RegularProcessor owner;
		public RegularProcessor owner(){return owner;}
		public void setOwner(RegularProcessor owner){this.owner = owner;}
	
	// This is the overhead of the timer interrupt, which is assumed
	// to execute at a priority level higher than the highest interrupt
	// priority.
	private ETG overhead;
		public ETG overhead(){return overhead;}
		public void setOverhead(ETG overhead){this.overhead = overhead;}
	
	private boolean isLocallySynchronized;
		public boolean isLocallySynchronized(){return isLocallySynchronized;}
		
		
	/* METHODS */
	  
	// Constructor
	protected Timer(Node node) {
		
		super(node);
		
		Element elem = (Element)node;
		
		long max = 0;
		if (elem.hasAttribute("Max_Overhead"))
			max = Tools.readLong(elem.getAttribute("Max_Overhead"));		    
	    long avg = 0;
	    if (elem.hasAttribute("Avg_Overhead"))
	    	avg = Tools.readLong(elem.getAttribute("Avg_Overhead"));
		long min = 0;
		if (elem.hasAttribute("Min_Overhead"))
			min = Tools.readLong(elem.getAttribute("Min_Overhead"));
			
	    if (max != 0 || avg != 0 || min != 0)
	    	overhead = new WABETG(max, avg, min);
	    else 
	    	overhead = null;
	    
	    if (elem.hasAttribute("Is_Locally_Synchronized"))
			isLocallySynchronized = elem.getAttribute("Is_Locally_Synchronized").equals("YES");
	}
	
	public void denormalize() {
		if (overhead != null)
			overhead.denormalize(owner.speedFactor());
	}


	@Override
	public String toString(){

		String string = super.toString() + 
		                "\nOverhead:";
		if (overhead != null)
			string += overhead.toString().replaceAll("\n", "\n\t");
		
		string += "\nOwner: ";
		if (owner != null)
			string += owner.name();
		
		return string;
	}
}
