package timers;

import org.w3c.dom.Node;

public class ClockSynchronizationObject extends TimingObject {
	
	/* FIELDS */
	
	private long localOffset;
		public long localOffset() {return localOffset;}
	
	
	/* METHODS */
	  
	// Constructor
	protected ClockSynchronizationObject(Node node) {
		super(node);
		localOffset = precision;
	}
	
	public void denormalize() {}

	
	
	@Override
	public String toString(){
		String string = "\nPrecision = " + precision;
		return "\n\nSynchronization timer:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}
