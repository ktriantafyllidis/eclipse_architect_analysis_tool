/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Interface: Ticker
 * Description: This represents a system that has a periodic ticker,
 *     i.e., a periodic interrupt that arrives at the system. When
 *      this interrupt arrives, all timed events whose expiration time
 *      has already passed, are activated. Other non timed events are
 *      handled at the time they are generated. In this model, the
 *      overhead by the timer interrupt is localized in a single
 *      periodic interrupt, but jitter is introduced in all timed events,
 *      because the best resolution is the ticker period. 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package timers;

import module.*;
import flow.*;
import org.w3c.dom.*;


import core.*;


import java.util.*;

public class Ticker extends Timer implements Timed {

	/* ATTRIBUTES */
	
	// Period of the ticker interrupt.
	private long period = Long.MAX_VALUE;
  
	// Element controlled by system clock
	private LinkedList<FlowEndPoint> systemTimedList = new LinkedList<FlowEndPoint>();
  
  
	/* METHODS */
	
	// Constructor
	public Ticker(Node node) {
		super(node);
		Element elem = (Element)node;
		if (elem.hasAttribute("Period"))
			period = Tools.readLong(elem.getAttribute("Period"));
	    
		//Clock.addTimedEvent(period, this); //ahora va al complete
	}
  
  
	// Adds a new system timed client
	public void addListener(FlowEndPoint client) {
		systemTimedList.add(client);
	}
  
	@Override
	public void complete() {
		Clock.addTimedEvent(period + owner.localOffset(), this);
	}
	
	// Is invoked for Clock when the activation period is elapsed
	public void update(){
		owner.timerRequire(this);
		Clock.addTimedEvent(period, this);
	}
  
	// Is invoked for te processor when the timer service execution is finished
	public void finish(){
		Iterator<FlowEndPoint> iter=systemTimedList.iterator();
		while(iter.hasNext())iter.next().tick();
		systemTimedList.clear();
	}
  
  
	
	@Override
	public String toString(){
		String string = "\nPeriod = " + period;
		return "\n\nTicker:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
	
}

