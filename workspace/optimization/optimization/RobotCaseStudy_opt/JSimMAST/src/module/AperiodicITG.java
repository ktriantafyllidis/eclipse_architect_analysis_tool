/*********************************************************************
 *                        Simulador JSimMast
 *                      Compatible con MAST 3.0
 * Computadores y Tiempo Real group. Universidad de Cantabria (Spain)
 *
 * Description: Genera eventos espaciados aperiodicamente de acuerdo al
 *   tipo de distribuci�n y datos establecidos en "param".
 *
 * Autors: Patricia L�pez Mart�nez [lopezpa@unican.es]
 *         Jos� M. Drake Moyano [drakej@unican.es]
 * Version: 1/1/09
 ********************************************************************/

package module;

public class AperiodicITG extends ITG {
	
	/* FIELDS */
	
	protected DistributionParameter data;
	
	
	/* METHODS */
	
	// Constructor
	public AperiodicITG(DistributionParameter data, long firstTime){
		super(firstTime);
		this.data = data;
	}
	
	public long next() {
		if (firstIsGenerated){
			firstIsGenerated=true;
			return firstTime;
		}else{
			return RandomGenerator.interarrivalTime(data);
		}
	}
	
	
	
	@Override
	public String toString(){
		String string = "\nData: " + data.toString();
		return "\n\tPattern: aperiodic" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}
