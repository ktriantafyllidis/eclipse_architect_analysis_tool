/*********************************************************************
 *                        Simulador JSimMast
 *                      Compatible con MAST 3.0
 * Computadores y Tiempo Real group. Universidad de Cantabria (Spain)
 *
 * Description: Contiene la lista de coeficientes que definen una 
 *    distribuci�n hyperexponencial.
 *
 * Autors: Patricia L�pez Martinez [lopezpa@unican.es]
 *         Jos� M. Drake Moyano [drakej@unican.es]
 * Version: 1-1-09
 ********************************************************************/
package module;
import java.util.*;
public class HyperExpCoefList {
   List<Coef> hyperExpCoefList= new ArrayList<Coef>();

   public void add(double weight,double expCoef){
	   hyperExpCoefList.add(new Coef(weight,expCoef,0.0,0.0));
   }
   
   public int order(){
	   return hyperExpCoefList.size();
   }
   
   public double mean(){
	   Coef hec;
	   double acum=0;
	   for (int n=0;n<order();n++){
		   hec=hyperExpCoefList.get(n);
		   acum+=hec.weight/hec.value;
	   }
	   return acum;
   }
   public void normalize(){
	   double acum=0;
	   double lim=0;
	   Coef hec;
	   
	   for (int n=0;n<order();n++){
		   acum+= hyperExpCoefList.get(n).weight;
	   }
	   for (int n=0;n<order();n++){
		   hec=hyperExpCoefList.get(n);
		   hec.weight/=acum;
		   hec.limInf=lim;
		   lim+=hec.weight;
		   hec.limSup=lim;
		   hyperExpCoefList.set(n, hec);
	   }
   }
   public Coef get(int n){
	   return hyperExpCoefList.get(n);
   }
}
