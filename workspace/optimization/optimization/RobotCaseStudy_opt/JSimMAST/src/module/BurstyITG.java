/*********************************************************************
 *                        Simulador JSimMast
 *                      Compatible con MAST 3.0
 * Computadores y Tiempo Real group. Universidad de Cantabria (Spain)
 *
 * Description: Genera eventos espaciados en r�fagas de acuerdo al
 *   tipo de distribuci�n y datos establecidos en "param".
 *
 * Autors: Patricia L�pez Mart�nez [lopezpa@unican.es]
 *         Jos� M. Drake Moyano [drakej@unican.es]
 * Version: 1/1/09
 ********************************************************************/

package module;

public class BurstyITG extends AperiodicITG {

	/* FIELDS */
	
    // Ventana temporal dentro de la que la generaci�n de eventos est� limitada.
    final long boundInterval;
    
    // N�mero m�ximo de eventos permitidos dentro de la ventana temporal boundInterval.
    final int maxArrivals;
    
    // Variable interna que representa en n�mero de eventos generados en el intervalo actual.
    int eventInInterval;
    
    // Time in the current bursty interval.
    long timeInInterval;
	
    // It represents the currents interarrival time. 
    long itg;
	
    
    /* METHODS */
    
    // Constructor
	public BurstyITG(DistributionParameter param, long boundInterval, int maxArrival, long firstTime){
		super(param, firstTime);
		this.boundInterval = boundInterval;
		this.maxArrivals = maxArrival;
		this.timeInInterval = 0;
		this.eventInInterval = 1;
	}
	
	public long next(){
		if (firstIsGenerated){
			firstIsGenerated=true;
			return firstTime;
		}else{
			itg=RandomGenerator.interarrivalTime(data);
			if (eventInInterval>=maxArrivals){
				if (itg<(boundInterval-timeInInterval)){
					itg=boundInterval-timeInInterval;
				}
				eventInInterval=1;
				timeInInterval=0;
			}else{
				eventInInterval+=1;
				timeInInterval=+itg;
			}
		}
		return itg;
	}
	
	
	
	@Override
	public String toString(){
		String string = "\nBound interval = " + boundInterval +
		                "\nMax arrivals = " + maxArrivals;
		return "\n\tPattern: bursty" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}
