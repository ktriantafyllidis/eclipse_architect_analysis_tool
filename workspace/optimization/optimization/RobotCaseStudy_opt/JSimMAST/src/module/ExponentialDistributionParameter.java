/*********************************************************************
 *                        Simulador JSimMast
 *                      Compatible con MAST 3.0
 * Computadores y Tiempo Real group. Universidad de Cantabria (Spain)
 *
 * Description: parametros de una distribuci�n exponencial
 *
 * Autors: Patricia L�pez Mart�nez [lopezpa@unican.es]
 *         Jos� M. Drake Moyano [drakej@unican.es]
 * Version: 1/1/09
 ********************************************************************/
package module;

public class ExponentialDistributionParameter extends DistributionParameter {
	public long mean;
	public ExponentialDistributionParameter(long mean){this.mean=mean;};
}
