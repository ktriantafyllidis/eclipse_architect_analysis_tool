/*********************************************************************
 *                        Simulador JSimMast
 *                      Compatible con MAST 3.0
 * Computadores y Tiempo Real group. Universidad de Cantabria (Spain)
 *
 * Description: Parametros correspondientes a una distribuci�n uniforme
 *   en el intervalo 0 a 2*mean.
 * 
 * Autors: Patricia L�pez Mart�nez [lopezpa@unican.es]
 *         Jos� M. Drake Moyano [drakej@unican.es]
 * Version: 1/1/09
 ********************************************************************/
package module;

public class UniformDistributionParameter extends DistributionParameter {
	public long mean;
	
	public UniformDistributionParameter(long mean){this.mean=mean;};

}
