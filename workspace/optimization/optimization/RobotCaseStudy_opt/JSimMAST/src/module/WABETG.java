/*********************************************************************
 *                        Simulador JSimMast
 *                      Compatible con MAST 3.0
 * Computadores y Tiempo Real group. Universidad de Cantabria (Spain)
 *
 * Description: Evalua un tiempo de ejecuci�n basandose en los tiempos
 *    de "wcet", "acet" y "bcet"
 *
 * Autors: Patricia L�pez Mart�nez [lopezpa@unican.es]
 *         Jos� M. Drake Moyano [drakej@unican.es]
 * Version: 1/1/09
 ********************************************************************/

package module;

public class WABETG extends ETG {
	
	/* FIELDS */
	
	// Worst execution time
    public long wcet = 1;
    
    // Average execution time
    public long acet = 1; 
    
    // Best execution time
    public long bcet = 1;
    
    
    /* METHODS */
    
    // Constructor
    public WABETG(long wcet, long acet, long bcet){
    	if (wcet != 0)
    		this.wcet = wcet;
    	if (acet != 0)
			this.acet = acet;
    	if (bcet != 0)
    		this.bcet = bcet;
    }
    
    public long next(){
    	if (wcet == bcet)
    		return wcet;
    	else if (RandomGenerator.normalizedUniformValue() < 0.5)
    		return (long)RandomGenerator.uniformValue(bcet, acet);
    	else
    		return (long)RandomGenerator.uniformValue(acet, wcet);
  
    }
    
	public void denormalize(double factor){
		wcet = (long)((double)wcet/factor);
		acet = (long)((double)acet/factor);
		bcet = (long)((double)bcet/factor);		
	}
	
	
	
	
	@Override
	public String toString(){
		return "\nExecution times:" + 
		       "\n\tworst = " + wcet + 
		       "\n\tavg = " + acet + 
		       "\n\tbest = " + bcet;   
	}

	@Override
	public ETG clone() {
		return new WABETG(wcet, acet, bcet);
	}
	
}