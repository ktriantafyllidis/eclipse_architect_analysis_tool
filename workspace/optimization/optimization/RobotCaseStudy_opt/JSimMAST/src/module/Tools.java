/*********************************************************************
 *                        Simulador JSimMast
 *                      Compatible con MAST 3.0
 * Computadores y Tiempo Real group. Universidad de Cantabria (Spain)
 *
 * Description: Set of general purpose methods.
 *
 * Autors: Patricia L�pez Mart�nez [lopezpa@unican.es]
 *         Jos� M. Drake Moyano [drakej@unican.es]
 * Version: 1/1/09
 ********************************************************************/
package module;

import java.util.*;

public class Tools {
	// Private operation: Convert a string with a list os identifier to a identifier list
	public static ArrayList<String> parses(String serie){
		ArrayList<String> list=new ArrayList<String>();
		serie=serie.replaceAll("/n", " ");
		serie=serie.replaceAll("/t", " ");
		while(serie.contains("  ")) {serie=serie.replaceAll("  ", " ");};
		while (serie.contains(" ")){
			list.add(serie.substring(0, serie.indexOf(" ")));
			serie=serie.substring(serie.indexOf(" ")+1);
		}
		if (!serie.equals(""))list.add(serie);
		return list;
	}
	
	public static long readLong(String s){
		return (long)(Double.parseDouble(s)*1.0E9);
	}
	
	static public String delimit(Long n){
	    	n=n/1000;
	    	String str="";
	    	String s;
	    	for(int i=0;i<3;i++){
	    		s= Long.toString(n%1000)+".";
	    		if (s.length()==3){s="0"+s;
	    		}else if(s.length()==2){s="00"+s;
	    		}
	    		str=s+str;
	    		n=n/1000;
	    	}
	    	return str;
	    }	

	public static String prettyPrint(String s, int numDecimals){

		if (s.equals(""))
			return s;
		
		int aux = 1;
		if (s.startsWith("-"))
			aux++;
		
		String out = "";
		if (s.contains("E")) {
			int indexE = s.indexOf("E");
			if (indexE > 2 + numDecimals)
				out = s.substring(0, 2 + numDecimals) + s.substring(indexE);
			else 
				out = s;
		} else{
			int indexDot = s.indexOf(".");
			String auxStr = "";
			for (int i = 0; i < s.length(); i++) {
				if (s.charAt(i) != '.' && auxStr.length() < numDecimals + aux) {
					auxStr += s.charAt(i);
				}
			}
			out = auxStr.substring(0, aux) + "." + auxStr.substring(aux);
			
			if (out.length() > 3) {
				while(out.endsWith("0")){
					out = out.substring(0, out.length() - 1);
				}
			}
			if (indexDot != 1)
				out += "E" + (indexDot - aux);
		}
		return out;
	}
	
	public String prettyPrint(double num, int numDecimals){
		String s = Double.toString(num);
		return prettyPrint(s, numDecimals);
	}

}
