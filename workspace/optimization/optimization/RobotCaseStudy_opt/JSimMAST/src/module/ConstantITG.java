/*********************************************************************
 *                        Simulador JSimMast
 *                      Compatible con MAST 3.0
 * Computadores y Tiempo Real group. Universidad de Cantabria (Spain)
 *
 * Description: Genera eventos espaciados periodicamente en "mean" a 
 * partir del "firstTime".
 *
 * Autors: Patricia L�pez Mart�nez [lopezpa@unican.es]
 *         Jos� M. Drake Moyano [drakej@unican.es]
 * Version: 1/1/09
 ********************************************************************/
package module;

public class ConstantITG extends ITG {
 
	/* FIELDS */
	
	private long mean;
	
	private long maxJitter;
	
	
	/* METHODS */
	
	// Constructor
	public ConstantITG(long firstTime, long mean, long maxJitter){
		super(firstTime);
		this.mean = mean;
		this.maxJitter = maxJitter;
	}
	
	public long next() {
		if (!firstIsGenerated){
			firstIsGenerated=true;
			return firstTime;
		}else{
//			lastTime+=mean;
//			return lastTime;
			return mean + (long)((double)maxJitter * RandomGenerator.uniformValue(-0.5, 0.5));
		}
	}


	
	@Override
	public String toString(){
		String string = "\nMean = " + mean +
		                "\nMax jitter = " + maxJitter;
		return "\n\tPattern: constant" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}
