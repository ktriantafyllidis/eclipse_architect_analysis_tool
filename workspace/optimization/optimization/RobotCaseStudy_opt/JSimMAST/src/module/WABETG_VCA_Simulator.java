package module;

public class WABETG_VCA_Simulator extends ETG{

/* FIELDS */
	
	// Worst execution time
    public long wcet = 1;
    
    // Average execution time
    public long acet = 1; 
    
    // Best execution time
    public long bcet = 1;
    
    
    /* METHODS */
    
    // Constructor
    public WABETG_VCA_Simulator(long wcet, long acet, long bcet){
    	if (wcet != 0)
    		this.wcet = wcet;
    	if (acet != 0)
			this.acet = acet;
    	if (bcet != 0)
    		this.bcet = bcet;
    }
    
    public long next(){
    	if (wcet == bcet)
    		return wcet;
    	else if (RandomGenerator.normalizedUniformValue() < 0.5)
    		return (long)RandomGenerator.uniformValue(bcet, acet);
    	else
    		return (long)RandomGenerator.uniformValue(acet, wcet);
  
    }
    
	public void denormalize(double factor){
		wcet = (long)((double)wcet);
		acet = (long)((double)acet);
		bcet = (long)((double)bcet);		
	}
	
	
	
	
	@Override
	public String toString(){
		return "\nExecution times:" + 
		       "\n\tworst = " + wcet + 
		       "\n\tavg = " + acet + 
		       "\n\tbest = " + bcet;   
	}

	@Override
	public ETG clone() {
		return new WABETG_VCA_Simulator(wcet, acet, bcet);
	}
}
