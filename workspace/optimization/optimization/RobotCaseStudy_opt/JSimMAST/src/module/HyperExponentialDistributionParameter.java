/*********************************************************************
 *                        Simulador JSimMast
 *                      Compatible con MAST 3.0
 * Computadores y Tiempo Real group. Universidad de Cantabria (Spain)
 *
 * Description: Define los parámetros de una distribución 
 *    hiperexponencial.(Todos los
 *   parámetros de la distribución se expresan en nanosegundos)
 *
 * Autors: Patricia López Martínez [lopezpa@unican.es]
 *         José M. Drake Moyano [drakej@unican.es]
 * Version: 1/1/09
 *********************************************************************/
package module;

public class HyperExponentialDistributionParameter extends
		DistributionParameter {
    public HyperExpCoefList data;
}
