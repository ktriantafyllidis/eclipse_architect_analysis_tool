/*********************************************************************
 *                        Simulador JSimMast
 *                      Compatible con MAST 3.0
 * Computadores y Tiempo Real group. Universidad de Cantabria (Spain)
 *
 * Description: Contiene una lista de valores para implementar las
 *    distribuciones discretas.
 *
 * Autors: Patricia L�pez Mart�nez [lopezpa@unican.es]
 *         Jos� M. Drake Moyano [drakej@unican.es]
 * Version: 1-1-09
 ********************************************************************/
package module;

import java.util.*;

public class DiscreteValueList {
	List<Coef> discreteValueList= new ArrayList<Coef>();

	public void add(double weight,double expCoef){
		discreteValueList.add(new Coef(weight,expCoef,0.0,0.0));
	}

	public int order(){
		   return discreteValueList.size();
	}

	public double mean(){
		   Coef value;
		   double acum=0;
		   for (int n=0;n<order();n++){
			   value=discreteValueList.get(n);
			   acum+=value.weight*value.value;
		   }
		   return acum;
	}
	public void normalize(){
		   double acum=0;
		   double lim=0;
		   Coef value;
		   
		   for (int n=0;n<order();n++){
			   acum+= discreteValueList.get(n).weight;
		   }
		   for (int n=0;n<order();n++){
			   value=discreteValueList.get(n);
			   value.weight/=acum;
			   value.limInf=lim;
			   lim+=value.weight;
			   value.limSup=lim;
			   discreteValueList.set(n, value);
		   }
	}
	public Coef get(int n){
		   return discreteValueList.get(n);
	}
}
