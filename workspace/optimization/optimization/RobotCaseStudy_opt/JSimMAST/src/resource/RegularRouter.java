package resource;

import org.w3c.dom.Node;

public class RegularRouter extends NetworkRouter {

	/* METHODS */
	
	// Constructor
	public RegularRouter(Node node){		
		super(node);
	}




	@Override
	public String toString(){
		return "\n\nRegular router: " + super.toString().replaceAll("\n", "\n\t");
	}
}
