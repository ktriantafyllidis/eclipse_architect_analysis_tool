/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: mmediateCeilingResource
 * Description: Uses the basic priority inheritance protocol.
 * 
 * Author: J.M. Drake, D. Garc�a
 * Date: 1-1-09
 ********************************************************************/

package resource;

import java.util.Iterator;
import java.util.PriorityQueue;

import org.w3c.dom.*;

public class PriorityInheritanceMutex extends MutualExclusionResource {

	/* METHODS */
  
	// Constructor
	public PriorityInheritanceMutex(Node node){
		super(node);
		waitForLockList = new PriorityQueue<SimThread>();
	}
	
	// A Processing resource require the resource for a scheduling server **
	@Override
	public void lock(SimThread server){
		
		super.lock(server);
	  
		// If is required, the currente priority of the owner scheduling server is updated
	  
		if(owner != null){
			if (owner.executingSeg().currentEvent().flowPriority() < waitForLockList.peek().executingSeg().currentEvent().flowPriority())
				owner.executingSeg().currentEvent().setFlowPriority(waitForLockList.peek().executingSeg().currentEvent().flowPriority());
		}
  	}

	@Override
	public String results(String offset) {
		String string = "";
		String sup = super.results(offset);
		if (!sup.equals(""))
			string = offset + "<mast_res:Priority_Inheritance_Mutex_Results Name=\"" + computingResourceName + "/" + name + "\">\n" + 
		       		 sup + 
		       		 offset + "</mast_res:Priority_Inheritance_Mutex_Results>\n\n";
		return string;
	}
	
	public void decide() {
		
		// I get the 1st server from the awaiting queue, removing it
		SimThread server = waitForLockList.remove();
		
		setOwner(server);
		setCurrentState(ASSIGNED);
		server.assignResource();
		
		// I block the servers waiting to lock the mutex
		Iterator<SimThread> iter = waitForLockList.iterator();
		while(iter.hasNext()){
			iter.next().block();
		}
  	}

	
	@Override
	public String toString(){
		return "\n\nPriority inheritance mutex:" + super.toString().replaceAll("\n", "\n\t");
	}
}
