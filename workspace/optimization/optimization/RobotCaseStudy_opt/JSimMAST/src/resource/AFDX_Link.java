package resource;

import module.ETG;
import module.Tools;
import module.WABETG;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class AFDX_Link extends Network{

	/* CONSTANTS */
	
	//private final int ETHERNET_OVERHEAD;
	//private final int PROTOCOL_OVERHEAD;
	
	
	/* FIELDS */
	
	private long maxPacketTransmissionTime; // ns
	private long minPacketTransmissionTime; // ns
		public long maxPacketTransmissionTime() {return maxPacketTransmissionTime;}
		public long minPacketTransmissionTime() {return minPacketTransmissionTime;}
		
	private ETG hwTxLatency;
	private ETG hwRxLatency;
	
	
	/* METHODS */
	
	// Constructor
	public AFDX_Link(Node node){
		
		super(node);
		
		Element elem = (Element)node;
		
		if (elem.hasAttribute("Max_Packet_Size")) {
			long maxPacketSize = Long.parseLong(elem.getAttribute("Max_Packet_Size"));
			maxPacketTransmissionTime =(long)(((double)maxPacketSize / throughput / speedFactor) * 1.0E9);
	    }
		if (elem.hasAttribute("Min_Packet_Size")) {
			long minPacketSize = Long.parseLong(elem.getAttribute("Min_Packet_Size"));
			minPacketTransmissionTime = (long)(((double)minPacketSize / throughput / speedFactor) * 1.0E9);
	    }
		
		long max = 0;
		if (elem.hasAttribute("Max_HW_Tx_Latency"))
			max = Tools.readLong(elem.getAttribute("Max_HW_Tx_Latency"));
		long avg = 0;
		if (elem.hasAttribute("Avg_HW_Tx_Latency"))
			avg = Tools.readLong(elem.getAttribute("Avg_HW_Tx_Latency"));
		long min = 0;
		if (elem.hasAttribute("Min_HW_Tx_Latency"))
			min = Tools.readLong(elem.getAttribute("Min_HW_Tx_Latency"));

		hwTxLatency = new WABETG(max, avg, min);
		
		max = 0;
		if (elem.hasAttribute("Max_HW_Rx_Latency"))
			max = Tools.readLong(elem.getAttribute("Max_HW_Rx_Latency"));
		avg = 0;
		if (elem.hasAttribute("Avg_HW_Rx_Latency"))
			avg = Tools.readLong(elem.getAttribute("Avg_HW_Rx_Latency"));
		min = 0;
		if (elem.hasAttribute("Min_HW_Rx_Latency"))
			min = Tools.readLong(elem.getAttribute("Min_HW_Rx_Latency"));
		hwRxLatency = new WABETG(max, avg, min);
		
		/*
		<xs:attribute name="Ethernet_Overhead" type="mast_mdl:Bit_Count" fixed="180"/>
		<xs:attribute name="Protocol_Overhead" type="mast_mdl:Bit_Count" fixed="376"/>
		*/
	}
	
	public String stateName(int state) {return null;}
	
	public String results(String offset) {return null;}
	
	public void decide() {}
	
	public void require(SimThread schedulingServer) {}

	@Override
	public String toString(){

		String string = "\nMax packet transmission time = " + maxPacketTransmissionTime + 
		                "\nMin packet transmission time = " + minPacketTransmissionTime + 
		                "\nHW Tx latency = " + hwTxLatency.toString() + 
		                "\nHW Rx latency = " + hwRxLatency.toString();
		
		return "\n\nAFDX link:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}
