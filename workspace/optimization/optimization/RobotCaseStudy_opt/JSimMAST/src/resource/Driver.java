package resource;

import org.w3c.dom.Node;

public abstract class Driver {
	
	/* FIELDS */
	
	protected String name;
	
	protected Node node;
	
	protected ComputingResource hostProc;
		public ComputingResource hostProc() {return hostProc;}
	
	protected Network ownerNet;
	
	
	/* METHODS */
	
	// Constructor
	protected Driver(Node node, Network network){
		this.node = node;
		ownerNet = network;
		name = "Driver_" + ownerNet.name() + "_in_"; // the name of the current driver is partially built right now
	}
	
	public static Driver newDriver(Node node, Network network) {
		if (node.getLocalName().equals("Packet_Driver"))
			return new PacketDriver(node, network);
		else if (node.getLocalName().equals("Character_Packet_Driver"))
			return new CharacterPacketDriver(node, network);
		else // "RTEP_Packet_Driver"
			return new RTEP_PacketDriver(node, network);
	}
	
	public abstract void complete();

	
	
	
	@Override
	public String toString(){
		return "\nName: " + name + 
		       "\nOwner network: " + ownerNet.name() + 
		       "\nHost processor: " + hostProc.name();
	}
}
