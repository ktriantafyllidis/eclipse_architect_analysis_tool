package resource;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class PacketBasedNetwork_VCA_Simulator extends PacketBasedNetwork{
	
	/* Description of the amount of data included in a packet, excluding any
	 * protocol information. 
	 * The maximum size is used in the calculation
	 * of the number of packets into which a large message is split, calculated
	 * as the ceiling of the message size divided by the maximum packet size. */
	private long maxPacketTransmissionTime = Long.MAX_VALUE; // ns
	private long minPacketTransmissionTime; // ns
	public long maxPacketTransmissionTime() {return maxPacketTransmissionTime;}
	public long minPacketTransmissionTime() {return minPacketTransmissionTime;}

	// Constructor 
		public PacketBasedNetwork_VCA_Simulator(Node node){
			
			super(node);
			
			Element elem = (Element)node;

			/*
			<xs:attribute name="Transmission_Kind" type="mast_mdl:Transmission_Kind" use="optional"/>
			<xs:attribute name="Max_Blocking" type="mast_mdl:Normalized_Execution_Time" use="optional"/>
			*/
			
			//this is 100 ms since that the number represents the ns time unit
			maxPacketTransmissionTime = 100000000;
			minPacketTransmissionTime = 0;

		}
	
	
}
