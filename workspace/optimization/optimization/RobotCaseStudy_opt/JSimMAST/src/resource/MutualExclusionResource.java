/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: SharedResource
 * Description: Abstract class root of the differente shared resource types 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package resource;

import org.w3c.dom.*;

import statemonitor.DisplayChangeStateMonitor;
import statemonitor.LogOnState;
import statemonitor.PercentOnState;
import statemonitor.StateMonitor;

import java.util.*;

import core.*;
import core.Controller.ProfileType;

public abstract class MutualExclusionResource extends Resource {

	/* CONSTANTS */
	
	// States
	private static final int FREE = 0;
	protected static final int ASSIGNED = 1;
	private static final int DECIDING = 2;
	
	
    /* ATTRIBUTES */
	
	private static List<String> stateNames;
	
	// List of scheduling server awaiting to lock the resource. 
	// It has not include the owner scheduling server.
	protected PriorityQueue<SimThread> waitForLockList;
		public PriorityQueue<SimThread> waitForLockList() {return waitForLockList;}
		public void setWaitForLockList(PriorityQueue<SimThread> theList) {waitForLockList = theList;}
	
	// Reference the scheduling serves that is the owner on the resource.
	protected SimThread owner; 
		public SimThread owner(){return owner;}
		public void setOwner(SimThread owner){
			this.owner = owner;
			if (computingResourceName == null)
				computingResourceName = owner.getProcessingResource().name();
		}
	
	protected String computingResourceName;
		
		
	/* METHODS */
	  	
	// Static contructor
	static {
		stateNames = new ArrayList<String>();
		stateNames.add(FREE, "FREE");
		stateNames.add(ASSIGNED, "ASSIGNED");
		stateNames.add(DECIDING, "DECIDING");
	}
	  
	// Constructor
	protected MutualExclusionResource(Node node){
		
		super(node);
		
		// I register a source in the Logger
		if (Controller.profile() == ProfileType.TRACES)
			sID = Logger.registerNewSource(name, Logger.SrcType.MutualExclusionResource);
	}
	  
	public static MutualExclusionResource newMutualExclusionResource(Node node){
		String localName = node.getLocalName();
		if (localName.equals("Immediate_Ceiling_Mutex"))
	  		return new ImmediateCeilingMutex(node);
		else if (localName.equals("Priority_Inheritance_Mutex"))
  			return new PriorityInheritanceMutex(node);
		else // localName.equals("SRP_Mutex")
  			return new SRP_Mutex(node);
	}
	
	public void complete(){
		addMonitors();
	}
	
	public void denormalize(){}
	
	public String stateName(int state){return stateNames.get(state);}
	
	/**
	 * This method returns the results generated in the simulation execution
	 * @return
	 */
	public String results(String offset) {

		String string = "";
		
		/*
		 *  I write the utilization results (<mast_res:Utilization_Results>)
		 *  The generation source of utilization results is 
		 *  the PercentOnState aggregated to the current MutualExclusionResource
		 *  under PERFORMANCE simulation profile.
		 *  I look for that PercentOnState.
		 */
		PercentOnState monitor = null;
		for (StateMonitor stateMonitor : stateMonitorList) {
			if (stateMonitor instanceof PercentOnState) {
				monitor = (PercentOnState)stateMonitor;
				break;
			}
		}
		if (monitor != null)
			string = offset + "\t<mast_res:Utilization_Results Total=\"" + monitor.utilizationResults(ASSIGNED) + "\"/>\n";
		
		return string;
	}
	
	/**
	 * A Processing resource requires the resource for a scheduling server
	 * @param server
	 */
	public void lock(SimThread server){
		waitForLockList.add(server);
		if(currentState != ASSIGNED) {  				
			setCurrentState(DECIDING); // for later assigning the resource of for later notifying the blocking state
			Controller.addDecisor(this);
		} else
			server.block();
	}

	/**
	 * A processing resource free the resource 
	 */
	public void unlock(){
		
		if(!waitForLockList.isEmpty()) {
			setCurrentState(DECIDING);
			Controller.addDecisor(this);
		} else
			setCurrentState(FREE);
		
		// I update the priority of the after unlock the shared resource
		owner.updatePriority();
		
		owner = null; // there isn't owner now
	}
 
	

	private void addMonitors(){
		switch (Controller.profile()) {
		case VERBOSE:
			stateMonitorList.add(new DisplayChangeStateMonitor(this));
			break;
		case TRACES:
			stateMonitorList.add(new LogOnState(this, FREE, Logger.MUTEX_FREE));
			stateMonitorList.add(new LogOnState(this, ASSIGNED, Logger.MUTEX_ASSIGNED));
			break;
		case PERFORMANCE:
			stateMonitorList.add(new PercentOnState(this));
			break;
		default: // SCHEDULABILITY
			break;
		}
	}

	
	@Override
	public String toString(){
		return super.toString() + 
		       "\nOwner: no owner at first.";
	}
	
}