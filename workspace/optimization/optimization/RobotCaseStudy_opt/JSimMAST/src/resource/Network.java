/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: Network
 * Description: Abstract class root of the processing resources that
 *    send messages as activity.
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package resource;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import utilityClasses.XMLparser;
import core.Controller;
import core.Logger;
import core.Controller.ProfileType;

public abstract class Network extends ProcessingResource {
	
	/* ATTRIBUTES */
		
	// Normalized network bandwidth in bits per time unit. The actual value is affected by the speed factor.
	protected double throughput;
		public double throughput() {return throughput;};
	
	//
	protected List<Driver> driverList;
		
	
	/* METHODS */
		
	// Constructor
	protected Network(Node node) {
		
		super(node);
		
		// I register a source in the Logger
		if (Controller.profile() == ProfileType.TRACES)
			sID = Logger.registerNewSource(name, Logger.SrcType.Network);
		
		Element elem = (Element)node;
		
		if (elem.hasAttribute("Throughput"))
			throughput = Double.parseDouble(elem.getAttribute("Throughput"));
		
		// The speedFactor of a network is affected by the throughput
		//speedFactor = speedFactor * throughput;
		
		List<Element> driverElemList = XMLparser.getChildElem(elem);
		for (Element driverElem : driverElemList) {
			if (driverList == null) // for the 1st one
				driverList = new ArrayList<Driver>(1);
			driverList.add(Driver.newDriver(driverElem, this));
		}
	}
	
	public static Network newNetwork(Node node) {
		String localName = node.getLocalName();
		if (localName.equals("Packet_Based_Network"))
	  		return new PacketBasedNetwork(node);
		else if (localName.equals("Packet_Based_Network_VCA_Simulator"))
			return new PacketBasedNetwork_VCA_Simulator(node);
		else if (localName.equals("RTEP_Network"))
			return new RTEP_Network(node);
		else // localName.equals("AFDX_Link")
			return new AFDX_Link(node);
	}

	@Override
	public void complete() {
		super.complete();
		if (driverList != null)
			for (Driver driver : driverList) {
				driver.complete();
			}
	}
		
	public void denormalize() {}
	
	
	@Override
	public String toString(){
		
		String string = super.toString() + 
		                "\nThroughput = " + throughput + 
		                "\nDrivers:";
		if (driverList != null) {
			for (Driver driver : driverList) {
				string += driver.toString();
			}
		}
		return string;
	}
}
