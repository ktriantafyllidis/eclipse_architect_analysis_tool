/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: FP_Params
 * Description: Represents a regular preemptive fixed priority
 *      parameters object.
 * 
 * Author: J.M. Drake
 * Date: 28-1-2010
 ********************************************************************/

package resource;

import org.w3c.dom.*;

public class FP_Params extends PriorityBasedParams {
	
    /* METHODS */
    	
    // Constructor
    public FP_Params(Node node) {
    	super(node);
    }

    public int currentPriority() {
    	return priority;
    }



	@Override
	public String toString(){
		return "\nFP params:" + super.toString().replaceAll("\n", "\n\t");
	}
}