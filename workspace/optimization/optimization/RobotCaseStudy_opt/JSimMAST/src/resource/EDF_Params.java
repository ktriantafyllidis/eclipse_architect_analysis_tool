/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: EDF_Params
 * Description: Parameter for interrupt Fixed priority data
 * 
 * Author: J.M. Drake
 * Date: 28-1-2010
 ********************************************************************/

package resource;

import module.Tools;

import org.w3c.dom.*;

public class EDF_Params extends SchedulingParams {

    /* ATTRIBUTES */
	
    private long deadline;
    	public long deadline() {return deadline;}
    	public void setDeadline(long deadline) {this.deadline = deadline;}
    
    	
    /* METHODS */
    	
    // Constructor
    public EDF_Params(Node node) {
    	
    	/* I don't care about the "Preassigned" attribute, so I consider
    	 * the "Deadline" attribute as required */
    	
    	deadline = Tools.readLong(((Element)node).getAttribute("Deadline"));
    }
    
	public void denormalize(double factor){};


	
	@Override
	public String toString(){
		return "\nEDF params:" + "\n\tDeadline = " + deadline;
	}
}