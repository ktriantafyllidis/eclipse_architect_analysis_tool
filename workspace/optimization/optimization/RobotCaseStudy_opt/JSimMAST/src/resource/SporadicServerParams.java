/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: SporadicServerParams
 * Description: Represents a task scheduled under the sporadic server
 *       scheduling algorithm.
 * 
 * Author: J.M. Drake
 * Date: 28-1-2010
 ********************************************************************/
package resource;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class SporadicServerParams extends PeriodicServerParams {

	/* FIELDS */
	
	// Represents the priority at which the task executes when there is no available execution capacity.
	private int backgroundPriority;
		public int backgroundPriority() {return backgroundPriority;}
		public void setBackgroundPriority(int backgroundPriority) {this.backgroundPriority = backgroundPriority;}
  
	// It is the maximum number of simultaneously pending replenishment operations.
	private int maxPendingReplenishments;
		public int maxPendingReplenishment(){return maxPendingReplenishments;}
		public void setMaxPendingReplenishments(int maxPendingReplenishments){this.maxPendingReplenishments = maxPendingReplenishments;}
  
	
	/* METHODS */
		
	// Constructor
	public SporadicServerParams(Node node) {
		
		super(node);
		
		Element elem = (Element)node;  
	  	
	  	if (elem.hasAttribute("Background_Priority"))
	  		backgroundPriority = Integer.parseInt(elem.getAttribute("Background_Priority"));
	  	
	  	if (elem.hasAttribute("Max_Pending_Replenishments"))
	  		maxPendingReplenishments = Integer.parseInt(elem.getAttribute("Max_Pending_Replenishments"));
	}

	
	
	
	@Override
	public String toString(){

		String string = "\nBackground priority = " + backgroundPriority + 
		                "\nMax pending replenishments = " + maxPendingReplenishments;
	  	
		return "\nSporadic server params:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}
