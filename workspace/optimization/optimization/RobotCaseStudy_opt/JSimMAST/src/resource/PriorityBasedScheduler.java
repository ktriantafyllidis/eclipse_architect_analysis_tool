/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: PriorityBasedScheduler
 * Description: It represents a scheduler that is able to handle the
 * 		 processing capacity of a proccesing suplier: Processing 
 *       Resource (PrimaryScheduler) or Scheduling Server 
 *       (SecondaryScheduler). It represents a fixed-priority policy.
 *      
 * 
 * Author: J.M. Drake
 * Date: 21-12-09
 ********************************************************************/

package resource;

import java.util.PriorityQueue;
import org.w3c.dom.*;

import module.*;
import core.*;

public class PriorityBasedScheduler extends Scheduler {

	/* METHODS */
	
	// Constructor
	public PriorityBasedScheduler(Element policyElem) {
		
		super(policyElem.getParentNode());
				
		setAwaitingServers(new PriorityQueue<SimThread>(5));
		
		long worst = 0;
		if (policyElem.hasAttribute("Worst_Context_Switch"))
			worst = Tools.readLong(policyElem.getAttribute("Worst_Context_Switch"));
		if (worst != 0)
			setContextSwitch();
		
		long avg = 0;
		if (policyElem.hasAttribute("Avg_Context_Switch"))
			avg = Tools.readLong(policyElem.getAttribute("Avg_Context_Switch"));
			
		long best = 0;
		if (policyElem.hasAttribute("Best_Context_Switch"))
			best = Tools.readLong(policyElem.getAttribute("Best_Context_Switch"));
			
		if (worst != 0)
			contextSwitch = new WABETG(worst, avg, best);
	    else 
	    	contextSwitch = null;
		
		// not used policyNode's attrs
		/*
		if (elem.hasAttribute("Max_Priority")) {}
		if (elem.hasAttribute("Min_Priority")) {}
		*/
		// TODO para el preprocesador, verificar que la prioridad de un server asignado a este scheduler est� en rango
	}
	
	public void require(SimThread server){
		
		switch (currentState){
		case FREE: 
		case PREEMTIBLE_EXECUTION:
			if (server != currentServer){
				if(awaitingServers.contains(server))
					awaitingServers.remove(server);   //JMD
				awaitingServers.add(server);
			
				if(currentServer == null || currentServer.executingSeg().currentEvent().flowPriority()<
							awaitingServers.peek().executingSeg().currentEvent().flowPriority()){			
					setCurrentState(PRE_DECIDING);
					Controller.addDecisor(this);
				}
			} else {
				setCurrentState(PRE_DECIDING);
				Controller.addDecisor(this);
			}
			break;
		default:
			if (server != currentServer){
				if(awaitingServers.contains(server))
					awaitingServers.remove(server);   //JMD
				awaitingServers.add(server);
			}
		}
	}

	public void decide() {
		if(currentState == PRE_DECIDING) {
			// There is no any server 
			if ((currentServer == null) && awaitingServers.isEmpty()){
				setCurrentState(FREE);
				setCurrentServer(null);
			} else if ( (currentServer != null) && ( awaitingServers.isEmpty() || 
					                               (currentServer.executingSeg().currentEvent().flowPriority() >= awaitingServers.peek().executingSeg().currentEvent().flowPriority())))	{
				host.require(currentServer);
				setCurrentState(PREEMTIBLE_EXECUTION);
			} else { 
				if (currentServer != null){
					awaitingServers.add(currentServer);
					setCurrentServer(null);
				}
				if (thereIsContextSwitchTime){
					setCurrentState(CONTEXT_SWITCH);
					host.require(null);
				} else {
					setCurrentServer(awaitingServers.poll());
					//setPreviousServer(currentSchedulingServer());                                    //JMD
					host.require(currentServer);
					if(currentServer.schedParams() instanceof NonPreemptible_FP_Params)
						setCurrentState(NON_PREEMTIBLE_EXECUTION);
					else
						setCurrentState(PREEMTIBLE_EXECUTION);
				}
			}
		} else {  // The state is POST_DECIDING
			if (currentServer == null) // CCC
				setCurrentServer(awaitingServers.poll());
			//setPreviousServer(currentServer);                                    //JMD
			host.require(currentServer);
			if(currentServer.schedParams() instanceof NonPreemptible_FP_Params)
				setCurrentState(NON_PREEMTIBLE_EXECUTION);
			else
				setCurrentState(PREEMTIBLE_EXECUTION);
		}
	}




	@Override
	public String toString(){
		return "\n\nScheduler" + (super.toString() + "\nPolicy: Fixed priority").replaceAll("\n", "\n\t");
	}
}