/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: ProcessingResource
 * Description: Absatract class of the resources with capacity for
 *       executing activities 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package resource;

import module.RandomGenerator;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import timers.ClockSynchronizationObject;

import core.Repository;

public abstract class ProcessingResource extends Resource implements ProcessingSupplier {

	/* ENUMS */
	
	private enum ProcResourceType{Regular_Processor, 
		                          Packet_Based_Network_VCA_Simulator,
		                          Packet_Based_Network, 
		                          RTEP_Network, 
		                          AFDX_Link, 
		                          Regular_Switch, 
		                          Regular_Router}
	
	
	/* ATTRIBUTES */
	
	protected double speedFactor = 1.0;
		public double speedFactor() {return speedFactor;}
	  
	// Is true if a server is scheduled
	private boolean isThereAwaitingServer;
		public boolean isThereAwaitingServer() {return isThereAwaitingServer;}
		public void setIsThereAwaitingServer(boolean awaitingServer) {isThereAwaitingServer = awaitingServer;}
		
	protected Scheduler scheduler;
		public Scheduler scheduler() {return scheduler;}
		public void setScheduler(Scheduler scheduler) {this.scheduler = scheduler;}
		
	protected SimThread executingServer;
	    public SimThread executingServer() {return executingServer;}
	    public void setExecutingServer(SimThread executingServer) {this.executingServer = executingServer;}
	  
	private ClockSynchronizationObject syncSource;
	
	private long localOffset = 0;
		public long localOffset(){return localOffset;}
	  
	    
	/* METHODS */
	
	// Constructor
	protected ProcessingResource(Node node){
		super(node);
		Element elem = (Element)node;
		if (elem.hasAttribute("Speed_Factor"))
			speedFactor = Double.parseDouble(elem.getAttribute("Speed_Factor"));
		else 
			speedFactor = 1.0;
	}
	
	public static ProcessingResource newProcessingResource(Node node){
		ProcResourceType type = ProcResourceType.valueOf(node.getLocalName());
		switch (type) {
		case Regular_Processor:
			return new RegularProcessor(node);
		case Packet_Based_Network:
		case Packet_Based_Network_VCA_Simulator:
		case RTEP_Network:
		case AFDX_Link:
			return Network.newNetwork(node);
		case Regular_Switch:
			return new RegularSwitch(node);
		default: //case Regular_Router:
			return new RegularRouter(node);
		}
	}
	
	public void complete() {
		Element elem = (Element)node;
		if (elem.hasAttribute("Synchronization_Source")){
			String syncSourceId = elem.getAttribute("Synchronization_Source");
			syncSource = (ClockSynchronizationObject)Repository.getModelElem(syncSourceId, "Timer");
			localOffset = (long)RandomGenerator.uniformValue(syncSource.localOffset() - syncSource.precision(), 
					                                         syncSource.localOffset() + syncSource.precision());
		}
	}
	
	/**
	 * This method returns the results generated in the simulation execution
	 * @return
	 */
	public abstract String results(String offset);
	
	// Is invoked for a primary scheduler in order to require the execution of a activity.
	//public abstract void require(SchedulableResource schedulingServer);
 
	
	
	@Override
	public String toString(){
		String string = super.toString() + 
		                "\nSpeed factor = " + speedFactor + 
		                "\nSynchronization source: ";
		if (syncSource != null)
			string += syncSource.name();
		return string;
	}
	
}