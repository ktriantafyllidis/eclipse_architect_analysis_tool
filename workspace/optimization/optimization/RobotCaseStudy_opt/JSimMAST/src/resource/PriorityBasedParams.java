package resource;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public abstract class PriorityBasedParams extends SchedulingParams {

	/* ATTRIBUTES */
    
	protected int priority;
		public int priority() {return priority;}
		public void setPriority(int priority) {this.priority = priority;}
	
		
	/* METHODS */
	
	// Constructor
    protected PriorityBasedParams(Node node) {
	  	
    	/* I don't care about the "Preassigned" attribute, so I consider
    	 * the "Priority" attribute as required */
    	
	  	priority = Integer.parseInt(((Element)node).getAttribute("Priority"));
    }
	
	public void denormalize(double factor) {}

	public abstract int currentPriority();



	@Override
	public String toString(){
		return "\nPriority = " + priority;
	}
}
