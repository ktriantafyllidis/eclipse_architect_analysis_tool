package resource;

import module.Tools;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class PartitionWindow {

	/* FIELDS*/
	
	private int partitionID;
	private String partitionName;
	private long startTime;
	private long length;
	
	
	/* METHODS */
	
	// Constructor
	public PartitionWindow(Node node) {
		Element elem = (Element)node;		
		partitionID = Integer.parseInt(elem.getAttribute("Partition_Id"));
		partitionName = elem.getAttribute("Partition_Name");
		startTime = Tools.readLong(elem.getAttribute("Start_Time"));
		length = Tools.readLong(elem.getAttribute("Length"));
	}
}
