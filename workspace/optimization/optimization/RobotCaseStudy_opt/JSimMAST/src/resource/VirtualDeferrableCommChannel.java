package resource;

import org.w3c.dom.Node;

public class VirtualDeferrableCommChannel extends VirtualPeriodicReplenishmentCommChannel {
	
	/* METHODS */
	
	// Constructor
	public VirtualDeferrableCommChannel(Node node) {
		super(node);
	}
}
