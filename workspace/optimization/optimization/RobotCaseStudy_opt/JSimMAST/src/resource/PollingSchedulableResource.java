/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: SchedulingServer
 * Description: Models a scheduling entity. Process, task or 
 * 		communication stream 
 * 
 * Author: J.M. Drake, D. Garc�a
 * Date: 1-1-09
 ********************************************************************/

package resource;

import java.util.ArrayList;
import java.util.List;

import operation.Operation;

import org.w3c.dom.Node;

import statemonitor.DisplayChangeStateMonitor;
import statemonitor.LogOnState;

import core.Clock;
import core.Controller;
import core.Logger;
import core.Timed;
import core.Controller.ProfileType;
import flow.Segment;

public class PollingSchedulableResource extends SimThread implements Timed {

	/* CONSTANTS */

	private static final int EXPECTING = 0;
	private static final int DECIDING_FROM_EXPECTING = 1;
	private static final int BUSY_PROCESSING = 2;
	private static final int BUSY_SUSPENDED = 3;
	private static final int BUSY_BLOCKED = 4;
	private static final int DECIDING_FROM_BUSY = 5;
	private static final int GETTING_BACK_PROCESSING = 6;
	private static final int GETTING_BACK_SUSPENDED = 7;
	private static final int GETTING_BACK_BLOCKED = 8;
	private static final int DECIDING_FROM_GETTING_BACK_AFTER_FINISH = 9;
	private static final int DECIDING_FROM_GETTING_BACK_AFTER_UPDATE = 10;
	
	
	/* FIELDS */
	
	private static List<String> stateNames;
	
	// total number of requirements
	private int Nr = 0;
	
	// total number of updates
	private int Nt = 0;
	
	//
	int n;
	

  	/* METHODS */
  		
	// Static constructor
	static{
		stateNames = new ArrayList<String>();
		stateNames.add(EXPECTING, "EXPECTING");
		stateNames.add(DECIDING_FROM_EXPECTING, "DECIDING_FROM_EXPECTING");
		stateNames.add(BUSY_PROCESSING, "BUSY_PROCESSING");
		stateNames.add(BUSY_SUSPENDED, "BUSY_SUSPENDED");
		stateNames.add(BUSY_BLOCKED, "BUSY_BLOCKED");
		stateNames.add(DECIDING_FROM_BUSY, "DECIDING_FROM_BUSY");
		stateNames.add(GETTING_BACK_PROCESSING, "GETTING_BACK_PROCESSING");
		stateNames.add(GETTING_BACK_SUSPENDED, "GETTING_BACK_SUSPENDED");
		stateNames.add(GETTING_BACK_BLOCKED, "GETTING_BACK_BLOCKED");
		stateNames.add(DECIDING_FROM_GETTING_BACK_AFTER_FINISH, "DECIDING_FROM_GETTING_BACK_AFTER_FINISH");
		stateNames.add(DECIDING_FROM_GETTING_BACK_AFTER_UPDATE, "DECIDING_FROM_GETTING_BACK_AFTER_UPDATE");
	}
	
  	// Constructor
  	public PollingSchedulableResource(Node paramsNode){
  		super(paramsNode.getParentNode());
  		schedParams = new PollingParams(paramsNode);
  		Clock.addTimedEvent(((PollingParams)schedParams).period(), this);
  	}
  	
  	@Override
  	public void complete(){
  		super.complete();
  		addMonitors();
  	}
  	
  	public String stateName(int state) {return stateNames.get(state);}
  	  	
	public void require(Segment segment) {
  		if(segment == executingSeg) { // current state = any PROCESSING, because I only reach this chunk of code from Segment.finish()
  			/* The current activity of the current segment is any of its activities
  			 * except the 1st one, and because of the flow priority may change, 
  			 * a new execution to the host scheduler is required */ 
  			if (currentState() == BUSY_PROCESSING)
  				prepareForReq2scheduler(BUSY_SUSPENDED, false);
			else if (currentState() == GETTING_BACK_PROCESSING)
				prepareForReq2scheduler(GETTING_BACK_SUSPENDED, false);						
		} else { // Np++, current state could be any
			// I store the new segment
			awaitingSegmentList.add(segment);
			Nr++;
			// The only possible transition is EXPECTING -> DECIDING_FROM_EXPECTING, because a segment is always non-preemptible
			//if (currentState() == EXPECTING)
				//setCurrentState(DECIDING_FROM_EXPECTING);
		}
	}
	
	public void decide() { // currentState = any DECIDING
		switch (currentState()) {
		case DECIDING_FROM_EXPECTING:
	  		if (!awaitingSegmentList.isEmpty()) { // Np > 0
	  			n++; // n = 0 -> n = 1
				// I decide the next segment to execute
	  			setExecutingSeg(awaitingSegmentList.getFirst());
				// Preparation for a requirement to the host scheduler (current activity of the current segment is its 1st activity)
				prepareForReq2scheduler(BUSY_SUSPENDED, true);
			} else // Np = 0
				setCurrentState(EXPECTING);
	  		break;
		case DECIDING_FROM_BUSY:
			if (Nt > Nr)
				switch (previousState()) {
				case BUSY_PROCESSING:
					setCurrentState(BUSY_PROCESSING);
					break;
				case BUSY_SUSPENDED:
					setCurrentState(BUSY_SUSPENDED);
					break;
				case BUSY_BLOCKED:
					setCurrentState(BUSY_BLOCKED);
					break;
				default:
					// TODO error			
				}
			else {
				n = 2;
				switch (previousState()) {
				case BUSY_PROCESSING:
					setCurrentState(GETTING_BACK_PROCESSING);
					break;
				case BUSY_SUSPENDED:
					setCurrentState(GETTING_BACK_SUSPENDED);
					break;
				case BUSY_BLOCKED:
					setCurrentState(GETTING_BACK_BLOCKED);
					break;
				default:
					// TODO error
				}
			}
			break;
		case DECIDING_FROM_GETTING_BACK_AFTER_FINISH:
			// I decide the next segment to execute
			setExecutingSeg(awaitingSegmentList.peek());
			// Preparation for a requirement to the host scheduler (current activity of the current segment is its 1st activity)
			if (n == 1)
				prepareForReq2scheduler(BUSY_SUSPENDED, true);
			else if (n > 1)
				prepareForReq2scheduler(GETTING_BACK_SUSPENDED, true);
			break;
		case DECIDING_FROM_GETTING_BACK_AFTER_UPDATE:
			if (awaitingSegmentList.size() > n)
				n++;
			switch (previousState()) {
			case GETTING_BACK_PROCESSING:
				setCurrentState(GETTING_BACK_PROCESSING);
				break;
			case GETTING_BACK_SUSPENDED:
				setCurrentState(GETTING_BACK_SUSPENDED);
				break;
			case GETTING_BACK_BLOCKED:
				setCurrentState(GETTING_BACK_BLOCKED);
				break;
			default:
				break;
			}
			break;
		default:
			// TODO error
		}
	}

	public void suspend() { // current state = any PROCESSING
  		if (currentState() == BUSY_PROCESSING)
			setCurrentState(BUSY_SUSPENDED);
  		else if (currentState() == GETTING_BACK_PROCESSING)
			setCurrentState(GETTING_BACK_SUSPENDED);
  		else {
  			// TODO error
  		}
	}
	
	public void resume() { // current state = any SUSPENDED
		if (currentState() == BUSY_SUSPENDED)
			setCurrentState(BUSY_PROCESSING);
  		else if (currentState() == GETTING_BACK_SUSPENDED)
			setCurrentState(GETTING_BACK_PROCESSING);
  		else {
  			// TODO error
  		}
	}

	public void finish() { // current state = any PROCESSING
		if(executingSeg.isLast()) {
			// Np--, n--
			awaitingSegmentList.removeFirst();
			n--;
			
			/*Segment previousExecuting = executingSegment;
	  		executingSegment = null;
	  		previousExecuting.finish();*/
			executingSeg.finish();
			
			switch (currentState()) {
			case BUSY_PROCESSING:
				setCurrentState(EXPECTING);
				break;
			case GETTING_BACK_PROCESSING:
				setCurrentState(DECIDING_FROM_GETTING_BACK_AFTER_FINISH);
				break;
			default:
	  			// TODO error
	  		}
		} else
			executingSeg.finish();
	}

	public void assignResource() { // current state = any BLOCKED 
		// I point to the next shared resource of the current activity of the current segment
		nextRequiredResourceIndex++;
		peekNextRequiredResource();
		// auto-suspend
		switch (currentState()) {
		case BUSY_BLOCKED:
			setCurrentState(BUSY_SUSPENDED);
			break;
		case GETTING_BACK_BLOCKED:
			setCurrentState(GETTING_BACK_SUSPENDED);
			break;
		default:
  			// TODO error
  		}
		// require to the host scheduler
		scheduler.require(this);
	}
	
	public void block() { // current state = any SUSPENDED
		if (currentState() == BUSY_SUSPENDED)
			setCurrentState(BUSY_BLOCKED);
  		else if (currentState() == GETTING_BACK_SUSPENDED)
			setCurrentState(GETTING_BACK_BLOCKED);
  		else {
  			// TODO error
  		}
	}

  	@Override
  	public void setCurrentState(int newState){
  		if (currentState() != newState){
  			switch (currentState()){
  			case EXPECTING:
  				if (newState == DECIDING_FROM_EXPECTING) {
  					if (executingSeg != null) 
  						setExecutingSeg(null);
  					Controller.addDecisor(this);
  				} else {}
  					// TODO error			
  				break;
  			case DECIDING_FROM_EXPECTING:
  				switch(newState){
  				case EXPECTING:
  					if (executingSeg != null) 
  						setExecutingSeg(null);
  					break;
  				case BUSY_SUSPENDED:
  					executingSeg.currentEvent().suspend();
  					break;
  				default:
  					// TODO error
  				}
  				break;
  			case BUSY_SUSPENDED:
  				switch(newState){
  				case BUSY_PROCESSING:
					executingSeg.currentEvent().resume();
			  		break;
  				case BUSY_BLOCKED:
  					executingSeg.currentEvent().block();
  					break;
  				case DECIDING_FROM_BUSY:
  					Controller.addDecisor(this);
  					break;
  				default:
  					// TODO error
  				}
  				break;
  			case BUSY_PROCESSING:
  				switch(newState){
  				case BUSY_SUSPENDED:
  					executingSeg.currentEvent().suspend();
  					break;
  				case EXPECTING:
  					setExecutingSeg(null);
  					break;
  				case DECIDING_FROM_BUSY:
  					Controller.addDecisor(this);
  					break;
  				default:
  					// TODO error
  				}
  				break;
  			case BUSY_BLOCKED:
  				switch(newState){
  				case BUSY_SUSPENDED:
  					executingSeg.currentEvent().unblock();
  					break;
  				case DECIDING_FROM_BUSY:
  					Controller.addDecisor(this);
  					break;
  				default:
  					// TODO error
  				}
  				break;
  			case DECIDING_FROM_BUSY:
  				switch(newState){
  				case BUSY_PROCESSING:
  				case BUSY_SUSPENDED:
  				case BUSY_BLOCKED:
  				case GETTING_BACK_PROCESSING:
  				case GETTING_BACK_SUSPENDED:
  				case GETTING_BACK_BLOCKED:
  					break;
  				default:
  					// TODO error
  				}
  				break;
  			case GETTING_BACK_SUSPENDED:
  				switch(newState){
  				case GETTING_BACK_PROCESSING:
					executingSeg.currentEvent().resume();
			  		break;
  				case GETTING_BACK_BLOCKED:
  					executingSeg.currentEvent().block();
  					break;
  				case DECIDING_FROM_GETTING_BACK_AFTER_UPDATE:
  					Controller.addDecisor(this);
  					break;
  				default:
  					// TODO error
  				}
  				break;
  			case GETTING_BACK_PROCESSING:
  				switch(newState){
  				case GETTING_BACK_SUSPENDED:
  					executingSeg.currentEvent().suspend();
  					break;
  				case DECIDING_FROM_GETTING_BACK_AFTER_FINISH:
  					setExecutingSeg(null);
  					Controller.addDecisor(this);
  					break;
  				case DECIDING_FROM_GETTING_BACK_AFTER_UPDATE:
  					Controller.addDecisor(this);
  					break;
  				default:
  					// TODO error
  				}
  				break;
  			case GETTING_BACK_BLOCKED:
  				switch(newState){
  				case GETTING_BACK_SUSPENDED:
					executingSeg.currentEvent().unblock();
					break;
				case DECIDING_FROM_GETTING_BACK_AFTER_UPDATE:
					Controller.addDecisor(this);
					break;
  				default:
  					// TODO error
  				}
  				break;
  			case DECIDING_FROM_GETTING_BACK_AFTER_FINISH:
  				switch(newState){
  				case BUSY_SUSPENDED:
  				case GETTING_BACK_SUSPENDED:
  					executingSeg.currentEvent().suspend();
  					break;
  				default:
  					// TODO error
  				}
  				break;
  			case DECIDING_FROM_GETTING_BACK_AFTER_UPDATE:
  				switch(newState){
  				case GETTING_BACK_PROCESSING:
  				case GETTING_BACK_SUSPENDED:
  				case GETTING_BACK_BLOCKED:
  					break;
  				default:
  					// TODO error
  				}
  				break;
  			}
  		}
  		super.setCurrentState(newState);
  	}
    
  	public void update() {
  		Nt++;
  		if (awaitingSegmentList.size() > 0) n++;
		switch (currentState()) {
		case EXPECTING:
			setCurrentState(DECIDING_FROM_EXPECTING);
			break;
		case BUSY_PROCESSING:
		case BUSY_SUSPENDED:
		case BUSY_BLOCKED:
			setCurrentState(DECIDING_FROM_BUSY);
			break;
		case GETTING_BACK_PROCESSING:
		case GETTING_BACK_SUSPENDED:
		case GETTING_BACK_BLOCKED:
			setCurrentState(DECIDING_FROM_GETTING_BACK_AFTER_UPDATE);
			break;
		default:
			break;
		}
		Clock.addTimedEvent(((PollingParams)schedParams).period(), this);
  	}
  	
  	private void addMonitors(){
  		if (Controller.profile() == ProfileType.VERBOSE)
			stateMonitorList.add(new DisplayChangeStateMonitor(this));
  		else if (Controller.profile() == ProfileType.TRACES) {
			stateMonitorList.add(new LogOnState(this, EXPECTING, Logger.SEGMENT_TO_FREE));
			stateMonitorList.add(new LogOnState(this, BUSY_PROCESSING, Logger.SEGMENT_TO_PROCESSING));
			stateMonitorList.add(new LogOnState(this, GETTING_BACK_PROCESSING, Logger.SEGMENT_TO_PROCESSING));
			stateMonitorList.add(new LogOnState(this, BUSY_BLOCKED, Logger.SEGMENT_TO_BLOCKED));
			stateMonitorList.add(new LogOnState(this, GETTING_BACK_BLOCKED, Logger.SEGMENT_TO_BLOCKED));
			stateMonitorList.add(new LogOnState(this, BUSY_SUSPENDED, Logger.SEGMENT_SUSPENDED));
			stateMonitorList.add(new LogOnState(this, GETTING_BACK_SUSPENDED, Logger.SEGMENT_SUSPENDED));
  		} else { // Controller.profile() == ProfileType.SCHEDULABILITY || PERFORMANCE
			
		}
  	}
  	
  	private void prepareForReq2scheduler(int suspendedState, boolean srp) {																				
		// I set the scheduling data
		if(schedParams instanceof PriorityBasedParams) setSegmentPriority();
		if (srp && srpParams != null) executingSeg.currentEvent().setFlowLevel(srpParams.preemptionLevel());
		// I point to the 1st shared resource of the current activity of the current segment
		nextRequiredResourceIndex = 0;
		peekNextRequiredResource();
		// auto-suspend
		setCurrentState(suspendedState);
		// require to the host scheduler
		scheduler.require(this);
  	}
  	
  	private void setSegmentPriority() {
  		int flowPriority;
		Operation currentOper = executingSeg.currentStep().operation();
		if(currentOper.thereIsOverriddenPriority()) // the current activity has a defined priority => flow priority will be the same, overriding server's priority
			flowPriority = currentOper.overriddenPriority();
		else // flow priority will be server's priority
			flowPriority = ((PriorityBasedParams)schedParams).priority();
		executingSeg.currentEvent().setFlowPriority(flowPriority);
	}

  	private void peekNextRequiredResource() {
  		List<MutualExclusionResource> lockList = executingSeg.currentStep().getLockList();
		if ((lockList != null) && lockList.size() > nextRequiredResourceIndex)
			nextRequiredResource = lockList.get(nextRequiredResourceIndex);
		else 
			nextRequiredResource = null;
  	}
  	
  	
  	
  	@Override
  	public String toString(){
  		return super.toString();
  	}
}