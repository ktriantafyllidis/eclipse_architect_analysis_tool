/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: PriorityBasedScheduler
 * Description: It represents a scheduler that is able to handle the
 * 		 processing capacity of a proccesing suplier: Processing 
 *       Resource (PrimaryScheduler) or Scheduling Server 
 *       (SecondaryScheduler). It represents an Earliest Deadline First
 *       policy.
 *      
 * 
 * Author: J.M. Drake y D. Garc�a
 * Date: 1-1-09
 ********************************************************************/

package resource;

import module.Tools;
import module.WABETG;
import core.*;
import java.util.*;

import org.w3c.dom.*;

public class EDF_Scheduler extends Scheduler {
	
	/* METHODS */
	
	// Constructor
	public EDF_Scheduler(Element policyElem){
		
		super(policyElem.getParentNode());
		
		setAwaitingServers(new PriorityQueue<SimThread>(5, new EDFComparator()));
		
		long worst = 0;
		if (policyElem.hasAttribute("Worst_Context_Switch"))
			worst = Tools.readLong(policyElem.getAttribute("Worst_Context_Switch"));
		if (worst != 0)
			setContextSwitch();
		
		long avg = 0;
		if (policyElem.hasAttribute("Avg_Context_Switch"))
			avg = Tools.readLong(policyElem.getAttribute("Avg_Context_Switch"));
			
		long best = 0;
		if (policyElem.hasAttribute("Best_Context_Switch"))
			best = Tools.readLong(policyElem.getAttribute("Best_Context_Switch"));
			
		if (worst != 0 || avg != 0 || best != 0)
			contextSwitch = new WABETG(worst, avg, best);
	    else 
	    	contextSwitch = null;
	}
	
	public void require(SimThread server){
		
		if (server != currentServer)
			awaitingServers.add(server);
		
		switch (currentState){
		case FREE:
		case PREEMTIBLE_EXECUTION:
			/*if (server != currentServer)
				awaitingServers.add(server);*/
			if( (currentServer == null) ||
			    (((EDF_Params)awaitingServers.peek().schedParams()).deadline()<
			    		((EDF_Params)currentServer.schedParams()).deadline())){
				setCurrentState(PRE_DECIDING);
				Controller.addDecisor(this);
			}
			break;
		default:
			/*if (server != currentServer)
				awaitingServers.add(server);*/
		}
	}

	public void decide() {
		
		if(currentState == PRE_DECIDING) {
			// There is no any server 
			if ((currentServer == null) && awaitingServers.isEmpty()){
				setCurrentState(FREE);
				setCurrentServer(null);
			} else if ((currentServer != null)&& 
					 (awaitingServers.isEmpty())|
					   (((EDF_Params)awaitingServers.peek().schedParams()).deadline()>=
					   ((EDF_Params)currentServer().schedParams()).deadline()))
			{
				host.require(currentServer);
				setCurrentState(PREEMTIBLE_EXECUTION);
			} else { 
				if (currentServer != null){
					awaitingServers.add(currentServer);
					setCurrentServer(null);
				}
				if (thereIsContextSwitchTime){
					setCurrentState(CONTEXT_SWITCH);
					host.require(null);
				}else{
					setCurrentServer(awaitingServers.poll());
					//setPreviousServer(currentSchedulingServer());                                    //JMD
					host.require(currentServer);
					setCurrentState(PREEMTIBLE_EXECUTION);	
				}
			}
		}else{  // The state is POST_DECIDING
			setCurrentServer(awaitingServers.poll());
			//setPreviousServer(currentSchedulingServer());                                    //JMD
			host.require(currentServer);
			setCurrentState(PREEMTIBLE_EXECUTION);	
		}
	}
		
	
	
	
	@Override
	public String toString(){
		return "\n\nScheduler" + (super.toString() + "\nPolicy: EDF").replaceAll("\n", "\n\t");
	}
	
	
	
	private class EDFComparator implements Comparator<SimThread>{
		public int compare(SimThread server1,SimThread server2){

			long diff= ((EDF_Params)(server2.schedParams())).deadline()-
						((EDF_Params)(server1.schedParams())).deadline();
			if (diff>0)return -1;
			else if(diff<0)return +1;
			else return 0;
		}
	}
}
