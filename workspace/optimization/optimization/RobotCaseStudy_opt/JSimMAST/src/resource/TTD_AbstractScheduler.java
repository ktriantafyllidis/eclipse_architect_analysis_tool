package resource;

import java.util.ArrayList;
import java.util.List;

import module.Tools;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class TTD_AbstractScheduler extends Scheduler {

	/* FIELDS */
	
	//
	protected long maf;
	
	//
	protected List<PartitionWindow> partitionTable = new ArrayList<PartitionWindow>();
	
	
	/* METHODS */
	
	// Constructor
	protected TTD_AbstractScheduler(Node policyNode){
		
		super(policyNode.getParentNode());

		Element policyElem = (Element)node;

		maf = Tools.readLong(policyElem.getAttribute("MAF")); // not necessary to denormalize because it's time
		
		NodeList nl = node.getChildNodes();
		for (int i = 0; i < nl.getLength(); i++){
			if (nl.item(i).getNodeType() == Node.ELEMENT_NODE) // localName = "Window"
				partitionTable.add(new PartitionWindow(nl.item(i)));
		}
	}
	
	public void require(SimThread schedulingServer) {
		// TODO implement require()
	}

	public void decide() {
		// TODO decide()
	}

}
