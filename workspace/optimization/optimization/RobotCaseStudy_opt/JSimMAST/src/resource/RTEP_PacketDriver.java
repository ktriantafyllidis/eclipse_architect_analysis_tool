package resource;

import operation.SimpleOperation;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import utilityClasses.XMLparser;
import core.Repository;

public class RTEP_PacketDriver extends PacketDriver {

	/* FIELDS */
	
	private String packetInterruptServerRef;
	private SimThread packetInterruptServer;
	
	private String packetISROperRef;
	private SimpleOperation packetISROper;
	
	private String tokenCheckOperRef;
	private SimpleOperation tokenCheckOper;
	
	private String tokenManageOperRef;
	private SimpleOperation tokenManageOper;
	
	private String tokenDiscardOperRef;
	private SimpleOperation tokenDiscardOper;
	
	private String tokenRetransmissionOperRef;
	private SimpleOperation tokenRetransmissionOper;
	
	private String packetRetransmissionOperRef;
	private SimpleOperation packetRetransmissionOper;
	
	
	/* METHODS */
	
	// Constructor
	public RTEP_PacketDriver(Node node, Network network) {
		
		super(node, network);
		
		Element elem = (Element)node;
		if (elem.hasAttribute("Packet_Interrupt_Server"))
			packetInterruptServerRef = elem.getAttribute("Packet_Interrupt_Server");
		else {
			Element packetInterruptServerElem = XMLparser.getChildElem(node, "Packet_Interrupt_Server").firstElement();
			packetInterruptServer = SimThread.newSchedulableResource(packetInterruptServerElem);			
		}
		if (elem.hasAttribute("Packet_ISR_Operation"))
			packetISROperRef = elem.getAttribute("Packet_ISR_Operation");
		else {
			Element packetISRoperElem = XMLparser.getChildElem(node, "Packet_ISR_Operation").firstElement();
			packetISROper = new SimpleOperation(packetISRoperElem);
		}
		if (elem.hasAttribute("Token_Check_Operation"))
			tokenCheckOperRef = elem.getAttribute("Token_Check_Operation");
		else {
			Element tokenCheckOperElem = XMLparser.getChildElem(node, "Token_Check_Operation").firstElement();
			tokenCheckOper = new SimpleOperation(tokenCheckOperElem);
		}
		if (elem.hasAttribute("Token_Manage_Operation"))
			tokenManageOperRef = elem.getAttribute("Token_Manage_Operation");
		else {
			Element tokenManageOperElem = XMLparser.getChildElem(node, "Token_Manage_Operation").firstElement();
			tokenManageOper = new SimpleOperation(tokenManageOperElem);
		}
		if (elem.hasAttribute("Token_Discard_Operation"))
			tokenDiscardOperRef = elem.getAttribute("Token_Discard_Operation");
		else {
			Element tokenDiscardOperElem = XMLparser.getChildElem(node, "Token_Discard_Operation").firstElement();
			tokenDiscardOper = new SimpleOperation(tokenDiscardOperElem);
		}
		if (elem.hasAttribute("Token_Retransmission_Operation"))
			tokenRetransmissionOperRef = elem.getAttribute("Token_Retransmission_Operation");
		else {
			Element tokenRetransmissionOperElem = XMLparser.getChildElem(node, "Token_Retransmission_Operation").firstElement();
			tokenRetransmissionOper = new SimpleOperation(tokenRetransmissionOperElem);
		}
		if (elem.hasAttribute("Packet_Retransmission_Operation"))
			packetRetransmissionOperRef = elem.getAttribute("Packet_Retransmission_Operation");
		else {
			Element packetRetransmissionOperElem = XMLparser.getChildElem(node, "Packet_Retransmission_Operation").firstElement();
			packetRetransmissionOper = new SimpleOperation(packetRetransmissionOperElem);
		}
	}
	
	@Override
	public void complete(){
		
		super.complete();
		
		// TODO revisar
		if (packetInterruptServer != null)
			packetInterruptServer.complete();
		else if (packetInterruptServerRef != null)
  			packetInterruptServer = (SimThread)Repository.getModelElem(packetInterruptServerRef, "SchedulableResource");
  		
  		if (packetISROperRef != null)
  			packetISROper = (SimpleOperation)Repository.getModelElem(packetISROperRef, "Operation");
  		if (tokenCheckOperRef != null)
  			tokenCheckOper = (SimpleOperation)Repository.getModelElem(tokenCheckOperRef, "Operation");
  		if (tokenManageOperRef != null)
  			tokenManageOper = (SimpleOperation)Repository.getModelElem(tokenManageOperRef, "Operation");
  		if (tokenDiscardOperRef != null)
  			tokenDiscardOper = (SimpleOperation)Repository.getModelElem(tokenDiscardOperRef, "Operation");
  		if (tokenRetransmissionOperRef != null)
  			tokenRetransmissionOper = (SimpleOperation)Repository.getModelElem(tokenRetransmissionOperRef, "Operation");
  		if (packetRetransmissionOperRef != null)
  			packetRetransmissionOper = (SimpleOperation)Repository.getModelElem(packetRetransmissionOperRef, "Operation");
  	}




	@Override
	public String toString(){
		
		String string = "\nPacket interrupt server: ";
		if (packetInterruptServerRef != null)
			string += packetInterruptServer.name();
		else
			string += packetInterruptServer.toString();
		
		string += "\nPacket ISR operation: "; 
		if (packetISROperRef != null)
			string += packetISROper.name();
		else
			string += packetISROper.toString();
		
		string += "\nToken check operation: ";
		if (tokenCheckOperRef != null)
			string += tokenCheckOper.name();
		else
			string += tokenCheckOper.toString();
		
		string = "\nToken manage operation: ";
		if (tokenManageOperRef != null)
			string += tokenManageOper.name();
		else
			string += tokenManageOper.toString();
		
		string += "\nToken discard operation: "; 
		if (tokenDiscardOperRef != null)
			string += tokenDiscardOper.name();
		else
			string += tokenDiscardOper.toString();
		
		string += "\nToken retransmission operation: ";
		if (tokenRetransmissionOperRef != null)
			string += tokenRetransmissionOper.name();
		else
			string += tokenRetransmissionOper.toString();
		
		string += "\nPacket retransmission operation: ";
		if (packetRetransmissionOperRef != null)
			string += packetRetransmissionOper.name();
		else
			string += packetRetransmissionOper.toString();
				
		return "\nRTEP packet driver:" + (super.toString() + string).replaceAll("\n", "\n\t");	
	}
}
