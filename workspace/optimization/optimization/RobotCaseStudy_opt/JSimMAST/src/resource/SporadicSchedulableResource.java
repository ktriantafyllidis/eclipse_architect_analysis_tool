/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: SchedulingServer
 * Description: Models a scheduling entity. Process, task or 
 * 		communication stream 
 * 
 * Author: J.M. Drake, D. Garc�a
 * Date: 23-12-09
 ********************************************************************/

package resource;

import java.util.ArrayList;

import org.w3c.dom.Node;

import statemonitor.DisplayChangeStateMonitor;
import statemonitor.LogOnState;
import core.Clock;
import core.Controller;
import core.Logger;
import core.Timed;
import core.Controller.ProfileType;
import flow.Segment;

public class SporadicSchedulableResource extends SimThread implements Timed {

	/* ENUMS */
	private enum SporadicSchedParamsType {Periodic_Server_Params, 
        							 	  Sporadic_Server_Params,
        							 	  Periodic_Server_Comm_Params,
        							 	  Sporadic_Server_Comm_Params}
	
	
	/* CONSTANTS */
	
	private static final int FREE = 0;
	private static final int DECIDING = 1;
	private static final int PROCESSING = 2;
	private static final int BLOCKED = 3;
	private static final int SUSPENDED = 4;
	
	
	/*Inner class */
	// Represents a pending replenishment action
	public class ReplenishmentSlot implements Timed{
		
		long slotCapacity;
		
		// When the server is suspended or blocked a replenishment action is created.
		ReplenishmentSlot(){
			Clock.addTimedEvent(activationTime+((SporadicServerParams)schedParams).replenishmentPeriod()-Clock.currentTime(), this);
			numPendingReplenishment=numPendingReplenishment + 1;
			slotCapacity = Clock.currentTime() - activationTime;
			((SporadicServerParams)schedParams).setCurrentCapacity(((SporadicServerParams)schedParams).currentCapacity() - slotCapacity);
			if (((SporadicServerParams)schedParams).currentCapacity() < 0)
				((SporadicServerParams)schedParams).setCurrentCapacity(0);
			
			if(numPendingReplenishment == ((SporadicServerParams)schedParams).maxPendingReplenishment()) {
				((SporadicServerParams)schedParams).setCurrentPriority(((SporadicServerParams)schedParams).backgroundPriority());
				updatePriority();
			}
		}
		
		// A replenishment is returned
		public void update() {
			
			((SporadicServerParams)schedParams).setCurrentCapacity(((SporadicServerParams)schedParams).currentCapacity()+slotCapacity);
			if(((SporadicServerParams)schedParams).currentCapacity() > ((SporadicServerParams)schedParams).initialCapacity())
				((SporadicServerParams)schedParams).setCurrentCapacity(((SporadicServerParams)schedParams).initialCapacity());
			
			numPendingReplenishment = numPendingReplenishment - 1;
			if (((SporadicServerParams)schedParams).currentPriority() != ((SporadicServerParams)schedParams).priority()){
				((SporadicServerParams)schedParams).setCurrentPriority(((SporadicServerParams)schedParams).priority());
				if (executingSeg != null){
					updatePriority();
					changeSchedulingData();
				}
			}
		}
	}
	
	
	/* FIELDS */
	
	private static ArrayList<String> stateNames;
	
	private long activationTime = -1;
	private int numPendingReplenishment = 0;

	
	/* METHODS */
	
	// Static constructor
	static{
		stateNames = new ArrayList<String>();
		stateNames.add(FREE, "FREE");
		stateNames.add(DECIDING, "DECIDING");
		stateNames.add(PROCESSING, "PROCESSING");
		stateNames.add(BLOCKED, "BLOCKED");
		stateNames.add(SUSPENDED, "SUSPENDED");
	}
	
	// Constructor
	public SporadicSchedulableResource(Node paramsNode) {
		super(paramsNode.getParentNode());
		SporadicSchedParamsType type = SporadicSchedParamsType.valueOf(paramsNode.getLocalName());
		switch (type) {
		case Periodic_Server_Params:
			schedParams = new PeriodicServerParams(paramsNode);
			break;
		case Sporadic_Server_Params:
			schedParams = new SporadicServerParams(paramsNode);
			break;
		case Periodic_Server_Comm_Params:
			schedParams = new PeriodicServerCommParams(paramsNode);
			break;
		default: // case Sporadic_Server_Comm_Params:
			schedParams = new SporadicServerCommParams(paramsNode);
			break;
		}
	}

	@Override
	public void complete(){
		super.complete();
		addMonitors();
	}
	
	public String stateName(int state) {return stateNames.get(state);}
	
  	public void decide(){
		if (currentState() == DECIDING){
			if (((SporadicServerParams)schedParams).currentCapacity() == 0)
				((SporadicServerParams)schedParams).setCurrentPriority(((SporadicServerParams)schedParams).backgroundPriority());
			if (executingSeg != null){
				if (((SporadicServerParams)schedParams).currentCapacity() == 0){
					updatePriority();
					changeSchedulingData();
				}
				return;
			}
	
			setExecutingSeg(awaitingSegmentList.removeFirst());
	
			if(schedParams instanceof PriorityBasedParams) {
				// Flow priority will be the same of the activity priority if activity has a define priority, 
				// else, it uses server's priority
				if(executingSeg.currentStep().operation().thereIsOverriddenPriority())
					executingSeg().currentEvent().setFlowPriority(executingSeg().currentStep().operation().overriddenPriority());
				else
					executingSeg().currentEvent().setFlowPriority(((PriorityBasedParams)schedParams).priority());
			}
			if (srpParams != null)
				executingSeg().currentEvent().setFlowLevel(srpParams.preemptionLevel());
	
			nextRequiredResourceIndex = 0;
			if ((executingSeg.currentStep().getLockList() != null) && executingSeg.currentStep().getLockList().size() > nextRequiredResourceIndex)
				nextRequiredResource = executingSeg.currentStep().getLockList().get(nextRequiredResourceIndex);
			else 
				nextRequiredResource = null;
	
			setCurrentState(SUSPENDED);
			scheduler.require(this);
		}
	}

	public void require(Segment segment) {
		if(segment == executingSeg) {
			if(schedParams instanceof SporadicServerParams) {
				if(executingSeg.currentStep().operation().thereIsOverriddenPriority())
					executingSeg().currentEvent().setFlowPriority(executingSeg().currentStep().operation().overriddenPriority());
				else
					executingSeg().currentEvent().setFlowPriority(((SporadicServerParams)schedParams).priority());
			}
			setCurrentState(SUSPENDED);
			scheduler.require(this);
		} else {
			awaitingSegmentList.add(segment);
			if (executingSeg == null){
				if(currentState != DECIDING) 
					Controller.addDecisor(this);
				setCurrentState(DECIDING);
			}
		}
  	}

  	public void finish() {
   		if(executingSeg.isLast()){
  			Segment previousExecuting = executingSeg;
  	  		setExecutingSeg(null);
  	  		previousExecuting.finish();
			if(!awaitingSegmentList.isEmpty()) { // I decide the next segment to execute
				if(currentState()!=DECIDING)
					Controller.addDecisor(this);
				setCurrentState(DECIDING);	
			} else
				setCurrentState(FREE);
		} else {
			executingSeg.finish();
			nextRequiredResourceIndex = 0;
	  		if ((executingSeg.currentStep().getLockList() != null) &&
	  			(executingSeg.currentStep().getLockList().size() > nextRequiredResourceIndex))
	  			nextRequiredResource = executingSeg.currentStep().getLockList().get(nextRequiredResourceIndex);
	  		else 
	  			nextRequiredResource = null;
	  		executingSeg.currentEvent().setExecutionTim(executingSeg.currentStep().etg().next());
		}
  	}
      	
  	public void assignResource() {
  		
  		nextRequiredResourceIndex++;
  		
  		if (executingSeg.currentStep().getLockList().size() > nextRequiredResourceIndex)
  			nextRequiredResource = executingSeg.currentStep().getLockList().get(nextRequiredResourceIndex);
  		else 
  			nextRequiredResource = null;
  
		if (currentState() == BLOCKED) {
			setCurrentState(SUSPENDED);
			scheduler.require(this);
		}
  	}

  	public void block() {
  		setCurrentState(BLOCKED);
  	}

  	public void resume() {
  		setCurrentState(PROCESSING);
  	}

  	public void suspend() {
  		setCurrentState(SUSPENDED);
  	}
  	
  	public void setCurrentState(int newState){

  		if (currentState()!=newState){
			//Time managing specific of SporadicSchedulingServer
			if (newState == PROCESSING && currentState() != PROCESSING
				                       && ((SporadicServerParams)schedParams).currentPriority() == ((SporadicServerParams)schedParams).priority()
				                       && activationTime != Clock.currentTime()){
				activationTime = Clock.currentTime();
				Clock.addTimedEvent(((SporadicServerParams)schedParams).currentCapacity(), this);
			}

			if (newState != PROCESSING && currentState() == PROCESSING
				                       && ((SporadicServerParams)schedParams).currentPriority() == ((SporadicServerParams)schedParams).priority()
				                       && activationTime != Clock.currentTime()){
				Clock.cancelTimedEvent(this);
				new ReplenishmentSlot();
			}	
  			
  			switch (currentState()){
  			case BLOCKED: 
  				executingSeg.currentEvent().unblock();
  				break;
  			case SUSPENDED: 
  				executingSeg.currentEvent().resume();
  				break;
  			default:
  				switch(newState){
  				case BLOCKED:
  					executingSeg.currentEvent().block();
  					break;
  				case SUSPENDED:
  					executingSeg.currentEvent().suspend();
  					break;
  				default:					
  				}
  			}

  			super.setCurrentState(newState);
  		}
  	}
	
	public void update() {
		if (Clock.currentTime()>=(activationTime+((SporadicServerParams)schedParams).currentCapacity())){
			if(currentState()!=DECIDING) 
				Controller.addDecisor(this);
			setCurrentState(DECIDING);
		}else{
			Clock.addTimedEvent(((SporadicServerParams)schedParams).currentCapacity()-(Clock.currentTime()-activationTime),
            this);                     			
		}
	}

	private void addMonitors(){
  		if (Controller.profile() == ProfileType.VERBOSE)
			stateMonitorList.add(new DisplayChangeStateMonitor(this));
  		else if (Controller.profile() == ProfileType.TRACES) {
			stateMonitorList.add(new LogOnState(this, FREE, Logger.SEGMENT_TO_FREE));
			stateMonitorList.add(new LogOnState(this, PROCESSING, Logger.SEGMENT_TO_PROCESSING));
			stateMonitorList.add(new LogOnState(this, BLOCKED, Logger.SEGMENT_TO_BLOCKED));
			stateMonitorList.add(new LogOnState(this, SUSPENDED, Logger.SEGMENT_SUSPENDED));
  		} else { // Controller.profile() == ProfileType.SCHEDULABILITY || PERFORMANCE
			
		}
  	}


  	@Override
  	public String toString(){
  		return super.toString();
  	}
}
