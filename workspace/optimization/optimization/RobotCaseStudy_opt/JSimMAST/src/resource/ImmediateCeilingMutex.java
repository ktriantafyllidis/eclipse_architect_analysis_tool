/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: mmediateCeilingResource
 * Description: Uses the immediate priority ceiling resource protocol.
 *     This is equivalent to Ada�s Priority Ceiling, or the POSIX
 *     priority protect protocol.
 * 
 * Author: J.M. Drake D. Garc�a
 * Date: 1-1-09
 ********************************************************************/

package resource;

import java.util.*;
import org.w3c.dom.*;

public class ImmediateCeilingMutex extends MutualExclusionResource {

	/* FIELDS */
	
	// Priority ceiling used for the resource. May be computed automatically by the tool, upon request.
	private int ceilingPriority;
	
	
	/* METHODS */
	
	// Constructor
	public ImmediateCeilingMutex(Node node){
		super(node);
		Element elem = (Element)node;
		waitForLockList = new PriorityQueue<SimThread>();
		//if (elem.hasAttribute("Ceiling"))
			ceilingPriority = Integer.parseInt(elem.getAttribute("Ceiling"));
	}
  
	@Override
	public String results(String offset) {
		String string = "";
		String sup = super.results(offset);
		if (!sup.equals(""))
			string = offset + "<mast_res:Immediate_Ceiling_Mutex_Results Name=\"" + computingResourceName + "/" + name + "\">\n" + 
		       		 sup + 
		       		 offset + "</mast_res:Immediate_Ceiling_Mutex_Results>\n\n";
		return string;
	}
	
	public void decide() {
		
		// I get the 1st server from the awaiting queue, removing it
		SimThread server = waitForLockList.remove();
		
		setOwner(server);
		setCurrentState(ASSIGNED);
		server.assignResource();
		
		// I update the current flow priority of the server
		server.executingSeg().currentEvent().setFlowPriority(ceilingPriority);
		
		// I block the servers waiting to lock the mutex
		Iterator<SimThread> iter = waitForLockList.iterator();
		while(iter.hasNext()){
			iter.next().block();
		}
	}

	
	
	@Override
	public String toString(){
		String string = "\nCeiling priority = " + ceilingPriority;
		return "\n\nImmediate ceiling mutex:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}
