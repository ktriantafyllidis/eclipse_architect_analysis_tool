/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: SchedulingParams
 * Description: Abstract root class, roots of the different kinds of
 *       scheduling parameters of a scheduling server.
 * 
 * Author: J.M. Drake
 * Date: 28-1-2010
 ********************************************************************/
package resource;


public abstract class SchedulingParams {
		
	/* METHODS */
	
	public abstract void denormalize(double factor);
}
