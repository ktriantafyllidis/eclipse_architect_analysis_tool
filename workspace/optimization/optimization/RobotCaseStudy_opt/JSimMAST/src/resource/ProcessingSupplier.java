/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Interface: ProcessingSuplier
 * Description: Represent the suplier processing capacity schedule for
 *      a scheduler (Primary o secondary scheduler). 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/
package resource;

public interface ProcessingSupplier {

	/**
  	 * This method is conceived to allow a scheduler to require the execution,
  	 * by the current processing supplier, of the <code>server</code> argument, 
  	 * typically scheduler current server.
  	 * @param server
  	 */
	public void require(SimThread server);
   
	public void setScheduler(Scheduler scheduler);
   
	public double speedFactor();
}
