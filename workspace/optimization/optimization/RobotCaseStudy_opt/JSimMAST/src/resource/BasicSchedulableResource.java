/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: SchedulingServer
 * Description: Models a scheduling entity. Process, task or 
 * 		communication stream 
 * 
 * Author: J.M. Drake, D. Garc�a
 * Date: 1-1-09
 ********************************************************************/

package resource;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;

import statemonitor.DisplayChangeStateMonitor;
import statemonitor.LogOnState;
import core.Controller;
import core.Logger;
import flow.Segment;

public class BasicSchedulableResource extends SimThread {

	/* ENUMS */
	private enum BasicSchedParamsType {Interrupt_FP_Params, 
		                               Non_Preemptible_FP_Params, 
		                               Fixed_Priority_Params, 
		                               EDF_Params}
	
	
	/* CONSTANTS */

	public static final int FREE = 0;
	public static final int DECIDING = 1;
	public static final int PROCESSING = 2;
	public static final int BLOCKED = 3;
	public static final int SUSPENDED = 4;
	
	
	/* FIELDS */
	
	private static List<String> stateNames;
	
	
  	/* METHODS */
		
	// Static constructor
	static{
		stateNames = new ArrayList<String>();
		stateNames.add(FREE, "FREE");
		stateNames.add(DECIDING, "DECIDING");
		stateNames.add(PROCESSING, "PROCESSING");
		stateNames.add(BLOCKED, "BLOCKED");
		stateNames.add(SUSPENDED, "SUSPENDED");
	}
	
  	// Constructor
  	public BasicSchedulableResource(Element paramsElem){
  		super(paramsElem.getParentNode());
		BasicSchedParamsType type = BasicSchedParamsType.valueOf(paramsElem.getLocalName());
		switch (type) {
		case Interrupt_FP_Params:
			schedParams = new Interrupt_FP_Params(paramsElem);
			break;
		case Non_Preemptible_FP_Params:
			schedParams = new NonPreemptible_FP_Params(paramsElem);
			break;
		case Fixed_Priority_Params:
			schedParams = new FP_Params(paramsElem);
			break;
		default: // case EDF_Params:
			schedParams = new EDF_Params(paramsElem);
			break;
		}
  	}
  	
  	@Override
  	public void complete(){
  		super.complete();
  		addMonitors();
  	}
  	
  	public String stateName(int state) {return stateNames.get(state);}
  	
  	public void require(Segment segment) {
		
  		if(segment == executingSeg) { // the executing segment has required the execution of its next step
  			
  			/* If the server is priority based, the flow priority could change.
  			 * If the operation of the step to execute has a defined priority, 
  			 * the flow priority will be that priority, 
  			 * else, it will be the server priority */
  			if(schedParams instanceof PriorityBasedParams) { 
				int prio;
				if(executingSeg.currentStep().operation().thereIsOverriddenPriority())
					prio = executingSeg.currentStep().operation().overriddenPriority();
				else
					prio = ((PriorityBasedParams)schedParams).priority();
				executingSeg.currentEvent().setFlowPriority(prio);
			}
			/* The current server is ready to require to its scheduler the scheduling of
			 * its executing segment, but it could happen that the server would be suspended, so I set
			 * the current state as SUSPENDED and when it would be scheduled, its state will be changed */
			setCurrentState(SUSPENDED);
			scheduler.require(this);
		} else { // another segment has required execution of a step. 
			/* The new segment has to wait (added to the awaiting queue) until the current
			 * server is released by the executing segment */
			awaitingSegmentList.add(segment);
			if (executingSeg == null){ // the current server is free
				Controller.addDecisor(this);
				setCurrentState(DECIDING);
			}
		}
  	}

  	public void finish() {
  		
  		// I report to the executing segment that the step execution has finished 
  		//executingSegment.finish();
   		
  		if(executingSeg.isLast()){ // the executed step is the last one of the executing segment
   			
  			/* I want to release the current server, but I need to keep a reference to
  			 * the executing segment to report to it that the step execution has finished */
  			Segment previousExecuting = executingSeg;
  	  		setExecutingSeg(null); // release
  			previousExecuting.finish();
			
  	  		if(!awaitingSegmentList.isEmpty()) { // there are awaiting segments => the next one to execute must be decided
				Controller.addDecisor(this);
				setCurrentState(DECIDING);	
			} else
				setCurrentState(FREE);
		} else {
			executingSeg.finish();
			
			//
			nextRequiredResourceIndex = 0;
	  		if ((executingSeg.currentStep().getLockList() != null) &&
	  			(executingSeg.currentStep().getLockList().size() > nextRequiredResourceIndex))
	  			nextRequiredResource = executingSeg.currentStep().getLockList().get(nextRequiredResourceIndex);
	  		else 
	  			nextRequiredResource = null;
	  		
	  		// �?
	  		executingSeg.currentEvent().setExecutionTim(executingSeg.currentStep().etg().next());
		}
  	}
    
  	public void assignResource() {
  		
  		nextRequiredResourceIndex++;
  		
  		// �este if-else es necesario? Si un mutex invoca al m�todo ser� pq ha de ser tomado por el server
  		if (executingSeg.currentStep().getLockList().size() > nextRequiredResourceIndex) // there are pending mutexes to lock
  			// I take the next one
  			nextRequiredResource = executingSeg.currentStep().getLockList().get(nextRequiredResourceIndex);
  		else 
  			nextRequiredResource = null;
  
  		
		if (currentState == BLOCKED) { // the current server was waiting to lock the next required resource 
			/* The current server is ready to require its host scheduler the scheduling of
			 * its executing segment, but it could happen that the server would be suspended, so I set
			 * the current state as SUSPENDED and when it would be scheduled, its state will be changed */
			setCurrentState(SUSPENDED);
			scheduler.require(this);
		}
  	}

  	public void block() {
  		setCurrentState(BLOCKED);
  	}

  	public void resume() {
  		setCurrentState(PROCESSING);
  	}

  	public void suspend() {
  		setCurrentState(SUSPENDED);
  	}
  
  	public void decide(){
  		
  		if (executingSeg != null) 
  			return; //CAMBIO DEL DIA 17
  		
  		// I set the executing segment as one of the awaiting segments
		setExecutingSeg(awaitingSegmentList.removeFirst());
  		
		/* If the server is priority based, the flow priority could change.
		 * If the operation of the step to execute has a defined priority, 
		 * the flow priority will be that priority, 
		 * else, it will be the server priority */
		if(schedParams instanceof PriorityBasedParams) { 
			int prio;
			if(executingSeg.currentStep().operation().thereIsOverriddenPriority())
				prio = executingSeg.currentStep().operation().overriddenPriority();
			else
				prio = ((PriorityBasedParams)schedParams).priority();
			executingSeg.currentEvent().setFlowPriority(prio);
		}
		
		if (srpParams != null)
			executingSeg().currentEvent().setFlowLevel(srpParams.preemptionLevel());
			
		nextRequiredResourceIndex = 0;
	  	if ((executingSeg.currentStep().getLockList() != null) && executingSeg.currentStep().getLockList().size() > nextRequiredResourceIndex)
	  		nextRequiredResource = executingSeg.currentStep().getLockList().get(nextRequiredResourceIndex);
	  	else 
	  		nextRequiredResource = null;

	  	/* The current server is ready to require its host scheduler the scheduling of
		 * its executing segment, but it could happen that the server will be suspended, so I set
		 * the current state as SUSPENDED and when it be scheduled its state will be changed */
		setCurrentState(SUSPENDED);
		scheduler.require(this);
  	}
  	
  	@Override
  	public void setCurrentState(int newState){
//  		if (currentState()!=newState){ 
  		if ((currentState()!=newState) && (newState!=FREE) ){        // JMD
  			switch (currentState()){
  			case BLOCKED: 
  				executingSeg.currentEvent().unblock();
  				break;
  			case SUSPENDED:
  				if (newState == PROCESSING)
					executingSeg.currentEvent().resume();
  				break;
  			default:
  				switch(newState){
  				case BLOCKED:
  					executingSeg.currentEvent().block();
  					break;
  				case SUSPENDED:
  					executingSeg.currentEvent().suspend();
  					break;
  				default:					
  				}
  			}
  		}
  		super.setCurrentState(newState);
  	}

  	private void addMonitors(){
  		switch (Controller.profile()) {
		case VERBOSE:
			stateMonitorList.add(new DisplayChangeStateMonitor(this));
			break;
		case TRACES:
			stateMonitorList.add(new LogOnState(this, FREE, Logger.SEGMENT_TO_FREE));
			stateMonitorList.add(new LogOnState(this, PROCESSING, Logger.SEGMENT_TO_PROCESSING));
			stateMonitorList.add(new LogOnState(this, BLOCKED, Logger.SEGMENT_TO_BLOCKED));
			stateMonitorList.add(new LogOnState(this, SUSPENDED, Logger.SEGMENT_SUSPENDED));
		default: // SCHEDULABILITY || PERFORMANCE
			break;
		}
  	}


  	@Override
  	public String toString(){
  		return super.toString();
  	}
}
