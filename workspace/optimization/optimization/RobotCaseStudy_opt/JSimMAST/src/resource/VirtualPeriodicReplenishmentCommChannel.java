package resource;

import module.Tools;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public abstract class VirtualPeriodicReplenishmentCommChannel extends VirtualCommChannelParams{

	/* FIELDS */
	
	private int budget;
	private long period; 
	private long deadline;
	
	
	/* METHODS */
	
	// Constructor
	protected VirtualPeriodicReplenishmentCommChannel(Node node) {
		
		super(node);
		
		Element elem = (Element)node;

		if (elem.hasAttribute("Budget"))
			budget = Integer.parseInt(elem.getAttribute("Budget"));
		
		if (elem.hasAttribute("Period"))
			period = Tools.readLong(elem.getAttribute("Period"));

		if (elem.hasAttribute("Deadline"))
			deadline = Tools.readLong(elem.getAttribute("Deadline"));
	}
	
}
