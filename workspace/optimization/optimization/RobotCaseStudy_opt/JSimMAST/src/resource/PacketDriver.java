package resource;

import operation.SimpleOperation;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import utilityClasses.XMLparser;
import core.Repository;
import flow.FlowEndPoint;
import flow.Merge;
import flow.Segment;
import flow.Step;

public class PacketDriver extends Driver {
	
	/* FIELDS */
	
	private String packetServerRef;
	protected SimThread packetServer;
	
	private String packetSendOperRef;
	protected SimpleOperation packetSendOper;
	
	private String packetReceiveOperRef;
	protected SimpleOperation packetReceiveOper;
	
	protected boolean messagePartitioning = true;
	protected boolean highUtilizationMode = true;
	
	protected Merge senderMerge;
	protected Merge receiverMerge;
	
	private Segment senderSegment;
	private Segment receiverSegment;
	
	
	/* METHODS */
	
	// Constructor
	public PacketDriver(Node node, Network network) {
		
		super(node, network);
		
		Element elem = (Element)node;
		
		if (elem.hasAttribute("Packet_Server"))
			packetServerRef = elem.getAttribute("Packet_Server");
		else {
			Element packetServerElem = XMLparser.getChildElem(node, "Packet_Server").firstElement();
			packetServer = SimThread.newSchedulableResource(packetServerElem);
		}
		if (elem.hasAttribute("Packet_Send_Operation"))
			packetSendOperRef = elem.getAttribute("Packet_Send_Operation");
		else {
			Element packetSendOperElem = XMLparser.getChildElem(node, "Packet_Send_Operation").firstElement();
			packetSendOper = new SimpleOperation(packetSendOperElem);
		}
		if (elem.hasAttribute("Packet_Receive_Operation"))
			packetReceiveOperRef = elem.getAttribute("Packet_Receive_Operation");
		else {
			Element packetReceiveOperElem = XMLparser.getChildElem(node, "Packet_Receive_Operation").firstElement();
			packetReceiveOper = new SimpleOperation(packetReceiveOperElem);
		}
		if (elem.hasAttribute("Message_Partitioning"))
			messagePartitioning = elem.getAttribute("Message_Partitioning").equals("YES");							
		
		if (elem.hasAttribute("High_Utilization_Mode"))
			highUtilizationMode = elem.getAttribute("High_Utilization_Mode").equals("YES");	
	}
	
  	public void complete(){
		
		if (packetServerRef != null)
			packetServer = (SimThread)Repository.getModelElem(packetServerRef, "SchedulableResource");
		else // packetServer != null
			packetServer.complete();
  		
		if (packetSendOperRef != null)
  			packetSendOper = (SimpleOperation)Repository.getModelElem(packetSendOperRef, "Operation");
  		if (packetReceiveOperRef != null)
			packetReceiveOper = (SimpleOperation)Repository.getModelElem(packetReceiveOperRef, "Operation");
  		
  		// I set the host processor
  		hostProc = (ComputingResource)packetServer.getProcessingResource();
  		
  		// I complete the driver name
  		name += hostProc.name();
  		
  		// I create the 2 FEPs
  		FlowEndPoint senderFEP = new FlowEndPoint(name + "_inputSend", null);
  		FlowEndPoint finalFEP = new FlowEndPoint(name + "_outputSend", null);
  		Step activity = new Step(packetSendOper, packetServer);
  			activity.addInputFEP(senderFEP);
  			activity.addOutputFEP(finalFEP);
  		senderSegment = new Segment(activity);
  		senderMerge = new Merge();
  		senderMerge.setName(name + "_senderMerge");
  		senderFEP.setSource(senderMerge);
  		senderMerge.addOutputFEP(senderFEP);
  		
  		FlowEndPoint receiverFEP = new FlowEndPoint(name + "_inputReceive", null);
  		finalFEP = new FlowEndPoint(name + "_outputReceive", null);
  		activity = new Step(packetReceiveOper, packetServer);
  			activity.addInputFEP(receiverFEP);
  			activity.addOutputFEP(finalFEP);
  		receiverSegment = new Segment(activity);
  		receiverMerge = new Merge();
  		receiverMerge.setName(name + "_receiverMerge");
  		receiverFEP.setSource(receiverMerge);
  		receiverMerge.addOutputFEP(receiverFEP);
  	}
  	
	
	
	@Override
	public String toString(){
		
		String string = "";
		if (packetServerRef != null)
			string += packetServer.name();
		else
			string += packetServer.toString();
		
		string += "\nPacket send operation: "; 
		if (packetSendOperRef != null)
			string += packetSendOper.name();
		else
			string += packetSendOper.toString();
		
		string += "\nPacket receive operation: ";
		if (packetReceiveOperRef != null)
			string += packetReceiveOper.name();
		else
			string += packetReceiveOper.toString();

		string += "\nMessage partitioning: ";
		if (messagePartitioning)
			string += "YES";
		else
			string += "NO";

		string += "\nHigh utilization mode: ";
		if (highUtilizationMode)
			string += "YES";
		else
			string += "NO";
			
		return "\nPacket driver:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}
