package resource;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class VirtualTokenBucketCommChannelParams extends VirtualCommChannelParams {

	/* FIELDS */
	
	private int budget;
	private int maxThroughput;
	
	
	/* METHODS */
	
	// Constructor
	public VirtualTokenBucketCommChannelParams(Node node) {

		super(node);
		
		Element elem = (Element)node;

		if (elem.hasAttribute("Budget"))
			budget = Integer.parseInt(elem.getAttribute("Budget"));
		
		if (elem.hasAttribute("Max_Throughput"))
			maxThroughput = Integer.parseInt(elem.getAttribute("Max_Throughput"));
	}
}
