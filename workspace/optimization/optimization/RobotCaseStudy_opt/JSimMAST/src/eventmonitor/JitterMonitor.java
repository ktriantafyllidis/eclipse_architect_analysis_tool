/**                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: GlobalTimeMonitor
 * Description: Momnitor register the jitter statistical data
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package eventmonitor;

import java.util.ArrayList;
import java.util.List;

import core.Clock;

import flow.FlowEndPoint;
import flow.FlowEvent;
import flow.WorkloadEvent;

public class JitterMonitor extends Statistical_EM {

	/* ATTRIBUTES */
	
	private List<JitterData> jitterDataList;
 
    
    /* METHODS */
    
    // Constructor
	public JitterMonitor(FlowEndPoint fep){
		super(fep);
		jitterDataList = new ArrayList<JitterData>();
		for (WorkloadEvent wEvent : hostFEP.ownerTrans().getWorkloadEvents()) {
			jitterDataList.add(new JitterData(wEvent));
		}
	}
	
	public void event(FlowEvent fev) {
		for (JitterData data : jitterDataList) {
			data.event(fev);
		}
	}

	public String simTimingResultsAttr(String offset){return "";}
	
	public String simTimingResultsElem(String offset){
		String string = "";
		for (JitterData data : jitterDataList) {
			//if (data.previousGlobalTime != -1){
			if (data.numEvents != 0) {
				string += offset + "<mast_res:Max_Jitter Referenced_Event=\"" + data.referenced.name() + 
				                   "\" Value=\"" + Clock.timeSeconds(data.maxJitter) + "\"/>\n";
				string += offset + "<mast_res:Avg_Jitter Referenced_Event=\"" + data.referenced.name() + 
                                   "\" Value=\"" + Clock.timeSeconds(data.acumJitter/data.numEvents) + "\"/>\n";

			}
		}
		return string;
	}
	
	private class JitterData{
		
		/* ATTRIBUTES */
				
		// Previous global time. At first there is not previous global time
	    private long previousGlobalTime = -1; // ns
	    private long maxJitter;               // ns
	    private long acumJitter = 0;          // ns
		private long numEvents = 0;
	    private WorkloadEvent referenced;
		
	    
		/* METHODS */
		
		// Constructor
		private JitterData(WorkloadEvent wEvent){
			referenced = wEvent;
		}
		
		private void event(FlowEvent fev){
			long globalTime = fev.globalTime(referenced);
			if (globalTime >= 0){
				if (previousGlobalTime >= 0){
					long jitter = globalTime - previousGlobalTime;
					acumJitter+=jitter;
					numEvents++;
					if (numEvents == 1)
						maxJitter = jitter;
					else if (jitter > maxJitter)
						maxJitter = jitter; 
				}
				previousGlobalTime = globalTime;
			}
		}
	}
}
