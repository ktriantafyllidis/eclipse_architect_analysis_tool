/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: LocalTimeMonitor
 * Description: Generates a logging message for each registered event
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/
package eventmonitor;

import flow.*;
import core.*;
import module.*;

public class DisplayOnEvent extends Notification_EM {
 
	// Constructor
	public DisplayOnEvent(FlowEndPoint fep){
		super(fep);
	}
	
	public void event(FlowEvent fev) {
		Controller.mssgOutput(Tools.delimit(Clock.currentTime()) + " " + 
				              hostFEP.ownerTrans().name() + "/" + hostFEP.name() + " \"happened\"");
	}
}
