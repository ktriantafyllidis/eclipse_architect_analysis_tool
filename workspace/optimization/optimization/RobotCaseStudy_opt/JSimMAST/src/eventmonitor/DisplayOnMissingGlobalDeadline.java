/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: LocalTimeMonitor
 * Description: Generates a logging message for each registered event
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/
package eventmonitor;

import module.Tools;

import org.w3c.dom.Element;

import core.Clock;
import core.Controller;

import flow.*;

public class DisplayOnMissingGlobalDeadline extends DisplayOnMissingLocalDeadline {
 
	/* ATTRIBUTES */
	
	private WorkloadEvent referenced;
	
	
	/* METHODS */
	
	// Constructor
	public DisplayOnMissingGlobalDeadline(FlowEndPoint fep, Element timingReqElem){
		super(fep, timingReqElem);
		referenced = (WorkloadEvent)hostFEP.ownerTrans().getEventHandler(timingReqElem.getAttribute("Referenced_Event"));
	}
	
	@Override
	public void event(FlowEvent fev) {
		long globalTime = fev.globalTime(referenced);
		if (globalTime > deadline)
			Controller.mssgOutput(Tools.delimit(Clock.currentTime()) + " " + 
			                      hostFEP.ownerTrans().name() + "/" + hostFEP.name() + "-" + referenced.name() +
			                      " \"global deadline missed\"");
	}
}
