/**                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: LocalMissingRatioMonitor
 * Description: Monitor register the Global missing ratio statistical data
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package eventmonitor;

import module.Tools;

import org.w3c.dom.Element;

import flow.*;
import core.*;

public class LocalMissingRatioMonitor extends Statistical_EM {
    
	/* ATTRIBUTES */
	 
	protected long deadline = 0; // ns
	protected long numMissedDeadlines = 0;
    protected long numEvents = 0;
    //protected double maxRatio=0.0;
	
    /* METHODS */
	
    // Constructor
	public LocalMissingRatioMonitor(FlowEndPoint fep, Element timingReqElem){
		super(fep);
		deadline = Tools.readLong(timingReqElem.getAttribute("Deadline"));
		// I don't use
		//maxRatio = Double.parseDouble(timingReqElem.getAttribute("Ratio"));
	}
    
	public void event(FlowEvent fev) {
		numEvents++;
		if (fev.localTime() > deadline) 
			numMissedDeadlines++;
	}
	
	public String simTimingResultsAttr(String offset){return "";}
	
	public String simTimingResultsElem(String offset){
		return offset + "<mast_res:Local_Miss_Ratio Ratio=\"" + (missingRatio()*100.0) + 
		                "\" Deadline=\"" + Clock.timeSeconds(deadline) + "\"\n/>";
	}
	
	protected double missingRatio() {
		if (numEvents == 0) 
			return 0.0;
		else 
			return ((double)numMissedDeadlines) / ((double)numEvents);
	}
}
