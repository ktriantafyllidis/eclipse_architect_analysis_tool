/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: WaitingTimeMonitor
 * Description: Mmonitor register the awaiting Time statistical data
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package eventmonitor;

import flow.*;
import core.*;

public class WaitingTimeMonitor extends Statistical_EM {

	/* ATTRIBUTES */
	
	private long maxWaitingTime = 0; // ns
	private long acumWaitingTime = 0;  // ns
	private long numEvents = 0;
	
	/* METHODS */
	
	// Constructor
	public WaitingTimeMonitor(FlowEndPoint fep){
		super(fep);
	}
	
	public void event(FlowEvent fev) {
		acumWaitingTime+=fev.waitingTime();
		numEvents++;
	

		if (numEvents == 1)
			maxWaitingTime = fev.waitingTime();
		else if (fev.waitingTime() > maxWaitingTime)
			maxWaitingTime = fev.waitingTime(); 
	}
	
	public String simTimingResultsAttr(String offset){
		if (numEvents != 0)
			return "\n" + offset + "Max_Waiting_Time=\"" + Clock.timeSeconds(maxWaitingTime) + 
		           "\" Avg_Waiting_Time=\"" + Clock.timeSeconds(acumWaitingTime / numEvents) + "\"";
		else
			return "";
    }
    
    public String simTimingResultsElem(String offset){return "";}
		
}