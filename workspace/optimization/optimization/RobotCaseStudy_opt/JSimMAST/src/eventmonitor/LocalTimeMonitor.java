/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: LocalTimeMonitor
 * Description: Mmonitor register the Local Time statistical data
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package eventmonitor;

import core.Clock;
import flow.*;

public class LocalTimeMonitor extends Statistical_EM {

	/* ATTRIBUTES */
	
	private long worstLocalResponseTime = 0; // ns
	private long bestLocaResponseTime; // ns
	private long acumLocalResponseTime = 0; // ns
	private long numEvents = 0;
	private String nominalDeadline;
	private String fullFepName;
	
	
	/* METHODS */
	
	// Constructor
	public LocalTimeMonitor(FlowEndPoint fep, String nominalDeadline){
		super(fep);
		this.nominalDeadline = nominalDeadline;
		fullFepName = hostFEP.ownerTrans().name() + "/" + hostFEP.name();
	}

	public void event(FlowEvent fev) {
		acumLocalResponseTime += fev.localTime();
		numEvents++;
		if (numEvents == 1) {
			worstLocalResponseTime = fev.localTime();
			bestLocaResponseTime = fev.localTime();
		} else {
			if (fev.localTime() > worstLocalResponseTime)
				worstLocalResponseTime = fev.localTime();
			if (fev.localTime() < bestLocaResponseTime)
				bestLocaResponseTime = fev.localTime();
		}
	}

	public String simTimingResultsAttr(String offset){
		if (numEvents != 0)
			return "\n" + offset + "Worst_Local_Response_Time=\"" + Clock.timeSeconds(worstLocalResponseTime) + 
		           "\" Avg_Local_Response_Time=\"" + Clock.timeSeconds(acumLocalResponseTime / numEvents) + 
		           "\" Best_Local_Response_Time = \"" + Clock.timeSeconds(bestLocaResponseTime) + "\"";
		else
			return "";
	}
	
	public String simTimingResultsElem(String offset){return "";}
		
	@Override
	public String eventNum(){
		return "" + numEvents;
	}
	
	@Override
	public String worstResponseTime(){
		return "" + worstLocalResponseTime;
	}
	
	@Override
	public String nominalDeadline(){
		return nominalDeadline;
	}
	
	@Override
	public String confidenceLevel(){
		return "";
	}

	@Override
	public String fullFepName(){
		return fullFepName;
	}
}