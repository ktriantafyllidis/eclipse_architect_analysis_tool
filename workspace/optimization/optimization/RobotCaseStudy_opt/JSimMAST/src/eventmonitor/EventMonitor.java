/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: EventMonitor
 * Description: Abstract clas of the monitor associates tu FlowEndPoint, 
 * 		which register the data of the regietered events
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package eventmonitor; 

import flow.*;

public abstract class EventMonitor {
	
	/* ATTRIBUTES */

	protected FlowEndPoint hostFEP;
	
	
	/* METHODS */
	
	// Constructor
	protected EventMonitor(FlowEndPoint fep){
		hostFEP = fep;
	}
	
	/**
	 * This method is meant to be executed each time that the activation flow
	 * arrives to the FEP where the current event monitor is located,
	 * performing an appropriate action, e. g., 
	 * register a new trace in the logger. 
	 * @param fev
	 */
	public abstract void event(FlowEvent fev);
	
}