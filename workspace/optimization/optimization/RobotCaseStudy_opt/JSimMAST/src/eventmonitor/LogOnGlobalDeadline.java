/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: LogOnGlobalDeadline
 * Description: An entry is registered on the logger when a global
 *        deadline is not satisfied
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package eventmonitor;

import org.w3c.dom.Element;

import core.Logger;
import flow.*;

public class LogOnGlobalDeadline extends LogOnLocalDeadline {

	/* FIELDS */

	private WorkloadEvent referenced;
	

	/* METHODS */
		
	// Constructor
	public LogOnGlobalDeadline(FlowEndPoint fep, Element timingReqElem){
		super(fep, timingReqElem);
		referenced = (WorkloadEvent)hostFEP.ownerTrans().getEventHandler(timingReqElem.getAttribute("Referenced_Event"));
	}
	
	@Override
	public void event(FlowEvent fev) {
		if (fev.globalTime(referenced) > deadline)
			Logger.registerNewTrace(hostFEP.sID(), Logger.MISSED_GLOBAL_DEADLINE);
		else 
			Logger.registerNewTrace(hostFEP.sID(), Logger.MET_GLOBAL_DEADLINE);
	}

}
