/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: StopOnGlobalDeadline
 * Description: The stopping condition is satisfied when a predefined 
 *        number of event is generated in the flow end point.
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package eventmonitor;

import core.*;
import flow.*;

public class StopOnNumEvent extends Finalization_EM {

	/* FIELDS */
		
	private long numEvents = 0;
	
	
	/* METHODS */

	// Constructor
	public StopOnNumEvent(FlowEndPoint fep){
		super(fep);
		Controller.addConditionalStopMonitor(this);
	}
	
	public void event(FlowEvent fev) {
		numEvents++;
		stopCondition = numEvents > Controller.transExecution();
		if (stopCondition)
			Controller.verifyConditionalStop();
	}

}
