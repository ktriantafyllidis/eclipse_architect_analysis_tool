/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: LocalTimeMonitor
 * Description: Generates a logging message for each registered event
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/
package eventmonitor;

import module.Tools;

import org.w3c.dom.Element;

import core.Clock;
import core.Controller;

import flow.*;

public class DisplayOnMissingLocalDeadline extends Notification_EM {
 
	/* ATTRIBUTES */
	
	protected long deadline;
	
	
	/* METHODS */
	
	// Constructor
	public DisplayOnMissingLocalDeadline(FlowEndPoint fep, Element timingReqElem){
		super(fep);
		deadline = Tools.readLong(timingReqElem.getAttribute("Deadline"));
	}
	
	public void event(FlowEvent fev) {
		if (fev.localTime() > deadline)
			Controller.mssgOutput(Tools.delimit(Clock.currentTime()) + " " + 
		                          hostFEP.ownerTrans().name() + "/" + hostFEP.name() + 
		                          " \"local deadline missed\"");
	}
	
}
