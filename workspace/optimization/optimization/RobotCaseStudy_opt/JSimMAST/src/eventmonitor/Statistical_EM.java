package eventmonitor;

import flow.FlowEndPoint;

public abstract class Statistical_EM extends EventMonitor {

    /* METHODS*/
    
    // Constructor
	protected Statistical_EM(FlowEndPoint fep){
		super(fep);
	}
	
	public abstract String simTimingResultsAttr(String offset);
	
	public abstract String simTimingResultsElem(String offset);
	
	public String eventNum(){
		return "";
	}
	
	public String worstResponseTime(){
		return "";
	}
	
	public String nominalDeadline(){
		return "";
	}
	
	public String confidenceLevel(){
		return "";
	}

	public String fullFepName(){
		return "";
	}
}
