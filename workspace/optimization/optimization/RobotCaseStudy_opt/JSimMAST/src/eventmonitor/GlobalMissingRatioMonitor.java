/**                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: GlobalMissingRatioMonitor
 * Description: Monitor register the Global missing ratio statistical data
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package eventmonitor;

import flow.*;
import org.w3c.dom.*;

import core.*;

public class GlobalMissingRatioMonitor extends LocalMissingRatioMonitor {

	/* ATTRIBUTES */
	
	private WorkloadEvent referenced;
	
	
    /* METHODS */
	
	// Constructor
	public GlobalMissingRatioMonitor(FlowEndPoint fep, Element timingReqElem){
		super(fep, timingReqElem);
		referenced = (WorkloadEvent)hostFEP.ownerTrans().getEventHandler(timingReqElem.getAttribute("Referenced_Event"));
	}
	
	@Override
	public void event(FlowEvent fev) {
		numEvents++;
		if (fev.globalTime(referenced) > deadline) 
			numMissedDeadlines++;
	}
	
	@Override
	public String simTimingResultsElem(String offset){
		return offset + "<mast_res:Global_Miss_Ratio Referenced_Event=\"" + referenced.name() + 
		                "\" Ratio=\"" + (missingRatio()*100.0) + "\" Deadline=\"" + Clock.timeSeconds(deadline) + "\"/>\n";
	}
}