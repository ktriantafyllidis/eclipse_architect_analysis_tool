package eventmonitor;

import core.Logger;
import flow.FlowEndPoint;
import flow.Fork;
import flow.Segment;

public abstract class LoggingMonitor extends EventMonitor{
	
	/* METHODS */
	
	// Constructor
	protected LoggingMonitor(FlowEndPoint fep){
		super(fep);
		/* 
		 * I have to check if a source corresponding to
		 * the host FEP has already been registered in the Logger (by another LoggingEM), i. e., 
		 * if the host FEP has an ID assigned.
		 * If not, I register a new source and get an ID  
		 */ 
		if (fep.source() instanceof Segment) {
			fep.setSID(((Segment)fep.source()).sid());
		}
		
		/*if (fep.sID() == -1){ // not ID assigned
			String sourceName = fep.name();
			if (fep.ownerTrans() != null)
				sourceName = fep.ownerTrans().name() + "/" + fep.name();		
			int id = Logger.registerNewSource(sourceName, srcType);
			fep.setSID(id);
		}*/
	}
}
