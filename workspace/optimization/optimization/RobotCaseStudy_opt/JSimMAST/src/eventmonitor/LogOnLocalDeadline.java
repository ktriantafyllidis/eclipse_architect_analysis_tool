/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: LogOnLocalDeadline
 * Description: An entry is registered on the logger when a local 
 *        deadline is not satisfied
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package eventmonitor;

import module.Tools;

import org.w3c.dom.Element;

import core.Logger;
import flow.*;

public class LogOnLocalDeadline extends LoggingMonitor {
	
	/* FIELDS */
		
	protected long deadline = 0;

		
	/* METHODS */
	
	// Constructor
	public LogOnLocalDeadline(FlowEndPoint fep, Element timingReqElem){
		//super(fep, Logger.SrcType.EventWithHardDeadline);
		super(fep);
		deadline = Tools.readLong(timingReqElem.getAttribute("Deadline"));
	}
	
	public void event(FlowEvent fev) {
		if (fev.localTime() > deadline)
			Logger.registerNewTrace(hostFEP.sID(), Logger.MISSED_LOCAL_DEADLINE);
		else
			Logger.registerNewTrace(hostFEP.sID(), Logger.MET_LOCAL_DEADLINE);
	}
}
