/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: BlockingTimeMonitor
 * Description: Mmonitor register the Blocking Time statistical data
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package eventmonitor;

import flow.*;

import core.Clock;

public class BlockingTimeMonitor extends Statistical_EM {

	/* ATTRIBUTES */
	
	private long maxBlockingTime = 0; // ns
	private long acumBlockingTime = 0;  // ns
	private long numEvents = 0;
	
	
	/* METHODS */
	
	// Constructor
	public BlockingTimeMonitor(FlowEndPoint fep){
		super(fep);
	}

	public void event(FlowEvent fev) {
		acumBlockingTime += fev.blockingTime();
		numEvents++;
		if (numEvents == 1)
			maxBlockingTime = fev.blockingTime();
		else if (fev.blockingTime() > maxBlockingTime)
			maxBlockingTime = fev.blockingTime(); 
	}
	
	public String simTimingResultsAttr(String offset){
		if (numEvents != 0)
			return "\n" + offset + "Max_Blocking_Time=\"" + Clock.timeSeconds(maxBlockingTime) + 
		           "\" Avg_Blocking_Time=\"" + Clock.timeSeconds(acumBlockingTime / numEvents) + "\"";
		else
			return "";
    }
	
	public String simTimingResultsElem(String offset){return "";}
		
}