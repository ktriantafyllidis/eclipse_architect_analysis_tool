/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: Join
 * Description: It is an event handler that generates its output event
 *    when all of its input events have arrived. For worst-case
 *    analysis to be possible it is necessary that all the inputevents
 *    are periodic with the same periods. This usually represents no
 *    problem if the concentrator is used to perform a �join� operation
 *    after a �fork� operation carried out with the Multicast event
 *    handler (see below).
 * 
 * Author: J.M. Drake y F.Aguilar
 * Date: 1-1-09
 ********************************************************************/

package flow;

import java.util.List;

import module.Tools;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class Join extends EventHandler {
	
	/* METHODS */
	
	// Constructor
	public Join(Node node, RegularEndToEndFlow trans) {
		super(node, trans);
	}

	public void complete(){
		
		Element elem = (Element)node;
		
		// FEPs for input events
		List<String> inputEventNameList = Tools.parses(elem.getAttribute("Input_Events_List"));
		FlowEndPoint fep;
		for (String inputEventName : inputEventNameList) {
			fep = ownerTrans.getFEP(inputEventName);
			if (fep != null) {
				fep.setTarget(this);
				inputFepList.add(fep);
			} else {
				// TODO a join has an input event that does not exist
			}
		}
		// FEP for output event
		fep = ownerTrans.getFEP(elem.getAttribute("Output_Event"));
		if (fep != null) {
			fep.setSource(this);
			outputFepList.add(fep);
		} else {
			// TODO a join has an output event that does not exist
		}
	}
	
	public void postComplete(){}
	
  	public void arriveNewEvent() {
		// Break if anyone input has not event
		for(int i = 0; i < inputFepList.size(); i++){
			if(inputFepList.get(i).numPendingFlowEvent() == 0) return;
		}
		FlowEvent ev = inputFepList.get(0).pollPendingFlowEvent();
		for(int i = 1; i < inputFepList.size(); i++){
			ev.nestedFlowEventList().add(inputFepList.get(i).pollPendingFlowEvent());
		}
		outputFepList.get(0).addFlowEvent(ev); //A�ado un evento de salida, y el resto asociados
	}		



  	@Override
	public String toString(){	
		String string = "\nInput feps: ";
		for (FlowEndPoint fep : inputFepList) {
			string += "\n\t" + fep.name();
		}
		string += "\nOutput fep: " + outputFepList.get(0).name();
		return "\nJoin:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}
