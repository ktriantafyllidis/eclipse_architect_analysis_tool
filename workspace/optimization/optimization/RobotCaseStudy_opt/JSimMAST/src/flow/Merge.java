/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: Merge
 * Description: It is an event handler that generates its output event
 *      when any one of its input events arrives.
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package flow;

import java.util.List;

import module.Tools;

import org.w3c.dom.*;

public class Merge extends EventHandler {
	
	/* METHODS */
	
	// Constructors
	public Merge(){};
	public Merge(Node node, RegularEndToEndFlow trans) {
		super(node, trans);
	}

	public void complete(){
		
		Element elem = (Element)node;
		
		// FEPs for input events
		List<String> inputEventNameList = Tools.parses(elem.getAttribute("Input_Events_List"));
		FlowEndPoint fep;
		for (String inputEventName : inputEventNameList) {
			fep = ownerTrans.getFEP(inputEventName);
			if (fep != null) {
				fep.setTarget(this);
				inputFepList.add(fep);
			} else {
				// TODO a merge has an input event that does not exist
			}
		}
		// FEP for output event
		fep = ownerTrans.getFEP(elem.getAttribute("Output_Event"));
		if (fep != null) {
			fep.setSource(this);
			outputFepList.add(fep);
		} else {
			// TODO a merge has an output event that does not exist
		}
	}
	
	public void postComplete(){}
	
  	// It is invoked when a new event is arrived to the event handler
  	public void arriveNewEvent() {
		//Busco por la lista de entradas la que ha generado el evento y lo lanzo por su salida
		for(int i = 0; i < inputFepList.size(); i++){
			if (inputFepList.get(i).numPendingFlowEvent() > 0){
				outputFepList.get(0).addFlowEvent(inputFepList.get(i).pollPendingFlowEvent());
				break;
			}
		}
  	}




  	@Override
	public String toString(){	
		String string = "\nInput feps: ";
		for (FlowEndPoint fep : inputFepList) {
			string += "\n\t" + fep.name();
		}
		string += "\nOutput fep: " + outputFepList.get(0).name();
		return "\nMerge:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}
