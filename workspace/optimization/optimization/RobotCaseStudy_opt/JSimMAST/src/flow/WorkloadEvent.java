/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: WorkloadEvent
 * Description: Abstract clas of the tiemed event generators
 * Author: J.M. Drake
 * Date: 28-1-2010
 ********************************************************************/

package flow;

import module.AperiodicITG;
import module.BoundedITG;
import module.BurstyITG;
import module.ConstantITG;
import module.ExponentialDistributionParameter;
import module.ITG;
import module.SingularITG;
import module.Tools;
import module.UniformDistributionParameter;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import timers.TimingObject;
import core.Clock;
import core.Controller;
import core.Logger;
import core.Repository;
import core.Timed;
import core.Controller.ProfileType;

public class WorkloadEvent extends EventHandler implements Timed {

	/* ENUMS */
	
	private enum WorkloadEventType {Periodic_Event, 
		                            Sporadic_Event, 
		                            Unbounded_Event, 
		                            Bursty_Event, 
		                            Singular_Event}
	
	
	/* FIELDS */
	
	private Node node;
	
	// The ITG defines the timing of generating event 
	private ITG itg;
	
	//
	private TimingObject timer;
	
	
	/* METHODS */
	
	// Constructor
	public WorkloadEvent(Node node, RegularEndToEndFlow trans){
		
		//super(trans);
		this.node = node;
		Element elem = (Element)node;
		name = elem.getAttribute("Name");
		ownerTrans = trans;
		
		// I create a FEP
		FlowEndPoint fep = new FlowEndPoint(name + "_auxFEP", trans);
		fep.setSource(this);
		outputFepList.add(fep);
		ownerTrans.addFEP(fep);
			
		// I create a fork
		Fork auxFork = new Fork();
		auxFork.setName(name + "_auxFork");
		auxFork.setOwnerTrans(trans);
		fep.setTarget(auxFork);
		auxFork.addInputFEP(fep);
		ownerTrans.addEventHandler(auxFork);
		
		// I create another FEP
		fep = new FlowEndPoint(name, trans);
		fep.setSource(auxFork);
		auxFork.addOutputFEP(fep);
		ownerTrans.addFEP(fep);
		
		// I register a source in the Logger
		if (Controller.profile() == ProfileType.TRACES)
			fep.setSID(Logger.registerNewSource(ownerTrans.name() + "/" + name, Logger.SrcType.WorkloadEvent));
			
		// I instantiate the itg field, depending on the type of the external event
		WorkloadEventType type = WorkloadEventType.valueOf(node.getLocalName());
		switch (type) {
		case Periodic_Event: // I create a constant ITG
			long period = 0;
			if(elem.hasAttribute("Period"))
				period = Tools.readLong(elem.getAttribute("Period"));
			long phase = 0;
			if(elem.hasAttribute("Phase"))
				phase = Tools.readLong(elem.getAttribute("Phase"));
			long maxJitter = 0;
			if(elem.hasAttribute("Max_Jitter"))
				maxJitter = Tools.readLong(elem.getAttribute("Max_Jitter"));
			
			itg = new ConstantITG(phase, period, maxJitter);
			
			// I look for the "Timer" attribute into the complete() method
			
			// TODO manage synchronized attribute
			if(elem.hasAttribute("Synchronized")){}
			
			break;
		case Sporadic_Event: // I create a bounded ITG
			long avgInterarrival = 0;
			if(elem.hasAttribute("Avg_Interarrival"))
				avgInterarrival = Tools.readLong(elem.getAttribute("Avg_Interarrival"));
			long minInterarrival = 0;
			if(elem.hasAttribute("Min_Interarrival"))
				minInterarrival = Tools.readLong(elem.getAttribute("Min_Interarrival"));
			if(elem.hasAttribute("Distribution"))
				if(elem.getAttribute("Distrubution").equals("POISSON")) 
					itg = new BoundedITG(new ExponentialDistributionParameter(avgInterarrival),
						  	             minInterarrival,
						  	             0); // In Mast model firstTime is zero 						  	
				else // UNIFORM
					itg = new BoundedITG(new UniformDistributionParameter(avgInterarrival),
						  				 minInterarrival,
						  				 0);
			// TODO if no attr distribution
			break;
		case Unbounded_Event: // I create an aperiodic ITG
			avgInterarrival = 0;
			if(elem.hasAttribute("Avg_Interarrival"))
				avgInterarrival = Tools.readLong(elem.getAttribute("Avg_Interarrival"));
			if(elem.hasAttribute("Distribution"))
				if(elem.getAttribute("Distrubution").equals("POISSON")) 
					itg = new AperiodicITG(new ExponentialDistributionParameter(avgInterarrival),
						  				   0);
				else // UNIFORM
					itg = new AperiodicITG(new UniformDistributionParameter(avgInterarrival),
						  	               0);
			// TODO if no attr distribution
			break;
		case Bursty_Event: // I create a bursty ITG
			avgInterarrival = 0;
			if(elem.hasAttribute("Avg_Interarrival"))
				avgInterarrival = Tools.readLong(elem.getAttribute("Avg_Interarrival"));
			long boundInterval = 0;
			if(elem.hasAttribute("Bound_Interval"))
				boundInterval = Tools.readLong(elem.getAttribute("Bound_Interval"));
			int maxArrivals = 1;
			if(elem.hasAttribute("Max_Arrivals"))
				maxArrivals = Integer.parseInt(elem.getAttribute("Max_Arrivals"));
			if(elem.hasAttribute("Distribution"))
				if(elem.getAttribute("Distrubution").equals("POISSON")) 
					itg = new BurstyITG(new ExponentialDistributionParameter(avgInterarrival),
						  	            boundInterval,
						  	            maxArrivals,
						  	            0);
				else // UNIFORM
					itg = new BurstyITG(new UniformDistributionParameter(avgInterarrival),
						  	            boundInterval,
						  	            maxArrivals,
						  	            0);
			// TODO if no attr distribution
			break;
		default: //case Singular_Event:  // I create a singular ITG
			phase = 0;
			if(elem.hasAttribute("Phase"))
				phase = Tools.readLong(elem.getAttribute("Phase"));
			itg = new SingularITG(phase);
			
			// I look for the "Timer" attribute into the complete() method
			
			break;		
		}
		Clock.addTimedEvent(itg.next(), this);
	}
	
	public void complete() {
		Element elem = (Element)node;
		if(elem.hasAttribute("Timer")) {		
			timer = (TimingObject)Repository.getModelElem(elem.getAttribute("Timer"), "Timer");
			outputFepList.get(0).setTimer(timer);
		}
	}
		
	public void postComplete(){}
	
	public void arriveNewEvent() {} // An external event has not input flow event

	public void update() {
		FlowEvent ev = FlowEvent.newFlowEvent(this);
		outputFepList.get(0).addFlowEvent(ev);
		Clock.addTimedEvent(itg.next(), this);
	}
		
	@Override
	public String toString(){
		
		String string = "\nType: ";
		
		WorkloadEventType type = WorkloadEventType.valueOf(node.getLocalName());
		switch (type) {
		case Periodic_Event:
			string += "periodic" + 
			          "\nTimer: ";
			if (timer != null)
				string += timer.name();
			break;
		case Sporadic_Event:
			string += "sporadic";
			break;
		case Unbounded_Event:
			string += "unbounded";
			break;
		case Bursty_Event:
			string += "bursty";
			break;
		default: //case Singular_Event:
			string += "singular" + 
	                  "\nTimer: ";
			if (timer != null)
				string += timer.name();
			break;		
		}
		string += "\nInput fep: " +
        		  "\nOutput fep: " + outputFepList.get(0).name() + 
        		  "\nInterarrival:" + itg.toString(); 
		return "\nWorkload event:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}
