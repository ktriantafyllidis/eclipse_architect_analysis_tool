/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: FlowEndPoint
 * Description: Contains the flow events genrates inside of a transaction. 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package flow;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import timers.AlarmClock;
import timers.Ticker;
import timers.TimingObject;
import utilityClasses.XMLparser;
import core.Clock;
import eventmonitor.EventMonitor;
import eventmonitor.Statistical_EM;

public class FlowEndPoint {
	
	/* ENUMS */
	private enum TimingRequirementType {Max_Output_Jitter_Req, 
		                                Hard_Global_Deadline, 
		                                Soft_Global_Deadline, 
		                                Hard_Local_Deadline, 
		                                Soft_Local_Deadline, 
		                                Global_Max_Miss_Ratio, 
		                                Local_Max_Miss_Ratio, 
		                                Queue_Size_Req,
		                                Composite_Observer}
	
	
	/* FIELDS */
  
	private String name;
		public String name() {return name;}

	private Node node;
		public Node node() {return node;}
  
	//
	private int sID = -1;
		public int sID() {return sID;}
		public void setSID(int sID) {this.sID = sID;}
	
	// Reference to the transaction of which is part
	private RegularEndToEndFlow ownerTrans;
		public RegularEndToEndFlow ownerTrans() {return ownerTrans;}
		
	// Reference to the event handler of which is input.
	private EventHandler target;
		public EventHandler target() {return target;}
		public void setTarget(EventHandler target) {this.target = target;}
  
	// Reference to the event handler of which is output.
	private EventHandler source;
		public EventHandler source() {return source;}
		public void setSource(EventHandler source) {this.source = source;}
  
	//Used system timer. If the timer is an AlarmClock the event handler execute the attend()method,
	// and if the timer is a Ticker, the event handler system is registered a listener of it.
	private TimingObject timer;
		public TimingObject timer(){return timer;}
		public void setTimer(TimingObject timer){this.timer = timer;}
		
	// List of associated event monitors 
	private List<EventMonitor> eventMonitorList = new ArrayList<EventMonitor>();
		public void addEventMonitor(EventMonitor eventMonitor){eventMonitorList.add(eventMonitor);}
		public boolean thereAreStatisticalMonitors(){
			for (EventMonitor monitor : eventMonitorList) {
				if (monitor instanceof Statistical_EM)
					return true;
			}
			return false;
		}

	// Queue of flow events pending to be attended. Is ordered by arriving time.
	private LinkedList<FlowEvent> pendingFlowEventList = new LinkedList<FlowEvent>(); // TODO pasar a PriorityQueue 
		public int numPendingFlowEvent() {return pendingFlowEventList.size();}
		public FlowEvent pollPendingFlowEvent() {
			FlowEvent ev = pendingFlowEventList.poll();
	  		ev.setActivationTime(Clock.currentTime());
	  		return ev;
	  	}
	  	
	// Stores received events waiting for system timer tick
	private LinkedList<FlowEvent> lastEvents = new LinkedList<FlowEvent>();
		//public int numLastFlowEvent() {return lastEvents.size();}
  
	// Requirements
	private List<Element> maxOutputJitterElemList;
		public boolean hasMaxOutputJitters(){return maxOutputJitterElemList != null;}
		public Iterator<Element> getMaxOutputJitterIter(){return maxOutputJitterElemList.iterator();}
	private List<Element> hardGlobalDeadlineElemList;
		public boolean hasHardGlobalDeadlines(){return hardGlobalDeadlineElemList != null;}
		public Iterator<Element> getHardGlobalDeadlineIter(){return hardGlobalDeadlineElemList.iterator();}
	private List<Element> hardLocalDeadlineElemList;
		public boolean hasHardLocalDeadlines(){return hardLocalDeadlineElemList != null;}
		public Iterator<Element> getHardLocalDeadlineIter(){return hardLocalDeadlineElemList.iterator();}
	private List<Element> softGlobalDeadlineElemList;
	private List<Element> softLocalDeadlineElemList;
		//public boolean hasSoftLocalDeadlines(){return softLocalDeadlineElemList != null;}
	private List<Element> localMaxMissRatioElemList;
		public boolean hasLocalMaxMissRatios(){return localMaxMissRatioElemList != null;}
		public Iterator<Element> getLocalMaxMissRatioIter(){return localMaxMissRatioElemList.iterator();}
	private List<Element> globalMaxMissRatioElemList;
		public boolean hasGlobalMaxMissRatios(){return globalMaxMissRatioElemList != null;}
		public Iterator<Element> getGlobalMaxMissRatioIter(){return globalMaxMissRatioElemList.iterator();}
	private List<Element> queueSizeElemList;
		public boolean hasReqs(){
			if ((softGlobalDeadlineElemList != null && softGlobalDeadlineElemList.size() > 0) || 
					(hardGlobalDeadlineElemList != null && hardGlobalDeadlineElemList.size() > 0) || 
					(softLocalDeadlineElemList != null && softLocalDeadlineElemList.size() > 0) || 
					(hardLocalDeadlineElemList != null && hardLocalDeadlineElemList.size() > 0) || 
					(globalMaxMissRatioElemList != null && globalMaxMissRatioElemList.size() > 0) || 
					(localMaxMissRatioElemList != null && localMaxMissRatioElemList.size() > 0) || 
					(maxOutputJitterElemList != null && maxOutputJitterElemList.size() > 0) || 
					(queueSizeElemList != null && queueSizeElemList.size() > 0)) {
				return true;
			}
			return false;
		}
		

	/* METHODS */

	// Constructor
	public FlowEndPoint(String name, RegularEndToEndFlow trans) {
		this.name = name;
		ownerTrans = trans;
	}
	
	public FlowEndPoint(Node node, RegularEndToEndFlow trans){
		
		this.node = node;
		Element elem = (Element)node;
		name = elem.getAttribute("Name");
		ownerTrans = trans;
		
		// Timing reqs
		List<Element> timingReqElemList = XMLparser.getChildElem(node);
		for (Element timingReqElem : timingReqElemList) {
			TimingRequirementType type = TimingRequirementType.valueOf(timingReqElem.getLocalName());
			switch (type) {
			case Hard_Global_Deadline:
				if (hardGlobalDeadlineElemList == null)
					hardGlobalDeadlineElemList = new ArrayList<Element>();
				hardGlobalDeadlineElemList.add(timingReqElem);
				break;
			case Soft_Global_Deadline:
				if (softGlobalDeadlineElemList == null)
					softGlobalDeadlineElemList = new ArrayList<Element>();
				softGlobalDeadlineElemList.add(timingReqElem);				
				break;
			case Hard_Local_Deadline:
				if (hardLocalDeadlineElemList == null)
					hardLocalDeadlineElemList = new ArrayList<Element>();
				hardLocalDeadlineElemList.add(timingReqElem);
				break;
			case Soft_Local_Deadline:
				if (softLocalDeadlineElemList == null)
					softLocalDeadlineElemList = new ArrayList<Element>();
				softLocalDeadlineElemList.add(timingReqElem);
				break;
			case Global_Max_Miss_Ratio:
				if (globalMaxMissRatioElemList == null)
					globalMaxMissRatioElemList = new ArrayList<Element>();
				globalMaxMissRatioElemList.add(timingReqElem);
				break;
			case Local_Max_Miss_Ratio:
				if (localMaxMissRatioElemList == null)
					localMaxMissRatioElemList = new ArrayList<Element>();
				localMaxMissRatioElemList.add(timingReqElem);
				break;
			case Max_Output_Jitter_Req:
				if (maxOutputJitterElemList == null)
					maxOutputJitterElemList = new ArrayList<Element>();
				maxOutputJitterElemList.add(timingReqElem);
				break;
			case Queue_Size_Req:
				if (queueSizeElemList == null)
					queueSizeElemList = new ArrayList<Element>();
				queueSizeElemList.add(timingReqElem);
				break;
			default: // Composite_Observer
				break;
			}
		}
	}
	
	// Execute adding the arriving flow event
	public void tick(){
		FlowEvent ev = lastEvents.pollFirst();
		Iterator<EventMonitor> iter = eventMonitorList.iterator();
		while(iter.hasNext()){
			iter.next().event(ev);
		}
		ev.setArrivingTime(Clock.currentTime());
		ev.setOwner(target);
		ev.setBlockingTime(0);
		ev.setBlockingInstant(-1);                   //JMD
		ev.setSuspensionTime(0);
		ev.setSuspensionInstant(-1);                 //JMD
		ev.setActProcessingInstant(-1);              //JMD
		ev.setExecutionTim(0);
	
		pendingFlowEventList.add(ev);
		if (target!=null)
			target.arriveNewEvent();
	    else {
	    	ev.freeFlowEvent();
	    	pendingFlowEventList.remove();
	    }
	}
	
	/**
	 * This method adds a new flow event to the pending queue
	 * @param fev
	 */
	public void addFlowEvent(FlowEvent fev){
		lastEvents.add(fev);
		if (timer != null){
		    if (timer instanceof Ticker)
				((Ticker)timer).addListener(this);
			else {
				((AlarmClock)timer).attend();
				tick();
			}
		} else 
			tick();
	}
  
	public String simulationTimingResults(String offset){
		
		String string = "";
		
		/*
		 * I want that the current FEP returns a string != "" only if it is a relevant FEP. 
		 * These kind of FEPs are characterized because they are the only 
		 * ones that have statistical event monitors aggregated, being those
		 * monitors the generation source of simulation timing results.
		 */
		if (thereAreStatisticalMonitors()) {
			string = offset + "<mast_res:Simulation_Timing_Results Event=\"" + name + "\"";			
			// Attributes after Event=""
			for (EventMonitor monitor : eventMonitorList) {
				if (monitor instanceof Statistical_EM) 
					string += ((Statistical_EM)monitor).simTimingResultsAttr(offset + "\t"); // This has sense for AwaitingtimeMonitor, etc
			}
			string += ">\n";
			// <Children>
			for (EventMonitor monitor : eventMonitorList) {
				if (monitor instanceof Statistical_EM) 
					string += ((Statistical_EM)monitor).simTimingResultsElem(offset + "\t"); // This has sense for GlobalTimeMonitor, etc
			}
			string += offset + "</mast_res:Simulation_Timing_Results>\n";
		}
		return string;
	}
	
	@Override
  	public String toString(){
  		  		
  		String string = "\nName:" + name + 
  		                "\nOwner trans: "; 
  		if (ownerTrans != null)
			string += ownerTrans.name();
  		
  		string += "\nSource eh: "; 
  		if (source != null)
  		    string += source.name();
  		               
  		string += "\nTarget eh: ";
  		if (target != null)
			string += target.name();
			
		string += "\nTimer: ";
  		if (timer != null)
			string += timer.name(); 
  		
		string += "\nEvent monitor list:";
  		for (EventMonitor eventMonitor : eventMonitorList) {
			string += "\t" + eventMonitor.toString(); 
		}
  		return "\nFEP" + string.replaceAll("\n", "\n\t");
  	}

	
}
