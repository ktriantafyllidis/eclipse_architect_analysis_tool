/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: EventHandler Controller
 * Description: Abstract class root of the event handler of a transaction. 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package flow;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Node;

public abstract class EventHandler {

	/* ENUMS */
	
	private enum EventHandlerType {Step,
		                           Merge, 
		                           Join, 
		                           Branch, 
		                           //Query_Server, 
		                           Fork, 
		                           Rate_Divisor, 
		                           Delay, 
		                           Offset, 
		                           Message_Fork, 
		                           Message_Delivery, 
		                           Message_Branch}
	
	
	/* ATTRIBUTES */
  
	protected Node node;

	// Reference to the transaction owner of the eventHandler
	protected RegularEndToEndFlow ownerTrans;
		public RegularEndToEndFlow ownerTrans() {return ownerTrans;}
		public void setOwnerTrans(RegularEndToEndFlow ownerTrans){this.ownerTrans = ownerTrans;}
	
	// Name of the event handler. If the event handler has no name, its value is a empty string
	protected String name;
		public String name(){return name;}
		public void setName(String name){this.name = name;}
	
	// 
	protected List<FlowEndPoint> inputFepList = new ArrayList<FlowEndPoint>();
		public FlowEndPoint getInputFEP(int i){return inputFepList.get(i);}
		public void addInputFEP(FlowEndPoint fep){inputFepList.add(fep);}
		public void setInputFepList(List<FlowEndPoint> fepList){inputFepList = fepList;}

	// 
	protected List<FlowEndPoint> outputFepList = new ArrayList<FlowEndPoint>();
		public FlowEndPoint getOutputFEP(int i){return outputFepList.get(i);}
		public void addOutputFEP(FlowEndPoint fep){outputFepList.add(fep);}
		public void setOutputFepList(List<FlowEndPoint> fepList){outputFepList = fepList;}

	// For automatic generation of EventHandler name
	static int index = 0; 
	
	
	/* METHODS */
	
	// Constructors
	protected EventHandler(){}
	protected EventHandler(Node node, RegularEndToEndFlow trans){
		this.node = node;
		ownerTrans = trans;
		name = "EventHandler_" + index++;
	}
	
	public static EventHandler newEventHandler(Node node, RegularEndToEndFlow trans){
	  
		EventHandlerType type = EventHandlerType.valueOf(node.getLocalName());
		switch (type) {
	  	case Step:
	  		return new Step(node, trans);
	  	case Merge:
	  		return new Merge(node, trans);
	  	case Join:
	  		return new Join(node, trans);
	  	case Branch:
	  		return new Branch(node, trans);
//	  	case Query_Server:
//	  		return new QueryServer(node, trans);
	  	case Fork:
	  		return new Fork(node, trans);
	  	case Rate_Divisor:
	  		return new RateDivisor(node, trans);
	  	case Offset:
	  		return new Offset(node, trans);
	  	default: 
	  /*case Delay: 
	    case Message_Delivery:
	    case Message_Fork:
	    case Message_Branch:*/
	  		return new Delay(node, trans);
		}
		//eh.name = "EventHandler_" + index++;
		//eh.owner = transaction;
	}

	/**
	 * Links the event handler with its input and output FEPs and stores them 
	 */
	public abstract void complete();
	
	public abstract void postComplete();
	
	/**
	 * This method is conceived to allow an input FEP to report when 
	 * a new flow event has arrived to the event handler. 
	 */
	public abstract void arriveNewEvent();




	@Override
	public String toString(){
		String string = "\nName: " + name + 
		                "\nOwner transaction: ";
		if (ownerTrans != null)
			string += ownerTrans.name();
		return string;
	}
}
