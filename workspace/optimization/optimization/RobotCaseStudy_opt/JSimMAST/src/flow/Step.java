/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: Activity
 * Description: It represents an instance of an operation, to be
 *      executed by a scheduling server. Every activity is aggregated
 *      in a segment
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package flow;

import resource.*;

import java.util.*;

import module.*;
import operation.*;
import core.*;
import org.w3c.dom.*;

public class Step extends EventHandler {
 
	/* FIELDS */
	
	// 
	private Operation operation;
		public Operation operation() {return operation;}
  
	//
	private SimThread server;
		public SimThread server() {return server;}
	
	//
	private ETG etg;
		public ETG etg() {return etg;}
		public void setETG(ETG etg) {this.etg = etg;}
	
	// List of the shared resources that are locked before the activity is executed.
	private List<MutualExclusionResource> lockList = new ArrayList<MutualExclusionResource>();
		public List<MutualExclusionResource> getLockList(){return lockList;}
		public void setLockList(List<MutualExclusionResource> list){lockList = list;}
  
	// List of shared resources that are unlocked after the activity is executed. 
	private List<MutualExclusionResource> unlockList = new ArrayList<MutualExclusionResource>();
		public List<MutualExclusionResource> getUnlockList(){return unlockList;}
		public void setUnlockList(List<MutualExclusionResource> list){lockList = list;}
  	
	//private boolean lockSchedulableResource = true;
	//private boolean unlockSchedulableResource = true;
	
	
	/* METHODS */
  
	// Constructors
		
	/**
	 * In this constructor the ETG is not set. It will be set into the deploy() method
	 * @param node
	 * @param trans
	 */
	public Step(Node node, RegularEndToEndFlow trans) {
		super(node, trans);
	}

	// Used inside of a segment.
	/**
	 * This constructor is used inside the deploy() method and when
	 * completing packet drivers.
	 * The ETG is set.
	 * @param operation
	 * @param server
	 */
	public Step(Operation oper, SimThread server){
		super(null, null);
		// TODO feps para enlazar???
		operation = oper;
		this.server = server;
		if (operation.getClass() == SimpleOperation.class){
			etg = ((SimpleOperation)operation).etg().clone();
			Iterator<String> iter = ((SimpleOperation)operation).getLockIter();
			if (iter != null)
				while(iter.hasNext()) {
					lockList.add((MutualExclusionResource)Repository.getModelElem(iter.next(), "MutualExclusionResource"));
				}
			iter = ((SimpleOperation)operation).getUnlockIter();
			if (iter != null)
				while(iter.hasNext()) {
					unlockList.add((MutualExclusionResource)Repository.getModelElem(iter.next(), "MutualExclusionResource"));
				}
		}
	}
  
	// Used inside of a message dispatcher
	public Step(long operationTime) {
		operation = new Message(operationTime);
		etg = ((Message)operation).etg();                // JMD
	} 

	public void complete(){
		
		Element elem = (Element)node;
		
		// I get the step inputFEP & outputFEP from the owner transaction
		
		FlowEndPoint fep = ownerTrans.getFEP(elem.getAttribute("Input_Event"));
		fep.setTarget(this);
		inputFepList.add(fep);
		
		fep = ownerTrans.getFEP(elem.getAttribute("Output_Event"));
		fep.setSource(this);
		outputFepList.add(fep);
		
		// I point to the step server & step operation, which are in the repository 
		server = (SimThread)Repository.getModelElem(elem.getAttribute("Step_Schedulable_Resource"), "SchedulableResource");
		operation = (Operation)Repository.getModelElem(elem.getAttribute("Step_Operation"), "Operation");
	}
	
	public void postComplete(){
		/* 
		 * I look if the current step starts a series of consecutive steps sharing the same server.
		 * If so, I create a new segment / message dispatcher, which will encompass all the steps 
		 * in the series (the current step and the subsequent ones), 
		 * adding it to the event handlers of the owner transaction (of the current step) 
		 */
		//ownerTrans.clearTempSegmentMap();
		EventHandler preEH = inputFepList.get(0).source();
		if (!(preEH instanceof Step) || (preEH instanceof Step) && 
			                            (!((Step)preEH).server().name().equals(server.name()))){
			
			// A new series starts => I create a new segment/message dispatcher
			if (!(server.scheduler() instanceof PacketBasedScheduler))
			{ // segment case
				ownerTrans.addSegment(new Segment(this));
			} 
			
	/*		//gadem add a new messageDispatcher 
			else if (server.scheduler().host() instanceof PacketBasedNetwork_VCA_Simulator)
			{ // message dispatcher case
				
				MessageDispatcher_VCA_Simulator md = new MessageDispatcher_VCA_Simulator(this);
				md.complete();
				ownerTrans.addSegment(md);
			}*/
			//gadem
			else 
			{ // message dispatcher case
				MessageDispatcher md = new MessageDispatcher(this);
				md.complete();
				ownerTrans.addSegment(md);
			}
		} 
		
	}
	
	public void arriveNewEvent() {}
  
	public ArrayList<Step> deploy(){
		ArrayList<Step> list = new ArrayList<Step>();
		if (operation.getClass() == SimpleOperation.class) { // cambio 30-3-2011. Antes la condicion incluia %% etg == null
			if(etg == null){ // viene de un node             //NEW NEW
				Iterator<String> iter = ((SimpleOperation)operation).getLockIter();
				if (iter != null)
					while(iter.hasNext()){
						lockList.add((MutualExclusionResource)Repository.getModelElem(iter.next(), "MutualExclusionResource"));
					}
				iter = ((SimpleOperation)operation).getUnlockIter();
				if (iter != null)
					while(iter.hasNext()){
						unlockList.add((MutualExclusionResource)Repository.getModelElem(iter.next(), "MutualExclusionResource"));
					}
				etg = ((SimpleOperation)operation).etg().clone();
			}
			list.add(this);
		} else if (operation.getClass() == Message.class && etg == null){ // viene de un node
			etg = ((Message)operation).etg().clone();
			list.add(this);
		} else if (operation.getClass() == CompositeOperation.class){
			Iterator<CodeOperation> operListIter = ((CompositeOperation)operation).getOperListIter();
			while (operListIter.hasNext()){
				CodeOperation oper = operListIter.next();
				if (!oper.thereIsOverriddenPriority()){
					oper.setOverriddenPriority(operation.overriddenPriority());
					oper.setIsPermanent(operation.isPermanent());
					oper.setThereIsOverriddenPriority(operation.thereIsOverriddenPriority());
				}
				Step step = new Step(oper, server);
				list.addAll(step.deploy());
			}
		} 
		else if (operation.getClass() == CompositeMessage.class){
			Iterator<Message> operListIter = ((CompositeMessage)operation).getOperListIter();
			while (operListIter.hasNext()){
				etg = ((Message)operation).etg().clone();
				list.add(this);
			}
		}
			else if (operation.getClass() == EnclosingOperation.class){
			Iterator<CodeOperation> operListIter = ((EnclosingOperation)operation).getOperListIter();
			while (operListIter.hasNext()){
				CodeOperation oper = operListIter.next();
				if (!oper.thereIsOverriddenPriority()){
					oper.setOverriddenPriority(operation.overriddenPriority());
					oper.setIsPermanent(operation.isPermanent());
					oper.setThereIsOverriddenPriority(operation.thereIsOverriddenPriority());
				}
				Step step = new Step(oper, server);
				list.addAll(step.nullActivityList());
			}
			etg = ((EnclosingOperation)operation).etg().clone(); // a�adido 30-3-2011
			list.add(this);
		}	  
		return list;
	} 
  
	private ArrayList<Step> nullActivityList(){
		ArrayList<Step> listOfNull = new ArrayList<Step>() ;
		if (operation.getClass() == SimpleOperation.class){
			etg = new ConstantETG(1);
			listOfNull.add(this);
		}else if (operation.getClass() == CompositeOperation.class){
			Iterator<CodeOperation> operListIter = ((CompositeOperation)operation).getOperListIter();
			while (operListIter.hasNext()){
				CodeOperation oper = operListIter.next();
				if (!oper.thereIsOverriddenPriority()){
					oper.setOverriddenPriority(operation.overriddenPriority());
					oper.setIsPermanent(operation.isPermanent());
					oper.setThereIsOverriddenPriority(operation.thereIsOverriddenPriority());
				}
				Step step = new Step(oper,server);
				listOfNull.addAll(step.nullActivityList());
			}
		}else if (operation.getClass() == EnclosingOperation.class){
			Iterator<CodeOperation> operListIter = ((EnclosingOperation)operation).getOperListIter();
			while (operListIter.hasNext()){
				Operation oper = operListIter.next();
				if (!oper.thereIsOverriddenPriority()){
					oper.setOverriddenPriority(operation.overriddenPriority());
					oper.setIsPermanent(operation.isPermanent());
					oper.setThereIsOverriddenPriority(operation.thereIsOverriddenPriority());
				}
				Step act = new Step(oper,server);
				listOfNull.addAll(act.nullActivityList());
			}
		}		
		else if (operation.getClass() == CompositeMessage.class){
			Iterator<Message> operListIter = ((CompositeMessage)operation).getOperListIter();
			while (operListIter.hasNext()){
				Message oper = operListIter.next();
			
				etg = ((Message)operation).etg().clone();
				listOfNull.add(this);
			}
		}
		return listOfNull;
	}



	@Override
	public String toString(){	
		/*String string = "\nInput fep: " + inputFepList.get(0).name() +
		                "\nOutput fep: " + outputFepList.get(0).name() +*/
		String string = "\nOperation: " + operation.name() + 
		                "\nSchedulable resource: " + server.name();
		/*if (etg != null)
			string += etg.toString(); // TODO sure it can be null???*/ 

		return "\nStep:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}