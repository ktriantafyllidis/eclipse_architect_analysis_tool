/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: DeliveryServer
 * Description: It is an event handler that generates one event in only
 *     one of its outputs each time an input event arrives. The output
 *     path is chosen at the time of the event generation. 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package flow;

import java.util.*;

import module.*;

import org.w3c.dom.*;

import utilityClasses.XMLparser;

public class Branch extends EventHandler {
	
	/* ENUMS */
	
	/*
	 * Policies used to determine the output path.
	 * SCAN: the output path is chosen in a cyclic fashion.
	 * RANDOM: the output path is chosen in a random fashion.
	 */
	public enum DeliveryPolicyType {SCAN, RANDOM};

	
	/* FIELDS */
	
	// The delivery policy.
	private DeliveryPolicyType deliveryPolicy = DeliveryPolicyType.RANDOM;
		public DeliveryPolicyType deliveryPolicy(){return deliveryPolicy;}
		public void setDeliveryPolicy(DeliveryPolicyType policy){this.deliveryPolicy = policy;}
	
	//
	private List<Integer> scanOutputList;
		public void addScanOutput(int scanOutput){scanOutputList.add(scanOutput);}
		public void createScanOutputList(){scanOutputList = new ArrayList<Integer>();}
	
	//
	private List<Float> randomWeightList = new ArrayList<Float>();
		public void addRandomWeight(float randomWeight){randomWeightList.add(randomWeight);}
		public void addRandomWeight(int i, float randomWeight){randomWeightList.add(i, randomWeight);}
		public int randomWeightListSize(){return randomWeightList.size();}
		public List<Float> randomWeightList(){return randomWeightList;}
		
	//
	private int nextScanOutput = 0;
	
	
	/* METHODS */
	
	// Constructor
	public Branch(){}
	public Branch(Node node, RegularEndToEndFlow trans) {
		
		super(node, trans);
		
		Element elem = (Element)node;
	  
		if (elem.hasAttribute("Request_Policy"))
			deliveryPolicy = DeliveryPolicyType.valueOf(elem.getAttribute("Request_Policy"));
		
		List<Element> outputWeightElemList = XMLparser.getChildElem(node);
		for (Element outputWeightElem : outputWeightElemList) {
			randomWeightList.add(Float.parseFloat(outputWeightElem.getAttribute("Weight")));
		}
	}
  
	public void complete(){

		Element elem = (Element)node;
		
		// I get the inputFEP & outputFEPs from the owner transaction
		
		FlowEndPoint fep = ownerTrans.getFEP(elem.getAttribute("Input_Event"));
		fep.setTarget(this);
		inputFepList.add(fep);
		
		List<String> outputEventList = Tools.parses(elem.getAttribute("Output_Events_List"));
		for (String outputEvent : outputEventList) {
			fep = ownerTrans.getFEP(outputEvent);
			fep.setSource(this);
			outputFepList.add(fep);
		}
		
		/* If there are not <Output_Weight> elems, I consider as many weights as outputs and
		 * I set them 1.0 as default */
		if (randomWeightList.size() == 0)
			for (int i = 0; i < outputFepList.size(); i++) {
				randomWeightList.add(i, 1.0f);
			}
			/*for (FlowEndPoint outputFep : outputFepList) {
				randomWeightList.add(1.0f);
			}*/
		 
		// If there are <Output_Weight> elems but less than outputs, I complete with 0s
		for (int i = randomWeightList.size(); i < outputFepList.size(); i++) {
			randomWeightList.add(i, 0.0f);
		}
		
		
		if (deliveryPolicy == DeliveryPolicyType.SCAN) {
			scanOutputList = new ArrayList<Integer>();
			int output = 0;
			for (Float weight : randomWeightList) {
				for (int i = 0; i < weight; i++) {
					scanOutputList.add(output);
				}
				output++;
			}
		} else {
			float acum = 0.0f;
			for (Float weight : randomWeightList) {
				acum += weight;
			}
			for (int i = 0; i < randomWeightList.size(); i++) {
				randomWeightList.set(i, randomWeightList.get(i) / acum);
			}
			float currentWeight = 0.0F;
			float aux;
			for (int i = 0; i < randomWeightList.size(); i++) {
				aux = randomWeightList.get(i);
				randomWeightList.set(i, currentWeight);
				currentWeight += aux;
			}
		}	
	}
	
	public void postComplete(){}
	
	@Override 
	public void arriveNewEvent() {
		FlowEvent ev = inputFepList.get(0).pollPendingFlowEvent();
		if(deliveryPolicy == DeliveryPolicyType.SCAN){
			outputFepList.get(scanOutputList.get(nextScanOutput)).addFlowEvent(ev);
			nextScanOutput = (nextScanOutput + 1) % scanOutputList.size();
			//currentOutput=(currentOutput + 1) % outputFlowEndPointList().size();
		} else {
			float currentWeight = (float)RandomGenerator.normalizedUniformValue();
			for (int i = randomWeightList.size() - 1;  i >= 0; i--) {
				if (randomWeightList.get(i) < currentWeight) {
					outputFepList.get(i).addFlowEvent(ev);
					break;
				}
			}
			//currentOutput=(int)Math.round(RandomGenerator.uniformValue(-0.5, (double)(outputFlowEndPointList().size())-0.5));
			//outputFlowEndPointList().get(currentOutput).addFlowEvent(ev);
		}
	}
	
	
	
	

	@Override
	public String toString(){	
		String string = "\nInput fep: " + inputFepList.get(0).name() +
		                "\nOutput feps: ";
		for (FlowEndPoint fep : outputFepList) {
			string += "\n\t" + fep.name();
		}
		string += "\nDelivery policy: " + deliveryPolicy + 
		          "\nOutput weights:";
		
		for (Float randomWeight : randomWeightList) {
			string += "\n" + randomWeight;
		}
		return "\nBranch:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}
