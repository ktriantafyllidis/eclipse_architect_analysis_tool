/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: Fork
 * Description: It is an event handler that generates one event in
 *      every one of its outputs each time an input event arrives. 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package flow;

import java.util.List;

import module.Tools;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class Fork extends EventHandler {
	
	/* METHODS */
  
	//Constructors
	public Fork(){};
	public Fork(Node node, RegularEndToEndFlow trans) {
		super(node, trans);
	}
	
	public void complete(){
		
		if (node != null) {
		
			Element elem = (Element)node;
			
			// FEP for input event
			FlowEndPoint fep = ownerTrans.getFEP(elem.getAttribute("Input_Event"));
			if (fep != null) {
				fep.setTarget(this);
				inputFepList.add(fep);
			}  else {
				// TODO a fork has an input event that does not exist
			}
			
			// FEPs for output events
			List<String> outputEventNameList = Tools.parses(elem.getAttribute("Output_Events_List"));
			for (String outputEventName : outputEventNameList) {
				fep = ownerTrans.getFEP(outputEventName);
				if (fep != null) {
					fep.setSource(this);
					outputFepList.add(fep);
				} else {
					// TODO a fork has an output event that does not exist
				}
			}
		}
	}
	
	public void postComplete(){}
	
	public void arriveNewEvent() {
		FlowEvent ev = inputFepList.get(0).pollPendingFlowEvent();
		outputFepList.get(0).addFlowEvent(ev);
		for(int i = 1; i < outputFepList.size(); i++) {
			outputFepList.get(i).addFlowEvent(ev.clone());
		}
	}

	public static void setQueryFork(Step requester, FlowEndPoint requestInput){
		
		Fork fork = new Fork();
		fork.setName(requester.name() + "_QueryFork");
		// TODO analyze why it is here
		//requester.owner().tempEventHandlerMap().put(fork.name, fork);
		requester.ownerTrans().addEventHandler(fork);
		
		fork.outputFepList.set(0, requester.outputFepList.get(0));
		fork.outputFepList.get(0).setSource(fork);
		fork.outputFepList.set(1, requestInput);
		requestInput.setSource(fork);

		FlowEndPoint fep = new FlowEndPoint(fork.name, requester.ownerTrans());
		requester.ownerTrans().addFEP(fep);
		fep.setSource(requester);
		fep.setTarget(fork);
		requester.outputFepList.set(0,fep);
	}

	
	
	
	@Override
	public String toString(){	
		String string = "\nInput fep: " + inputFepList.get(0).name() +
		                "\nOutput feps: ";
		for (FlowEndPoint fep : outputFepList) {
			string += "\n\t" + fep.name();
		}
		return "\nFork:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}
