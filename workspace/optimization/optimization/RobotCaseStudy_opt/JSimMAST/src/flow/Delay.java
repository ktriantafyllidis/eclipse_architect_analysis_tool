/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: Delay
 * Description: It is an event handler that generates its output event
 *      after a time interval has elapsed from the arrival of the input
 *      event.
 * 
 * Author: J.M. Drake y F. Aguilar
 * Date: 1-1-09
 ********************************************************************/

package flow;

import java.util.*;

import org.w3c.dom.*;

import resource.NetworkRouter;
import resource.NetworkSwitch;
import timers.TimingObject;

import core.*;
import flow.Branch.DeliveryPolicyType;
import module.*;

public class Delay extends EventHandler implements Timed {
	
	/* ENUMS */
	private enum DelayType {Delay, 
							Offset,
		                    Message_Delivery,
		                    Message_Fork,
		                    Message_Branch}
	
	
	/* ATTRIBUTES */	
  
	//
	private ETG delay;
		public ETG delay() {return delay;}
  
	//
	protected PriorityQueue<FlowEvent> delayedEvents = new PriorityQueue<FlowEvent>(5, new ActivationTimeComparator());
	
	//
	private String timerRef;
	private String switchRef;
	
	// 
	private DelayType type;
	
	
	
	/* METHODS */
		
	// Constructor
	public Delay(Node node, RegularEndToEndFlow trans) {
		
		super(node, trans);
		Element elem = (Element)node;
		
		type = DelayType.valueOf(elem.getLocalName());
			FlowEndPoint fep;
			ArrayList<String> outputEventList;
		switch (type) {
		case Delay:
		case Offset:
			if (elem.hasAttribute("Timer"))
				timerRef = elem.getAttribute("Timer");
			// FEP for input event
			fep = trans.getFEP(elem.getAttribute("Input_Event"));
			fep.setTarget(this);
			inputFepList.add(fep);
			// FEP for output event
			fep = trans.getFEP(elem.getAttribute("Output_Event"));
			fep.setSource(this);
			outputFepList.add(fep);
			// Delay
			long min = 0;
			if (elem.hasAttribute("Delay_Min_Interval"))
				min = Tools.readLong(elem.getAttribute("Delay_Min_Interval"));	
			long max = 0;
			if (elem.hasAttribute("Delay_Max_Interval"))
				max = Tools.readLong(elem.getAttribute("Delay_Max_Interval"));
			long avg = 0;
			if (elem.hasAttribute("Delay_Avg_Interval"))
				avg = Tools.readLong(elem.getAttribute("Delay_Max_Interval"));
			delay = new WABETG(min, avg, max);
			break;
		case Message_Delivery:
			switchRef = elem.getAttribute("Switch");
			// FEP for input event
			fep = trans.getFEP(elem.getAttribute("Input_Event"));
			fep.setTarget(this);
			inputFepList.add(fep);
			// FEP for output event
			fep = trans.getFEP(elem.getAttribute("Output_Event"));
			fep.setSource(this);
			outputFepList.add(fep);
			// Delay -in the postcomplete() method-
			break;
		case Message_Fork:
			switchRef = elem.getAttribute("Switch");
			// FEP for input event
			fep = trans.getFEP(elem.getAttribute("Input_Event"));
			fep.setTarget(this);
			inputFepList.add(fep);
			// Aux FEP
			fep = new FlowEndPoint(elem.getAttribute("Input_Event") + "_aux", trans);
			fep.setSource(this);
			trans.addFEP(fep);
			outputFepList.add(fep);
			// Fork
			Fork fork = new Fork();
			fork.setName(name + "_messageFork");
			fork.setOwnerTrans(trans);
			fep.setTarget(fork);
			fork.inputFepList.add(fep);
			trans.addEventHandler(fork);
			// FEPs for output events
			outputEventList = Tools.parses(elem.getAttribute("Output_Events_List"));
			for(int i = 0; i < outputEventList.size(); i++){
				fep = trans.getFEP(outputEventList.get(i));
				fep.setSource(fork);
				fork.outputFepList.add(fep);
			}
			// Delay -in the postcomplete() method-
			break;
		default: //case Message_Branch:
			switchRef = elem.getAttribute("Switch");
			// FEP for input event
			fep = trans.getFEP(elem.getAttribute("Input_Event"));
			fep.setTarget(this);
			inputFepList.add(fep);
			// Aux FEP
			fep = new FlowEndPoint(elem.getAttribute("Input_Event") + "_aux", trans);
			fep.setSource(this);
			trans.addFEP(fep);
			outputFepList.add(fep);
			// Branch
			Branch branch = new Branch();
			branch.setName(name + "_messageBranch");
			branch.setOwnerTrans(trans);
			fep.setTarget(branch);
			branch.inputFepList.add(fep);
			trans.addEventHandler(branch);
			// FEPs for output events
			outputEventList = Tools.parses(elem.getAttribute("Output_Events_List"));
			for(int i = 0; i < outputEventList.size(); i++){
				fep = trans.getFEP(outputEventList.get(i));
				fep.setSource(branch);
				branch.outputFepList.add(fep);
			}
			
			branch.setDeliveryPolicy(DeliveryPolicyType.valueOf(elem.getAttribute("Delivery_Policy")));
			NodeList nl = node.getChildNodes();
			for(int i = 0; i < nl.getLength(); i++){
				if (nl.item(i).getNodeType() == Node.ELEMENT_NODE) { // <Output_Weigh>
					branch.addRandomWeight((Float.parseFloat(((Element)nl.item(i)).getAttribute("Weight"))));
				}
			}
			for (int i = branch.randomWeightListSize(); i < outputFepList.size(); i++) {
				branch.addRandomWeight(i, 0.0f);
			}
			if (branch.deliveryPolicy() == DeliveryPolicyType.SCAN) {
				branch.createScanOutputList();
				int output = 0;
				for (Float weight : branch.randomWeightList()) {
					for (int i = 0; i < weight; i++) {
						branch.addScanOutput(output);
					}
					output++;
				}
			} else {
				float acum = 0.0f;
				for (Float weight : branch.randomWeightList()) {
					acum += weight;
				}
				float previousWeight = 0.0F;
				for (Float weight : branch.randomWeightList()) {
					weight = previousWeight + weight / acum;
					previousWeight = weight;
				}
			}
			break;
		}
	}
	
	public void complete(){}
	
	/*public void complete(){

		Element elem = (Element)node;
		
		type = DelayType.valueOf(elem.getLocalName());
			FlowEndPoint fep;
			List<String> outputEventList;
		switch (type) {
		case Delay:
		case Message_Delivery:
			fep = ownerTrans.getFEP(elem.getAttribute("Input_Event"));
			fep.setTarget(this);
			inputFepList.add(fep);
			fep = ownerTrans.getFEP(elem.getAttribute("Output_Event"));
			fep.setSource(this);
			outputFepList.add(fep);
			break;
		case Message_Fork:
			fep = ownerTrans.getFEP(elem.getAttribute("Input_Event"));
			fep.setTarget(this);
			inputFepList.add(fep);
			
			fep = new FlowEndPoint(elem.getAttribute("Input_Event") + "_aux", ownerTrans);
			ownerTrans.addFEP(fep);
			fep.setSource(this);
			outputFepList.add(fep);
			
			Fork fork = new Fork();
			fork.setName(name + "_messageFork");
			fep.setTarget(fork);
			fork.inputFepList.add(fep);
			ownerTrans.addEventHandler(fork);
			
			outputEventList = Tools.parses(elem.getAttribute("Output_Events_List"));
			for (String outputEvent : outputEventList) {
				fep = ownerTrans.getFEP(outputEvent);
				fep.setSource(fork);
				fork.outputFepList.add(fep);
			}
			break;
		default: //case Message_Branch:
			fep = ownerTrans.getFEP(elem.getAttribute("Input_Event"));
			fep.setTarget(this);
			inputFepList.add(fep);
			
			fep = new FlowEndPoint(elem.getAttribute("Input_Event") + "_aux", ownerTrans);			
			fep.setSource(this);
			outputFepList.add(fep);
			ownerTrans.addFEP(fep);
			
			Branch branch = new Branch();
			branch.setName(name + "_messageBranch");
			ownerTrans.addEventHandler(branch);
			fep.setTarget(branch);
			branch.inputFepList.add(fep);

			outputEventList = Tools.parses(elem.getAttribute("Output_Events_List"));
			for (String outputEvent : outputEventList) {
				fep = ownerTrans.getFEP(outputEvent);
				fep.setSource(branch);
				branch.outputFepList.add(fep);
			}
			
			branch.setPolicy(DeliveryPolicyType.valueOf(elem.getAttribute("Delivery_Policy")));
			NodeList nl = node.getChildNodes();
			for(int i = 0; i < nl.getLength(); i++){
				if (nl.item(i).getNodeType() == Node.ELEMENT_NODE) { // <Output_Weigh>
					branch.addRandomWeight((Float.parseFloat(((Element)nl.item(i)).getAttribute("Weight"))));
				}
			}
			for (int i = branch.randomWeightListSize(); i < outputFepList.size(); i++) {
				branch.addRandomWeight(i, 0.0f);
			}
			if (branch.policy() == DeliveryPolicyType.SCAN) {
				branch.createScanOutputList();
				int output = 0;
				for (Float weight : branch.randomWeightList()) {
					for (int i = 0; i < weight; i++) {
						branch.addScanOutput(output);
					}
					output++;
				}
			} else {
				float acum = 0.0f;
				for (Float weight : branch.randomWeightList()) {
					acum += weight;
				}
				float previousWeight = 0.0F;
				for (Float weight : branch.randomWeightList()) {
					weight = previousWeight + weight / acum;
					previousWeight = weight;
				}
			}
			break;
		}
	}*/
	
	public void postComplete() {
		switch (type) {
		case Delay:
		case Offset:
			if (timerRef != null) {
				TimingObject timer = (TimingObject)Repository.getModelElem(timerRef, "Timer");
				outputFepList.get(0).setTimer(timer);
			}
			break;
		case Message_Delivery:
			NetworkSwitch netSwitch = (NetworkSwitch)Repository.getModelElem(switchRef, "ProcessingResource");
			delay = netSwitch.getDeliveryLatency();
			break;
		case Message_Fork:
			NetworkSwitch netSwitch2 = (NetworkSwitch)Repository.getModelElem(switchRef, "ProcessingResource");
			delay = netSwitch2.calculateForkLatency(outputFepList.size());
			break;
		default: //case Message_Branch:
			NetworkRouter netRouter = (NetworkRouter)Repository.getModelElem(switchRef, "ProcessingResource");
			delay = netRouter.getBranchLatency(outputFepList.size());
			break;
		}
	}
  
	public void arriveNewEvent() {
		FlowEvent ev = inputFepList.get(0).pollPendingFlowEvent();
		ev.setActivationTime(Clock.currentTime() + delay.next());   //CHANGE
		if (delayedEvents.isEmpty()) {
			delayedEvents.add(ev);
			Clock.addTimedEvent(ev.activationTime() - Clock.currentTime(), this);
		} else if (delayedEvents.peek().activationTime() <= ev.activationTime())
			delayedEvents.add(ev);
		else {
			Clock.cancelTimedEvent(this);
			Clock.addTimedEvent(ev.activationTime() - Clock.currentTime(), this);
			delayedEvents.add(ev);
		}
	}
 
	public void update() {
		FlowEvent ev = delayedEvents.poll();
		outputFepList.get(0).addFlowEvent(ev);
		while (!delayedEvents.isEmpty() && delayedEvents.peek().activationTime() == ev.activationTime()){
			outputFepList.get(0).addFlowEvent(delayedEvents.poll());
		}
		if(!delayedEvents.isEmpty())
			Clock.addTimedEvent(delayedEvents.peek().activationTime() - Clock.currentTime(), this);
	}
	
	
	
	
	@Override
	public String toString(){
		
		String string = "";
		
		switch (type) {
		case Delay:
			string = "\nInput fep: " + inputFepList.get(0).name() +
			         "\nOutput fep: " + outputFepList.get(0).name() + 
			         "\nDelay:" + delay.toString();				
			if (timerRef != null)
				string += "\nTimer: " + outputFepList.get(0).timer().name();
			string += "\nDelay:" + (super.toString() + string).replaceAll("\n", "\n\t");
		default:
			break;
		}
		return string;
	}
	
	
	protected class ActivationTimeComparator implements Comparator<FlowEvent>{
	  public int compare(FlowEvent e1,FlowEvent e2){
		  if (e1.activationTime()< e2.activationTime())return +1;
		  else if (e1.activationTime()>e2.activationTime())return -1;
		  else return 0;
	  }
	};
}
