/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: private List<String> operRefList = new ArrayList<String>();
 * Description: It is a container that group those elements beloging to
 *        end to end flow transaction 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package flow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import utilityClasses.XMLparser;
import core.Controller;
import core.ModelElement;
import core.Repository;
import eventmonitor.CesarMonitor;
import eventmonitor.DisplayOnEvent;
import eventmonitor.GlobalTimeMonitor;
import eventmonitor.JitterMonitor;
import eventmonitor.LogOnEvent;
import eventmonitor.LogOnGlobalDeadline;
import eventmonitor.ThroughputMonitor;

public class RegularEndToEndFlow extends ModelElement {

	/* ENUM */
	private enum TransChildrenType {Periodic_Event, 
		                            Sporadic_Event, 
		                            Unbounded_Event, 
		                            Bursty_Event, 
		                            Singular_Event,
		                            Internal_Event, 
		                            Step, 
		                            Merge, 
		                            Join, 
		                            Branch, 
		                            //Queried_Branch, 
		                            Fork, 
		                            Rate_Divisor, 
		                            Delay, 
		                            Message_Delivery, 
		                            Message_Fork, 
		                            Message_Branch, 
		                            Offset}
	
	
	/* ATTRIBUTES */
 
	// Flow endpoints declared in the transaction, using their name as key
	private Map<String, FlowEndPoint> fepMap = new HashMap<String, FlowEndPoint>();
    	public FlowEndPoint getFEP(String name) {
    		return fepMap.get(name);
    	}
    	public void addFEP(FlowEndPoint fep) {
			fepMap.put(fep.name(), fep);
		}
	
	// Event handlers of the transaction
	private Map<String, EventHandler> eventHandlerMap = new HashMap<String, EventHandler>();
		public EventHandler getEventHandler(String name) {
			return eventHandlerMap.get(name);
		}
		public void addEventHandler(EventHandler eventHandler) {
			eventHandlerMap.put(eventHandler.name(), eventHandler);
		}
		public List<WorkloadEvent> getWorkloadEvents(){
			List<WorkloadEvent> wEventList = new ArrayList<WorkloadEvent>();
			for (EventHandler eh : eventHandlerMap.values()) {
				if (eh instanceof WorkloadEvent) {
					wEventList.add((WorkloadEvent)eh);
				}
			}
			return wEventList;
		}
		
	private Map<String, Segment> segmentMap = new HashMap<String, Segment>();
		public void addSegment(Segment segment) {
			segmentMap.put(segment.name(), segment);
		}
		public void clearTempSegmentMap() {
			segmentMap.clear();
		}
		
	
	/* METHODS */
	
    // Constructor
	public RegularEndToEndFlow(Node node) {
		
		super(node);
		
		List<Element> transChildElemList = XMLparser.getChildElem(node); 
		WorkloadEvent extEv = null;
		FlowEndPoint fep = null;
		EventHandler eh = null;
		for (Element transChildElem : transChildElemList) {
			TransChildrenType type = TransChildrenType.valueOf(transChildElem.getLocalName());
			switch (type) {
			// External events
			case Periodic_Event:
			case Sporadic_Event:
			case Unbounded_Event:
			case Bursty_Event:
			case Singular_Event: // I create an incomplete external event, adding it to the current transaction 
				extEv = new WorkloadEvent(transChildElem, this);
				eventHandlerMap.put(extEv.name(), extEv);
				break;
			// Internal events
			case Internal_Event: // I create a FEP, adding it to the current transaction
				fep = new FlowEndPoint(transChildElem, this); //fep = FlowEndPoint.newFlowEndPoint(transChildElem, this);
				fepMap.put(fep.name(), fep);
				break;
			// Event handlers
			case Step:
			case Merge:
			case Join:
			case Branch:
			//case Queried_Branch:
			case Fork:
			case Rate_Divisor:
			case Delay:
			case Message_Delivery:
			case Message_Fork:
			case Message_Branch:
			case Offset: // I create an incomplete event handler (non-deployed in the case of a step), adding it to the current transaction
				eh = EventHandler.newEventHandler(transChildElem, this);
				eventHandlerMap.put(eh.name(), eh);
				break;
			default:
				break;
			}
		}
	}
  
	public void complete(){

		// 1st round over event handlers (has sense for every kind)
		for (EventHandler eh : eventHandlerMap.values()) {
			eh.complete();
		}
		
		// 2nd round over event handlers (only has sense for QueryServer and Step)
		for (EventHandler eh : eventHandlerMap.values()) {
			eh.postComplete();
			/*if(eh instanceof Step){	
				// I look for series of consecutive steps sharing the same server
				preEH = eh.getInputFEP(0).source();
				if (!(preEH instanceof Step) || (preEH instanceof Step) && 
					                            (!((Step)eh).server().name().equals(((Step)preEH).server().name()))){
					// I create a new segment/message dispatcher
					if (!(((Step)eh).server().scheduler() instanceof PacketBasedScheduler)){ // segment case
						Segment segment = new Segment((Step)eh);
						segment.setName(eh.name() + "-segment");
						segment.setOwnerTrans(this);
						segment.getInputFEP(0).setTarget(segment);
						segment.getOutputFEP(0).setSource(segment);
						tempEventHandlerMap.put(segment.name(), segment);
					} else { // message dispatcher case
						MessageDispatcher md = new MessageDispatcher((Step)eh);
						md.setName(eh.name() + "-mssgDispatcher");
						md.setOwnerTrans(this);
						md.getInputFEP(0).setTarget(md);
						md.getOutputFEP(0).setSource(md);
						md.complete();
						tempEventHandlerMap.put(md.name(), md);
					}
				}		
			}*/
		}
		eventHandlerMap.putAll(segmentMap);
		addEventMonitors();
	}

	public void denormalize(){
		for (EventHandler eh : eventHandlerMap.values()) {

		    if (eh instanceof MessageDispatcher)
				((MessageDispatcher)eh).denormalize();
			else if (!(eh instanceof MessageDispatcher) && eh instanceof Segment)
				((Segment)eh).denormalize();
		}
	}
	
	public String results(String offset){
		String string = offset + "<mast_res:End_To_End_Flow_Results Name=\"" + name + "\">\n";
		// I write the simulation timing results (<mast_res:Simulation_Timing_Results>) of relevant FEPs
		for (FlowEndPoint fep : fepMap.values()) {
			string += fep.simulationTimingResults(offset + "\t"); // Only relevant FEPs will return a string != ""
		}
		return string + offset + "</mast_res:End_To_End_Flow_Results>\n\n";
	}
	
	private void addEventMonitors(){
		
		// I add event monitors to all FEPs or to some of them, depending on the simulation profile
		
		switch (Controller.profile()) {
		case SCHEDULABILITY:
		case PERFORMANCE:
			// I add a GlobalTimeMonitor, a ThroughputMonitor and a JitterMonitor to every sink FEP
			for (FlowEndPoint fep : fepMap.values()) {
				if (fep.target() == null) { // sink FEP
					
					if (fep.hasHardGlobalDeadlines()) {
						Iterator<Element> iter = fep.getHardGlobalDeadlineIter();
						Element firstHardGlobalDeadlineElem = iter.next();
						String refEvent = firstHardGlobalDeadlineElem.getAttribute("Referenced_Event");
						String deadline = firstHardGlobalDeadlineElem.getAttribute("Deadline");
						
						WorkloadEvent wEvent = (WorkloadEvent)this.getEventHandler(refEvent);
						GlobalTimeMonitor globalTimeMonitor = new GlobalTimeMonitor(fep, deadline, wEvent);
						fep.addEventMonitor(globalTimeMonitor);
						Repository.addTimeMonitor(globalTimeMonitor);
					} else
						fep.addEventMonitor(new GlobalTimeMonitor(fep, "", null));					

					fep.addEventMonitor(new ThroughputMonitor(fep));
					fep.addEventMonitor(new JitterMonitor(fep));
					
					fep.addEventMonitor(new CesarMonitor(fep, null));
				}
			}
			break;
		case VERBOSE:
			// I add a DisplayOnEvent to every FEP
			for (FlowEndPoint fep : fepMap.values()) {
				fep.addEventMonitor(new DisplayOnEvent(fep));
			}
			break;
		default: // case TRACES
			// I add a LogOnEvent to every WorkloadEvent FEP
			for (WorkloadEvent wEvent : getWorkloadEvents()) {
				// I get the FEP with same name as the WorkloadEvent
				FlowEndPoint fep = fepMap.get(wEvent.name());
				fep.addEventMonitor(new LogOnEvent(fep));
			}
			// I add a LogOnGlobalDeadline to every FEP with hard global deadlines
			for (FlowEndPoint fep : fepMap.values()) {
				if (fep.hasHardGlobalDeadlines()) {
					Iterator<Element> iter = fep.getHardGlobalDeadlineIter();
					while (iter.hasNext()) {
						fep.addEventMonitor(new LogOnGlobalDeadline(fep, iter.next()));					
					}
				}
			}
			break;
		}
		
		// I let every segment adding event monitors
		for (EventHandler eh : eventHandlerMap.values()) {
			if (eh instanceof Segment)
				((Segment)eh).addEventMonitors();
		}
	}
	
	@Override
	public String toString(){
		
		String string = ""; //"\nWorkload events:";
		for (EventHandler eh : eventHandlerMap.values()) {
			if (eh instanceof WorkloadEvent)
				string += eh.toString(); 
		}
		//string += "\nFEPs:";
		for (FlowEndPoint fep : fepMap.values()) {
			string += fep.toString();
		}
		//string += "\nEvent handlers:";
		for (EventHandler eh1 : eventHandlerMap.values()) {
			if (!(eh1 instanceof WorkloadEvent) && !(eh1 instanceof Step))
				string += eh1.toString(); 
		}
		return "\n\nEndToEndFLow: " + (super.toString() + string).replaceAll("\n", "\n\t");
	}

}
