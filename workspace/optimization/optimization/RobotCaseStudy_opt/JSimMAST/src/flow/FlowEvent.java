/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: FlowEvent
 * Description: Represent a flow activation between two event handler.
 * Its transmits all the data relative to the flow time. 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package flow;

import core.*;

import java.util.*;

public class FlowEvent {

	/* ATTRIBUTES */
	
	// Instant of the generation of the flow event      in the event generator (External event)
	private long generationTime;
    	public long generationTime(){return generationTime;}
    
	// Instant at which the flow event arrives at the input FlowEndPoint of the current segment.
	private long arrivingTime;
		public long arrivingTime(){return arrivingTime;}
		public void setArrivingTime(long time){arrivingTime = time;}
	
	// Instant at which the current segment begins to attend the flow event
	private long activationTime;
		public long activationTime(){return activationTime;}
		public void setActivationTime(long time){activationTime = time;}
	
	// Time during which the segment remains blocked (awaiting to lock a mutex).
	private long blockingTime;
		public long blockingTime(){return blockingTime;}
		public void setBlockingTime(long time){blockingTime = time;}
	
	// Instant at which the segment begins to suffer the current block.
	private long blockingInstant;
		//public long blockingInstant(){return blockingInstant;}
		public void setBlockingInstant(long time){blockingInstant = time;}
		
	// Time during which the segment remains suspended (awaiting to be scheduled in processor).
	private long suspensionTime;
		public long suspensionTime(){ return suspensionTime;}
		public void setSuspensionTime(long time){suspensionTime = time;}
		
	// Instant at which the segment begins to suffer the current suspension
	private long suspensionInstant;
		public long suspensionInstant(){return suspensionInstant;}
		public void setSuspensionInstant(long time){suspensionInstant = time;}

	// Instant at which the segment restarts execution
	private long actProcessingInstant;
		public void setActProcessingInstant(long time){actProcessingInstant = time;}
	
	// Time during which the step has been executing
	private long executedTime;
		
	// total execution time of the current step
	private long executionTime;
		public void setExecutionTim (long time){
			executionTime = time;
			executedTime = 0;
		}
	
	// Reference to the originating external event 
	private WorkloadEvent source;
		public WorkloadEvent source() {return source;}
		
	// EventHandler in which the flow event is being attended
	private EventHandler owner;
		public EventHandler owner(){return owner;}
		public void setOwner(EventHandler owner){this.owner = owner;}
		
	// Current priority of the flow activation
	private int flowPriority = 0;
		public int flowPriority(){return flowPriority;}
		public void setFlowPriority(int priority){flowPriority = priority;}
	  
	// SRP level of the flow activation
	private int flowLevel = 0;
		public int flowLevel(){return flowLevel;};
		public void setFlowLevel(int level){flowLevel = level;}
	  
	// List of associated Event. Used when several events are merged.
	private List<FlowEvent> nestedFlowEventList = new ArrayList<FlowEvent>(0);
		public List<FlowEvent> nestedFlowEventList(){return nestedFlowEventList;}

	// List of not used flow events
	private static List<FlowEvent> freeFlowEventList = new ArrayList<FlowEvent>(0); 
	

	/* METHODS */
	
	public static FlowEvent newFlowEvent(WorkloadEvent source){
		FlowEvent ev;
		if (freeFlowEventList.size() == 0)
			ev = new FlowEvent();
			else
			ev = freeFlowEventList.remove(freeFlowEventList.size()-1);
 
	  	ev.source = source;
		ev.generationTime = Clock.currentTime();
		ev.arrivingTime = 0;
		ev.activationTime = 0;
		ev.blockingTime = 0;
		ev.blockingInstant = -1;                                   // JMD
		ev.actProcessingInstant = -1; // Have not a valid time     // JMD
		ev.suspensionTime = 0;
		ev.suspensionInstant=-1;                                   // JMD
		ev.executedTime = 0;
		ev.executionTime = 0;
		ev.flowPriority = 0;
		ev.flowLevel = 0;
		
		ev.nestedFlowEventList.clear();
	  
		return ev;
	}
 
	/*
	 * This method returns the flowEvent (among the nested flow events of
	 * the current flow event and itself) that has as source the 
	 * WorkloadEvent argument or null if no one is found.
	 */
	private FlowEvent referencedFlowEvent(WorkloadEvent referenced) {
		if (source == referenced) 
			return this;
		for (int i = 0; i < nestedFlowEventList.size(); i++){
			if (nestedFlowEventList.get(i).source == referenced)
				return nestedFlowEventList.get(i);
		}
		return null;
	}

	/**
	 * This method returns the global time of the current FlowEvent regarding the WorkloadEvent argument, 
	 * calculated as the difference between the current time and the generation time 
	 * of the FlowEvent originated from that external event, being it one FlowEvent
	 * among the nested FlowEvents of the current one and itself.
	 * 
	 * @param referenced
	 * @return
	 */
	public long globalTime(WorkloadEvent referenced) {
		FlowEvent fev = referencedFlowEvent(referenced);
		if (fev == null) 
			return -1;
		else 
			return Clock.currentTime() - fev.generationTime;
	}

	/**
	 * This method returns the local time of the current FlowEvent, calculated as
	 * the difference between the current time and the arriving time.
	 * @return
	 */
	public long localTime() {
		return Clock.currentTime() - arrivingTime;
	}

	/**
	 * This method returns the time that the current FlowEvent is waiting at the 
	 * input FlowEndPoint of the current segment, calculated as
	 * the difference between the activation time and the arriving time.
	 * @return
	 */
	public long waitingTime() {
		return activationTime - arrivingTime;
	}

	/**
	 * This methods returns
	 * @return
	 */
	public long remainingTime(){
	//	if (blockingInstant!=0)return executionTime-localTime()+suspendingTime+(blockingTime+Clock.currentTime()-blockingInstant);
	//	if (suspendingInstant!=0)return executionTime-localTime()+(suspendingTime+Clock.currentTime()-suspendingInstant)+blockingTime;
	//	return executionTime-localTime()+blockingTime+suspendingTime;
		  	  
		if (actProcessingInstant >= 0) // if (actProcessingInstant != 0)
			return executionTime - (executedTime + Clock.currentTime() - actProcessingInstant);
		else
			return executionTime - executedTime;
	}
	
	/**
	 * This method is called when the segment becomes blocked, 
	 * setting the blocking instant as the current time and, 
	 * if the segment had executed, refreshing (incrementing) the execution time
	 * and resetting the actProcessingInstant.
	 */
	public void block(){
		blockingInstant = Clock.currentTime();
		if (actProcessingInstant >= 0){ // if (actProcessingInstant != 0){    //JMD
			executedTime += Clock.currentTime() - actProcessingInstant;   // he cambiado el orden de la resta
			actProcessingInstant = -1;
		}
	}
	
	/**
	 * This method is called when the segment is unblocked, 
	 * refreshing the block time, reseting the block instant 
	 * and setting the actProcessingInstant as the current time.
	 */
	
	public void unblock(){
		if (blockingInstant >= 0){
			blockingTime += Clock.currentTime() - blockingInstant;
			blockingInstant = -1;                                           //JMD
		}
		// CCC
		// actProcessingInstant = Clock.currentTime();
	}
	
	/**
	 * This method is called when the segment becomes suspended, 
	 * setting the suspending instant as the current time and, 
	 * if the segment had executed, refreshing the execution time
	 * and reseting the actProcessingInstant.
	 */
	public void suspend(){
		suspensionInstant = Clock.currentTime();
		if (actProcessingInstant >= 0) {                                      // JMD
			executedTime += Clock.currentTime() - actProcessingInstant; 
			actProcessingInstant = -1;
		}

	}
	
	/**
	 * This method is called when the segment is resumed, 
	 * refreshing the suspension time, reseting the suspension instant 
	 * and setting the actProcessingInstant as the current time.
	 */
	public void resume(){
		if(suspensionInstant >= 0){                                           //JMD
			suspensionTime += Clock.currentTime() - suspensionInstant;
			suspensionInstant = -1;
		}
																				//JMD
		actProcessingInstant = Clock.currentTime();
	}
	
	// Delete the FlowEvent.
	public void freeFlowEvent(){
		if (!nestedFlowEventList.isEmpty()) {
			for (FlowEvent ev : nestedFlowEventList) {
				ev.freeFlowEvent();
			}
			nestedFlowEventList.clear();
		}
		freeFlowEventList.add(this);
	}
	
	@Override
	public FlowEvent clone() {
		FlowEvent ev = newFlowEvent(this.source);
		//ev.activationTime=this.activationTime;
		//ev.arrivingTime=this.arrivingTime;
		//ev.blockingTime=this.blockingTime;
		ev.flowPriority=this.flowPriority;
		ev.generationTime=this.generationTime;
		//ev.owner=this.owner;
		ev.source=this.source;
		//ev.suspendingTime=this.suspendingTime();
		for(int i=0;i<nestedFlowEventList.size();i++){
			ev.nestedFlowEventList.add(nestedFlowEventList.get(i).clone());
		}
		return ev;
	}

}