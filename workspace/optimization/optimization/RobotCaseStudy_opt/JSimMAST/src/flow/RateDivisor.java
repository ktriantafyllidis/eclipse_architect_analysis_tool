/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: RateDivisor
 * Description: It is an event handler that generates one output event
 *      when a number of input events equal to the Rate Factor have
 *      arrived. 
 * 
 * Author: J.M. Drake y F. Aguilar.
 * Date: 1-1-09
 ********************************************************************/

package flow;

import java.util.*;
import org.w3c.dom.*;

public class RateDivisor extends EventHandler {
  
	/* FIELDS */
	
	// current factor rate set
	private int rateFactor = 1;

	private int count = 0;
	
	private ArrayList<FlowEvent> lostEvents;
  
	
	/* METHODS */
	
	//Constructor
	public RateDivisor(Node node, RegularEndToEndFlow trans) {
		
		super(node, trans);
		
		Element elem = (Element)node;
			
		if (elem.hasAttribute("Rate_Factor"))
			rateFactor = Integer.parseInt(elem.getAttribute("Rate_Factor"));
		
		lostEvents = new ArrayList<FlowEvent>(rateFactor);
	}
	
	public void complete(){
		
		Element elem = (Element)node;
		
		// FEP for input event
		FlowEndPoint fep = ownerTrans.getFEP(elem.getAttribute("Input_Event"));
		if (fep != null) {
			fep.setTarget(this);
			inputFepList.add(fep);
		} else {
			// TODO a rate divisor has an input event that does not exist
		}
		
		// FEP for output event
		fep = ownerTrans.getFEP(elem.getAttribute("Output_Event"));
		if (fep != null) {
			fep.setSource(this);
			outputFepList.add(fep);
		} else {
			// TODO a rate divisor has an output event that does not exist
		}
	}

	public void postComplete(){}
	
	public void arriveNewEvent() {
		count =(count + 1) % rateFactor;
		
		if(count == 0){
			FlowEvent ev = inputFepList.get(0).pollPendingFlowEvent();
			ev.nestedFlowEventList().addAll(lostEvents);
			lostEvents.clear();
			outputFepList.get(0).addFlowEvent(ev);
		} else
			lostEvents.add(inputFepList.get(0).pollPendingFlowEvent());
	}




	@Override
	public String toString(){
		String string = "\nInput fep: " + inputFepList.get(0).name() +
		                "\nOutput fep: " + outputFepList.get(0).name() +
		                "\nRate factor = " + rateFactor;
		return "\nRate divisor:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}