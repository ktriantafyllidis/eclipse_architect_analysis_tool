/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: Logger
 * Description: It stores the traces that are generated in the 
 *        simulation execution.
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package core;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.io.*;

public class Logger {
	
	/* INNER CLASSES */
	
	private static class Source{
		private int id;
		private String name;
		private SrcType type;
		public Source(int id, String name, SrcType type){
			this.id = id;
			this.name = name;
			this.type = type;
		}
	}
	
	
	/* ENUMS */
	
	public enum SrcType{ComputingResource, 
		                Network,
		                MutualExclusionResource,
		                WorkloadEvent,
		                Segment} 
//						EventWithHardDeadline}
	
	
	/* CONSTANTS */
	
	// Maximum number of messages that are stored in the temporal list.
	private static final int MAX_NUM_TEMP_STORE = 4096;
	
	// For computing resources
	public static final int REGULAR_PROCESSOR_FREE = 0;
	public static final int REGULAR_PROCESSOR_ATTENDING_TIMER = 1;
	public static final int REGULAR_PROCESSOR_ATTENDING_INTERRUPT = 2;
	public static final int REGULAR_PROCESSOR_ATTENDING_SCHEDULER = 3;
	public static final int REGULAR_PROCESSOR_ATTENDING_CONTEXTSWITCH = 4;
	public static final int REGULAR_PROCESSOR_ATTENDING_INTERRUPT_CONTEXTSWITCH = 5;
	public static final int REGULAR_PROCESSOR_LOCKING_RESOURCE = 6;
	
	// For networks
	public static final int PACKET_BASED_NETWORK_FREE = 7;
	public static final int PACKET_BASED_NETWORK_TRANSMITING_PACKET = 8;
		
	// For mutuxes
	public static final int MUTEX_FREE = 9;
	public static final int MUTEX_ASSIGNED = 10;
	
	// For segments
	public static final int SEGMENT_END_EVENT = 11;
	public static final int SEGMENT_TO_FREE = 12;
	public static final int SEGMENT_TO_PROCESSING = 13;
	public static final int SEGMENT_TO_BLOCKED = 14;
	public static final int SEGMENT_SUSPENDED = 15;
	
	// 
	public static final int WORKLOAD_EVENT_GENERATED = 16;
	public static final int MISSED_LOCAL_DEADLINE = 17;
	public static final int MISSED_GLOBAL_DEADLINE = 18;
	public static final int MET_LOCAL_DEADLINE = 19;
	public static final int MET_GLOBAL_DEADLINE = 20;
	
	
	
	/* FIELDS */
	
	// Path of the message file
	//private static String tracesPath;
    
	// It stores the messages in a permanent mode
	private static FileWriter tracesFW;
		
	// Message types (each one has an ID and a name)
	private static Map<Integer, String> mssgTypesMap;
    
    // Sources (each one has an ID, a name and a type)
	/*private static Map<Integer, String> srcNamesMap = new TreeMap<Integer, String>();
	private static TreeMap<Integer, String> srcTypesMap = new TreeMap<Integer, String>();
	private static int numSources = 0;*/
    private static List<Source> srcList = new ArrayList<Source>();
    	public static void clearSrcList(){srcList.clear();}

	// Traces (each one has a time, a source ID and a message type ID)
	private static long[] traceTimes = new long[MAX_NUM_TEMP_STORE];
	private static int[] traceSIDs = new int[MAX_NUM_TEMP_STORE];
	private static int[] traceMIDs = new int[MAX_NUM_TEMP_STORE];
	private static int numTraces = 0;
	
	
	/* METHODS */
	
	/**
	 * 
	 */
	private static void setMessageTypes(){
		
		mssgTypesMap = new TreeMap<Integer, String>();
		
		mssgTypesMap.put(REGULAR_PROCESSOR_FREE, "RegularProcessor/FREE");
		mssgTypesMap.put(REGULAR_PROCESSOR_ATTENDING_TIMER, "RegularProcessor/ATTENDING_TIMER");
		mssgTypesMap.put(REGULAR_PROCESSOR_ATTENDING_INTERRUPT, "RegularProcessor/ATTENDING_INTERRUPT");
		mssgTypesMap.put(REGULAR_PROCESSOR_ATTENDING_SCHEDULER, "RegularProcessor/ATTENDING_SCHEDULER");
		mssgTypesMap.put(REGULAR_PROCESSOR_ATTENDING_CONTEXTSWITCH, "RegularProcessor/ATTENDING_CONTEXTSWITCH");
		mssgTypesMap.put(REGULAR_PROCESSOR_ATTENDING_INTERRUPT_CONTEXTSWITCH, "RegularProcessor/ATTENDING_INTERRUPT_CONTEXTSWITCH");
		mssgTypesMap.put(REGULAR_PROCESSOR_LOCKING_RESOURCE, "RegularProcessor/LOCKING_RESOURCES");
		
		mssgTypesMap.put(PACKET_BASED_NETWORK_FREE, "PacketBasedNetwork/FREE");
		mssgTypesMap.put(PACKET_BASED_NETWORK_TRANSMITING_PACKET, "PacketBasedNetwork/TRANSMITING_PACKET");
		
		mssgTypesMap.put(MUTEX_FREE, "Mutex/FREE");
		mssgTypesMap.put(MUTEX_ASSIGNED, "Mutex/ASSIGNED");
		
		mssgTypesMap.put(SEGMENT_END_EVENT, "SegmentEndEvent");
		mssgTypesMap.put(SEGMENT_TO_FREE, "Segment_to_Free");
		mssgTypesMap.put(SEGMENT_TO_PROCESSING, "Segment_to_Processing");
		mssgTypesMap.put(SEGMENT_TO_BLOCKED, "Segment_to_Blocked");
		mssgTypesMap.put(SEGMENT_SUSPENDED, "Segment_to_Suspended");
		
		mssgTypesMap.put(WORKLOAD_EVENT_GENERATED, "Workload event received");
		mssgTypesMap.put(MISSED_LOCAL_DEADLINE, "Local deadline fail");
		mssgTypesMap.put(MISSED_GLOBAL_DEADLINE, "Global deadline fail");
		mssgTypesMap.put(MET_LOCAL_DEADLINE, "Local deadline met");
		mssgTypesMap.put(MET_GLOBAL_DEADLINE, "Global deadline met");
	}

	/**
	 * This method registers a new source.
	 * @param name
	 * @param type
	 * @return The ID assigned to the new source.
	 */
	public static int registerNewSource(String name, SrcType type){
		/*numSources++;
		srcNamesMap.put(numSources - 1, name);
		srcTypesMap.put(numSources - 1, type);
		return numSources - 1;*/
		int id = srcList.size();
		srcList.add(new Source(id, name, type));
		return id;
	}	
	
	/**
	 * This method registers a new trace.
	 * @param sID
	 * @param mID
	 */
	public static void registerNewTrace(int sID, int mID){
	
		traceTimes[numTraces] = Clock.currentTime();
		
		if (0 <= sID && sID < srcList.size())
			traceSIDs[numTraces] = sID;
		
		if (0 <= mID && mID < mssgTypesMap.values().size())
			traceMIDs[numTraces] = mID;
		
		numTraces++;

	  	if (numTraces == MAX_NUM_TEMP_STORE)
	  		storeTraces(false);
	}
 
	// Creates and opens the traces message file. If the file exist it is cleaned.
	public static void init(String traces_Path) throws AbortedException {
		
		if (mssgTypesMap == null)
			setMessageTypes();
		
		// Delete the file, if it exists.
		(new File(traces_Path)).delete();  
      
		// Open the file to write
		try {
			tracesFW = new FileWriter(traces_Path, true);
		} catch (IOException e){
			throw new AbortedException("The logger do not can open the messages file " + traces_Path);
		}
				
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		String generationDate = df.format(new Date(System.currentTimeMillis()));
	  
		try{
			// I write the header of the *.trc.xml file (the XML declaration and the MAST declaration)
			tracesFW.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			tracesFW.write("<?mast file Type=\"XML-Mast-Trace-File\" version=\"1.1\"?>\n\n");
			
			// I write the root element with its attributes
			tracesFW.write("<mast_trace:TRACE_FILE");
			tracesFW.write("\n\txmlns:mast_trace=\"http://mast.unican.es/xmlmast/trace\"");
			tracesFW.write("\n\txmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
			tracesFW.write("\n\txsi:schemaLocation=\"http://mast.unican.es/xmlmast/trace ../JSimMast_V_Schemas/Mast_Trace_V.xsd\"");
			tracesFW.write("\n\tModel_Name=\"" + Repository.modelName() + "\"");
		  	tracesFW.write("\n\tModel_Date=\"" + Repository.modelDate() + "\"");
		  	tracesFW.write("\n\tGeneration_Tool=\"" + "JSimMast_V0" + "\"");
		  	tracesFW.write("\n\tGeneration_Profile=\"" + Controller.profile() + "\"");
		  	tracesFW.write("\n\tGeneration_Date=\"" + generationDate + "\"");
		  	tracesFW.write("\n\tStart_Time=\"" + "0" + "\"");
		  	tracesFW.write("\n\tEnd_Time=\"" + "0000" + "\">");
		  			  
		  	// I write the list of message types
		  	tracesFW.write("\n\n\t<mast_trace:Msg_Type_List>");
		  	for (Entry<Integer, String> entry : mssgTypesMap.entrySet()) {
		  		tracesFW.write("\n\t\t<mast_trace:Msg_Type Mid=\"" + entry.getKey() + "\" Type=\"" + entry.getValue() + "\"/>");
			}
		  	tracesFW.write("\n\t</mast_trace:Msg_Type_List>");
		  
		  	// I write the list of sources
		  	tracesFW.write("\n\n\t<mast_trace:Src_List>");
		  	for (Source source : srcList) {
		  		tracesFW.write("\n\t\t<mast_trace:Src Sid=\"" + source.id + 
		  				                          "\" Name=\"" + source.name + 
		  				                          "\" Type=\"" + source.type + "\"/>");
			}
		  	tracesFW.write("\n\t</mast_trace:Src_List>");
		  	
		  	// I start to write the list of traces
		  	tracesFW.write("\n\n\t<mast_trace:Msg_List>");
		  
		} catch (IOException e){
			Controller.errorOutput("The logger can not write in the traces file");
		}
	}
  
	// Saves the temporal messages list and close the messages List
	public static void saveTracesFile(){
		storeTraces(true);
		try{
			tracesFW.close();
		}catch(IOException e){
			Controller.errorOutput("The logger can not close the traces file");
		}
	}
	
	/*
	 * This method writes the 1st trace of the current bunch of traces and 
	 * after that writes the subsequent traces, 
	 * filtering those ones that are equal as the previous one.
	 * Finally, it can write the end of the list of traces and 
	 * the end of the file or it can reset the traces counter.
	 */
	private static void storeTraces(boolean isLastBunch){
  		try{
  			tracesFW.write("\n\t\t<mast_trace:Msg T=\"" + traceTimes[0] + "\" S=\"" + traceSIDs[0] + "\" M=\"" + traceMIDs[0] + "\"/>");
  			
  			int limit = MAX_NUM_TEMP_STORE;
  			if (isLastBunch)
				limit = numTraces; // < MAX_NUM_TEMP_STORE
  			
  			for (int i = 1; i < limit; i++){
  				boolean isAsPrevious = traceTimes[i] == traceTimes[i - 1] && 
  									   traceSIDs[i] == traceSIDs[i - 1] && 
  									   traceMIDs[i] == traceMIDs[i - 1];
  				if(!isAsPrevious)
  					tracesFW.write("\n\t\t<mast_trace:Msg T=\"" + traceTimes[i] + "\" S=\"" + traceSIDs[i] + "\" M=\"" + traceMIDs[i] + "\"/>");	  					
  			}
  			if (isLastBunch) {
  				tracesFW.write("\n\t</mast_trace:Msg_List>");
  				tracesFW.write("\n\n</mast_trace:TRACE_FILE>");
  			} else {
  				numTraces = 0;
  			}
  		}catch (IOException e){
  			Controller.errorOutput("The logger can not write in the traces file");
  		}
	}

}
