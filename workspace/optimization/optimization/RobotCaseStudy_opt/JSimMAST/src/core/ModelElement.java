/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Abstract class: ModelElement
 * Description: Root class for the elements of the model. Each element
 *    has a node of the model description and a name
 *  
 * 
 * Author: D.Garc�a y J.M. Drake
 * Date: 1-9-09
 ********************************************************************/

package core;

import org.w3c.dom.*;

public abstract class ModelElement {
	
	/* ATTRIBUTES */
	
	// Name of the element. For each element type, the name is a key.
	protected String name;
		public String name() {return name;}
		public void setName(String name) {this.name = name;}
    
	// XML node with the element description
	protected Node node;
		public Node node() {return node;}  
		public void setNode(Node node) {this.node = node;}


	/* METHODS */  
	
	// Constructor: it is used in virtual elements that is not present in the model.
	public ModelElement(){}
	
	// Constructor: Constructs an elements from the XML model description. 
	public ModelElement(Node node){
		if (node != null) { // caution doubt to Interrupt schedulers
			this.node = node;
		    name = ((Element)node).getAttribute("Name");
		}
	}
	
	// Is invoked in the second phase of the model construction: it sets the referenced to other element of the model   
	public abstract void complete();
    
	// Is invoked in the third phase of the model construction 
	public abstract void denormalize();

	
	
	@Override
	public String toString(){
		return "\nName: " + name;
	}
}