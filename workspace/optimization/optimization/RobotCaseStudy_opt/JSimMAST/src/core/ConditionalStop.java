/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Interface: ConditionStop
 * Description: Interface offered for the conditional stop monitor.
 *      If all of the conditional stop monitor return true, the 
 *      simulation is stopped.
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/
package core;
public interface ConditionalStop {
  // Return true if the condition for stopping is satisfied.	
  boolean stop();
}
