/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: Controller
 * Description: Main class of the simulator. When a program build a
 *     instance of this class, it gegenerates a new simulator. 
 *     It offers the external interface of the simulator. 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package core;

import guis.JSimMastGUI;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Controller {

	/* ENUMS */
	public enum ProfileType {
	  VERBOSE,        /** Es adecuado para fases de prueba del simulador. 
	                       - Genera como una lista ilimitada de mensajes para el operador:
	                       	 . mensajes de cada flow event que se genera
	                       	 . Mensaje por cada cambio de estado de cada stated element                       producen en los elementos del simulador.
	                       - Los mensajes de almacenan en una lista ilimitada.
	                       - Finaliza: Cuando se alcanza el tiempo absoluto de simulaci�n
	                         establecido.*/ 
	  TRACES,         /** Genera un fichero de trazas que permita visualizar la evoluci�n
	                      en el tiemo de la simulaci�n.
 						   - Genera un fichero de traza con:
    						 � Eventos de inicio y finalizaci�n de las transiciones.
    						 � Eventos con requisitos temporales.
    						 � Evento de inicio y finalizaci�n de segmento
    						 . Cambios de estado de los stated resources.
  						   - Finaliza al alcanzarse el tiempo de simulaci�n establecido.*/
	  SCHEDULABILITY, /** Genera la informaci�n que se necesita para analizar la planifi-
	                      cabilidad de tiempo real estrictos.
                          - Genera como resultados:
                            � Se monitorizan los tiempos locales y globales de los eventos
                              fin de segmento.
                            . Se monitorizan los tiempos de los eventos con requisitos de
                              tiempo real.
                            � Se monitorizan los porcentajes de utilizaci�n de los recursos.
                            � Se generan los mensajes que indican incumplimeinto de plazos.
                          - La finalizaci�n de la simulaci�n se puede programas en funci�n de:
                            � En un tiempo de simulaci�n determinado.
                            � Cuando cada transacci�n se ha ejecutado un n�mero m�nimo de veces.*/
	  PERFORMANCE};   /** Se utiliza cuando se analiza un sistema de tiempo real laxo. 
                          Se monitorizan la finalizaciones de las transacciones y los niveles
                          de uso de los recursos.
   						  - Se monitorizan:
                            � Los eventos de finalizaci�n de las transacciones.
                            � Los niveles de utilizaci�n de los recursos.
                          - La simulaci�n se finaliza:
                            � Se alcanza el tiempo de ejecuci�n establecido.
                            � Cada transacci�n se ha ejecutado un m�nimo n�mero de veces
                              establecido.*/
	  
	  
	/* FIELDS */
	  
	private static JSimMastGUI gui;
	
	//private static String modelPath;
	private static String resPath;
	//private static String tracesPath;

	// Profile used in the execution of the simulation.
	private static ProfileType profile;
		public static ProfileType profile() {return profile;}

	// Is true if the simulation is not finished.
	private static boolean isRunning;
		// Executive finalization of the simulation
		public static void executiveStop() {isRunning = false;}

	// Permanent list of conditional stop monitors.
	private static ArrayList<ConditionalStop> conditionalStopMonitorList = new ArrayList<ConditionalStop>();
		public static void addConditionalStopMonitor(ConditionalStop conditionalStopMonitor) {conditionalStopMonitorList.add(conditionalStopMonitor);}

	// Temporal list with the model elements which decide at the end of the simulation step.
	private static List<Decisor> changedElementList = new ArrayList<Decisor>();
	  	public static void addDecisor(Decisor resource) {changedElementList.add(resource);}

	// Optional min. number of each transaction that are required to finalize the simulation.
	private static long transExecutions;
		public static long transExecution() {return transExecutions;}
	  
	// Lock used to suspend the operator thread in awaitMssg method.
//QUITAR    private static Object lock;
	  
	// Store the last message for operator
	private static String lastMssg;
	
	

	/* METHODS */
	
	
	/**
	 * @param modelPath Path of the model file
	 * @param profile Profile of the simulation
	 * @param resultPath Path of the results file
	 * @param tracesPath Path of the trsves file
	 */
	public void initController(String modelPath, 
		                              ProfileType profile,
		                              String resultPath,
		                              String tracesPath){
		
		try{
	  
			//Controller.modelPath = modelPath;
			Controller.resPath = resultPath;
			//Controller.tracesPath = tracesPath;
			Controller.profile = profile;
			
			isRunning = true;
//QUITAR			lock = new Object();
			 
			// I init the resources of simulator
			Clock.init();
			Logger.clearSrcList();
			Repository.init(modelPath);
			//mssgOutput("SIMULATION MODEL IS BUILT");
			
		}catch(AbortedException e){
			errorOutput("Aborted Simulation building: " + e.getMessage());
		}
		
		if (profile == ProfileType.TRACES) {
			try{  
				Logger.init(tracesPath);
				//mssgOutput("TRACES FILE IS BUILT");
			}catch(AbortedException e){
				errorOutput("Aborted Traces building: " + e.getMessage());
			}
		}
	}
	
	

	/**
	 * @param modelPath Path of the model file
	 * @param profile Profile of the simulation
	 * @param resultPath Path of the results file
	 * @param tracesPath Path of the trsves file
	 */
	public static void initController(String modelPath, 
		                              ProfileType profile,
		                              String resultPath,
		                              String tracesPath, 
		                              JSimMastGUI gui){
		Controller.gui = gui; 
		
		try{
			// I check if the name of the model file is valid
			if (!modelPath.endsWith(".mdl.xml"))
				throw new AbortedException("The file name of the model " + modelPath + " is not valid");
	  
			//Controller.modelPath = modelPath;
			Controller.resPath = resultPath;
			//Controller.tracesPath = tracesPath;
			Controller.profile = profile;
			
			isRunning = true;
//QUITAR			lock = new Object();
			 
			// I init the resources of simulator
			Clock.init();
			Logger.clearSrcList();
			Repository.init(modelPath);
			mssgOutput("SIMULATION MODEL IS BUILT");
			
		}catch(AbortedException e){
			errorOutput("Aborted Simulation building: " + e.getMessage());
		}
		
		if (profile == ProfileType.TRACES) {
			try{  
				Logger.init(tracesPath);
				mssgOutput("TRACES FILE IS BUILT");
			}catch(AbortedException e){
				errorOutput("Aborted Traces building: " + e.getMessage());
			}
		}
	}
	
	/** 
	 * Constructor with only model path, and other default parameter.
	 * As defaults are used VERBOSE profile an the model with differents
	 * extension for results and traces files.
	 */
	public static void initController(String modelPath){  
		initController(modelPath, 
				       ProfileType.VERBOSE, 
				       modelPath.replaceFirst(".mdl.xml", ".res.xml"), 
				       modelPath.replaceFirst(".mdl.xml", ".trc.xml"), null);
	}
	
	/**
	 * Constructor with only model path, and other default parameter.
	 * As defaults are used VERBOSE profile an the model with differents
	 * extension for results and traces files.
	 * @param modelPath
	 * @param profile
	 */
	public static void initController(String modelPath, ProfileType profile){
		initController(modelPath, 
				       profile, 
				       modelPath.replaceFirst(".mdl.xml", ".res.xml"), 
				       modelPath.replaceFirst(".mdl.xml", ".trc.xml"), null);
	}

	// Execute the simulation until a specific time is reached
	public static void execute(long finalTime){
		execute(finalTime, Long.MAX_VALUE);
	}

	// Init the simulation until a specific time is reach or until every transaction is executed for a number of times.
	public static void execute(long finalTime, long transExecutions){
	  
		double reportedPercent = 0.0;
		final double PERCENT_STEP = 0.1;
		
		Controller.transExecutions = transExecutions;

		mssgOutput("SIMULATION GO ON");
	 
		while (Clock.currentTime() <= finalTime) {// || isRunning){
			if (!isRunning) 
				break;
		 	Clock.dispatch();
		 	schedule();
		 	if ((Clock.currentTime() > reportedPercent * finalTime)) {
				mssgOutput("% " + Math.round(reportedPercent * 100.0) + " reported");
				reportedPercent += PERCENT_STEP;
			}
		 	Thread.yield();
		}
		isRunning = true; // the simulation is enabled for next execution
		mssgOutput("THE SIMULATION IS ENDED");
	}
	
	
	// Init the simulation until a specific time is reach or until every transaction is executed for a number of times.
	public static void execute_no_gui(long finalTime, long transExecutions){
	  
		double reportedPercent = 0.0;
		final double PERCENT_STEP = 0.1;
		
		Controller.transExecutions = transExecutions;

		mssgOutput("SIMULATION GO ON");
	 
		while (Clock.currentTime() <= finalTime) {// || isRunning){
			if (!isRunning) 
				break;
		 	Clock.dispatch();
		 	schedule();
		 	if ((Clock.currentTime() > reportedPercent * finalTime)) {
				mssgOutput("% " + Math.round(reportedPercent * 100.0) + " reported");
				reportedPercent += PERCENT_STEP;
			}
		 	Thread.yield();
		}
		isRunning = true; // the simulation is enabled for next execution
		mssgOutput("THE SIMULATION IS ENDED");
	}

	// Verifies is all conditional stop monitor are satifies. In this case, isRunning is set true.
	public static void verifyConditionalStop(){
		Iterator<ConditionalStop> iter = conditionalStopMonitorList.iterator();
		boolean allStopped = true;
		while (iter.hasNext()){
			allStopped = allStopped & iter.next().stop();
		}
		isRunning = !allStopped;
	}

	// Generates the results of the simulation and saves them in the result file
	public static void generateResultsFile(){
	  FileWriter resFile;
	  try{
		  resFile = new FileWriter(resPath);
	 	  resFile.write(Repository.result());
	 	  resFile.close();
	 	  mssgOutput("THE RESULTS HAVE BEEN GENERATED");
	  }catch (IOException e){
		  errorOutput("The results file can not be written");
	  }
	}
	
	
	// Generates the results of the simulation and saves them in the result file
	public static void generateResultsFile_no_gui(){
	  FileWriter resFile;
	  try{
		  resFile = new FileWriter(resPath);
	 	  resFile.write(Repository.result());
	 	  resFile.close();
	 	  //mssgOutput("THE RESULTS HAVE BEEN GENERATED");
	  }catch (IOException e){
		  errorOutput("The results file can not be written");
	  }
	}
	

	// Generates the traces of the simulation and saves them in the traces file
	public static void generateTracesFile(){
		Logger.saveTracesFile();
		mssgOutput("THE TRACES GENERATION IS ENDED");
	}
	
	//gadem
	// Generates the traces of the simulation and saves them in the traces file
	public static void generateTracesFile_no_gui(){
		Logger.saveTracesFile();
		//mssgOutput("THE TRACES GENERATION IS ENDED");
	}
	
	
	static LinkedList<Decisor> tempList=new LinkedList<Decisor>(changedElementList);
	
	// Invokes the decide() method on the resources stores in the changedElementsList, and reset the list
	private static void schedule() {
	  while(!changedElementList.isEmpty()){
		  tempList.clear();
		  tempList.addAll(changedElementList);
		  changedElementList.clear();
		  
		  Iterator<Decisor> iter = tempList.iterator();
		  while(iter.hasNext()){
			  iter.next().decide();
		  }
	  }
	}
  
	// Sends a message error to operator
	public static void errorOutput(String errMssg){
		lastMssg = "ERROR: " + errMssg;
		/*synchronized(lock){
			lock.notify();
		}*/
		//System.out.println(lastMssg);
		//gui.processMessage(lastMssg);
  	}
  
	// Sends a message to operator
	public static void mssgOutput(String mssg){
	  lastMssg = mssg;
	  /*synchronized(lock){
		  lock.notify();
	  }*/
	  //System.out.println(lastMssg);
	  //gui.processMessage(lastMssg);
	}
}