/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: Clock
 * Description: Managing the simulation time and the timed event. 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/
package core;

import java.util.*;

public class Clock {
	
	/* ATTRIBUTES */

	// Simulation time expressed as nanosecond
	private static long currentTime;
		public static long currentTime(){return currentTime;}

	// Timed event list required to Clock. The event is ordered by time
	private static PriorityQueue<TimedEvent> timedEventList = new PriorityQueue<TimedEvent>();
		
	
	/* METHODS */
  
	// Init the clock class
	public static void init(){
		currentTime = 0;
		TimedEvent.init();
		timedEventList = new PriorityQueue<TimedEvent>();
	}
	  
  // Return the time expresed in seconds
	public static double timeSeconds(long tns){
		return ((double)tns)*1.0E-9;
	}

	
	// Advanced time to next timed event. Invokes all the timed elements with the new time.
	public static void dispatch(){
		currentTime = timedEventList.peek().time();
		//System.err.println(currentTime);
		/*if (currentTime%110 == 0) {
			System.out.println("\nCurrent time = " + currentTime);
			System.out.println("Timed event list size = " +  timedEventList.size());
		}*/
		while (!timedEventList.isEmpty() && (timedEventList.peek().time() == currentTime)){
			timedEventList.poll().dispatch();
		}
	}

  // Adds e new timed event to be execute at a specific time on the an element
	public static void addTimedEvent(long time, Timed source){
		timedEventList.add(TimedEvent.newTimedEvent(currentTime + time,source));
	}

  // Remove the TimedEvent which the source is specified
	
	public static void cancelTimedEvent(Timed source){
		Iterator<TimedEvent> iter = timedEventList.iterator();
		TimedEvent te = null;
		boolean found = false;
		while(iter.hasNext()){
			te = iter.next();
			if(te.source() == source){
			   found = true;
			   break;
		   }
	    }
		if(found)
			timedEventList.remove(te);
	}

  
}
