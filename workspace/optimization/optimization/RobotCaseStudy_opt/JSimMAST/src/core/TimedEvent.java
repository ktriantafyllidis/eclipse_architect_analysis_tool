/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: TimedEvent
 * Description: It referenced a timed even of the simulation. 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/
package core;
import java.util.*;

public class TimedEvent implements Comparable{
	
	/* ATTRIBUTES */
    private long time;
    	public long time(){return time;}
    
    private Timed source;
    	public Timed source(){return source;}
    
    private static LinkedList<TimedEvent> freeTimedEventList;
    
    /* METHODS */
    
    public static TimedEvent newTimedEvent(long time, Timed source){
    	TimedEvent te;
    	
    	if (freeTimedEventList.size() == 0)
    		te = new TimedEvent();
    	else 
    		te = freeTimedEventList.poll();
    	
    	te.source = source;
    	te.time = time;
    	
    	return te;
    }
    
    public static void init(){
    	//LinkedList<TimedEvent> freeTimedEventList = new LinkedList<TimedEvent>();
    	freeTimedEventList = new LinkedList<TimedEvent>();
    }
    
    /*public void free(){
    	freeTimedEventList.add(this);
    }*/
    
    public void dispatch(){
    	source.update();
    	freeTimedEventList.add(this);
    }
    
    public int compareTo(Object o){
    	long dif=((TimedEvent)o).time-this.time;
    	if (dif>0) {return -1;}
    	else {
    		if (dif==0) {return 0;}
 	    	else {return 1;}
    	}
    }
}
