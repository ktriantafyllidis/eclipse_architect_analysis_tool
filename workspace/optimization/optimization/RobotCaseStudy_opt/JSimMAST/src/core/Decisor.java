/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Interface: Decissor
 * Description: Interface offered for the resources.
 *      The controller invokes the decide() method on every resource 
 *      queued in the changedElementList, as the last phase oh each 
 *      simulation step.
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/
package core;
public interface Decisor {
  // Se invoca cuando el elemento ha recibido todos los eventos que
  // corresponden a ese instante de simulación.
  void decide();
}
