package utilityClasses;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringsHandler {

	public static String handleParameterizedAttr(String string, Map<String, String> paramMap) {
		
		// Worst case: string arg. is composed of several tokens separated by ' ', by '.' or both
		// Example: jobList = "@consolePort@.goToPosition @consolePort@.writeLn"
		
		if (!string.contains("@")) // string is not parameterized
			return string;
		else { // parameterized
			if (string.contains(" ") || string.contains(".")) { // string consists of several tokens
				
				// I split the string to manage each wsToken (tokens separated by whitespace)
				String[] wsTokens = string.split("\\s+");
				for (int i = 0; i < wsTokens.length; i++) {
					// I look for wsTokens composed of several dotTokens (subtokens separated by '.')
					if (wsTokens[i].contains(".")) {
						// I split the wsToken and manage each of its dotTokens
						String[] dotTokens = wsTokens[i].split("\\.");
						for (int j = 0; j < dotTokens.length; j++) {
							dotTokens[j] = manageParameterizedAttr(dotTokens[j], paramMap);
						}
						// I reconstruct the wsToken
						wsTokens[i] = dotTokens[0] + '.' + dotTokens[1];
					} else
						wsTokens[i] = manageParameterizedAttr(wsTokens[i], paramMap);
				}
				// I reconstruct the string
				String recString = "";
				for (String wsToken : wsTokens) {
					recString += wsToken + ' ';
				}
				// I remove the tailing whitespace and return
				return recString.trim();
				
			} else // string consists of only 1 token
				return manageParameterizedAttr(string, paramMap);
		}
	}
	
	private static String manageParameterizedAttr(String string, Map<String, String> paramMap) {
		
		if (!string.contains("@")) // string is not parameterized
			return string;
		else { // parameterized
			// I quit the @s
			do {
				int n1 = string.indexOf("@");
				int n2 = string.indexOf("@", n1 + 1);
				String target = string.substring(n1, n2 + 1);
				String replacement = string.substring(n1 + 1, n2);
				string = string.replace(target, replacement);
			} while (string.contains("@"));
			
			// I try to resolve looking into the map
			if (paramMap.containsKey(string) && !paramMap.get(string).equals(""))
				return paramMap.get(string); // the string has been resolved
			else 
				return '@' + string + '@'; // I return the string as it was, without resolving it
		}
	}
	
	
	/**
	 * @param regex
	 * @param candidate
	 * @return
	 */
	public static boolean checkString(String regex, String candidate){
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(candidate);
		return matcher.matches();
	}

	/**
	 * This method simplifies the inputText argument.
	 * 
	 * It converts all multiline comments in the form: 
	 * // ... 
	 * // ... 
	 * ...... 
	 * // ...
	 * 
	 * into one (long) comment line in the form: /* ... * /
	 * 
	 * It also converts all '\n' and all '\t' into ' ' and in addition it
	 * removes all unnecessary spaces that go with delimiters.
	 * 
	 * @param inputText
	 * @return The text simplified.
	 */
	public static String flatText(String inputText) {

		String flatText = inputText;

		// [ //... => /* ... */ ]
		while (flatText.contains("//")) {
			int n = flatText.indexOf("//");
			flatText = flatText.replaceFirst("//", "/*");
			flatText = flatText.substring(0, n)
					+ flatText.substring(n).replaceFirst("\n", "*/\n");
		}

		for (int i = 0; i < 2; i++) {

			// [ /* ... */ ]
			// [ /* ... */ => /* ... */ /* ... */ /* ... */ ]
			// [ /* ... */ ]

			// flatText = flatText.replace("\n"," ");
			// flatText = flatText.replace("\t"," ");
			// flatText = flatText.replace("  "," ");
			flatText = flatText.replaceAll("\\s+", " ");

			// [ /* ... */ /* ... */ /* ... */ => /* ... ... ... */ ]
			flatText = flatText.replace("*/ /*", " ");

			/*
			 * String nt=""; while (inputText.length() != nt.length()){
			 * inputText = nt; nt = inputText.replace("  "," "); }
			 */

			// I remove all unnecessary spaces that go with delimiters.
			flatText = flatText.replace(" {", "{");
			flatText = flatText.replace("{ ", "{");
			flatText = flatText.replace(" }", "}");
			flatText = flatText.replace("} ", "}");
			flatText = flatText.replace(" (", "(");
			flatText = flatText.replace("( ", "(");
			flatText = flatText.replace(" )", ")");
			flatText = flatText.replace(") ", ")");
			flatText = flatText.replace(", ", ",");
			flatText = flatText.replace(" ,", ",");
			flatText = flatText.replace("; ", ";");
			flatText = flatText.replace(" ;", ";");
			flatText = flatText.replace("= ", "=");
			flatText = flatText.replace(" =", "=");
			flatText = flatText.replace(" #", "#");

			flatText = flatText.replace("# ", "#");
			flatText = flatText.replace(" *", "*");
			flatText = flatText.replace("* ", "*");
			flatText = flatText.replace("/ ", "/");
			flatText = flatText.replace(" /", "/");
			flatText = flatText.replace("! ", "!");
			flatText = flatText.replace(" !", "!");
			flatText = flatText.replace("+ ", "+");
			flatText = flatText.replace(" +", "+");
		}
		return flatText;
	}
	
	/**
	 * This method accepts a string representing the path of a file (no matter
	 * if using '/' or '\') and extracts the file name (last segment of the path)
	 * 
	 * @param filePath 
	 * @return The name of the file (last segment of the <code>filepath</code>
	 *  argument)
	 */
	public static String getFileNameFromPath(String filePath) {
		
		// I get the last slash index
		int n = filePath.lastIndexOf("/");
		if (n == -1)
			n = filePath.lastIndexOf("\\");
		
		// I return the file name
		if (n == -1)
			return filePath;
		else
			return filePath.substring(n + 1);
	}
	
	/**
	 * This method splits the <code>locator</code> argument in path & reference.
	 * @param locator A string in the form: path::reference 
	 * @return A <code> String[]</code> containing the two pieces (path &
	 * reference) of the <code>locator</code> argument.
	 */
	public static String[] splitLocator(String locator) {

		String[] pieces = new String[2];
		
		int n = locator.indexOf("::");
		if (n == -1) {
			pieces[0] = locator;
			pieces[1] = "";
		} else {
			pieces[0] = locator.substring(0, n);
			pieces[1] = locator.substring(n + 2);
		}
		return pieces;
	}
}
