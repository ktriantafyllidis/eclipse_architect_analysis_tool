/**********************************************************
 * Grupo Computadores y Tiempo Real (CTR) UNIVERSIDAD DE CANTABRIA
 * 
 * @author C�sar
 * @version =
 * @version 0.1 Last review: 2008/November/06 Class to navigate within a DOM
 *          tree and retrieve the children of a Node
 *********************************************************/

package utilityClasses;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import exceptions.JSimMastException;

public class XMLparser {

	/* METHODS */

	/* Parsing methods */
	
	/**
	 * This method DOM-parses a XML file stored at the path specified by the
	 * argument and returns its root element.
	 * 
	 * @param xmlFilePath
	 *            The path of the XML file.
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws ParseableFileNotFoundException
	 */
	private static Document parseXMLfile(String xmlFilePath) throws JSimMastException {
		//File xmlFile = new File(xmlFilePath.toLowerCase());
		DocumentBuilderFactory theFactory = DocumentBuilderFactory.newInstance();
		theFactory.setNamespaceAware(true); // I support namespaces
		DocumentBuilder parser = null;
		Document domTree = null;
		try {
			parser = theFactory.newDocumentBuilder();
			domTree = parser.parse(xmlFilePath);
		} catch (ParserConfigurationException e) {
			throw new JSimMastException("Problems creating the DOM-parser for XML file \"" + StringsHandler.getFileNameFromPath(xmlFilePath) + "\"");
		} catch (SAXException e) {
			throw new JSimMastException("Problems with the XML file \"" + StringsHandler.getFileNameFromPath(xmlFilePath) + "\" while parsing.");
		} catch (IOException e) {
			throw new JSimMastException("Problems with the XML file while parsing.");
		}
		return domTree;
	}
	
	/**
	 * This method DOM-parses a XML file stored at the path specified by the
	 * argument and returns its root element.
	 * 
	 * @param xmlFilePath
	 *            The path of the XML file.
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws ParseableFileNotFoundException
	 */
	private static Document parseXMLfile(File xmlFile) throws JSimMastException {
		
		DocumentBuilderFactory theFactory = DocumentBuilderFactory.newInstance();
		theFactory.setNamespaceAware(true); // I support namespaces
		DocumentBuilder parser = null;
		Document domTree = null;
		try {
			parser = theFactory.newDocumentBuilder();
			domTree = parser.parse(xmlFile);
		} catch (ParserConfigurationException e) {
			throw new JSimMastException("Problems creating the DOM-parser for XML file \"" + xmlFile.getName() + "\"");
		} catch (SAXException e) {
			throw new JSimMastException("Problems with the XML file \"" + xmlFile.getName() + "\" while parsing.");
		} catch (IOException e) {
			throw new JSimMastException("Problems with the XML file while parsing.");
		}
		return domTree;
	}

	/**
	 * This method DOM-parses a XML file stored at the path specified by the
	 * argument and returns its root element.
	 * 
	 * @param xmlFilePath
	 *            The path of the XML file.
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws ParseableFileNotFoundException
	 */
	private static Document parseXMLfile(InputStream xmlIS) throws JSimMastException {
		DocumentBuilderFactory theFactory = DocumentBuilderFactory.newInstance();
		theFactory.setNamespaceAware(true); // I support namespaces
		DocumentBuilder parser = null;
		Document domTree = null;
		try {
			parser = theFactory.newDocumentBuilder();
			domTree = parser.parse(xmlIS);
		} catch (ParserConfigurationException e) {
			throw new JSimMastException("Problems creating the DOM-parser for XML file");
		} catch (SAXException e) {
			throw new JSimMastException("Problems with the XML file while parsing.");
		} catch (IOException e) {
			throw new JSimMastException("Problems with the XML file while parsing.");
		}
		return domTree;
	}
	
	/**
	 * This method DOM-parses a XML file stored at the path specified by the
	 * argument and returns its root element.
	 * 
	 * @param xmlFilePath
	 *            The path of the XML file.
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws ParseableFileNotFoundException
	 */
	public static Document parseXMLfile(Object xmlSource) throws JSimMastException {
		if (xmlSource instanceof String)
			return parseXMLfile((String)xmlSource);
		else if (xmlSource instanceof File)
			return parseXMLfile((File)xmlSource);
		else if (xmlSource instanceof InputStream)
			return parseXMLfile((InputStream)xmlSource);
		else
			throw new JSimMastException("not xml source");
	}
	
	
	/* */
	
	/**
	 * This method accepts an Element which is pretended to be part 
	 * of a DOM tree and a File/InputStream which is supposed to be a XML file, 
	 * parses it and adds the resultant DOM tree to the 1st DOM tree, appending the 
	 * rootElement of the 2nd DOM tree to the 1st argument. 
	 * Consequently, the 1st DOM tree will be modified.
	 * @param hookElement
	 * @param XMLsource
	 */
	public static Document addDOMtree(Document firstDOMtree, Element hookElem, Document secondDOMtree){		
        Node hangingElem = firstDOMtree.importNode(secondDOMtree.getDocumentElement(), true);		        
        hookElem.appendChild(hangingElem);
        return firstDOMtree;
	}
	
	/**
	 * This method accepts a NodeList and returns an array (with non-empty cells) which
	 * contains the nodes in the argument whose type is Element. 
	 * @param nodeList
	 * @return Those nodes in the argument whose type is Element
	 */
	public static Element[] filterNodes(NodeList nodeList){				
		/*
		 * I create an array to dump the childNodes of type Element and to count how many they are.
		 * This array will have empty cells.
		 */
		Element[] elems = new Element[nodeList.getLength()];
		int numElems = 0;
		for (int i = 0; i < nodeList.getLength(); i++) {
			if (nodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
				elems[i] = (Element)nodeList.item(i);
				numElems++;
			}
		}
		
		/* I create another array with length equal to number of childElements
		   in order to store them without empty cells */
		Element[] elemsTight = new Element[numElems];
		int k = 0;
		for (int i = 0; i < elems.length; i++) {
			if (elems[i] != null) {
				elemsTight[k] = elems[i];
				k++;
			}
		}
		return elemsTight;
	}

	
	/* J. M. methods */

	/**
	 * This method returns a <code>Vector</code> containing all
	 * <code>Element</code> typed children of the <code>Node</code> specified by
	 * the <code>parent</code> argument.
	 * 
	 * @param parent
	 * @return
	 */
	public static Vector<Element> getChildElem(Node parent) {

		Vector<Element> v = new Vector<Element>();
		NodeList nl = parent.getChildNodes();

		for (int i = 0; i < nl.getLength(); i++) {
			if (nl.item(i).getNodeType() == Node.ELEMENT_NODE) {
				v.add((Element)(nl.item(i)));
			}
		}
		return v;
	}

	/**
	 * This method returns a <code>Vector</code> containing all
	 * <code>Element</code> typed children of the <code>Node</code> specified by
	 * the <code>parent</code> argument whose local name is <code>name</code>.
	 * 
	 * @param parent
	 * @param nodeName
	 * @return
	 */
	public static Vector<Element> getChildElem(Node parent, String name) {

		Vector<Element> v = new Vector<Element>();
		NodeList nl = parent.getChildNodes();

		for (int i = 0; i < nl.getLength(); i++) {
			if ((nl.item(i).getNodeType() == Node.ELEMENT_NODE)
					&& (nl.item(i).getLocalName().equals(name))) {
				v.add((Element)(nl.item(i)));
			}
		}
		return v;
	}

	/**
	 * This method returns the <code>TextNode</code> typed child of the
	 * <code>Node</code> specified by the <code>parent</code> argument. If no
	 * such a child exists the method returns "".
	 * 
	 * @param parent
	 * @return
	 */
	public static String getTextNode(Node parent) {// , String nodeName){

		NodeList nl = parent.getChildNodes();

		for (int i = 0; i < nl.getLength(); i++) {
			if ((nl.item(i).getNodeType() == Node.TEXT_NODE)) {// &&
				// (nl.item(i).
				// getLocalName
				// ().equals(
				// nodeName))){
				return nl.item(i).getTextContent();
			}
		}
		return "";
	}

	/* Aggregated to J. M. methods */
	
	/**
	 * This method returns a <code>Vector</code> containing all
	 * <code>Element</code> typed children of the <code>Node</code> specified by
	 * the <code>parent</code> argument whose local name matches the regular
	 * expression <code>regex</code>.
	 * 
	 * @param parent
	 * @param nodeName
	 * @return
	 */
	public static Vector<Element> getChildElemByRegex(Node parent, String regex) {

		Vector<Element> v = new Vector<Element>();
		NodeList nl = parent.getChildNodes();

		for (int i = 0; i < nl.getLength(); i++) {
			if ((nl.item(i).getNodeType() == Node.ELEMENT_NODE)
					&& StringsHandler.checkString(regex, nl.item(i).getLocalName())) {
				v.add((Element)(nl.item(i)));
			}
		}
		return v;
	}
	
	/**
	 * This method returns a <code>boolean</code> indicating if the 
	 * <code>parent</code> argument has <code>Element</code> typed children. 
	 * 
	 * @param parent
	 * @return
	 */
	public static boolean hasChildElem(Node parent) {

		NodeList nl = parent.getChildNodes();

		for (int i = 0; i < nl.getLength(); i++) {
			if (nl.item(i).getNodeType() == Node.ELEMENT_NODE) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * This method returns a <code>boolean</code> indicating if the 
	 * <code>parent</code> argument has <code>Element</code> typed children with
	 * localname equals to the <code>name</code> argument. 
	 * 
	 * @param parent
	 * @return
	 */
	public static boolean hasChildElem(Node parent, String name) {

		NodeList nl = parent.getChildNodes();

		for (int i = 0; i < nl.getLength(); i++) {
			if ( (nl.item(i).getNodeType() == Node.ELEMENT_NODE) 
					&& (nl.item(i).getLocalName().equals(name)) ) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * This method returns a <code>boolean</code> indicating if the 
	 * <code>parent</code> argument has <code>Element</code> typed children with
	 * localname matching the regular expression <code>regex</code>.
	 * 
	 * @param parent
	 * @return
	 */
	public static boolean hasChildElemByRegex(Node parent, String regex) {

		NodeList nl = parent.getChildNodes();

		for (int i = 0; i < nl.getLength(); i++) {
			if ( (nl.item(i).getNodeType() == Node.ELEMENT_NODE) && 
				  StringsHandler.checkString(regex, nl.item(i).getLocalName())) {
				return true;
			}
		}
		return false;
	}
	
	
	/* Repository mehods */
	
	/**
	 * This method accepts an <code>Element</code> in a DOM-tree and a string
	 * representing the way into the tree towards one of its descendants and
	 * returns that descendant <code>Element</code>.
	 * 
	 * @param ancestor
	 *            The <code>Element</code> in the DOM-tree representing the
	 *            starting point from which the search will be performed.
	 * @param ID
	 *            The identifier used to identify the descendants in the
	 *            <code>ref</code> argument.
	 * @param ref
	 *            The way from the <code>ancestor</code> argument to the
	 *            required descendant, in the form: 
	 *            childID.grandChildID. ... .requiredDescendantID
	 * @return The required descendant <code>Element</code>, whose ID = last
	 *         segment of the <code>reference</code> argument or null if such a
	 *         descendant does not exist.
	 */
	public static Element getElemByRef(Element ancestor, String ID, String ref) {

		// I get the children of ancestor
		Vector<Element> children = getChildElem(ancestor);

		/* Recursive algorithm */

		int n = ref.indexOf(".");

		// Direct case (the required descendant is one of the children)
		if (n == -1)
			for (Element child : children) {
				if (child.getAttribute(ID).equals(ref)) {
					return child;
				}
			}
		// Recursive case
		else {
			String firstSegment = ref.substring(0, n);
			String subRef = ref.substring(n + 1);

			for (Element child : children) {
				if (child.getAttribute(ID).equals(firstSegment)) {
					getElemByRef(child, ID, subRef);
				}
			}
		}
		return null;
	}
	
	/**
	 * This method accepts an <code>Element</code> in a DOM-tree and a string
	 * representing the way into the tree towards one of its descendants and
	 * returns that descendant <code>Element</code>.
	 * 
	 * @param ancestor
	 *            The <code>Element</code> in the DOM-tree representing the
	 *            starting point from which the search will be performed.
	 * @param reference
	 *            The way from the <code>ancestor</code> argument to the
	 *            required descendant, in the form: 
	 *            ancestorChild.ancestorGrandChild. ... .requiredDescendant
	 * @return The required descendant <code>Element</code>, whose localName =
	 *         last segment of the <code>reference</code> argument or null if
	 *         such a descendant does not exist.
	 */
	/*public static Element getElemByID(Element ancestor, String localName, String ID) {

		Element requiredElem = 
		
		// I get the children of ancestor
		Vector<Element> children = getChildElem(ancestor);

		 Recursive algorithm 

		// Direct case (the required descendant is one of the children)
		for (Element child : children) {
			if (child.getLocalName().equals(localName) && child.getAttribute("name").equals(ID)) {
				return child;
			} 
		}
		// Recursive case
		else {
			String firstSegment = ref.substring(0, n);
			String subReference = ref.substring(n + 1);

			for (Element child : children) {
				if (child.getLocalName().equals(firstSegment)) {
					getElemByRef(child, subReference);
				}
			}
		}
		return null;
	}*/
	
	/**
	 * This method accepts a string representing the locator of a DOM-tree
	 * <code>Element</code> and returns that <code>Element</code>
	 * 
	 * @param locator
	 *            xmlFilePath::rootChildID.rootGrandChildID. ... .requiredElementID
	 * @return The required <code>Element</code> of the DOM-tree (tree
	 *         corresponding to the XML file stored at xmlFilePath) whose
	 *         ID = last segment of the <code>locator</code> argument or
	 *         null if such a descendant does not exist.
	 * @throws JSimMastException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 */
	public Element getElemByLocator(String locator, String ID) 
			throws ParserConfigurationException, SAXException, JSimMastException{
		
		// I split the locator in path & reference
		String path = StringsHandler.splitLocator(locator)[0];
		String ref = StringsHandler.splitLocator(locator)[1];
		
		// I get the root of the XML file
		Element root = parseXMLfile(path).getDocumentElement();
		
		// I get the required Element
		if (ref.equals(""))
			return root;
		else
			return getElemByRef(root, ID, ref);
	}
	
}
