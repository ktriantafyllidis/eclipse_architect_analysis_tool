/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Abstract class: Operation
 * Description: Root class for the operation elements of the model. 
 * 
 * Author: J.M. Drake
 * Date: 20-12-09
 ********************************************************************/

package operation;

import java.util.List;

import org.w3c.dom.*;

import utilityClasses.XMLparser;
import core.*;

public abstract class Operation extends ModelElement {
	
	/* FIELDS */
	
	// If false the change of priority is in effect only until
	// the operation is completed. if true the change of the
	// priority is in effect until another permanent overridden
	// priority, or until the end of the segment of activities,
	// i.e., a set of consecutive activities (consecutive in the
	// transaction graph) executed by the same scheduling server.
	private boolean isPermanent = false;
		public boolean isPermanent() {return isPermanent;}
		public void setIsPermanent(boolean isPermanent) {this.isPermanent = isPermanent;}
	
	// The overridden priority represents a priority level above
	// the normal priority level that at which the operation would execute:
	private int overriddenPriority = -1;
		public int overriddenPriority() {return overriddenPriority;}
		public void setOverriddenPriority(int overriddenPriority) {this.overriddenPriority = overriddenPriority;}
	
	private boolean thereIsOverriddenPriority = false;
		public boolean thereIsOverriddenPriority() {return thereIsOverriddenPriority;}
		public void setThereIsOverriddenPriority(boolean thereIsOverriddenPriority) {this.thereIsOverriddenPriority = thereIsOverriddenPriority;}
	
	
	/* METHODS */
	
	// Constructors
	protected Operation(){}
	protected Operation(Node node){
		
		super(node);
		
		List<Element> operChildElemList = XMLparser.getChildElem(node);
		for (Element operChildElem : operChildElemList) {
			String localName = operChildElem.getLocalName();
    		if(localName.equals("Overridden_Fixed_Priority")) {
    			isPermanent = false;
    			thereIsOverriddenPriority = true;
    			overriddenPriority = Integer.parseInt(operChildElem.getAttribute("Priority"));
    			break;
    		} else if (localName.equals("Overridden_Permanent_FP")) {
    			isPermanent = true;
    			thereIsOverriddenPriority = true;
    			overriddenPriority = Integer.parseInt(operChildElem.getAttribute("Priority"));
    			break;
    		}
	    }
	}
	
	public static Operation newOperation(Node node){
		String localName = node.getLocalName();
		if (localName.equals("Simple_Operation"))
		    return new SimpleOperation(node);
		else if (localName.equals("Message"))
	    	return new Message(node);
		else if (localName.equals("Composite_Operation"))
	    	return new CompositeOperation(node);
		else if (localName.equals("Composite_Message"))
	    	return new CompositeMessage(node);
		else // localName.equals("Enclosing_Operation")
	    	return new EnclosingOperation(node);
	}

	public void complete(){}
	
	public void denormalize(){}
	


	@Override
	public String toString(){
		String string = super.toString() + 
		                "\nOverridden priority = ";
		if (overriddenPriority != -1)
			string += overriddenPriority;
		return string;
	}
	
}
