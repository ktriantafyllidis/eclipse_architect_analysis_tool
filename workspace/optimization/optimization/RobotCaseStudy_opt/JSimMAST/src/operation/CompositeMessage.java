package operation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import core.Repository;

import utilityClasses.XMLparser;

public class CompositeMessage extends Message{
	
	
	// List of operations
	private List<String> operRefList = new ArrayList<String>();
	private List<Message> operList = new ArrayList<Message>();
	public Iterator<Message> getOperListIter(){return operList.iterator();}
	
		
	/* METHODS */
	
	// Constructor
	public CompositeMessage(Node node){
		super(node);
		List<Element> compOperChildElemList = XMLparser.getChildElem(node);
		for (Element compOperChildElem : compOperChildElemList) {
			if(compOperChildElem.getLocalName().equals("Operation"))
				operRefList.add(compOperChildElem.getAttribute("Name"));
		}
	}
	
	@Override
	public void complete(){
		for (String operRef : operRefList) {
			operList.add(((Message)Repository.getModelElem(operRef, "Operation")));
		}
	}
	

	
	@Override
	public String toString(){
		String string = "\nList of operations:";
		for (Operation oper : operList) {
			string += "\n\t" + oper.name();
		}
		return "\n\nComposite message:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
	

}
