/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Abstract class: Operation
 * Description: They represent a piece of code, or a message.. 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package operation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import module.ETG;
import module.Tools;
import module.WABETG;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import utilityClasses.XMLparser;

public class SimpleOperation extends CodeOperation{
	
	/* FIELDS */
	
	// Execution time
	private ETG etg;
		public ETG etg() {return etg;}
	
	// List of references to the mutexes that must be locked before executing the operation.
	private List<String> mutexToLockRefList = new ArrayList<String>(0);
		public Iterator<String> getLockIter(){
			if (mutexToLockRefList != null) {
				return mutexToLockRefList.iterator();
			}
			return null;
		}

	// List of references to the mutexes that must be unlocked after executing the operation.
	private List<String> mutexToUnlockRefList = new ArrayList<String>(0);
		public Iterator<String> getUnlockIter(){
			if (mutexToUnlockRefList != null) {
				return mutexToUnlockRefList.iterator();
			}
			return null;
		}
	
		
	/* METHODS */
	
	// Constructor
	public SimpleOperation(Node node){

		super(node);
		
		Element elem = (Element)node;
		
		long worst = 0;
		if (elem.hasAttribute("Worst_Case_Execution_Time"))
			worst = Tools.readLong(elem.getAttribute("Worst_Case_Execution_Time"));
		
		long avg = 0;
		if (elem.hasAttribute("Avg_Case_Execution_Time"))
			avg = Tools.readLong(elem.getAttribute("Avg_Case_Execution_Time"));
		
		long best = 0;
		if (elem.hasAttribute("Best_Case_Execution_Time"))
			best = Tools.readLong(elem.getAttribute("Best_Case_Execution_Time"));
		
		etg = new WABETG(worst, avg, best);
		
		List<Element> simpleOperChildElemList = XMLparser.getChildElem(node);
		String localName;
		for (Element simpleOperChildElem : simpleOperChildElemList) {
			 localName = simpleOperChildElem.getLocalName();
			if(localName.equals("Mutex")) {
				mutexToLockRefList.add(simpleOperChildElem.getAttribute("Name"));
				mutexToUnlockRefList.add(simpleOperChildElem.getAttribute("Name"));
				break;
			} else if(localName.equals("Mutex_To_Lock"))
				mutexToLockRefList.add(simpleOperChildElem.getAttribute("Name"));
			else if(localName.equals("Mutex_To_Unlock"))
				mutexToUnlockRefList.add(simpleOperChildElem.getAttribute("Name"));
		}
	}
	
	
	
	@Override
	public String toString(){
		String string = super.toString() + "\nMutex list:";
		if (mutexToLockRefList != null)
			for (String sharedResourceToLockRef : mutexToLockRefList) {
				string += "\n\t" + sharedResourceToLockRef;
			}

		else if (mutexToUnlockRefList != null)
			for (String sharedResourceToUnlockRef : mutexToUnlockRefList) {
				string += "\n\t" + sharedResourceToUnlockRef;
			}
		return "\n\nSimple operation" + (string + etg.toString()).replaceAll("\n", "\n\t");
	}
}