Mast enhancements
-----------------

- Less pessimistic treatment  of polling tasks
  see examples/polling_server_example.txt

- Report cause of restriction failure even when no "verbose" option is
  specified

Editor bugs
-----------

Change the name of the transaction not propagated to mss file. Then,
when wrong names found in mss file, transaction graph is not displayed

Chanching the deadline of a hard deadline does not have an effect

Mast bugs
---------

- Explain results in test/vars.txt and tests/var1.txt with varying priorities

- Error when using zero tranzactions

- Error when missing mast-model file in mast_xml_convert_results


* - Mixture of EDF + immediate priority ceiling not detected (See
    controller_mal)
  
    Solved by adding "SRP Only" restriction to "EDF_Monoprocessor analysis"


* - Failure when network has FP policy
  
    Solved by adding a new consistency check (rule 27)

* - Bad level assignment (too low)

    Solved by adding SRP_Parameters objects when they are not declared
    This is done in Scheduling_Servers.Adjust 

* - Empty transaction not allowed

    We allow an empty task in the parser
    New consistency checks not needed, because covered by consistent
    transaction graphs

*- Best case treatment of sporadic servers incorrect in linear-analysis-tools
    Reason is the misstretment of cbij and cbijown.
    done

- See controller_ss: Sporadic_Server with unbounded task does not
    work correctly. Problem is that when background level coincides
    with something else the analysis takes very long

    will solve by creating a new task to model the background (later)


Editor enhancements
-------------------

- Analyze button does not work.

- Changes to a transaction are not cancelled when cancel button is pressed

- Conversion of type of shared object is not allowed

- Conversion between composite and enclosing operations is not allowed

- Attributes menu disappears when button is released

- View servers option can be simplified by searching ths the list of
  event andlers directly, not traversing the graph

- View WCETS in operations

- View Periods in transactions

- View priorities in Scheduling servers

* - Do not ask "are you sure?" before leaving.

    Done, except when changes are discarded


Editor bugs
-----------

- If a network has two drivers on the same processor, only one is shown.

- When a simple operation is converted to "enclosing", the menu to add
  operations is not active.

- Filename when it does not end with ".txt"
    
       done

- Delete scheduler being used by a scheduling server
       done

- Delete Processing resource being used by a scheduler
       done

- Delete Scheduling server being used by an activity
       done

- Delete Operation being used by an activity
       done

- Delete Operation being used by another operation
       done
       
- Delete shared object being used by an operation
       done

- If an activity is changed to system-timed activity it looses its
  links
       done

- Choosing "priority" policy in Query server produces "invalid value" error
       done 

- When Changing the server of a transaction the links are eliminated
       done

- Constraint error
    see r1.txt. Done

- When cancelling transactions the links are eliminated
       done

- Delete an event referenced by some timing requirement
       done

- Delete an output event from a transaction. The corresponding output link
  is not deleted 
       done

- When you delete items from a transaction it is not drawn correctly
       done

- Add simple in a transaction raises invalid value
       done

- Other deletions in transactions?
       OK

- Secondary scheduler is not correctly shown
     - done

- Server synchronization parameters always set to SRP
     - done

- View servers of a shared object does not work on enclosing operations
     - done

- Open does not work on files that do not have the "txt" extension
      - done

- Import does not show messages with repeated elements
     - done
