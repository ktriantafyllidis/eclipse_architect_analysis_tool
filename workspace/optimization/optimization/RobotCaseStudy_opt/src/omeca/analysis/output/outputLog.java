package omeca.analysis.output;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

public class outputLog {
	
	private File file = null;
	
	PrintWriter pw;
	
	public void newFile(String path)
	{
		
		
		file = new File(path);
		try {
			file.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		
		try {
			pw = new PrintWriter(new FileOutputStream(file));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void writeToFile(ArrayList<ArrayList<String>> analysisResults, String typeOfData)
	{

		pw.println(typeOfData + "\n");
		
		
		for(int i=0; i<analysisResults.size(); i++)
		{
			pw.println(analysisResults.get(i));
		}
		
		pw.println("\n\n");
		
	}
	
	
	
	public void close()
	{
		pw.close();
	}
	
	
	
	
	public void outputLog()
	{
		
		
	}

}
