package omeca.analysis.output;


import java.io.*;
import java.util.ArrayList;

public class readScheAnalysisResults {
	
	private BufferedReader bf;
	
	private ArrayList<ArrayList<String>> cpuUtilization = new ArrayList<ArrayList<String>>();
	private ArrayList<ArrayList<String>> networkUtilization = new ArrayList<ArrayList<String>>();
	private ArrayList<ArrayList<String>> responseDelay = new ArrayList<ArrayList<String>> ();
	
	public void openOutputFile(String filePath)
	{
		try {
			bf  = new BufferedReader(new FileReader(filePath));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void close()
	{
		try {
			bf.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public ArrayList<ArrayList<String>>  getCPUUtilization()
	{
		String NameOfPR;
		String Processing_Usage;
        String line, next;
        
        // Loop through each line, stashing the line into our line variable.
        try {
			for (line = bf.readLine(); line != null; line = next)
			{
				next = bf.readLine();
			    // Increment the count and find the index of the word
		    	ArrayList<String> _processingResource = new ArrayList<String>();

			    int indexfound = line.indexOf("Processing_Resource (");
			    if (indexfound > -1) 
			    {
			        //System.out.println("Word was found at position " + indexfound + " on line " + linecount);
			        NameOfPR = next.replace("Name     =>", " ").replace(",", " ").trim();
			        _processingResource.add(NameOfPR);
			        // System.out.println("The name of the Processing resource is: " + NameOfPR);
			        next = bf.readLine();
			        next = bf.readLine();
			        next = bf.readLine();
			        Processing_Usage = next.replace("Total  => ", " ").replace("%)));", " ").trim();
			        //System.out.println("The Resource Usage is: " + Processing_Usage);
			        _processingResource.add(Processing_Usage);

			        cpuUtilization.add(_processingResource);
			    }  


			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        // Close the file after done searching
        try {
			bf.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return cpuUtilization;
        
	}
		
	public ArrayList<ArrayList<String>>  getResponseDelays()
	{
		ArrayList<String> wcet = new ArrayList<String>();
		ArrayList<String> bcet = new ArrayList<String>();
		
		String NameOfPR;
		String delay;
        String line, next;
        
        int counter = 0;
        
        String transaction = null;
        
        // Loop through each line, stashing the line into our line variable.
        try {
			for (line = bf.readLine(); line != null; line = next)
			{
				next = bf.readLine();
			    // Increment the count and find the index of the word
		    	ArrayList<String> _responseDelay = new ArrayList<String>();

			    int indexfound = line.indexOf("Transaction (");
			    if (indexfound > -1) 
			    {
			        //System.out.println("Word was found at position " + indexfound + " on line " + linecount);
			        NameOfPR = next.replace("Name     =>", " ").replace(",", " ").trim();
			        _responseDelay.add(NameOfPR);
			        // System.out.println("The name of the Processing resource is: " + NameOfPR);
			        next = bf.readLine();
			        
			        for (line = bf.readLine(); line != null; line = next)
			        {
			        
			        	transaction += next;
			        	next = bf.readLine();
			        	
			        	//find the number of the internal events
			        	if (line.indexOf("Worst_Global_Response_Times") > -1)
			        	{
			        		//go to the very next line and get the value
			        		next = bf.readLine();
			        		
			        		if (next.indexOf("Time_Value") > -1)
			        		{
			        			wcet.add(next.replace("Time_Value       =>", " ").replace(")),", " ").trim());
			        			
			        		}
			        		counter ++;
			        	}
			        	else if (line.indexOf("Best_Global_Response_Times") > -1)
			        	{
			        		
			        		//go to the very next line and get the value
			        		next = bf.readLine();
			        		
			        		if (next.indexOf("Time_Value") > -1)
			        		{
			        			bcet.add(next.replace("Time_Value       =>", " ").replace(")),", " ").trim());
			        			
			        		}
			        			
			        	}
			        	
			        	//find the last line of the transaction
			        	else if (line.indexOf(";") > -1)
			        	{
			        		//find the last 
			        		_responseDelay.add(wcet.get(counter-1));
			        		_responseDelay.add(bcet.get(counter-1));
			        		//returns an array with [transactioName, wcet, bcet]
			        		responseDelay.add(new ArrayList<String>(_responseDelay));
			        		wcet.clear();
			        		bcet.clear();

			        		counter = 0;
			        		transaction = null;
			        		break;
			        		
			        	}
			        	
			        }
			        
			    }
			    
						    
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        // Close the file after done searching
        try {
			bf.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        //System.out.println("The time delay is " + responseDelay);
        
        return responseDelay;
        
	}
		
	
}