package omeca.analysis.output;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import omeca.analysis.classes.*;


/**This class reads the simulation analysis output and returns the vectors
 * */
public class readSimulationResults {
	
	utilization Util = new utilization();
	
	private ArrayList<ArrayList<String>> cpuUtilization = new ArrayList<ArrayList<String>>();
	private ArrayList<ArrayList<String>> networkUtilization = new ArrayList<ArrayList<String>>();
	private ArrayList<ArrayList<String>> responseDelay = new ArrayList<ArrayList<String>> ();
	
	private String simulationResults;
	private Document doc;
	
	
	public void openOutputFile(String simulationResults)
	{
		this.simulationResults = simulationResults;
		
		File file = new File(simulationResults);
		 
		DocumentBuilder dBuilder = null;
		try {
			dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 
		try {
			this.doc = dBuilder.parse(file);
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	//get the child nodes recursively 
	private Node getChildren(Node node)
	{
		NodeList ndl = node.getChildNodes();
		for (int i=0; i<ndl.getLength(); i++)
		{
			Node tmpNode = ndl.item(i);
			 if(tmpNode.getNodeName().equals("mast_res:Computing_Resource_Results"))
			 {
				 getChildren(tmpNode);
				 System.out.println(((Element) tmpNode).getAttribute("Name"));
			 }
			 else if(tmpNode.getNodeName().equals("mast_res:Detailed_Utilization_Results"))
			 {
				 System.out.println(((Element) tmpNode).getAttribute("Total"));
			 }
		}
		
		return node;
	}
	
	
	public void getCPUUtilizationClass()
	{
		NodeList nodeListRoot = doc.getElementsByTagName("*");
		//get the child nodes recursively
	    /*	for (int i = 0; i < nodeListRoot.getLength(); i++)
		{
			Node node = nodeListRoot.item(i);
			getChildren(node);
			
		}*/
					
	    for (int i = 0; i < nodeListRoot.getLength(); i++) 
	    {
			Node node = nodeListRoot.item(i);
				
		    	 if(node.getNodeName().equals("mast_res:Computing_Resource_Results"))
		    	 {
		    		 cpuUtilization cpuUtil = new cpuUtilization();
		    		 String totalName = ((Element) node).getAttribute("Name");
		    		 String []tmp = totalName.split("_n");
		    		 cpuUtil.nameOfCPU = tmp[0].replace(" ", "").trim();
		    		 tmp = tmp[1].split("_c");
		    		 cpuUtil.node = tmp[0].replace(" ","").trim();	 
		    		 cpuUtil.utilization.coreNum = tmp[1].trim();
		    		 //System.out.println(((Element) node).getAttribute("Name"));
		    		 NodeList tmp1 = node.getChildNodes();
		    		 for (int j=0; j<tmp1.getLength(); j++)
		    		 {
		    			 if (tmp1.item(j).getNodeName().equals("mast_res:Detailed_Utilization_Results"))
		    			 {
		    				 Element elm = (Element) tmp1.item(j);
		    				 cpuUtil.utilization.util = elm.getAttribute("Total");
		    				 //System.out.println(elm.getAttribute("Total"));
		    				 break;
		    			 }
		    		 }
		 
		    		 Util.cpuUtilizations.add(cpuUtil);
		    			    		 		    		 
		    	 }
		    	

	    }
		
	}
	
	
	public ArrayList<ArrayList<String>> getCPUUtilization()
	{
		ArrayList<String> cpuUtilTmp =  new ArrayList<String>();
		NodeList nodeListRoot = doc.getElementsByTagName("*");
		//get the child nodes recursively
	    /*	for (int i = 0; i < nodeListRoot.getLength(); i++)
		{
			Node node = nodeListRoot.item(i);
			getChildren(node);
			
		}*/
					
	    for (int i = 0; i < nodeListRoot.getLength(); i++) 
	    {
			Node node = nodeListRoot.item(i);
				
		    	 if(node.getNodeName().equals("mast_res:Computing_Resource_Results"))
		    	 {
		    		 
		    		 cpuUtilTmp.add(((Element) node).getAttribute("Name"));
		    		 //System.out.println(((Element) node).getAttribute("Name"));
		    		 NodeList tmp1 = node.getChildNodes();
		    		 for (int j=0; j<tmp1.getLength(); j++)
		    		 {
		    			 if (tmp1.item(j).getNodeName().equals("mast_res:Detailed_Utilization_Results"))
		    			 {
		    				 Element elm = (Element) tmp1.item(j);
		    				 cpuUtilTmp.add(elm.getAttribute("Total"));
		    				 //System.out.println(elm.getAttribute("Total"));
		    				 break;
		    			 }
		    		 }
		 
		    		 cpuUtilization.add(new ArrayList<String>(cpuUtilTmp));
		    		 cpuUtilTmp.clear();
		    			    		 		    		 
		    	 }
		    	

	    }
		
	     //System.out.println(cpuUtilization);
		 return this.cpuUtilization;
		
	}
	public void getNetworkUtilizationClass()
	{
		
		
		NodeList nodeListRoot = doc.getElementsByTagName("*");
		
		 for (int i = 0; i < nodeListRoot.getLength(); i++) 
		 {
			 networkUtilization netUtil = new networkUtilization();
			 Node node = nodeListRoot.item(i);
					
			    	 if(node.getNodeName().equals("mast_res:Network_Results"))
			    	 {
			    		 
			    		 netUtil.nameOfNetwork = ((Element) node).getAttribute("Name");
			    		 //System.out.println(((Element) node).getAttribute("Name"));
			    		 NodeList tmp1 = node.getChildNodes();
			    		 for (int j=0; j<tmp1.getLength(); j++)
			    		 {
			    			 if (tmp1.item(j).getNodeName().equals("mast_res:Detailed_Utilization_Results"))
			    			 {
			    				 Element elm = (Element) tmp1.item(j);
			    				 netUtil.utilization = elm.getAttribute("Total");
			    				 //System.out.println(elm.getAttribute("Total"));
			    				 break;
			    			 }
			    		 }
			 
			    		 Util.networkUtilizations.add(netUtil);
			    			    		 		    		 
			    	 }
		    }
	}
	
	public ArrayList<ArrayList<String>> getNetworkUtilization()
	{
		ArrayList<String> netUtilTmp =  new ArrayList<String>();
		NodeList nodeListRoot = doc.getElementsByTagName("*");
		
		 for (int i = 0; i < nodeListRoot.getLength(); i++) 
		    {
				Node node = nodeListRoot.item(i);
					
			    	 if(node.getNodeName().equals("mast_res:Network_Results"))
			    	 {
			    		 
			    		 netUtilTmp.add(((Element) node).getAttribute("Name"));
			    		 //System.out.println(((Element) node).getAttribute("Name"));
			    		 NodeList tmp1 = node.getChildNodes();
			    		 for (int j=0; j<tmp1.getLength(); j++)
			    		 {
			    			 if (tmp1.item(j).getNodeName().equals("mast_res:Detailed_Utilization_Results"))
			    			 {
			    				 Element elm = (Element) tmp1.item(j);
			    				 netUtilTmp.add(elm.getAttribute("Total"));
			    				 //System.out.println(elm.getAttribute("Total"));
			    				 break;
			    			 }
			    		 }
			 
			    		 networkUtilization.add(new ArrayList<String>(netUtilTmp));
			    		 netUtilTmp.clear();
			    			    		 		    		 
			    	 }
			    	

		    }
		 //System.out.println(networkUtilization);
		
		 return this.networkUtilization;
	}
	
	
	public void getResponseDelaysClass()
	{
		
		NodeList nodeListRoot = doc.getElementsByTagName("mast_res:End_To_End_Flow_Results");
		
		 for (int i = 0; i < nodeListRoot.getLength(); i++) 
		    {
			    responseDelay respDelay = new responseDelay();
				Node node = nodeListRoot.item(i);
				respDelay.eventName = ((Element) node).getAttribute("Name");
				NodeList tmp0 = node.getChildNodes();
				 
			    for(int k=0; k<tmp0.getLength(); k++)
			    {
			    	Node node2 = tmp0.item(k);
				
			    	 if(node2.getNodeName().equals("mast_res:Simulation_Timing_Results"))
			    	 {

			    		 //System.out.println(((Element) node).getAttribute("Name"));
			    		 NodeList tmp1 = node2.getChildNodes();
			    		 for (int j=0; j<tmp1.getLength(); j++)
			    		 {
			    			 if (tmp1.item(j).getNodeName().equals("mast_res:Worst_Global_Response_Time"))
			    			 {
			    				 Element elm = (Element) tmp1.item(j);
			    				 respDelay.WCET = msTosec(elm.getAttribute("Value"));
			    				 //System.out.println(elm.getAttribute("Total"));
			    			 }
			    			 else if (tmp1.item(j).getNodeName().equals("mast_res:Avg_Global_Response_Time"))
			    			 {
			    				 Element elm = (Element) tmp1.item(j);
			    				 respDelay.ACET = msTosec(elm.getAttribute("Value"));
			    				 //System.out.println(elm.getAttribute("Total"));
			    			 }
			    			 else if (tmp1.item(j).getNodeName().equals("mast_res:Best_Global_Response_Time"))
			    			 {
			    				 Element elm = (Element) tmp1.item(j);
			    				 respDelay.BCET = msTosec(elm.getAttribute("Value"));
			    				 //System.out.println(elm.getAttribute("Total"));
			    			 }
			    		 }
			 
			    		 Util.responseDelays.add(respDelay);
		    			    		 		    		 
			    	 }
				
			    }
		    }
		 //System.out.println(responseDelay);
	
	}
	
	
	public ArrayList<ArrayList<String>> getResponseDelays()
	{
		ArrayList<String> responseTmp =  new ArrayList<String>();
		NodeList nodeListRoot = doc.getElementsByTagName("mast_res:End_To_End_Flow_Results");
		
		
		 for (int i = 0; i < nodeListRoot.getLength(); i++) 
		    {
				Node node = nodeListRoot.item(i);
				responseTmp.add(((Element) node).getAttribute("Name"));
				NodeList tmp0 = node.getChildNodes();
				 
			    for(int k=0; k<tmp0.getLength(); k++)
			    {
			    	Node node2 = tmp0.item(k);
				
			    	 if(node2.getNodeName().equals("mast_res:Simulation_Timing_Results"))
			    	 {

			    		 //System.out.println(((Element) node).getAttribute("Name"));
			    		 NodeList tmp1 = node2.getChildNodes();
			    		 for (int j=0; j<tmp1.getLength(); j++)
			    		 {
			    			 if (tmp1.item(j).getNodeName().equals("mast_res:Worst_Global_Response_Time"))
			    			 {
			    				 Element elm = (Element) tmp1.item(j);
			    				 responseTmp.add(msTosec(elm.getAttribute("Value")));
			    				 //System.out.println(elm.getAttribute("Total"));
			    			 }
			    			 else if (tmp1.item(j).getNodeName().equals("mast_res:Avg_Global_Response_Time"))
			    			 {
			    				 Element elm = (Element) tmp1.item(j);
			    				 responseTmp.add(msTosec(elm.getAttribute("Value")));
			    				 //System.out.println(elm.getAttribute("Total"));
			    			 }
			    			 else if (tmp1.item(j).getNodeName().equals("mast_res:Best_Global_Response_Time"))
			    			 {
			    				 Element elm = (Element) tmp1.item(j);
			    				 responseTmp.add(msTosec(elm.getAttribute("Value")));
			    				 //System.out.println(elm.getAttribute("Total"));
			    			 }
			    		 }
			 
			    		 responseDelay.add(new ArrayList<String>(responseTmp));
			    		 responseTmp.clear();
			    			    		 		    		 
			    	 }
				
			    }
		    }
		 //System.out.println(responseDelay);
		
		 return this.responseDelay;
	}
	
	public String msTosec(String ms)
	{

		float f = Float.parseFloat(ms);
		//f = f/1000;
		String sec = String.valueOf(f);
		
		return sec;
	}
	
	
	public utilization getSimulationResults(String currentDir, String resultsName)
	{
		this.openOutputFile(currentDir + "/models_simulation/" + resultsName);
		this.getCPUUtilizationClass();
		this.getNetworkUtilizationClass();
		this.getResponseDelaysClass();
		
		return this.Util;
		
	}
	
	
	
}

	

	
