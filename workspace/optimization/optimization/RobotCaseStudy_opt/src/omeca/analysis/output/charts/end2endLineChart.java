package omeca.analysis.output.charts;

import java.util.ArrayList;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

public class end2endLineChart extends lineChart{
	
	
	ArrayList<ArrayList<String>> simDelays = new ArrayList<ArrayList<String>>(); 
	ArrayList<ArrayList<String>> schedDelays = new ArrayList<ArrayList<String>>(); 
	ArrayList<ArrayList<String>> deadlines = new ArrayList<ArrayList<String>>();
	
	public end2endLineChart(ArrayList<ArrayList<String>> simDelays, ArrayList<ArrayList<String>> schedDelays, ArrayList<ArrayList<String>> deadlines)
	{
		this.simDelays = simDelays;
		this.schedDelays = schedDelays;
		this.deadlines = deadlines;
        final CategoryDataset dataset = createDataset();
        final JFreeChart chart = createChart(dataset);
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(800, 450));
        setContentPane(chartPanel);

	}
	//this is for loading the values at the same name 
	public float searchListName(ArrayList<ArrayList<String>>list, String name)
	{
		float value;
		for (int i=0; i<list.size();i++)
		{
			if (list.get(i).get(0).equals(name))
			{
				value = Float.parseFloat(list.get(i).get(1));
				return value;
			}
			
		}
		return 1;
	}
	
	private CategoryDataset createDataset() 
	{
		
		final String series1 = "Deadlines";
        final String series2 = "Schedulability";
        final String series3 = "Simulation";
        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        for (int i=0; i<simDelays.size(); i++)
        {
         	String name = simDelays.get(i).get(0);
         	float value = Float.parseFloat(simDelays.get(i).get(1));
         	if (value < 1)
         	{
         		dataset.addValue(value, series1, name);   
            	dataset.addValue(searchListName(schedDelays, name), series2, name);
            	dataset.addValue(searchListName(deadlines, name), series3, name);
         	}
        }
        
        return dataset;
	
	}
	

	
	
	
	

}
