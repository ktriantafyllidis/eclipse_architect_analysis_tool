package omeca.performanceModels;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.NodeList;

import omeca.performanceModels.helperClasses.Operation;
import omeca.performanceModels.helperClasses.OperationMetricsForAllPlatforms;
import omeca.performanceModels.helperClasses.PerformanceMetrics;
import omeca.performanceModels.helperClasses.PlatformSpecifications;


/*
 * In this class i load the performance models placed in the path ComponentModels/ProMo_models/
 * 
 * 
 * */
public class loadPerformanceModels {
	

		
		
		//maintain some lists with the name of the platforms and the names of the operations
		//i use list of objects because for each operation i have to give as well the performance metrics
		public ArrayList<Object> Operations = new ArrayList<Object>();
		public ArrayList<PlatformSpecifications> PlatformSpecifications_ = new ArrayList<PlatformSpecifications>();
		public ArrayList<String> platform_tmp = new ArrayList<String>();
		
		
		org.w3c.dom.Document doc;
		File file;

		
		 
		 public void openFile(File file) throws Exception
		 {
			 this.file = file;
			 this.open();
			 this.find_platform_specs();
			 this.find_operation_names();
		 }
		 

		 
		 
		
		public void open() throws Exception
		{

			//read xml promo model
			//File fXmlFile = new File(file_path);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(file);	
			doc.getDocumentElement().normalize();
			
		}
			
			
		
		
		 public void find_platform_specs() throws XPathExpressionException
		 {
			 
			 
			    XPathFactory factory = XPathFactory.newInstance();
		        XPath xpath = factory.newXPath();
		        
		        
		        //get the name and the specs of the platform except the type_of_cpu
		        String expr = "//element[@name='profilingPlatforms']/complexType/sequence/element[@name!='type_of_cpu']";
		        Object nodes = xpath.evaluate(expr,doc,XPathConstants.NODESET);
		        NodeList nodelist = (NodeList) nodes;
		        //get the items from the nodelist
		        //System.out.println("the nodelist size is : " + nodelist.getLength());
		        for(int i=0; i<nodelist.getLength();i++)
		        {
		        	org.w3c.dom.Node tmp = nodelist.item(i); 
		        	
		            //print
		        	org.w3c.dom.Element ch = (org.w3c.dom.Element) tmp;
		            Attr attr = ((org.w3c.dom.Element) ch).getAttributeNode("name");
		            
		            PlatformSpecifications platform = new PlatformSpecifications();
		            platform.nameOfPlatform = attr.getValue();
		            
		            
				    //I get the performance metrics and store to an ArrayList
				    String metrics = "./complexType/attribute";
				    Object metricsNodes = xpath.evaluate(metrics,tmp,XPathConstants.NODESET);
				    NodeList metricsNodelist = (NodeList) metricsNodes;
		            
				    
				    for (int j=0; j<metricsNodelist.getLength(); j++)
				    {
				    	org.w3c.dom.Node metricAttr = metricsNodelist.item(j);
				    	org.w3c.dom.Element metricElem = (org.w3c.dom.Element) metricAttr;
				        Attr nameMetric = ((org.w3c.dom.Element) metricElem).getAttributeNode("name");
				        Attr valueMetric = ((org.w3c.dom.Element) metricElem).getAttributeNode("value");

				        //System.out.println("The names are " + nameMetric.getValue());
				        //operation.performanceMetrics.add(/*nameMetric.getValue() + "=" +*/ valueMetric.getValue());
				        
				        //call the for loop of the class 
				        if (nameMetric.getValue().equals("number_of_threads"))
				        {
				        	platform.numberOfThreads = 	valueMetric.getValue();	        	
				        }
				        else if (nameMetric.getValue().equals("frequency"))
				        {
				        	platform.frequency = 	valueMetric.getValue();	
				        }
				        else if (nameMetric.getValue().equals("number_of_cores"))
				        {
				        	platform.numberOfCores = 	valueMetric.getValue();	
				        }
				        else if (nameMetric.getValue().equals("L1_cache_size"))
				        {
				        	platform.L1CacheSize = 	valueMetric.getValue();	
				        }
				        else if (nameMetric.getValue().equals("L2_cache_size"))
				        {
				        	platform.L2CacheSize = 	valueMetric.getValue();
				        }
				        else if (nameMetric.getValue().equals("L3_cache_size"))
				        {
				        	platform.L3CacheSize = 	valueMetric.getValue();
				        }
				        
				    }
		            
		            
				    PlatformSpecifications_.add(platform);
		            
		            
		        }
		       // System.out.println("the specsList size is : " + PlatformSpecifications_);
		 }

		 	 
		 
		 /**
		  * This method returns the names of the operations in an Arraylist
		  * */
		 public void find_operation_names() throws XPathExpressionException
		 {
			
			 
			    XPathFactory factory = XPathFactory.newInstance();
		        XPath xpath = factory.newXPath();
		        
		        
		        //get the name and the specs of the platform except the type_of_cpu
		        String expr = "//schema/element/complexType/sequence/element[@name!='implementedOperation' and @name!='externalAttribute' and @name!='profilingPlatforms']";
		        Object nodes = xpath.evaluate(expr,doc,XPathConstants.NODESET);
		        NodeList nodelist = (NodeList) nodes;

		        for(int i=0; i<nodelist.getLength();i++)
		        {
		        	org.w3c.dom.Node tmp = nodelist.item(i); 
		        
		        	
		            //print
		        	org.w3c.dom.Element ch = (org.w3c.dom.Element) tmp;
		            Attr attr = ((org.w3c.dom.Element) ch).getAttributeNode("name");
		            
		            
		            String name_of_operation = attr.getValue();

		            
		            //expr = "/complexType/attribute";
		            //NodeList specs = (NodeList) xpath.evaluate(expr,tmp,XPathConstants.NODESET);
		            
		            //System.out.println("the specsList size is : " + specs.getLength());
		            
		            Operations.add(name_of_operation);

		        }
	            //System.out.println("the name is : " + Operations);
		 }
		
		 
		 
		 
		 /**
		  * This is a method that returns a class of type Operation. This class includes information 
		  * about the performance metrics that the ProMo tool provides for each operation.
		  * 
		  * @param: String NameOfOperation => The name of the operation to be extracted
		  * 
		  * **/
		 public OperationMetricsForAllPlatforms OperationPerformanceMetrics(String NameOfOperation) throws XPathExpressionException
		 {
			 
			Operation operation = new Operation();
			 
			//this is the class that contains the operation metrics for more than one platform
			OperationMetricsForAllPlatforms operationforallplatforms = new OperationMetricsForAllPlatforms();
			ArrayList<String> operationMetric = new ArrayList<String>();
			XPathFactory factory = XPathFactory.newInstance();
			XPath xpath = factory.newXPath();
			
			operation.NameOfOperation = NameOfOperation;

			//get the name and the specs of the platform except the type_of_cpu
			String expr = "//element[@name='"+ NameOfOperation +"']/complexType/sequence/element[@name='cpuUsage']/complexType/sequence/element";
			Object nodes = xpath.evaluate(expr,doc,XPathConstants.NODESET);
			NodeList nodelist = (NodeList) nodes;
			
			//get the list that is referenced to the two platforms for each operation
			for(int i=0; i<nodelist.getLength();i++)
			{
				org.w3c.dom.Node tmp = nodelist.item(i); 
			
			    //print
				org.w3c.dom.Element ch = (org.w3c.dom.Element) tmp;
			    Attr attr = ((org.w3c.dom.Element) ch).getAttributeNode("name");
			    //String name_of_platform = attr.getValue();
			    //set the name of the platformo
			    operation.platform = getThePlatform(attr.getValue());
			    
			
			    //I get the performance metrics and store to an ArrayList
			    String metrics = "./complexType/attribute";
			    Object metricsNodes = xpath.evaluate(metrics,tmp,XPathConstants.NODESET);
			    NodeList metricsNodelist = (NodeList) metricsNodes;
			    
			    PerformanceMetrics perfMetrics =  new PerformanceMetrics();
			    
			    
			    for (int j=0; j<metricsNodelist.getLength(); j++)
			    {
			    	org.w3c.dom.Node metricAttr = metricsNodelist.item(j);
			    	org.w3c.dom.Element metricElem = (org.w3c.dom.Element) metricAttr;
			        Attr nameMetric = ((org.w3c.dom.Element) metricElem).getAttributeNode("name");
			        Attr valueMetric = ((org.w3c.dom.Element) metricElem).getAttributeNode("value");

			        //System.out.println("The names are " + nameMetric.getValue());
			        if (nameMetric.getValue().contains("instructions_"))
			        {
			        	perfMetrics.InstructionsRetired.add(valueMetric.getValue());
			        }
			        else if (nameMetric.getValue().contains("cycles_"))
			        {
			        	perfMetrics.CyclesRetired.add(valueMetric.getValue());
			        }
			        else if (nameMetric.getValue().contains("L2_miss_"))
			        {
			        	perfMetrics.L2CacheMiss.add(valueMetric.getValue());
			        }
			        else if (nameMetric.getValue().contains("L3_miss_"))
			        {
			        	perfMetrics.L3CacheMiss.add(valueMetric.getValue());
			        }

			    }
			    operation.PerformanceMetrics = perfMetrics;
			    //System.out.println("The platoforms are  : " + operation.platform + "--->" + operation.performanceMetricsAll);
			    operationforallplatforms.OperationMetricsForAllPlatforms.add(operation);
			    
			}
		    


			 return operationforallplatforms;
		 }
		 
		 
		 private PlatformSpecifications getThePlatform(String platformName)
		 {
			 for (int i=0; i<PlatformSpecifications_.size();i++)
			 {
				 if (PlatformSpecifications_.get(i).nameOfPlatform.equals(platformName))
				 {
					 return PlatformSpecifications_.get(i);
				 }
			 }
			 
			 return null;
			 
		 }

	

}
