package omeca.performanceModels.helperClasses;

import java.util.ArrayList;


/**
 * This class contains the performance metrics for each operation but for all platforms. For instance,
 * when an operation has been profiled for more than one platform, then the arrayList of the Operation metrics
 * integrates 2 or more Operation classes, which contain the performance metrics. The SWComponent class
 * contains a number of OperationMetricsForAllPlatforms, since that a SW component may encapsulate a number 
 * of individual operations.
 * **/
public class OperationMetricsForAllPlatforms{
	
	//This is the name of the Platform
	public ArrayList<Operation> OperationMetricsForAllPlatforms = new ArrayList<Operation>();
	
	
}
