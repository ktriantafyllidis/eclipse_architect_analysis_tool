package omeca.performanceModels.helperClasses;

public class PlatformSpecifications {

	
	public String nameOfPlatform;
	public String numberOfThreads;
	public String frequency;
	public String numberOfCores;
	public String L1CacheSize;
	public String L2CacheSize;
	public String L3CacheSize;
	

	public String getFrequency()
	{
		return this.frequency;
	}
	
	public String nameOfPlatform()
	{
		return this.nameOfPlatform;
	}
	
	public String numberOfThreads()
	{
		return this.numberOfThreads;
	}
	
	
	public String L1CacheSize()
	{
		return this.L1CacheSize;
	}
	
	public String L2CacheSize()
	{
		return this.L3CacheSize;
	}
	
	public String L3CacheSize()
	{
		return this.L3CacheSize;
	}
	
	
	
	
}
