package omeca.mastModel.readMastModel;

import java.io.BufferedReader;
import java.io.IOException;
import omeca.mastModel.classes.network;
import omeca.mastModel.classes.networks;
import omeca.mastModel.classes.processor;
import omeca.mastModel.classes.processors;

public class readProcessors {
	
    String line, next;
    public processors Processors = new processors();
    public networks Networks = new networks();
    

	
	public void getProcessors(BufferedReader bf)
	{		
		
		 try {
				for (line = bf.readLine(); line != null; line = next)
				{
					next = bf.readLine();
	
				    //int indexfound = line.indexOf("Operation (");
				    if (line.contains("Processing_Resource")) 
				    {
					    	//next line we read if it is processor or network processor
					    	if (next.contains("Regular_Processor"))
					    	{
						    	processor cpu = new processor();
						    	while(!(next.contains(");")))
						    	{
						    		//cpu processing resource
						    		
						    		if (next.contains("Name"))
						    		{
						    			String tmp[];
						    			tmp = next.split("=> ");						    			
						    			cpu.nameOfProcessor = tmp[tmp.length-1].trim();
	
		
						    		}
						    		else if (next.contains("Worst_ISR_Switch"))
						    		{
						    			String tmp[];
						    			tmp = next.split("=> ");
						    			cpu.Worst_ISR_Switch = tmp[tmp.length-1].trim();
	
		
						    		}
						    		else if (next.contains("Best_ISR_Switch"))
						    		{						    			
						    			String tmp[];
						    			tmp = next.split("=> ");
						    			cpu.Best_ISR_Switch = tmp[tmp.length-1].trim();
		
						    		}
						    		else if (next.contains("Avg_ISR_Switch"))
						    		{
						    			String tmp[];
						    			tmp = next.split("=> ");
						    			cpu.Avg_ISR_Switch = tmp[tmp.length-1].trim();
		
						    		}
						    		else if (next.contains("Max_Interrupt_Priority"))
						    		{
						    			String tmp[];
						    			tmp = next.split("=> ");
						    			cpu.Max_Interrupt_Priority = tmp[tmp.length-1].trim();
		
						    		}
						    		else if (next.contains("Min_Interrupt_Priority"))
						    		{
						    			String tmp[];
						    			tmp = next.split("=> ");
						    			cpu.Min_Interrupt_Priority = tmp[tmp.length-1].trim();
						    		} 		
									next = bf.readLine();
					    	}
			    
			    			Processors.Processors.add(cpu);
					    	
				    	}
					    	
				    	else if (next.contains("Packet_Based_Network"))
				    	{
				    		//network proecessing resource
				    		network net = new network();
				    		while(!(next.contains(");")))
				    		{
				    			if(next.contains("Name"))
				    			{
					    			String tmp[];
					    			tmp = next.split("=> ");
					    			net.nameOfNetwork = tmp[tmp.length-1].trim();
				    			}
				    			else if (next.contains("Speed_Factor"))
				    			{
					    			String tmp[];
					    			tmp = next.split("=> ");
				    				net.speed_Factor = tmp[tmp.length-1].trim();
				    			}
				    			next = bf.readLine();
				    		}
			    			
			    			Networks.Networks.add(net);
				    	}
				    }
				}
		 }catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		 try {
			bf.reset();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
