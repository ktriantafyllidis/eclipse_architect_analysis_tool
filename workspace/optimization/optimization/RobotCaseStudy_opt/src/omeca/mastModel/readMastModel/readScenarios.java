package omeca.mastModel.readMastModel;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

import omeca.mastModel.classes.activity;
import omeca.mastModel.classes.operation;
import omeca.mastModel.classes.operations;
import omeca.mastModel.classes.tasks;
import omeca.mastModel.classes.transaction;
import omeca.mastModel.classes.transactions;

public class readScenarios {
	
	 String line, next;
	 public transactions Transactions = new transactions();
	 private operations oper = new operations();  
	 private tasks tsk = new tasks();   
	 
	 public ArrayList<ArrayList<String>>deadlines = new ArrayList<ArrayList<String>>();
	 private ArrayList<String>deadline = new ArrayList<String>();
	 
	    readScenarios(operations operations, tasks tasks)
	    {
	    	this.oper = operations;
	    	this.tsk = tasks;
	    }
	    
	    private operation addOperation(String name)
	    {
	    	for (int i =0; i < oper.Operations.size(); i++)
	    	{
	    		if (oper.Operations.get(i).name.equals(name))
	    		{
	    			return oper.Operations.get(i);
	    		}
	    	}
	    	return null;
	    }
	    
		public void getTransactions(BufferedReader bf)
		{
		 try {
				for (line = bf.readLine(); line != null; line = next)
				{
					next = bf.readLine();

				    if (line.contains("Transaction (")) 
				    {			    		
				    	transaction trans = new transaction();
				    	
				    	while(!(next.contains(");")))
				    	{
				    		//transactions
				    		next = bf.readLine();
				    		if (next.contains("Name"))
				    		{
				    			String [] tmp;
				    			tmp = next.split("=> ");
				    			if(trans.name == null)
				    			{
				    				trans.name = tmp[tmp.length-1].replace(",", "").trim();
				    				//i keep this for the diagram
				    				deadline.add(trans.name);
				    			}
				    			
				    			
				    		}
				    		else if (next.contains("External_Events => ("))
				    		{
				    						    			
						    	while(!(next.contains("),")))
						    	{
						    		next = bf.readLine();
						    		if(next.contains("Type"))
						    		{
						    			String [] tmp;
						    			tmp = next.split("=> ");
						    			trans.typeExternal = tmp[tmp.length-1].replace(",", "").trim();
						    		}
						    		else if(next.contains("Period"))
						    		{
						    			String [] tmp;
						    			tmp = next.split("=> ");
						    			trans.period = tmp[tmp.length-1].replace(")", "").trim();
						    		}
						    	}
				    		}
				    		else if (next.contains("Timing_Requirements => ("))
				    		{
				    			
				    			while(!(next.contains("),")))
				    			{
				    				next = bf.readLine();
				    				if (next.contains("Type"))
				    				{
						    			String [] tmp;
						    			tmp = next.split("=> ");
						    			trans.deadlineType = tmp[tmp.length-1].trim();

				    				}
				    				
				    				else if (next.contains("Deadline"))
				    				{
						    			String [] tmp;
						    			tmp = next.split("=> ");
						    			trans.deadline = tmp[tmp.length-1].trim();
						    			//i keep this for the diagram
						    			deadline.add(trans.deadline);
				    				}	
				    			}
				    		}
				    		
				    		else if (next.contains("Event_Handlers  => ("))
				    		{
				    			activity act = new activity();
				    			while(!(next.contains("    )")))
				    			{
				    				next = bf.readLine();
				    				if (next.contains("Activity_Operation"))
				    				{
						    			String [] tmp;
						    			tmp = next.split("=> ");
						    			//check operation lists 
						    			String operName = tmp[tmp.length-1].trim();
						    			act.Operation = addOperation(operName);
				    				}
				    				
				    				else if (next.contains("Activity_Server"))
				    				{
						    			String [] tmp;
						    			tmp = next.split("=> ");
						    			//check server lists
						    			//String taskName = tmp[tmp.length-1].trim();
						    			trans.activities.add(act);
					
				    				}	
				    			}
				    		}
				    						    	
				    		//next = bf.readLine();

				    	}
		    			//i keep this for the diagram
		    			deadlines.add(deadline);
		    			
				    	Transactions.Transactions.add(trans);
				    }
				    				    	
				   }  
		  

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		 try 
		 {
			bf.reset();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		}


}
