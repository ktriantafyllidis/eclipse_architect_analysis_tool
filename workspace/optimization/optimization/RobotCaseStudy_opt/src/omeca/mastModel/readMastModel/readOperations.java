package omeca.mastModel.readMastModel;

import java.io.BufferedReader;
import java.io.IOException;

import omeca.mastModel.classes.operation;
import omeca.mastModel.classes.operations;
import omeca.mastModel.classes.task;
import omeca.mastModel.classes.tasks;

public class readOperations {
	
    String line, next;
    public operations Operations = new operations();
    
    
	public void getOperations(BufferedReader bf)
	{
	 try {
			for (line = bf.readLine(); line != null; line = next)
			{
				next = bf.readLine();

			    if (line.contains("Operation (")) 
			    {			    		
			    	operation oper = new operation();
			    	while(!(next.contains(");")))
			    	{
			    		//operations

			    		if (next.contains("Type"))
			    		{
			    			String tmp[];
			    			tmp = next.split("=> ");
			    			oper.type = tmp[tmp.length-1].trim();

			    		}
			    		else if (next.contains("Name"))
			    		{
			    			String [] tmp;
			    			tmp = next.split("=> ");
			    			oper.name = tmp[tmp.length-1].trim();
			    		}
			    		else if (next.contains("Worst_Case_Execution_Time"))
			    		{
			    			String [] tmp;
			    			tmp = next.split("=> ");
			    			oper.wcet = tmp[tmp.length-1].trim();

			    		}
			    		else if (next.contains("Best_Case_Execution_Time "))
			    		{
			    			String tmp[];
			    			tmp = next.split("=> ");
			    			oper.bcet = tmp[tmp.length-1].trim();

			    		}
			    		else if (next.contains("Avg_Case_Execution_Time"))
			    		{
			    			String tmp[];
			    			tmp = next.split("=> ");
			    			oper.acet = tmp[tmp.length-1].trim();

			    		}
			    		
			    		else if (next.contains("Composite_Operation_List"))
			    		{
			    			String tmp[];
			    			tmp = next.split("=> ");
			    			oper.compOperList = tmp[tmp.length-1].trim();

			    		}
			    		
			    		else if (next.contains("Max_Message_Size"))
			    		{
			    			String tmp[];
			    			tmp = next.split("=> ");
			    			oper.maxMesSize = tmp[tmp.length-1].trim();

			    		}
			    		
			    		else if (next.contains("Avg_Message_Size"))
			    		{
			    			String tmp[];
			    			tmp = next.split("=> ");
			    			oper.avgMesSize = tmp[tmp.length-1].trim();

			    		}
			    		
			    		else if (next.contains("Min_Message_Size"))
			    		{
			    			String tmp[];
			    			tmp = next.split("=> ");
			    			oper.minMesSize = tmp[tmp.length-1].trim();

			    		}
			    	
			    		next = bf.readLine();

			    	}
			    	Operations.Operations.add(oper);
			    }
			    				    	
			   }  
	  

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 
	 try 
	 {
		bf.reset();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	}

}
