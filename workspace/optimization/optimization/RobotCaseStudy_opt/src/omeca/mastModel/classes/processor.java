package omeca.mastModel.classes;

//processing resource
public class processor {

	public String type = "Regular_Processor";
	public String nameOfProcessor;
	public String speed_Factor = "1.0";
	public String Worst_ISR_Switch = "3.5E-6";			
	public String Best_ISR_Switch = "2.8E-6";			
	public String Avg_ISR_Switch = "3.0E-6";			
	public String Max_Interrupt_Priority = "119";
	public String Min_Interrupt_Priority = "101";
}
