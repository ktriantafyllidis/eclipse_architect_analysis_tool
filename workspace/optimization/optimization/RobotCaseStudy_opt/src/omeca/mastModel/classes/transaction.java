package omeca.mastModel.classes;

import java.util.ArrayList;

public class transaction {

	public String name;
	public String typeExternal = "Periodic";
	public String period;
	public String deadlineType;
	public String deadline;
	public ArrayList<activity> activities = new ArrayList<activity>();
}
