package omeca.casestudy.convsersion;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.ArrayList;



/**
 * MAST => MAST-2 models                       
 *<p>
 * Simulation analysis of models compatible with MAST2 model format    
 * 
 * <p>
 * We support simulation analysis by giving the parameters of simulation 
 * profile, the time of the simulation and the minimum transaction time.
 * !!!Attention: JsimMast does not support the simulation of "Character_Packet_Driver"
 * and "RTEP_Packet_Driver"
 *
 * @param  String Input path of the MAST model
 * @param  String file name of the MAST model 
 * @param  String Output path of the MAST-2 model
 * @param  String file name of the MAST-2 model 
 */

public class mast_to_mast2 {
	
	
	/*	This method converts the packetBasedNetwork resource type to
	 * packetBasedNetwork_VCA_Simulator, which is the compatible with
	 * the new JsimMast tool implemented by the VCA group
	 * 
	 * */
	public void packBasedNet_to_packBasedNetVCA(String pathOfModel) throws IOException
	{
		
		FileInputStream fstream = new FileInputStream(pathOfModel);
		// Get the object of DataInputStream
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;
		String newModel = "";
		
		String str;
		// Read File Line By Line
		
		while ((strLine = br.readLine()) != null) 
		{
		    if (strLine.contains("Packet_Based_Network"))
		    {
		    	//change the line 
		    	strLine = strLine.replace("Packet_Based_Network", "Packet_Based_Network_VCA_Simulator");
		    	//System.out.println("The new strline is " + strLine);
		    }
		    newModel += strLine + "\n";
		    
		}
		//System.out.println(newModel);
		// Close the input stream
		in.close();
		
		Writer writer = null;
		FileWriter fw = new FileWriter(pathOfModel);
		BufferedWriter bw = new BufferedWriter(fw);
		try {
		    bw.write(newModel);
		} catch (IOException ex) {
		  // report
		} finally {
		   try {bw.close();} catch (Exception ex) {}
		}
		
		
		
	}
	//mast_mdl:Packet_Based_Network
	
	
	public mast_to_mast2(String mast_model_path, String mast_model_name, String mast2_model_path, String mast2_model_name)
	{
		
		StringBuffer output = new StringBuffer();
		String mast_to_mast2;
		
		//get the name of operating system
		
		if (System.getProperty("os.name").equals("Linux"))
		{
			mast_to_mast2 = "./mast_linux/to_mast2";
			System.out.println ("OS= " + System.getProperty("os.name"));
		}
		
		else
		{
			mast_to_mast2 = "cmd /c start mast/to_mast2.exe";
			System.out.println ("OS = " + System.getProperty("os.name"));
		}
		
		
		mast_to_mast2 += " " + mast_model_path + "/" + mast_model_name + " " + mast2_model_path + "/" + mast2_model_name + ".mdl.xml";
		
        try {
            ArrayList<String> argList = new ArrayList<String>();
            //first argument is the folder and the make file 
            //argList.add(destination);

            String[] args = argList.toArray(new String[argList.size()]);

            Process lChldProc = Runtime.getRuntime().exec(mast_to_mast2);
            try {
                lChldProc.waitFor();
                //container.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                } catch (InterruptedException ex) 
                {
                	lChldProc.destroy();
                
                }
            
	            BufferedReader reader = new BufferedReader(new InputStreamReader(lChldProc.getInputStream()));
	            String line = "";			
	            while ((line = reader.readLine())!= null) 
	            {
	            	output.append(line + "\n");
	            }
        } catch (IOException ex)
        {
       
        } 
        System.out.println(output);
		
	}

}
