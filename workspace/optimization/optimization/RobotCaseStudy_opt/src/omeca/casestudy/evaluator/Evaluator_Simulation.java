package omeca.casestudy.evaluator;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import omeca.casestudy.objectives.Objective;


import guis.*;


/**
 * JSimMast simulation analysis                          
 *<p>
 * Simulation analysis of models compatible with MAST2 model format    
 * 
 * <p>
 * We support simulation analysis by giving the parameters of simulation 
 * profile, the time of the simulation and the minimum transaction time.
 *
 * @param  String Input path of the model
 * @param  String Output path/file name of the analysis results. 
 * @param  String Traces path/file name
 * @param  String simulation profile {PERFORMANCE, TRACES, SCHEDULABILITY}
 * @param  Long Simulation time parameter
 * @param  Long Minimum number of transactions during the simulation analysis
 */
public class Evaluator_Simulation {
	
	
	protected String modelName;
	protected String resultsName;
	protected String tracesName;
	protected String profile_combo;
	protected long finalTime;
	protected long minNumTransactions;
	
	protected Collection<Objective> objectives = new ArrayList<Objective>();

public Evaluator_Simulation(String _modelName,	String _resultsName, String _tracesName, String _profile_combo, long _finalTime, long _minNumTransactions) {

	
	this.modelName = _modelName;
	this.resultsName = _resultsName;
	this.tracesName = _tracesName;
	this.profile_combo = _profile_combo;
	this.finalTime = _finalTime;
	this.minNumTransactions = _minNumTransactions;
	
	JSimMastGUI run_simulation = new JSimMastGUI();
	run_simulation.sim_mast_no_gui(this.modelName, this.resultsName, this.tracesName, this.profile_combo, this.finalTime, this.minNumTransactions);
	
}
/*
public Collection<Objective> evaluate() {

	DummyAnalyzer analyzer = new DummyAnalyzer(this.model, this.output);
	
	//objectives.add(new ResponseTime( analyzer.getResponseTime() ));
	objectives.add(new CpuUtilization( analyzer.getCpuUtilization() ));
	
	return objectives;
}
*/

}
/*
class DummyAnalyzer {

String outputFile;

public DummyAnalyzer(String MASTmodel, String outputFile) {
	this.outputFile = outputFile;

	String mast_path;
	
	//get the name of operating system

	
	if (System.getProperty("os.name").equals("Linux"))
	{
		mast_path = "./mast_linux/mast_analysis";
		System.out.println ("OS= " + System.getProperty("os.name"));
	}
	
	else
	{
		mast_path = "cmd /c start mast/mast_analysis.exe";
		System.out.println ("OS = " + System.getProperty("os.name"));
	}
	
	String path = mast_path+" default -v -l -c -p "+ MASTmodel + " " + outputFile;
	
    try {
        ArrayList<String> argList = new ArrayList<String>();
        //first argument is the folder and the make file 
        //argList.add(destination);

        String[] args = argList.toArray(new String[argList.size()]);

        Process lChldProc = Runtime.getRuntime().exec(path);
        try {
            lChldProc.waitFor();
            //container.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            } catch (InterruptedException ex) {
            	lChldProc.destroy();
            
        }
    } catch (IOException ex) {
   
    }    
}

public ArrayList<ArrayList<String>> getResponseTime() 
{
	
	outputFile responseTime  = new outputFile();
	responseTime.getValue(this.outputFile, "Processing_Resource ");
	final ArrayList<ArrayList<String>> processingResourceList = responseTime.getValue(this.outputFile, "Processing_Resource ");
	return processingResourceList;
}

public ArrayList<ArrayList<String>> getCpuUtilization() 
{
	outputFile responseTime  = new outputFile();
	responseTime.getValue(this.outputFile, "Processing_Resource ");
	final ArrayList<ArrayList<String>> processingResourceList = responseTime.getValue(this.outputFile, "Processing_Resource ");
	return processingResourceList;
}

}*/