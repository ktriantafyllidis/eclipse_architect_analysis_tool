package omeca.casestudy;




import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.jfree.ui.RefineryUtilities;
import omeca.analysis.classes.utilization;
import omeca.analysis.output.outputLog;
import omeca.analysis.output.readScheAnalysisResults;
import omeca.analysis.output.readSimulationResults;
import omeca.analysis.output.charts.barChart;
import omeca.analysis.output.charts.end2endLineChart;
import omeca.analysis.output.charts.lineChart;
import omeca.analysis.output.charts.pieChart;
import omeca.analysis.output.charts.schedProcUtilChart;
import omeca.casestudy.evaluator.*;
import omeca.casestudy.models.MAST;
import omeca.casestudy.models.PreMAST;
import omeca.casestudy.objectives.E2E;
import omeca.casestudy.objectives.NetworkUtilization;
import omeca.casestudy.objectives.Objective;
import omeca.casestudy.objectives.ResourcesUtilization;
import omeca.casestudy.objectives.ResponseTime;
import omeca.casestudy.objectives.RU;
import omeca.casestudy.convsersion.*;
import omeca.mastModel.classes.transactions;
import omeca.mastModel.readMastModel.readMastFile;
import omeca.mastModel.readMastModel.readScenarios;
import omeca.network.inputfiles.NetworkTransmissions;
import omeca.network.schedulabilityAnalysis.*;
import omeca.network.simulationAnalysis.execNS3simAnalysis;
import omeca.performanceModels.loadCost;
import omeca.performanceModels.loadPerformanceModels;

public class Main {

	/**
	 * @param args
	 */
	
	static boolean NS3 = true;
	
	//here i give the name of the file to be tested
	final static String model_name = "3robots_3nodes";
	
	
	public static void main(String[] args) throws Exception {
		
		String currentDir = System.getProperty("user.dir");
		String outputFile = currentDir + "/output_files/" + model_name+ ".mast.out";
		
		
		//I open a file called premast which i process in order to create a mast file which includes all the network delays computed with the network analysis tools
		//both schedulability (SAN tool) as well as simulation analysis (Ns3 tool) 
		PreMAST premast = new PreMAST(new File("models/" + model_name + ".mast"));
		//premast.applySomeChanges();
		
		//FIXME
		//here some code needs to be added in order to apply the DOF changes automatically
		//MAST mast = premast.generateMAST();
	
		
		
		//check the network configuration -- fix the path
		NetworkTransmissions Net = new NetworkTransmissions();		
		Net.getNetworkTransfers(new File("models/" + model_name +".mast"));
		convertModelToDelays delays = new convertModelToDelays();
		if(NS3 == true)
		{
			//test
			new execNS3simAnalysis();
			delays.passToMastModel(currentDir + "/NetworkSimulatorNS3/delays.txt", currentDir + "/models/" + model_name + ".mast");
		}
		else
		{
			execSANschedAnalysis net_sim = new execSANschedAnalysis();
			net_sim.executeMathematica();
		
			// read the delays from the delays.txt file and put them into the mast file
			delays.passToMastModel(currentDir + "/NetworkSimulator/delays.txt", currentDir + "/models/" + model_name + ".mast");
		}

		
		
		  //Here i do the schedulability analysis of the composed system
		 // this takes a few seconds and computes the WCET. Having already chosen alternatives that do not satisfy the RT requirements 
		 // we chose the architectures that satisfy the RT requirements and we perform simulation analysis to identify possible bottlencks.
		 
		 
		


		schedAnalysisType SchedType = new schedAnalysisType();
		SchedType.setTypeOfAnalysis("holistic");
		Evaluator_Schedulability_Analysis evaluator = new Evaluator_Schedulability_Analysis(SchedType.getTypeOfAnalysis(), "NetworkSimulator/" + "robot1" + ".mast", outputFile);
		readScheAnalysisResults schedRes = new readScheAnalysisResults();
		schedRes.openOutputFile(currentDir + "/output_files/" + model_name + ".mast.out");
		ResourcesUtilization SchedulabilityResources_Util = new ResourcesUtilization(schedRes.getCPUUtilization());
		SchedulabilityResources_Util.printResourcesUtilization();
 		schedRes.close();
		
		schedRes.openOutputFile(currentDir + "/output_files/" + model_name + ".mast.out");
		ResponseTime SchedulabilityRes_Time = new ResponseTime(schedRes.getResponseDelays());
		SchedulabilityRes_Time.printResponseTime();
		schedRes.close();
		
		//create logfile for delays of schedulability analysis
		outputLog fileSched = new outputLog();
		fileSched.newFile(currentDir + "/output_files/" + model_name + ".schedulabilityResults");
		fileSched.writeToFile(SchedulabilityResources_Util.value(), "Processor Utilization");
		fileSched.writeToFile(SchedulabilityRes_Time.value(), "Response time of events");
		fileSched.close();
		
		//working on the automated generation 
		
		//read the mast file 
		readMastFile mastFile = new readMastFile(new File("models/" + model_name +".mast"));
		ArrayList<ArrayList<String>>deadlines = mastFile.getDeadlines();
		
		//open performance models files 
		loadPerformanceModels PM  = new loadPerformanceModels();
		File pm_file =  new File(System.getProperty("user.dir") + "/ComponentModels/ProMo_models/Gmapping_t420.promo.xml");
		PM.openFile(pm_file);
		System.out.println("The platform name is + " + PM.PlatformSpecifications_.get(0).nameOfPlatform);
	
		//load the HW cost 
		loadCost lc = new loadCost();
		//System.out.println("The cpu costs are " + lc.cpuCost_.get(0).priceOfCPU);

		
		//get the scenarios 
  		transactions scenarios = Net.getScenarios();
		//read the obectives 
		E2E e2e = new E2E(SchedulabilityRes_Time, scenarios);
		System.out.println("The objective worst case deadline is " + e2e.getE2E_w());
		System.out.println("The objective average case deadline is " + e2e.getE2E_a());
		
		RU util = new RU(SchedulabilityResources_Util);
		System.out.println("The objective worst core utilization is " + util.getCoreUtil_w());
		System.out.println("The objective average core utilization is " + util.getCoreUtil_a());
		System.out.println("The objective worst network utilization is " + util.getNetUtil_w());
		System.out.println("The objective average network utilization is " + util.getNetUtil_a());
		
		
		
		
		
		
		
		
		
		
		//Convert Mast model to Mast-2 model giving the paths and the names of the models
		 
		
		mast_to_mast2 mast2 = new mast_to_mast2(currentDir + "/models", model_name  + ".mast", currentDir + "/models_simulation", model_name + "2");
		mast2.packBasedNet_to_packBasedNetVCA(currentDir + "/models_simulation/"+model_name+"2.mdl.xml");	
		//open and read the mast2 xml file 
		MsgSize2MsgDelay model = new MsgSize2MsgDelay();
		model.openMast2Model(currentDir + "/models_simulation/"+model_name+"2.mdl.xml");
		model.convertMsgSize2MsgDelayOnlyAverage(delays.getNetworkDelays());
	
		
		//Here i perform the simulation analysis providing the necessary variables, like the paths of the models (In/Out) and the analysis configuration parameters 
		 
	
		String modelName = model_name + "2.mdl.xml";
		String resultsName = model_name + "2.res.xml";
		String tracesName = model_name + "2.trc.xml";

		/*		profile_combo: 
		 * 	- PERFORMANCE
		 *  - SCHEDULABILITY
		 *  - VERBOSE
		 *  - TRACE
		 * 
		 * */
			
		String profile_combo = "PERFORMANCE";
		
		long finalTime = 1000;
		long minNumTransactions = 1000;
		
		Evaluator_Simulation sim_eval = new Evaluator_Simulation(modelName, resultsName, tracesName, profile_combo, finalTime, minNumTransactions);
		
		
		//show the results
		readSimulationResults simRes = new readSimulationResults();
		//this is enabled for test
		utilization util1 = simRes.getSimulationResults(currentDir, resultsName); //this is the version with the classes UNTESTED
		simRes.openOutputFile(currentDir + "/models_simulation/" + resultsName);
		ResourcesUtilization SimulationCPU_Util = new ResourcesUtilization(simRes.getCPUUtilization());
		SimulationCPU_Util.printResourcesUtilization();

		NetworkUtilization SimulationNet_Util = new NetworkUtilization(simRes.getNetworkUtilization());
		SimulationNet_Util.printNetworkUtilization();
		
		ResponseTime SimulationRes_Time = new ResponseTime(simRes.getResponseDelays());
		SimulationRes_Time.printResponseTime();
		
		//print to file
		outputLog fileSim = new outputLog();
		fileSim.newFile(currentDir + "/output_files/" + model_name + ".simulationResults");
		fileSim.writeToFile(SimulationCPU_Util.value(), "Processor Utilization");
		fileSim.writeToFile(SimulationNet_Util.value(), "Network Utilization");
		fileSim.writeToFile(SimulationRes_Time.value(), "Response time of events");
		fileSim.close();
		
		
		//read the processors 
		
		//
		
		//read the end to end delays 
		
		
		//create charts to print results 
		
        pieChart demo = new pieChart("Comparison", "Which operating system are you using?");
        demo.pack();
        demo.setVisible(true);
        
        end2endLineChart end2endDiagram = new end2endLineChart(simRes.getResponseDelays(),schedRes.getResponseDelays(),deadlines);
        end2endDiagram.pack();
        RefineryUtilities.centerFrameOnScreen(end2endDiagram);
        end2endDiagram.setVisible(true);
        
        schedProcUtilChart procUtilChart = new schedProcUtilChart(schedRes.getCPUUtilization());
        procUtilChart.pack();
        RefineryUtilities.centerFrameOnScreen(procUtilChart);
        procUtilChart.setVisible(true);
        
       // final lineChart demo2 = new lineChart("Line Chart", "Line Chart2");
        //demo2.pack();
        //RefineryUtilities.centerFrameOnScreen(demo2);
        //demo2.setVisible(true);
		
		
		
	}

}
