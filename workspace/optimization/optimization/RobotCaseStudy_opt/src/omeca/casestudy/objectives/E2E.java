package omeca.casestudy.objectives;

import java.util.ArrayList;

import omeca.mastModel.classes.transactions;

public class E2E {

	protected float e2eDelay;
	private transactions scenarios;
	private String [] deadlineData = new String[2];
	
	E2E_objectives e2e = new E2E_objectives();

	int normalization = 0;
	
	public E2E(ResponseTime schedulabilityRes_Time, transactions scenarios)
	{
	
		this.scenarios = scenarios;
		float di;
		e2e.e2e_w.add("empty");
		e2e.e2e_w.add("0");
		
		//from the list get the worst time delay/deadline 
		//for ratios >1 we give a penalty
		//i have the delays but i dont have the deadlines 
		for (int i = 0; i <  schedulabilityRes_Time.responseTime.size(); i++)
		{
			ArrayList<String> responseDelayItem = schedulabilityRes_Time.responseTime.get(i);

			String nameOfScenario = responseDelayItem.get(0);
			String worstDelay = responseDelayItem.get(1);
			String bestDelay = responseDelayItem.get(2);
			
			//deadlineData
			findDeadline(nameOfScenario);
			
			di = Float.parseFloat(worstDelay) / Float.parseFloat(this.deadlineData[1]);
			
			
			//FIXME
			//compute the worst for only hard real time requirements 
		//	if (this.deadlineData[0].equals("Soft_Global_Deadline"))
		//	{
				update_E2E_w(di, nameOfScenario);
		//	}
			
				
				update_E2E_a(di);
			
			System.out.println(worstDelay);
			
		}
		
		this.e2e.e2e_a = e2e.e2e_a / this.normalization;

		
	}

	
	//this computes the worst case delay objective 
	//it keeps only the worst case deadline and the name of the scenario for this deadline
	private void update_E2E_w(float di, String nameOfScenario)
	{
		if (Float.parseFloat(e2e.e2e_w.get(1)) < di)
		{
			e2e.e2e_w.set(0, nameOfScenario);
			e2e.e2e_w.set(1, Float.toString(di));
		}
		
	}
	
	
	
	/* we give weights in the computation of the average delay objective
	 * 
	 * Soft deadline -> 1
	 * Firm deadline -> 3 
	 * Hard deadline -> 5
	 * other deadline -> 3 (for any error in the syntax)
	 * 
	 * 
	 * */
	private void update_E2E_a(float di)
	{
		float weight;
		
		if (deadlineData[0].equals("Soft_Global_Deadline"))
		{			
			weight = 1.0f;
			
		}
		
		
		else if (deadlineData[0].equals("Firm_Global_Deadline"))
		{
			weight = 3.0f;
		}
		
		else if (deadlineData[0].equals("Hard_Global_Deadline"))
		{
			weight = 5.0f;
		}
		
		else
		{
			weight = 3.0f;
		}
		
		e2e.e2e_a  = e2e.e2e_a + di * weight; 
		this.normalization += weight;
	}
	
	
	
	public float getE2E_a()
	{
		return e2e.e2e_a;
	}
	
	
	public float getE2E_w()
	{
		return Float.parseFloat(e2e.e2e_w.get(1));
	}
	
	private void findDeadline(String scenarioName)
	{
			
 		for (int i = 0; i < scenarios.Transactions.size() ; i++)
		{
			if(scenarios.Transactions.get(i).name.toLowerCase().equals(scenarioName))
			{
				this.deadlineData[0] = scenarios.Transactions.get(i).deadlineType;
				this.deadlineData[1] = scenarios.Transactions.get(i).deadline;
			}
			
		}
		
	}
	

}
