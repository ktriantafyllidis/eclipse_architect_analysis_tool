package omeca.casestudy.objectives;

import java.util.ArrayList;

public class ResponseTime implements Objective {

	protected final ArrayList<ArrayList<String>> responseTime;
	
	public ResponseTime(ArrayList<ArrayList<String>> arrayList) {
		this.responseTime = arrayList;
	}
	
	@Override
	public ArrayList<ArrayList<String>> value() {
		return responseTime;
	}
	
	public void printResponseTime()
	{
		System.out.println(this.responseTime);
	}

}
