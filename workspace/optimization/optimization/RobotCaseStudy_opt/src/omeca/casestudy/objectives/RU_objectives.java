package omeca.casestudy.objectives;

import java.util.ArrayList;

public class RU_objectives {
	
	
	public ArrayList<String> coreUtil_w = new ArrayList<String>();
	public float coreUtil_a; 
	
	public ArrayList<String> netUtil_w = new ArrayList<String>();
	public float netUtil_a;
	
	public RU_objectives()
	{
		coreUtil_w.add("empty");
		coreUtil_w.add("0");
		coreUtil_a = 0.0f;
		
		netUtil_w.add("empty");
		netUtil_w.add("0");
		netUtil_a = 0.0f;
	}

}
