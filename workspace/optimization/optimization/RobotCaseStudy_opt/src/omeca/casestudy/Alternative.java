package omeca.casestudy;

import java.util.List;

import omeca.casestudy.dof.*;

public class Alternative {

	protected List<Processor> processors;
	protected List<RAM> rams;
	protected List<NetworkProtocol> networkProtocols;
	protected List<SchedulingPolicy> schedulingPolicies;
	protected Mapping mapping;
	
	public Alternative(List<Processor> processors, List<RAM> rams, List<NetworkProtocol> networkProtocols, List<SchedulingPolicy> schedulingPolicies, Mapping mapping) {
		this.processors = processors;
		this.rams = rams;
		this.networkProtocols = networkProtocols;
		this.schedulingPolicies = schedulingPolicies;
		this.mapping = mapping;
	}
	
	public List<Processor> getProcessors() {
		return processors;
	}
	
	public List<RAM> getRAMs() {
		return rams;
	}
	
	public List<NetworkProtocol> getNetworkProtocols() {
		return networkProtocols;
	}
	
	public List<SchedulingPolicy> getSchedulingPolicies() {
		return schedulingPolicies;
	}
	
	public Mapping getMapping() {
		return mapping;
	}
	
}
