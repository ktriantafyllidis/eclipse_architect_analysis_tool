package omeca.casestudy.models;

public class MAST {
	
	protected final String model;
	
	public MAST(String model) {
		this.model = model;
	}
	
	public String getModelText() {
		return model;
	}
	
}
