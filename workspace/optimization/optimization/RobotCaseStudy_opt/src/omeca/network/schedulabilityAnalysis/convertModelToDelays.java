package omeca.network.schedulabilityAnalysis;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;

import omeca.network.inputfiles.*;

public class convertModelToDelays {
	

	
	public BufferedReader bf, bd;
	private ArrayList<Object> MessageTransmissionList = new ArrayList<Object> ();
	private ArrayList<Object> TransactionList = new ArrayList<Object> ();
	public ArrayList<Object> tmp = new ArrayList<Object>();
	public ArrayList<Object> tmp1 = new ArrayList<Object>();
	
	
	private ArrayList<ArrayList<String>> delays = new ArrayList<ArrayList<String>>();

	private ArrayList<String> ActivityOperationList = new ArrayList<String> ();
    String line, next;
	
    
    //open the model file which is in mast format
	public void openFile(String filePath)
	{
		try {
			bf  = new BufferedReader(new FileReader(filePath));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//maybe i have to change this
		try {
			bf.mark(1000000);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
	
	
	
	public void openDelayFile(String filePath)
	{
		try {
			bd  = new BufferedReader(new FileReader(filePath));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//maybe i have to change this
		try {
			bd.mark(1000000);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
	
	public ArrayList<ArrayList<String>> getNetworkDelays()
	{
		return this.delays;
	}
	
	
	//find the transmission operation and add it into a list
	public void ReadDelayLine()
	{
	
			
		 try {
				for (line = bd.readLine(); line != null; line = next)
				{
							
					ArrayList<String> split_string =  new ArrayList<String>();
					String[] _split = line.split(",");

					split_string.add(_split[0].replace("{", "").trim());
					split_string.add(_split[1].replace("}", "").trim());
					if(_split.length>2)
					{
						split_string.add(_split[2].trim());
						split_string.add(_split[3].replace("}", "").trim());
					}

					delays.add(split_string);
					//System.out.println("The line is " + split_string.get(0));
					//System.out.println("The line is " + split_string.get(1));

			    	next = bd.readLine();
				    }

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		}
	
	
	

	
	/**This method finds the messageTransmission operations and passes them the delays that the 
	 * network analysis tools provide. The idea is that for each message transmission though network
	 * operation, we create an individual instantiation for this operation. So, an operation that is going 
	 * to be used multiple times by different transactions, is expressed as an individual instantiation
	 * in message transmission operations. 
	 * The current message transmission operations can be deleted and in their position we add
	 * the instantiations including the delays computed by the network analysis tools.
	 * 
	 * **/
	public void findMessageTransmission(String path)
	{
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(path, "UTF-8");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//writer.write(bf.toString());

		
		
				
		 try {
				for (line = bf.readLine(); line != null; line = next)
				{
							
					tmp = new ArrayList<Object>();
				    //int indexfound = line.indexOf("Operation (");
				    boolean indexfound = line.contains("Operation (");
				   
				    if (indexfound) 
				    {   	
				    	writer.write(line + "\n");
					    for (line = bf.readLine(); line != null; line = next)
				    	{
				    		boolean _Message_Transmission = line.contains("Message_Transmission");
				    		//next = bf.readLine();
				    		if(_Message_Transmission)
				    		{
				    			writer.write("\tType                        => Simple,"+"\n");
				    			//go to next line and find the name of the operation
				    			line = bf.readLine();
				    			writer.write(line + "\n");
				    			String eventName = line.replace("Name", "").replace("=>", "").replace("," ,"").trim();
				    			//System.out.println( eventName);
				    			
				    			//for each event name search the delay list and check if there is any delay and set this delay
				    			for(int i=0; i<delays.size();i++)
				    			{
				    				if(delays.get(i).get(0).equals(eventName))
				    				{


						    			if(delays.get(i).size()>2)
						    			{
							    			bf.readLine();
							    			line = "\tWorst_Case_Execution_Time	=> " + delays.get(i).get(2) + "E-3 ,";
							    			
							    			writer.write(line+"\n");
							    			bf.readLine();
							    			line = "\tBest_Case_Execution_Time	=> " + delays.get(i).get(2) + "E-3 ,";
							    			writer.write(line+"\n");
							    			bf.readLine();
							    			
							    			line = "\tAvg_Case_Execution_Time		=> " + delays.get(i).get(2) + "E-3";
							    			writer.write(line+"\n");
						    			}
						    			else
						    			{
							    			bf.readLine();
							    			line = "\tWorst_Case_Execution_Time		=> " + Float.valueOf(delays.get(i).get(1)) + "E-3 ";
							    			bf.readLine();
							    			writer.write(line);
							    			bf.readLine();
							    			writer.write("\n");

						    			}
				    					//System.out.println(line);
					    				break;
				    				}
				    				

				    			}
	
				    		}	
				    		else
				    		{
				    			writer.write(line+"\n");
				    		}
				    		next = bf.readLine();
				    	}
				    	
				    }
				    if(line != null)
				    	writer.write(line+"\n");
			    	next = bf.readLine();
				}
				
				 writer.close();
				//System.out.println("The transaction list is " + TransactionList);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	
	
	private void closeDelayFile()
	{
	     try {
				bd.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	private void closeFile()
	{
	     try {
				bf.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	
	//open the delay file
	public void passToMastModel(String path, String modelName)
	{
		this.openDelayFile(path);
		this.ReadDelayLine();
		//System.out.println("The line is " + delays);
		this.closeDelayFile();
		
		//open the model mast file
		this.openFile(modelName);
		this.findMessageTransmission(System.getProperty("user.dir") + "/NetworkSimulator/robot1.mast");
		this.closeFile();
	}

}
