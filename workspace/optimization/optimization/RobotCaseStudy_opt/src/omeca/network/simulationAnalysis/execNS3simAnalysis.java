package omeca.network.simulationAnalysis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


/**@author ktrianta*/

/**	This is the class than enables the simulation analysis
 *  It runs a binary file containg the tcp network simulation.
 *  The analysis is based on the input and the configuration file.
 *  * 
 * **/
public class execNS3simAnalysis {
	
	String execFolder = "NetworkSimulatorNS3/";
	String execPath = null;


	
	public  execNS3simAnalysis()
	{
		
			if (System.getProperty("os.name").equals("Linux"))
			{
				StringBuffer output = new StringBuffer();
/*				String currentDir = System.getProperty("user.dir");
				System.out.println(currentDir);*/
				
				try 
				{
					Runtime r = Runtime.getRuntime ( );
					//String[] cmdArgs = { "./NetworkSimulatorNS3/adhocTCP/adhocTCP", "/NetworkSimulatorNS3/adhocTCP/input.txt", "/NetworkSimulatorNS3/adhocTCP/conf.txt" };
				    String command = "./NetworkSimulatorNS3/adhocTCP" + " " + execFolder + "input.txt" + " " + execFolder + "conf.txt" + " " + execFolder;			
					String[] envp = new String[]{ "LD_LIBRARY_PATH=/home/ktrianta/eclipse_cpp/workspace/ns-allinone-3.19/ns-3.19/build" };        
					Process p = r.exec(command, envp);
					
							
					try 
		            {
		            	p.waitFor();
		                //container.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		            } 
		            catch (InterruptedException ex) 
		            {
		                p.destroy();
		            }
		            
					BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		            String line = "";			
		            while ((line = reader.readLine())!= null) 
		            {
		            	output.append(line + "\n");
		            }
				}
				catch (IOException ex) 
				{
		       
		        }  
			
				System.out.println(output);
			}
			
			
			
			
			else 
			{
				System.out.println ("Your OS " + System.getProperty("os.name") + " does not currently support NS3");
			}
	}
			

	
}
